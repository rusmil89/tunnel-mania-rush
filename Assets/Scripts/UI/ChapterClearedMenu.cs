﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using TMPro;
using Tunnel.Ads;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: ChapterClearedMenu
    /// DESCRIPTION: Klasa koja kontrolise staticki meni koji se pojavljuje kada igrac stigne uspesno do kraja nekog chaptera
    /// </summary>
    public sealed class ChapterClearedMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {        
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Elements")]
        [SerializeField] private TMP_Text _clearedText;
        [SerializeField] private TMP_Text _chapterText;
        [SerializeField] private TMP_Text _chapterNumberText;
        [SerializeField] private TMP_Text _tapToOpenText;
        [SerializeField] private TMP_Text _doubleRewardButtonText;
        [SerializeField] private TMP_Text _spinTheWheelTitleText;
        [SerializeField] private TMP_Text _spinTheWheelDescriptionText;
        [SerializeField] private TMP_Text _startButtonText;
        [SerializeField] private TMP_Text _claimButtonText;
        [SerializeField] private Image _flashImage;
        [SerializeField] private Image _chapterNumberBackgroundImage;
        [SerializeField] private Image _rewardImage;
        [SerializeField] private Button _rewardButton;
        [SerializeField] private Button _claimButton;
        [SerializeField] private Button _doubleRewardButton;
        [SerializeField] private GameObject _fireworksParticlesHolder;
        
        [Header("Spin The Wheel")]
        [SerializeField] private GameObject _spinTheWheelElements;
        [SerializeField] private GameObject _wheelGameObject;
        [SerializeField] private GameObject _pointerGameObject;
        [SerializeField] private Button _startSpinningButton;
        [SerializeField] private TMP_Text[] _spinTheWheelValuesTexts;

        [Header("Animations Elements")]
        [SerializeField] private RectTransform _coin;
        [SerializeField] private RectTransform _token;
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private ChapterClearedMenuAnimations _chapterClearedMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private CurrencyBoughtAnimations _currencyBoughtAnimations;
        private int[] _spinTheWheelValues;
        private System.Random _currentRandomSpinAngle;
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Animations elements
        /// </summary>
        private IState _rootState;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private readonly int[] _randomSpinAngles = {30, 90, 150, 210, 270, 330};
        private const int ANGLE_OFFSET = 25;
        private const float CLAIM_BUTTON_DELAY = 3f;
        private const float LOADING_MENU_SCENE_DELAY = 0.25f;
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType { get; private set; }

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent => default;

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject { get; private set; }

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        public TMP_Text ClearedText => _clearedText;

        public TMP_Text ChapterText => _chapterText;

        public TMP_Text ChapterNumberText => _chapterNumberText;

        public Image FlashImage => _flashImage;

        public Image ChapterNumberBackgroundImage => _chapterNumberBackgroundImage;

        public Button RewardButton => _rewardButton;
        
        public Image RewardImage => _rewardImage;
        
        public Vector3 FlashImageScale { get; } = new Vector3(3, 10, 3);

        public GameObject WheelGameObject => _wheelGameObject;

        public GameObject PointerGameObject => _pointerGameObject;

        public Button StartSpinningButton => _startSpinningButton;
        
        public float RandomSpinAngle { get; private set; }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            const string scriptableObjectsPath = Constants.Editor.Paths.SCRIPTABLE_OBJECTS;
            _tapToOpenText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("Tap"));
            _clearedText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("Cleared"));
            _chapterText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("Chapter"));
            _chapterNumberText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("ChapterNumber"));
            _doubleRewardButtonText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("DoubleRewardButtonText"));
            _flashImage = gameObject.Descendants().OfComponent<Image>().First(s => s.name.Contains("Flash"));
            _rewardImage = gameObject.Descendants().OfComponent<Image>().First(s => s.name.Contains("Present"));
            _rewardButton = RewardImage.GetComponent<Button>();
            _chapterNumberBackgroundImage = gameObject.Descendants().OfComponent<Image>().First(s => s.name.Contains("ChapterNumber"));
            _spinTheWheelElements = gameObject.Descendants().First(s => s.name.Contains("SpinTheWheelElements"));
            _wheelGameObject = _spinTheWheelElements.Child("WheelImage");
            _pointerGameObject = _spinTheWheelElements.Child("Pointer");
            _spinTheWheelTitleText = _spinTheWheelElements.Child("RewardTitleText").GetComponent<TMP_Text>();
            _spinTheWheelDescriptionText = gameObject.Descendants().First(s => s.name.Contains("DescriptionText")).GetComponent<TMP_Text>();
            _startButtonText = gameObject.Descendants().First(s => s.name.Contains("StartButtonText")).GetComponent<TMP_Text>();
            _claimButtonText = gameObject.Descendants().First(s => s.name.Contains("ClaimButtonText")).GetComponent<TMP_Text>();
            _startSpinningButton = _spinTheWheelElements.GetComponentInChildren<Button>(true);
            _claimButton = _spinTheWheelElements.Descendants().OfComponent<Button>().First(s => s.name.Contains("Claim"));
            _doubleRewardButton = _spinTheWheelElements.Descendants().OfComponent<Button>().First(s => s.name.Contains("Double"));
            _fireworksParticlesHolder = gameObject.Descendants().First(s => s.name.Contains("Fireworks")).gameObject;
            _spinTheWheelValuesTexts = gameObject.Descendants().Where(x => x.name.Contains("WheelImageValue")).OfComponent<TMP_Text>().ToArray();
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _coin = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = gameObject.Descendants().First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{scriptableObjectsPath})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        private void OnDestroy()
        {
            _rootState.Exit();
            _chapterClearedMenuAnimations.DisposeAnimations();
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                    .Enter(state => HandleEnteringShownState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonListeners();
            RegisterObservableListeners();
            AnimateChapterClearedElements();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void SetInitialValues()
        {
            _chapterClearedMenuAnimations = new ChapterClearedMenuAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            _currencyBoughtAnimations = new CurrencyBoughtAnimations(this);
            _spinTheWheelValues = new []
            {
                RemoteConfigClient.Instance.SpinTheWheelReward01, 
                RemoteConfigClient.Instance.SpinTheWheelReward04, 
                RemoteConfigClient.Instance.SpinTheWheelReward03, 
                RemoteConfigClient.Instance.SpinTheWheelReward06, 
                RemoteConfigClient.Instance.SpinTheWheelReward02, 
                RemoteConfigClient.Instance.SpinTheWheelReward05
            };
            _spinTheWheelValuesTexts[0].text = RemoteConfigClient.Instance.SpinTheWheelReward01.ToString();
            _spinTheWheelValuesTexts[1].text = RemoteConfigClient.Instance.SpinTheWheelReward04.ToString();
            _spinTheWheelValuesTexts[2].text = RemoteConfigClient.Instance.SpinTheWheelReward03.ToString();
            _spinTheWheelValuesTexts[3].text = RemoteConfigClient.Instance.SpinTheWheelReward06.ToString();
            _spinTheWheelValuesTexts[4].text = RemoteConfigClient.Instance.SpinTheWheelReward02.ToString();
            _spinTheWheelValuesTexts[5].text = RemoteConfigClient.Instance.SpinTheWheelReward05.ToString();
            ChapterNumberText.text = _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value].ToString();
        }

        private void SetLocalization()
        {
            _chapterText.SetText(_languageClient.CurrentLanguage.Chapter.Value);
            _clearedText.SetText(_languageClient.CurrentLanguage.Cleared.Value);
            _tapToOpenText.SetText(_languageClient.CurrentLanguage.TapToOpen.Value);
            _spinTheWheelTitleText.SetText(_languageClient.CurrentLanguage.BonusWheel.Value);
            _spinTheWheelDescriptionText.SetText(_languageClient.CurrentLanguage.SpinTheWheel.Value);
            _startButtonText.SetText(_languageClient.CurrentLanguage.Start.Value);
            _claimButtonText.SetText(_languageClient.CurrentLanguage.Claim.Value);
        }

        private void RegisterButtonListeners()
        {
            RewardButton.OnClickAsObservable()
                .Subscribe(HandleRewardButtonClick)
                .AddTo(this);
            StartSpinningButton.OnClickAsObservable()
                .Subscribe(HandleStartSpinningButtonClick)
                .AddTo(this);
            _claimButton.OnClickAsObservable()
                .Subscribe(HandleClaimButtonClick)
                .AddTo(this);
            _doubleRewardButton.OnClickAsObservable()
                .Subscribe(HandleDoubleRewardButtonClick)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<RewardedVideoInChapterClearedCompleted>()
                .Subscribe(HandleRewardedVideoInChapterClearedCompleted)
                .AddTo(this);
        }

        private void HandleEnteringShownState()
        {
            LoadAds();
        }

        private void AnimateChapterClearedElements()
        {
            _chapterClearedMenuAnimations.AnimateChapterClearedElements();
            FlashImage.gameObject.Parent().SetActive(true);
        }

        private static void LoadAds()
        {
            AdsClient.Instance.LoadInterstitialAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.CHAPTER_CLEARED_CLAIM));
            AdsClient.Instance.LoadRewardedAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.CHAPTER_CLEARED_DOUBLE_REWARD));
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            if (CurrenciesTransforms != null && CurrenciesTransforms.Count != 0)
            {
                Observable.FromCoroutine(_currencyBoughtAnimations.Animate)
                    .Delay(TimeSpan.FromSeconds(LOADING_MENU_SCENE_DELAY))
                    .Subscribe(_ => MessageBroker.Default.Publish(new ShouldLoadMainMenuScene()))
                    .AddTo(this);
            }
        }

        private void HandleEnteringHiddenState()
        {
            _spinTheWheelElements.SetActive(false);
        }

        /// <summary>
        /// Metoda koja gasi sve elementeu chapter cleared meniju
        /// WHY: Zato sto nakon sto njih ugasimo palimo spin the wheel elemente koji se nalaze u istom meniju
        /// </summary>
        private void DisableAllChapterClearedComponents()
        {
            ClearedText.enabled = false;
            ChapterText.enabled = false;
            ChapterNumberText.enabled = false;
            _tapToOpenText.enabled = false;
            FlashImage.enabled = false;
            ChapterNumberBackgroundImage.enabled = false;
        }

        #endregion





        #region Event Listeners

        /// <summary>
        /// Metoda koja se poziva kada korisnik pritisne ikonicu poklona
        /// WHY: Zato sto tada treba da se pojavi Spin The Wheel sistem i da igrac dobije nagradu
        /// </summary>
        /// <param name="unit"></param>
        private void HandleRewardButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            HandleEnteringSpinTheWheelState();
        }

        private void HandleEnteringSpinTheWheelState()
        {
            DisableAllChapterClearedComponents();
            _chapterClearedMenuAnimations.DisposeAnimations();
            _chapterClearedMenuAnimations.AnimateDisablingThePresentBox();
            _chapterClearedMenuAnimations.AnimateEnablingSpinTheWheel();
            _spinTheWheelElements.SetActive(true);
        }

        /// <summary>
        /// Metoda koja se poziva kada igrac pritisne Start dugme ispod spin the wheel sistema
        /// WHY: Zato sto tada krece da se okrece sistem i pruza sansu igracu da dobije neku nagradu
        /// </summary>
        /// <param name="unit"></param>
        private void HandleStartSpinningButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            HandleEnteringSpinningTheWheelState();
        }

        private void HandleEnteringSpinningTheWheelState()
        {
            MessageBroker.Default.Publish(new ChapterClearedMenuStartedSpinningWheel());
            _currentRandomSpinAngle = new System.Random();
            var randomIndex = _currentRandomSpinAngle.Next(_randomSpinAngles.Length);
            RandomSpinAngle = _currentRandomSpinAngle.Next(_randomSpinAngles[randomIndex] - ANGLE_OFFSET, _randomSpinAngles[randomIndex] + ANGLE_OFFSET);
            StartSpinningButton.gameObject.SetActive(false);
            Observable.FromCoroutine(_chapterClearedMenuAnimations.AnimateSpinningTheWheel)
                .SelectMany(FireAnimationFinishedEvent)
                .SelectMany(InstantiateGameObjects(randomIndex))
                .SelectMany(DelayClaimButtonActivation)
                .Subscribe()
                .AddTo(this);
        }

        private IEnumerator FireAnimationFinishedEvent()
        {
            MessageBroker.Default.Publish(new ChapterClearedMenuFinishedSpinningWheel());
            yield return null;
        }

        private IEnumerator InstantiateGameObjects(int randomIndex)
        {
            if (randomIndex is 0 or 4)
            {
                _doubleRewardButtonText.text = $"+{_spinTheWheelValues[randomIndex]}";
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(list => PrepareAnimationElements(list, _topMenuTokensPosition, Constants.CurrencyType.Token, randomIndex))
                    .AddTo(this);
            }
            else
            {
                _doubleRewardButtonText.text = $"+{_spinTheWheelValues[randomIndex]}";
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                    .Subscribe(list => PrepareAnimationElements(list, _topMenuCoinsPosition, Constants.CurrencyType.Coin, randomIndex))
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }

            yield return null;
        }

        private void PrepareAnimationElements(List<RectTransform> list, RectTransform topMenuPosition, Constants.CurrencyType currencyType, int randomIndex)
        {
            CurrenciesTransforms = list;
            CurrencyAnimationEndObject = topMenuPosition;
            CurrencyType = currencyType;
            CurrencyAmountEarned = _spinTheWheelValues[randomIndex];
            _doubleRewardButton.gameObject.SetActive(true);
            _fireworksParticlesHolder.SetActive(true);
        }

        private IEnumerator DelayClaimButtonActivation()
        {
            yield return new WaitForSeconds(CLAIM_BUTTON_DELAY);
            _claimButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// Metoda koja osluskuje kada igrac klikne na Claim dugme nakon sto se spin the wheel uspesno zavrsi
        /// WHY: Zato sto tada treba da pustimo interstitial i da damo igracu odredjenu nagradu koju je dobio na spin the wheel
        /// </summary>
        /// <param name="unit"></param>
        private void HandleClaimButtonClick(Unit unit)
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogClaimButtonClicked();
#endif
            AdsClient.Instance.ShowInterstitialAds(AdPlacement.PlacementWithName(Constants.AdsPlacements.CHAPTER_CLEARED_CLAIM));
            MessageBroker.Default.Publish(new ChapterClearedMenuClaimButtonClicked());
            MessageBroker.Default.Publish(new ButtonClicked());
            _claimButton.gameObject.SetActive(false);
            _doubleRewardButton.gameObject.SetActive(false);
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        /// <summary>
        /// Metoda koja osluskuje kada igrac klikne na double reward dugme nakon sto se spin the wheel uspesno zavrsi
        /// WHY: Zato sto tada treba da pozovemo RV reklamu
        /// </summary>
        /// <param name="unit"></param>
        private void HandleDoubleRewardButtonClick(Unit unit)
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogDoubleRewardButtonClicked();
#endif

            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ChapterClearedMenuDoubleRewardButtonClicked());
            _claimButton.gameObject.SetActive(false);
            _doubleRewardButton.gameObject.SetActive(false);
            AdsClient.Instance.ShowRewardedVideo(AdPlacement.PlacementWithName(Constants.AdsPlacements.CHAPTER_CLEARED_DOUBLE_REWARD));
        }

        /// <summary>
        /// Metoda koja osluskuje da li je ograc uspesno odgledao RV za dupliranje nagrade i tek ako jeste mu dajemo duplu nagradu
        /// WHY: Zato sto moramo da sprecimo davanje nagrade ukoliko nije odgledan video do kraja
        /// </summary>
        /// <param name="rewardedVideoInChapterClearedCompleted"></param>
        private void HandleRewardedVideoInChapterClearedCompleted(RewardedVideoInChapterClearedCompleted rewardedVideoInChapterClearedCompleted)
        {
            CurrencyAmountEarned *= 2;
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<ChapterClearedMenu>
        {
            
        }

        #endregion
    }
}
