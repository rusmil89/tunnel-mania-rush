﻿using System.Collections;
using System.Linq;
using Core.Events;
using EasyMobile;
using Localization;
using TMPro;
using Tunnel.Ads;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.LevelLoad;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: FinishMenu
    /// DESCRIPTION: Klasa koja kontrolise ponasanje finish menija
    /// </summary>
    public sealed class FinishMenu : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("UI Elements")]
        [SerializeField] private Transform _panel;
        [SerializeField] private Slider _progressPercentSlider;
        [SerializeField] private Button _continueButton;
        [SerializeField] private Image _rewardImage;
        [SerializeField] private GameObject _fireworksParticlesHolder;
        [SerializeField] private TMP_Text _percentageText;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _continueText;

        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;
        [SerializeField] private GameData _gameData;

        [Header("Scene Objects")] 
        [SerializeField] private LevelDataLoading _levelDataLoading;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private FinishMenuAnimations _finishMenuAnimations;
        private int _finishMenuInterstitialFrequency;
        private LanguageClient _languageClient;

        #endregion





        #region Properties
        
        public Slider ProgressSlider => _progressPercentSlider;

        public TMP_Text PercentageText => _percentageText;

        public Image RewardImage => _rewardImage;

        public float Progress { get; private set; }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _progressPercentSlider = gameObject.Descendants().OfComponent<Slider>().First();
            _continueButton = gameObject.Descendants().OfComponent<Button>().First(s => s.name.Contains("Continue"));
            _rewardImage = gameObject.Descendants().OfComponent<Image>().First(s => s.name.Contains("Reward"));
            _percentageText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("Percentage"));
            _fireworksParticlesHolder = gameObject.Descendants().First(s => s.name.Contains("Fireworks")).gameObject;
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            _title = gameObject.Descendants().First(s => s.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _continueText = gameObject.Descendants().First(s => s.name.Contains("ButtonText")).GetComponent<TMP_Text>();
            
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            SetInitialValues();
        }

        private void SetInitialValues()
        {
            _levelDataLoading = FindObjectOfType<LevelDataLoading>();
            _finishMenuAnimations = new FinishMenuAnimations(this);
            _continueButton.gameObject.SetActive(false);
            _finishMenuInterstitialFrequency = RemoteConfigClient.Instance.FinishMenuInterstitialFrequency;
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            HandleMenuShowed(Unit.Default);
            SetLocalization();
            RegisterObservableListeners();
            RegisterButtonListeners();
        }

        private void OnDestroy()
        {
            _finishMenuAnimations.ClearCompositeDisposables();
        }

        #endregion





        #region Custom Methods

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.Perfect.Value);
            _continueText.SetText(_languageClient.CurrentLanguage.Continue.Value);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(HandleMenuShowed)
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _finishMenuAnimations.ClearCompositeDisposables())
                .AddTo(this);
        }

        private void RegisterButtonListeners()
        {
            _continueButton.OnClickAsObservable()
                .Subscribe(HandleContinueButton)
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        /// <summary>
        /// Metoda koja se poziva pri svakom prikazivanju ovog menija
        /// WHY: Zato sto ga treba podesiti
        /// </summary>
        /// <param name="unit"></param>
        private void HandleMenuShowed(Unit unit)
        {
            PlayerReachedTheEndOfLevelObservableListener();
            AdsClient.Instance.LoadInterstitialAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.FINISH_MENU));
        }

        /// <summary>
        /// Metoda koja osluskuje kada je igrac stigao do kraja levela
        /// WHY: Zato sto tada treba da se odigraju nekoliko dogadjaja, pre nego igrac nastavi dalje
        /// </summary>
        private void PlayerReachedTheEndOfLevelObservableListener()
        {
            _fireworksParticlesHolder.SetActive(true);
            Observable.FromCoroutine(PrepareAnimation)
                .SelectMany(_finishMenuAnimations.AnimateTheProgress)
                .SelectMany(PerformActionsAfterFinishingAnimation)
                .Subscribe()
                .AddTo(this);
        }

        private IEnumerator PrepareAnimation()
        {
            if (_playerData.PersistentData.CurrentLevel.Value == 1)
            {
                ProgressSlider.value = 0;
            }
            Progress = (float) _playerData.PersistentData.CurrentLevel.Value / 
                           (float) _levelDataLoading.CurrentWorldData.Chapters[_playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value] - 1].Levels.Count;
            yield return null;
        }

        private IEnumerator PerformActionsAfterFinishingAnimation()
        {
            MessageBroker.Default.Publish(new ProgressAnimationFinished());
            _continueButton.gameObject.SetActive(true);
            if (_playerData.PersistentData.CurrentLevel.Value == _levelDataLoading.CurrentWorldData.Chapters[_playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value] - 1].Levels.Count)
            {
                _continueButton.gameObject.SetActive(false);
                MessageBroker.Default.Publish(new ChapterClearedMenuCalled());
            }

            yield return null;
        }

        private void HandleContinueButton(Unit unit)
        {
            MessageBroker.Default.Publish(new FinishMenuContinueButtonClicked());
            if (_playerData.PersistentData.CurrentLevel.Value % _finishMenuInterstitialFrequency == 0)
            {
                AdsClient.Instance.ShowInterstitialAds(AdPlacement.PlacementWithName(Constants.AdsPlacements.FINISH_MENU));
            }

            _continueButton.gameObject.SetActive(false);
            _fireworksParticlesHolder.SetActive(false);
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<FinishMenu>
        {
            
        }

        #endregion
    }
}
