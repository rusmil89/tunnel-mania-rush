﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Localization;
using RSG;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
        
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Tokens_Menu
    /// DESCRIPTION: Klasa koja kontrolise meni za kupovinu tokena
    /// </summary>
    public class TokensMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Objects")]
        [SerializeField] private Transform _panel;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _tokensPack01Button;
        [SerializeField] private Button _tokensPack02Button;
        [SerializeField] private Button _tokensPack03Button;
        [SerializeField] private TMP_Text _tokensPack01CostText;
        [SerializeField] private TMP_Text _tokensPack02CostText;
        [SerializeField] private TMP_Text _tokensPack03CostText;
        [SerializeField] private TMP_Text _tokensPack01RewardText;
        [SerializeField] private TMP_Text _tokensPack02RewardText;
        [SerializeField] private TMP_Text _tokensPack03RewardText;
        [SerializeField] private TMP_Text _title;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _token;
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencyBoughtAnimations _tokensMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Token;

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject => _topMenuTokensPosition;

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var tokensPackObjects = gameObject.Descendants().Where(x => x.name.Contains("TokensPack")).ToArray();
            List<Button> getButtons = tokensPackObjects.Select(energyPackGameObject => energyPackGameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.REFILL_BUTTON)).GetComponent<Button>()).ToList();
            List<TMP_Text> tokensCostText = tokensPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("RefillButtonText")).GetComponent<TMP_Text>()).ToList();
            List<TMP_Text> tokensRewardText = tokensPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("AmountText")).GetComponent<TMP_Text>()).ToList();
            _tokensPack01Button = getButtons[0];
            _tokensPack02Button = getButtons[1];
            _tokensPack03Button = getButtons[2];
            _tokensPack01CostText = tokensCostText[0];
            _tokensPack02CostText = tokensCostText[1];
            _tokensPack03CostText = tokensCostText[2];
            _tokensPack01RewardText = tokensRewardText[0];
            _tokensPack02RewardText = tokensRewardText[1];
            _tokensPack03RewardText = tokensRewardText[2];
            _title = gameObject.Descendants().First(x => x.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        // Start is called before the first frame update
        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterObservableListeners();
            RegisterButtonsListeners();
            InstantiateTokens();
            Observable.Timer(TimeSpan.FromMilliseconds(500))
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
        }

        private void SetInitialValues()
        {
            _tokensMenuAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            CurrenciesTransforms = new List<RectTransform>();
            _tokensPack01CostText.text = RemoteConfigClient.Instance.TokensPack01Cost.ToString();
            _tokensPack02CostText.text = RemoteConfigClient.Instance.TokensPack02Cost.ToString();
            _tokensPack03CostText.text = RemoteConfigClient.Instance.TokensPack03Cost.ToString();
            _tokensPack01RewardText.text = RemoteConfigClient.Instance.TokensPack01Reward.ToString();
            _tokensPack02RewardText.text = RemoteConfigClient.Instance.TokensPack02Reward.ToString();
            _tokensPack03RewardText.text = RemoteConfigClient.Instance.TokensPack03Reward.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.GetTokens.Value);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _rootState.Exit())
                .AddTo(this);
        }

        private void RegisterButtonsListeners()
        {
            _tokensPack01Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingTokensPack(RemoteConfigClient.Instance.TokensPack01Cost, RemoteConfigClient.Instance.TokensPack01Reward))
                .AddTo(this);
            _tokensPack02Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingTokensPack(RemoteConfigClient.Instance.TokensPack02Cost, RemoteConfigClient.Instance.TokensPack02Reward))
                .AddTo(this);
            _tokensPack03Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingTokensPack(RemoteConfigClient.Instance.TokensPack03Cost, RemoteConfigClient.Instance.TokensPack03Reward))
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(ExitButtonEventListener)
                .AddTo(this);
        }

        private void InstantiateTokens()
        {
            if (CurrenciesTransforms == null || CurrenciesTransforms.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(tokens => CurrenciesTransforms = tokens)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            Observable.FromCoroutine(_tokensMenuAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new TokensMenuCalled()))
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogEarnVirtualCurrency(nameof(_playerData.PersistentData.Tokens), CurrencyAmountEarned);
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.Coins), CurrencyAmountSpent);
#endif
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion





        #region Event Listeners

        private void HandleBuyingTokensPack(int cost, int reward)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (_playerData.PersistentData.Coins.Value >= cost)
            {
                CurrencyAmountEarned = reward;
                CurrencyAmountSpent = cost;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new ShopMenuCalled());
            }
        }

        private void ExitButtonEventListener(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<TokensMenu>
        {
            
        }

        #endregion
    }
}
