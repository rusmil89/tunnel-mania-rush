﻿using Tunnel.UI;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStaticMenu", menuName = "ScriptableObjects/Menus/Static/NewStaticMenu", order = 1)]
public class StaticMenuSO : MenuSO
{
    [HideInInspector] public StaticMenu staticMenu;

    public override void Show()
    {
        IsMenuShown = true;
        staticMenu.transform.GetChild(0).gameObject.SetActive(true);
    }

    public override void Hide()
    {
        IsMenuShown = false;
        staticMenu.transform.GetChild(0).gameObject.SetActive(false);
    }
}
