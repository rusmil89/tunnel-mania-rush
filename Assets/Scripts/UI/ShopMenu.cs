﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIEffects;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using TMPro;
using Tunnel.Ads;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    #region Event Arguments

    internal sealed class FailedArgs
    {
        public IAPProduct IapProduct { get; set; }
        public string Message { get; set; }
    }

    internal sealed class CompletedArgs
    {
        public IAPProduct IapProduct { get; set; }
    }

    #endregion
    
    /// <summary>
    /// SCENE: Shop
    /// GAME_OBJECT: Shop
    /// DESCRIPTION: Klasa koja kontrolise ponasanje shop menija koji se pojavljuje u menu i level sceni svaki put kada korisnik zeli da nesto kupi svojom voljom ili kada nema dovoljno sredstava za odredjenu kupovinu
    /// </summary>
    public class ShopMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Objects")]
        [SerializeField] private Transform _panel;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _coinsPack01Button;
        [SerializeField] private Button _coinsPack02Button;
        [SerializeField] private Button _coinsPack03Button;
        [SerializeField] private Button _coinsPack04Button;
        [SerializeField] private TMP_Text _coinsPack01CostText;
        [SerializeField] private TMP_Text _coinsPack02CostText;
        [SerializeField] private TMP_Text _coinsPack03CostText;
        [SerializeField] private TMP_Text _coinsPack04CostText;
        [SerializeField] private TMP_Text _coinsPack01RewardText;
        [SerializeField] private TMP_Text _coinsPack02RewardText;
        [SerializeField] private TMP_Text _coinsPack03RewardText;
        [SerializeField] private TMP_Text _coinsPack04RewardText;
        [SerializeField] private TMP_Text _attemptsText;
        [SerializeField] private TMP_Text _title;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private RectTransform _coin;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencyBoughtAnimations _shopMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Coin;

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent => default;

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject => _topMenuCoinsPosition;

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        private static IObservable<CompletedArgs> PurchaseCompletedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<IAPProduct>, CompletedArgs>(
                    h => iapProduct => h(new CompletedArgs {IapProduct = iapProduct}),
                    h => InAppPurchasing.PurchaseCompleted += h,
                    h => InAppPurchasing.PurchaseCompleted -= h);
            }
        }

        private static IObservable<FailedArgs> PurchaseFailedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<IAPProduct, string>, FailedArgs>(
                    h => (iapProduct, message) => h(new FailedArgs {IapProduct = iapProduct, Message = message}),
                    h => InAppPurchasing.PurchaseFailed += h,
                    h => InAppPurchasing.PurchaseFailed -= h);
            }
        }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var tokensPackObjects = gameObject.Descendants().Where(x => x.name.Contains("CoinsPack")).ToArray();
            List<Button> getButtons = tokensPackObjects.Select(energyPackGameObject => energyPackGameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.REFILL_BUTTON)).GetComponent<Button>()).ToList();
            List<TMP_Text> tokensCostText = tokensPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("RefillButtonText")).GetComponent<TMP_Text>()).ToList();
            List<TMP_Text> tokensRewardText = tokensPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("AmountText")).GetComponent<TMP_Text>()).ToList();
            _coinsPack01Button = getButtons[0];
            _coinsPack02Button = getButtons[1];
            _coinsPack03Button = getButtons[2];
            _coinsPack04Button = getButtons[3];
            _coinsPack01CostText = tokensCostText[0];
            _coinsPack02CostText = tokensCostText[1];
            _coinsPack03CostText = tokensCostText[2];
            _coinsPack04CostText = tokensCostText[3];
            _coinsPack01RewardText = tokensRewardText[0];
            _coinsPack02RewardText = tokensRewardText[1];
            _coinsPack03RewardText = tokensRewardText[2];
            _coinsPack04RewardText = tokensRewardText[3];
            _attemptsText = gameObject.Descendants().First(component => component.name.Contains("AttemptsText")).GetComponent<TMP_Text>();
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            _title = gameObject.Descendants().First(component => component.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _coin = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().FirstOrDefault(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = gameObject.Descendants().First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        // Start is called before the first frame update
        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(menuState => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                    .Enter(menuState => HandleEnteringShownState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(menuState => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(menuState => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterObservableListeners();
            RegisterButtonsListeners();
            InstantiateCoins();
            Observable.Timer(TimeSpan.FromMilliseconds(500))
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
        }

        private void SetInitialValues()
        {
            _shopMenuAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            CurrenciesTransforms = new List<RectTransform>();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            _coinsPack02CostText.text = InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_Small_Coin_Pack).localizedPriceString;
            _coinsPack03CostText.text = InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_Medium_Coin_Pack).localizedPriceString;
            _coinsPack04CostText.text = InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_Large_Coin_Pack).localizedPriceString;
#elif UNITY_EDITOR 
            _coinsPack02CostText.text = InAppPurchasing.GetIAPProductByName(EM_IAPConstants.Product_Small_Coin_Pack).Price;
            _coinsPack03CostText.text = InAppPurchasing.GetIAPProductByName(EM_IAPConstants.Product_Medium_Coin_Pack).Price;
            _coinsPack04CostText.text = InAppPurchasing.GetIAPProductByName(EM_IAPConstants.Product_Large_Coin_Pack).Price;
#endif
            _coinsPack01RewardText.text = RemoteConfigClient.Instance.CoinsPack01Reward.ToString();
            _coinsPack02RewardText.text = RemoteConfigClient.Instance.CoinsPack02Reward.ToString();
            _coinsPack03RewardText.text = RemoteConfigClient.Instance.CoinsPack03Reward.ToString();
            _coinsPack04RewardText.text = RemoteConfigClient.Instance.CoinsPack04Reward.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.Shop.Value);
            _coinsPack01CostText.SetText(_languageClient.CurrentLanguage.Free.Value);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _rootState.Exit())
                .AddTo(this);
            PurchaseCompletedAsObservable
                .Subscribe(HandlePurchaseCompletedEvent)
                .AddTo(this);
            PurchaseFailedAsObservable
                .Subscribe(HandlePurchaseFailedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<CoinsBought>()
                .Subscribe(HandleSuccessfulAdsWatchingForCoins)
                .AddTo(this);
            _playerData.PersistentData.BuyCoinsForAdsDailyAttempts
                .SubscribeToText(_attemptsText)
                .AddTo(this);
            _playerData.PersistentData.BuyCoinsForAdsDailyAttempts
                .Where(value => value == 0)
                .Subscribe(_ => DisableBuyingCoinsWithAdsButton())
                .AddTo(this);
        }

        private void RegisterButtonsListeners()
        {
            _coinsPack01Button.OnClickAsObservable()
                .TakeWhile(_ => _playerData.PersistentData.BuyCoinsForAdsDailyAttempts.Value > 0 && DateTime.Today.Subtract(_playerData.PersistentData.BuyCoinsForAdsLastAttemptDay.Value).Days >= 1)
                .Subscribe(_ => HandleBuyingCoinsForAds())
                .AddTo(this);
            _coinsPack02Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingCoinsPack(EM_IAPConstants.Product_Small_Coin_Pack))
                .AddTo(this);
            _coinsPack03Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingCoinsPack(EM_IAPConstants.Product_Medium_Coin_Pack))
                .AddTo(this);
            _coinsPack04Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingCoinsPack(EM_IAPConstants.Product_Large_Coin_Pack))
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(_ => HandleExitButton())
                .AddTo(this);
        }

        private void InstantiateCoins()
        {
            if (CurrenciesTransforms == null || CurrenciesTransforms.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                    .Subscribe(coins => CurrenciesTransforms = coins)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringShownState()
        {
            if (_coinsPack01Button.interactable)
            {
                AdsClient.Instance.LoadRewardedAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.BUY_COINS));
            }

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogShopMenuOpened();
#endif
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            Observable.FromCoroutine(_shopMenuAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new ShopMenuCalled()))
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogEarnVirtualCurrency(nameof(_playerData.PersistentData.Coins), CurrencyAmountEarned);
#endif
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion





        #region Event Listeners

        private void HandleBuyingCoinsForAds()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            AdsClient.Instance.ShowRewardedVideo(AdPlacement.PlacementWithName(Constants.AdsPlacements.BUY_COINS));
        }

        private void HandleBuyingCoinsPack(string productName)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            InAppPurchasing.Purchase(productName);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogInAppClicked(productName);
#endif
            
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            switch (productName)
            {
                case EM_IAPConstants.Product_Small_Coin_Pack: 
                    CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack02Reward;
                    break;
                case EM_IAPConstants.Product_Medium_Coin_Pack: 
                    CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack03Reward;
                    break;
                case EM_IAPConstants.Product_Large_Coin_Pack: 
                    CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack04Reward;
                    break;
            }
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
#endif
        }

        private void HandleExitButton()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        private void HandlePurchaseCompletedEvent(CompletedArgs completedArgs)
        {
            switch (completedArgs.IapProduct.Name)
            {
                case EM_IAPConstants.Product_Small_Coin_Pack:
                {
                     CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack02Reward;
                }
                    break;
                case EM_IAPConstants.Product_Medium_Coin_Pack:
                {
                    CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack03Reward;
                }
                    break;
                case EM_IAPConstants.Product_Large_Coin_Pack:
                {
                    CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack04Reward;
                }
                    break;
                case EM_IAPConstants.Product_Remove_Ads:
                {
                    Advertising.RemoveAds();
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            
#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogSuccessfulInAppPurchase(completedArgs.IapProduct.Name, completedArgs.IapProduct.Price);
#endif
        }

        private void HandlePurchaseFailedEvent(FailedArgs failedArgs)
        {
            Logging.LogError($"PurchaseFailedEventListener - product: {failedArgs.IapProduct.Name}");
            //TODO: prikazi meni koji obavestava korisnika da je doslo do greske
        }

        /// <summary>
        /// Metoda koja se poziva tek kada korisnik uspesno odgleda RV za dobijanje koina
        /// WHY: Zato sto samo u tom slucaju treba da nagradimo korisnika i smanjimo broj dnevnih kupovina koina za RV
        /// </summary>
        /// <param name="coinsBought"></param>
        private void HandleSuccessfulAdsWatchingForCoins(CoinsBought coinsBought)
        {
            _playerData.PersistentData.BuyCoinsForAdsDailyAttempts.Value--;
            CurrencyAmountEarned = RemoteConfigClient.Instance.CoinsPack01Reward;
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        /// <summary>
        /// Metoda koja se poziva nakon sto korisnik potrosi sva 3 pokusaja za dopunjavanje koina za RV
        /// WHY: Zato sto tada zabranjujem dalju dopunu i belezim vreme kada se to desilo kako bih sledeceg dana ponovo omogucio korisniku 3 pokusaja
        /// </summary>
        private void DisableBuyingCoinsWithAdsButton()
        {
            _playerData.PersistentData.BuyCoinsForAdsLastAttemptDay.Value = DateTime.Today;
            _coinsPack01Button.interactable = false;
            Destroy(_coinsPack01Button.GetComponent<UIShiny>());
            MainThreadDispatcher.StartCoroutine(DelayGrayscaleEffect());
        }

        private IEnumerator DelayGrayscaleEffect()
        {
            yield return null;
            var effect = _coinsPack01Button.gameObject.AddComponent<UIEffect>();
            effect.effectMode = EffectMode.Grayscale;
        }

        #endregion





        #region Factory

        public class Factory :PlaceholderFactory<ShopMenu>
        {
            
        }

        #endregion
    }
}
