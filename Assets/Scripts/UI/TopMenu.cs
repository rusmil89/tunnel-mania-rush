﻿using System.Linq;
using Core.Events;
using Dreamteck.Splines;
using Localization;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    public class TopMenu : StaticMenu
    {
        #region Public Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private TopMenuAnimations _topMenuAnimations;
        private Button _shieldsButton;
        private Color _defaultLevelsProgressImageColor;
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("UI Elements")]
        [SerializeField] private Slider _progressSlider;
        [SerializeField] private TMP_Text _currentMetersText;
        [SerializeField] private TMP_Text _highScoreValueText;
        [SerializeField] private TMP_Text _numberOfShields;
        [SerializeField] private TMP_Text _scoreText;
        [SerializeField] private TMP_Text _highScoreText;
        [SerializeField] private TMP_Text _levelText;
        [SerializeField] private GameObject _shieldsGroupOfObjects;
        [SerializeField] private Button _pauseButton;
        [SerializeField] private Image[] _levelImages;
        
        [Header("Scene Elements")]
        [SerializeField] private SplineFollower _playerSplineFollower;

        [Header("Scriptable Objects")] 
        [SerializeField] private GameData _gameData;
        [SerializeField] private MetersSO _meters;
        [SerializeField] private global::PlayerData _playerData;
        [SerializeField] private WorldData[] _allWorldData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _gameData = AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _meters = AssetDatabase.FindAssets(Constants.Editor.Filters.METERS, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<MetersSO>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            var allWorldDataGUIDs =
                AssetDatabase.FindAssets(Constants.Editor.Filters.WORLD_DATA, new[] { Constants.Editor.Paths.SCRIPTABLE_OBJECTS });
            _allWorldData = allWorldDataGUIDs
                .Select(s => AssetDatabase.LoadAssetAtPath<WorldData>(AssetDatabase.GUIDToAssetPath(s)))
                .Where(s => !s.name.Contains("Tutorial"))
                .ToArray();

            var allRectTransforms = gameObject.Descendants().OfComponent<RectTransform>();
            _progressSlider = allRectTransforms.First(s => s.name.StartsWith("Progress")).GetComponent<Slider>();
            _currentMetersText = allRectTransforms.First(s => s.name.StartsWith("ScoreValueText")).GetComponent<TMP_Text>();
            _highScoreValueText = allRectTransforms.First(s => s.name.StartsWith("HighScoreValueText")).GetComponent<TMP_Text>();
            _numberOfShields = allRectTransforms.First(s => s.name.StartsWith("NumberOfShields")).GetComponent<TMP_Text>();
            _shieldsGroupOfObjects = allRectTransforms.First(s => s.name.StartsWith("Shields")).gameObject;
            _pauseButton = allRectTransforms.First(s => s.name.StartsWith("PauseButton")).GetComponent<Button>();
            _levelImages = allRectTransforms.First(s => s.name.StartsWith("Levels")).gameObject.Children().OfComponent<Image>().ToArray();
            
            UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
            bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
            bool prefabConnected = UnityEditor.PrefabUtility.GetPrefabInstanceStatus(gameObject) == UnityEditor.PrefabInstanceStatus.Connected;
            if (!isValidPrefabStage && prefabConnected)
            {
                ReferencesClient.CheckReferences(this, gameObject);
            }
        }
#endif

        private void Awake()
        {
            SetInitialValues();
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterEventListeners();
            RegisterObservableListeners();
        }

        private void OnDestroy()
        {
            _meters.Value = 0;
            _topMenuAnimations.ClearCompositeDisposals();
        }

        #endregion





        #region Custom Methods

        /// <summary>
        /// Metoda koja postavlja pocetna podesavanja za top menu
        /// WHY: zato sto je mnogo bitno da kod ostane citljiv
        /// </summary>
        private void SetInitialValues()
        {
            staticMenuSo.staticMenu = this;
            _shieldsButton = _shieldsGroupOfObjects.GetComponent<Button>();
            _topMenuAnimations = new TopMenuAnimations();
            _highScoreValueText.text = _playerData.PersistentData.PlayerMaxRecordedDistance.Value.ToString("F");
            _defaultLevelsProgressImageColor = _levelImages[0].color;
            SetLevelsLook();
        }

        private void SetLevelsLook()
        {
            var numberOfLevels = _allWorldData[_playerData.PersistentData.CurrentWorld.Value - 1]
                .Chapters[_playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value] - 1].Levels.Count;
            if (numberOfLevels == 5)
            {
                foreach (var levelImage in _levelImages)
                {
                    levelImage.gameObject.SetActive(true);
                }
            }
            else
            {
                for (int i = _levelImages.Length - 1; i >= numberOfLevels; i--)
                {
                    _levelImages[i].gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Metoda koja postavlja prevode
        /// </summary>
        private void SetLocalization()
        {
            _scoreText.SetText(_languageClient.CurrentLanguage.Score.Value);
            _highScoreText.SetText(_languageClient.CurrentLanguage.HighScore.Value);
            _levelText.SetText(_languageClient.CurrentLanguage.Level.Value);
        }

        private void RegisterEventListeners()
        {
            _pauseButton.OnClickAsObservable()
                .Subscribe(_ => PauseButtonEventListener())
                .AddTo(this);
            _shieldsButton.OnClickAsObservable()
                .Subscribe(_ => ShieldButtonEventListener())
                .AddTo(this);
            _playerData.PersistentData.Shields
                .SubscribeToText(_numberOfShields)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            Observable.EveryUpdate()
                .Subscribe(EveryUpdateObservableEventListener)
                .AddTo(this);
            Observable.IntervalFrame(5)
                .Subscribe(HandleUpdatePlayerPathLength)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(PlayerDiedEventHandler)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerRevived>()
                .Subscribe(PlayerRevivedEventHandler)
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuCalled>()
                .Subscribe(HandleChapterClearedMenuCalled)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfLevel>()
                .Subscribe(SetLevelsProgressImages)
                .AddTo(this);
            _playerData.PersistentData.Shields
                .Where(shields => shields == 0)
                .Subscribe(HandleAnimatingAddShieldsButton)
                .AddTo(this);
        }

        #endregion
        




        #region Event Listeners
        
        /// <summary>
        /// Osluskivac klika na Pause dugme u top meniju
        /// WHY: Bolja kontrola nad tim sta se sve poziva kada je klikne na pause. Ako se ovo kontrolise preko Inspectora onda je gubljenje referenci prava nocna mora
        /// Ovde kompajler istog momenta prijavi gresku ukoliko se ona pojavi
        /// </summary>
        private void PauseButtonEventListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new PauseMenuCalled());
        }

        /// <summary>
        /// Osluskivac klika na dugme koje pokazuje broj stitova igraca i nudi mogucnost kupovine istih
        /// WHY: Bolja kontrola nad tim sta se sve poziva kada je klikne na dugme. Ako se ovo kontrolise preko Inspectora onda je gubljenje referenci prava nocna mora
        /// Ovde kompajler istog momenta prijavi gresku ukoliko se ona pojavi
        /// </summary>
        private void ShieldButtonEventListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new GetShieldsMenuCalled());
        }

        /// <summary>
        /// Osluskivac koji reaguje na event kada igrac pogine
        /// </summary>
        /// <param name="playerDied">parametri koji se prosledjuju sa eventom</param>
        private void PlayerDiedEventHandler(PlayerDied playerDied)
        {
            if (_playerData.PersistentData.PlayerMaxRecordedDistance.Value < _meters.Value)
            {
                _playerData.PersistentData.PlayerMaxRecordedDistance.Value = _meters.Value;
            }
        }

        /// <summary>
        /// Osluskivac koji reaguje na event kada igrac ponovo ozivi
        /// </summary>
        private void PlayerRevivedEventHandler(PlayerRevived playerRevived) => _highScoreValueText.text = _playerData.PersistentData.PlayerMaxRecordedDistance.Value.ToString("F");

        /// <summary>
        /// Osluskivac koji reaguje kada se pozove chapter cleared menu
        /// </summary>
        /// <param name="chapterClearedMenuCalled">chapter cleared menu event</param>
        private void HandleChapterClearedMenuCalled(ChapterClearedMenuCalled chapterClearedMenuCalled)
        {
            DisableProgressSlider();
            ResetLevelImages();
            SetHighScore();
        }

        /// <summary>
        /// Metoda koja osluskuje kada je igrac stigao do kraja jednog chaptera
        /// WHY: Zato sto tada treba da resetujemo odredjene parametre
        /// </summary>
        private void DisableProgressSlider() => _progressSlider.gameObject.SetActive(false);

        /// <summary>
        /// Metoda koja postavlja male slicice koje predstavljaju levele u jednom chapteru na default boju
        /// WHY: Zato sto nakon zavrsetka jednog chaptera, igrac krece drugi i onda se progres levela resetuje kako bi se validno pokazao za trenutni chapter
        /// </summary>
        private void ResetLevelImages()
        {
            foreach (var levelImage in _levelImages)
            {
                levelImage.color = _defaultLevelsProgressImageColor;
            }
        }

        /// <summary>
        /// Metoda koja postavlja high score igracu ukoliko on stigne do kraja chaptera bez da je ijednog trenutka poginuo
        /// WHY: Zato sto high score treba i tada beleziti
        /// </summary>
        private void SetHighScore()
        {
            if (_playerData.PersistentData.PlayerMaxRecordedDistance.Value < _meters.Value)
            {
                _playerData.PersistentData.PlayerMaxRecordedDistance.Value = _meters.Value;
            }
        }

        /// <summary>
        /// Osluskivac Observable stream-a koji simulira Update metodu
        /// WHY: Zato sto je mnogo optimizovaniji za izvrsavanje
        /// </summary>
        /// <param name="obj">parametar koji prosledjuje UniRx plugin kada se koristi Update Stream</param>
        private void EveryUpdateObservableEventListener(long obj)
        {
            if (_gameData.IsGamePaused) return;
            
            _progressSlider.value = (float) _playerSplineFollower.result.percent;
            _meters.Value += _gameData.PlayerCurrentSpeed * Time.deltaTime;
        }

        /// <summary>
        /// Osluskivac Observable stream-a koji se okida svakog 5-og frame da bi se updatetovala vrednost metra u top meniju
        /// WHY: Zato sto je svaki update UI-a veoma skupa operacija i na ovaj nacin pokusavamo da smanjimo tu skupu operaciju koliko god je moguce, a da korisnik ne primeti znacajnu razliku
        /// </summary>
        /// <param name="obj">parametar koji prosledjuje UniRx plugin kada se koristi Update Stream</param>
        private void HandleUpdatePlayerPathLength(long obj) => _currentMetersText.text = _meters.Value.ToString("F");

        /// <summary>
        /// Metoda koja osluskuje dogadjaj kada igrac stigne do kraja levela i postavlja progres u chapteru
        /// WHY: Zato sto zelimo da korisnik u svakom trenutku zna gde se trenutno u chapteru nalazi (immediate feedback)
        /// </summary>
        /// <param name="playerReachedEndOfLevel"></param>
        private void SetLevelsProgressImages(PlayerReachedEndOfLevel playerReachedEndOfLevel) => _levelImages[_playerData.PersistentData.CurrentLevel.Value - 1].color = Color.cyan;
        
        /// <summary>
        /// Metoda koja animira dugme za dodavanje stitova
        /// </summary>
        /// <param name="shieldsNumber"></param>
        private void HandleAnimatingAddShieldsButton(int shieldsNumber) => _topMenuAnimations.AnimateAddShieldsButton(_shieldsButton.gameObject);

        #endregion
    }
}