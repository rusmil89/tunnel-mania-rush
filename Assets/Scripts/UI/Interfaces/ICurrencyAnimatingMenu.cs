using System.Collections.Generic;
using Tunnel.Utilities;
using UnityEngine;

namespace Tunnel.UI
{
    public interface ICurrencyAnimatingMenu
    {
        Constants.CurrencyType CurrencyType { get; }
        int CurrencyAmountEarned { get; }
        int CurrencyAmountSpent { get; }
        List<RectTransform> CurrenciesTransforms { get; }
        RectTransform CurrencyAnimationStartObject { get; }
        RectTransform CurrencyAnimationEndObject { get; }
        GameObject CurrencyAmountPrefab { get; }
    }
}