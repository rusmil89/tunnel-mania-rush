﻿using System.Linq;
using Core.Events;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/UnlockButton
    /// DESCRIPTION: Klasa koja kontrolise ponasanje unlock dugmeta koje otkljucava novi chapter
    /// </summary>
    public sealed class UnlockChapterButton : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _unlockButton;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetInitialValues();
            RegisterEventListeners();
        }

        private void SetInitialValues()
        {
            _unlockButton = GetComponent<Button>();
        }

        private void RegisterEventListeners()
        {
            _unlockButton.OnClickAsObservable()
                .Subscribe(_ => UnlockButtonEventHandler())
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void UnlockButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new UnlockChapterMenuCalled());
        }

        #endregion
    }
}
