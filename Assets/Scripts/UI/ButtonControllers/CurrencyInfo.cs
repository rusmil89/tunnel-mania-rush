using TMPro;
using Tunnel.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Apstraktna klasa koja kontrolise ponasanje objekta koji sluzi za prikazivanje informacija o nekoj valuti u Menu sceni
    /// </summary>
    public abstract class CurrencyInfo : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        protected TMP_Text CurrencyAmountText;
        protected Button CurrencyButton;
        protected Animator ButtonAnimator;
        protected ParticleSystem PoofVfxParticleSystem;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] protected global::PlayerData PlayerData;

        [Header("Prefabs")] 
        [SerializeField] protected GameObject PoofVFXGameObject;
        
        /// <summary>
        /// Consts
        /// </summary>
        protected const float SCALE_FACTOR = 1.2f;
        protected const float SCALE_DURATION = 0.2f;

        #endregion





        #region Monobehaviour Events

        protected abstract void Start();
        protected abstract void OnDestroy();

        #endregion





        #region Custom Methods

        protected abstract void Animate();

        #endregion
    }
}