﻿using System.Linq;
using Core.Events;
using EasyMobile;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/BottomButtons/Achievements
    /// DESCRIPTION: Klasa koja kontrolise ponasanje dugmeta za google play services achievements u menu sceni
    /// </summary>
    public sealed class AchievementsButton : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _achievementsButton;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerDataController;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerDataController = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetInitialValues();
            RegisterEventListeners();
        }

        private void SetInitialValues()
        {
            _achievementsButton = GetComponent<Button>();
        }

        private void RegisterEventListeners()
        {
            _achievementsButton.onClick.AddListener(AchievementsButtonEventHandler);
        }

        private void OnDestroy()
        {
            UnregisterEventListeners();
        }

        private void UnregisterEventListeners()
        {
            _achievementsButton.onClick.RemoveAllListeners();
        }

        #endregion





        #region Event Listeners
        
        private void AchievementsButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            OpenAchievementsUI();
        }

        #endregion
        
        
        


        #region Custom Methods

        private static void OpenAchievementsUI()
        {
            if (GameServices.IsInitialized())
            {
                GameServices.ShowAchievementsUI();
            }
            else
            {
                NativeUI.ShowToast("Google Game services is not initialized!");
            }
        }

        #endregion
    }
}
