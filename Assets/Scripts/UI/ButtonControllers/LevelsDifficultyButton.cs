﻿#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
using Tunnel.Analytics;
#endif
using System;
using System.Collections;
using System.Linq;
using Core.Events;
using Localization;
using TMPro;
using Tunnel.Audio;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    public sealed class LevelsDifficultyButton : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Space]
        [Header("SO")]
        [SerializeField] private global::PlayerData _playerDataController;
        [SerializeField] private GameData _gameController;
        
        [Space]
        [Header("UI")]
        [SerializeField] private TMP_Text _difficultyLevelsText;
        [SerializeField] private GameObject _mediumDifficultyImage;
        [SerializeField] private GameObject _hardDifficultyImage;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private float _easyMovementSpeed;
        private float _easyRotationSpeed;
        private float _mediumMovementSpeed;
        private float _mediumRotationSpeed;
        private float _hardMovementSpeed;
        private float _hardRotationSpeed;
        private Button _levelsDifficultyButton;
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float TIMEOUT = 0.125f;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerDataController = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameController = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            GetRequiredComponents();
        }

        private void GetRequiredComponents()
        {
            _levelsDifficultyButton = GetComponent<Button>();
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
            RemoteConfigClient.Instance.Ready
                .Where(isReady => isReady)
                .Subscribe(SetRemoteSettings)
                .AddTo(this);
        }

        private void OnEnable()
        {
            RegisterEventListeners();
        }

        private void RegisterEventListeners()
        {
            _levelsDifficultyButton.onClick.AddListener(LevelsDifficultyEventHandler);
        }

        private void OnDisable()
        {
            UnregisterEventListeners();
        }

        private void UnregisterEventListeners()
        {
            _levelsDifficultyButton.onClick.RemoveAllListeners();
        }

        #endregion





        #region Coroutines

        private void SetRemoteSettings(bool b)
        {
            _easyMovementSpeed = RemoteConfigClient.Instance.EasyMovementSpeed;
            _easyRotationSpeed = RemoteConfigClient.Instance.EasyRotationSpeed;
            _mediumMovementSpeed = RemoteConfigClient.Instance.MediumMovementSpeed;
            _mediumRotationSpeed = RemoteConfigClient.Instance.MediumRotationSpeed;
            _hardMovementSpeed = RemoteConfigClient.Instance.HardMovementSpeed;
            _hardRotationSpeed = RemoteConfigClient.Instance.HardRotationSpeed;
            
            switch (_playerDataController.PersistentData.DifficultyOption)
            {
                case PlayerData.PersistentData.Difficulty.Easy:
                    SetDifficultyParameters(_easyMovementSpeed, _easyRotationSpeed, false, false, _languageClient.CurrentLanguage.Easy.Value);
                    break;
                case PlayerData.PersistentData.Difficulty.Medium:
                    SetDifficultyParameters(_mediumMovementSpeed, _mediumRotationSpeed, true, false, _languageClient.CurrentLanguage.Medium.Value);
                    break;
                case PlayerData.PersistentData.Difficulty.Hard:
                    SetDifficultyParameters(_hardMovementSpeed, _hardRotationSpeed, true, true, _languageClient.CurrentLanguage.Hard.Value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion





        #region Custom Methods

        public void ChangeDifficultyLevel()
        {
            switch (_playerDataController.PersistentData.DifficultyOption)
            {
                case PlayerData.PersistentData.Difficulty.Easy:
                    _playerDataController.PersistentData.DifficultyOption = PlayerData.PersistentData.Difficulty.Medium;
                    SetDifficultyParameters(_mediumMovementSpeed, _mediumRotationSpeed, true, false, _languageClient.CurrentLanguage.Medium.Value);
                    break;
                case PlayerData.PersistentData.Difficulty.Medium:
                    _playerDataController.PersistentData.DifficultyOption = PlayerData.PersistentData.Difficulty.Hard;
                    SetDifficultyParameters(_hardMovementSpeed, _hardRotationSpeed, true, true, _languageClient.CurrentLanguage.Hard.Value);
                    break;
                case PlayerData.PersistentData.Difficulty.Hard:
                    _playerDataController.PersistentData.DifficultyOption = PlayerData.PersistentData.Difficulty.Easy;
                    SetDifficultyParameters(_easyMovementSpeed, _easyRotationSpeed, false, false, _languageClient.CurrentLanguage.Easy.Value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        Analytics.WarmAppGamesAnalytics.LogLevelDifficultyEvent(_playerDataController.PersistentData.DifficultyOption.ToString());
#endif
        }

        private void SetDifficultyParameters(float movementSpeed, float rotationSpeed, bool isMediumImageOn, bool isHardImageOn, string buttonText)
        {
            _gameController.PlayerMovementSpeed = movementSpeed;
            _gameController.PlayerRotationSpeed = rotationSpeed;
            _mediumDifficultyImage.SetActive(isMediumImageOn);
            _hardDifficultyImage.SetActive(isHardImageOn);
            _difficultyLevelsText.text = buttonText.ToUpper();
        }

        #endregion





        #region Event Listeners
        
        private void LevelsDifficultyEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            ChangeDifficultyLevel();
        }

        #endregion
    }
}