﻿using System.Linq;
using Core.Events;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/BottomButtons/DailyRewards
    /// DESCRIPTION: Klasa koja kontrolise ponasanje dugmeta daily rewards u menu sceni
    /// </summary>
    public sealed class DailyRewardsButton : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _dailyRewardsButton;
        private GameObject _indicator;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetInitialValues();
            RegisterButtonsListeners();
            RegisterObservableListeners();
        }

        private void SetInitialValues()
        {
            _dailyRewardsButton = GetComponent<Button>();
            _indicator = gameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.INDICATOR)).gameObject;
        }

        private void RegisterButtonsListeners()
        {
            _dailyRewardsButton.OnClickAsObservable()
                .Subscribe(ListenToDailyRewardsButton)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _playerData.PersistentData.IsTodayRewardCollected
                .Subscribe(isTodayRewardCollected => _indicator.SetActive(!isTodayRewardCollected))
                .AddTo(this);
        }

        #endregion





        #region Event Listeners


        private void ListenToDailyRewardsButton(Unit obj)
        { 
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new DailyRewardMenuCalled());
        }

        #endregion
    }
}
