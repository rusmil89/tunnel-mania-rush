﻿using System.Linq;
using Core.Events;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/BottomButtons/Shop
    /// DESCRIPTION: Klasa koja kontrolise ponasanje dugmeta za poziv prodavnice
    /// </summary>
    public class ShopButton : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _shopButton;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetInitialValues();
            RegisterObservableListeners();
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            _shopButton = GetComponent<Button>();
        }

        private void RegisterObservableListeners()
        {
            _shopButton.OnClickAsObservable()
                .Subscribe(ShopButtonEventListener)
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void ShopButtonEventListener(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ShopMenuCalled());
        }

        #endregion
    }
}
