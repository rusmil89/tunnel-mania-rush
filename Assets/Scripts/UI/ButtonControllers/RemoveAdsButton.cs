﻿using System;
using System.Linq;
using Core.Events;
using EasyMobile;
using TMPro;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Purchasing.Security;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Canvas/Home/RemoveAdsButton
    /// DESCRIPTION: Klasa koja kontrolise rad dugmeta za kupovinu RemoveAds in-app-a
    /// </summary>
    public sealed class RemoveAdsButton : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerDataController;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _removeAdsButton;
        private TMP_Text _priceText;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float SCALE_FACTOR = 1.2f;
        private const float SCALE_DURATION = 0.2f;
        private const float REMOVE_ADS_OBJECT_SCALE_DELAY = 5f;

        #endregion





        #region Properties

        private static IObservable<CompletedArgs> PurchaseCompletedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<IAPProduct>, CompletedArgs>(
                    h => iapProduct => h(new CompletedArgs {IapProduct = iapProduct}),
                    h => InAppPurchasing.PurchaseCompleted += h,
                    h => InAppPurchasing.PurchaseCompleted -= h);
            }
        }

        private static IObservable<FailedArgs> PurchaseFailedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<IAPProduct, string>, FailedArgs>(
                    h => (iapProduct, message) => h(new FailedArgs {IapProduct = iapProduct, Message = message}),
                    h => InAppPurchasing.PurchaseFailed += h,
                    h => InAppPurchasing.PurchaseFailed -= h);
            }
        }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerDataController = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            RegisterObservableListeners();
            SetInitialValues();
        }

        #endregion





        #region Custom Methods
        
        private void RegisterObservableListeners()
        {
            PurchaseCompletedAsObservable
                .Subscribe(PurchaseCompletedEventListener)
                .AddTo(this);
            PurchaseFailedAsObservable
                .Subscribe(PurchaseFailedEventListener)
                .AddTo(this);
        }

        private void SetInitialValues()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            var isRemoveAdsOwned = InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_Remove_Ads);
            if (isRemoveAdsOwned)
            {
                gameObject.SetActive(false);
            }
            else
            {
                ConfigureButton();
                RegisterButtonsListeners();
                AnimateRemoveAdsObject();
            }
#elif UNITY_IOS && !UNITY_EDITOR
        //TODO: implementirati iOS funkcionalnost
#elif UNITY_EDITOR
            ConfigureButton();
#endif
        }

        private void ConfigureButton()
        {
            _removeAdsButton = GetComponent<Button>();
            _priceText = gameObject.Descendants().First(child => child.name.Contains("PriceText")).GetComponent<TMP_Text>();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                _priceText.text = InAppPurchasing.GetProductLocalizedData(EM_IAPConstants.Product_Remove_Ads).localizedPriceString;
#elif UNITY_EDITOR
            _priceText.text = InAppPurchasing.GetIAPProductByName(EM_IAPConstants.Product_Remove_Ads).Price;
#endif
        }

        private void RegisterButtonsListeners()
        {
            _removeAdsButton.OnClickAsObservable()
                .Subscribe(_ => RemoveAdsEventHandler())
                .AddTo(this);
        }

        /// <summary>
        /// Metoda koja animira objekat za kupovinu in-app-a za ukljanjanje reklama
        /// WHY: Da bi privlacilo paznju igracima i nateralo ih da kliknu
        /// </summary>
        private void AnimateRemoveAdsObject()
        {
            var period = TimeSpan.FromSeconds(REMOVE_ADS_OBJECT_SCALE_DELAY);
            Observable.Interval(period)
                .Subscribe(_ => LeanTween.scale(_removeAdsButton.gameObject, Vector3.one * SCALE_FACTOR, SCALE_DURATION).setFrom(Vector3.one).setLoopPingPong(3))
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void RemoveAdsEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            InAppPurchasing.Purchase(EM_IAPConstants.Product_Remove_Ads);
        }

        private void PurchaseCompletedEventListener(CompletedArgs completedArgs)
        {
            if (completedArgs.IapProduct.Type == IAPProductType.NonConsumable && completedArgs.IapProduct.Name.Equals(EM_IAPConstants.Product_Remove_Ads))
            {
                Advertising.RemoveAds();
                gameObject.SetActive(false);
            }
        }
        
        private void PurchaseFailedEventListener(FailedArgs failedArgs)
        {
            Logging.LogError($"PurchaseFailedEventListener - product: {failedArgs.IapProduct.Name}");
            //TODO: prikazi meni koji obavestava korisnika da je doslo do greske
        }

        #endregion
    }
}
