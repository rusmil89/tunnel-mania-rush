﻿using System;
using System.Linq;
using Core.Events;
using TMPro;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/Panel/TopButtons/EnergyHolder
    /// DESCRIPTION: Klasa koja kontrolise strukturu i ponasanje game objekta koji prikazuje energiju
    /// </summary>
    public sealed class EnergyInfo : CurrencyInfo
    {
        #region Private Fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Space]
        [Header("UI")]
        [SerializeField] private GameObject _fullEnergyImage;
        [SerializeField] private GameObject _mediumEnergyImage;
        [SerializeField] private GameObject _emptyEnergyImage;
        [SerializeField] private Image _energyFillImage;
        [SerializeField] private TMP_Text _currentEnergyText;
        [SerializeField] private TMP_Text _timerText;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private GameObject _indicator;
        private readonly CompositeDisposable _timerDisposable = new CompositeDisposable();
        private readonly CurrenciesAddingToTopMenuAnimation _energiesAddingToTopMenuAnimation = new CurrenciesAddingToTopMenuAnimation
        {
            CurrencyType = Constants.CurrencyType.Energy
        };
        
        #endregion





        #region Properties

        private IObservable<int> CreateCountDownObservable(int countTimeInSeconds)
        {
            return Observable
                .Timer(TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(1))
                .Select(x => (int)(countTimeInSeconds - x))
                .TakeWhile(x => x > 0);
        }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            PlayerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var allDescendants = gameObject.Descendants();
            _energyFillImage = allDescendants.First(x => x.name.Equals("Fill")).GetComponent<Image>();
            _fullEnergyImage = allDescendants.First(x => x.name.Equals("FullEnergyImage")).gameObject;
            _mediumEnergyImage = allDescendants.First(x => x.name.Equals("MediumEnergyImage")).gameObject;
            _emptyEnergyImage = allDescendants.First(x => x.name.Equals("EmptyEnergyImage")).gameObject;
            _currentEnergyText = allDescendants.First(x => x.name.Equals("CurrentEnergyText")).GetComponent<TMP_Text>();
            _timerText = allDescendants.First(x => x.name.Equals("EnergyTimerText")).GetComponent<TMP_Text>();
            PoofVFXGameObject = gameObject.AfterSelf().First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        protected override void Start()
        {
            SetInitialValues();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            StartEnergyRefillProcess();
        }

        protected override void OnDestroy()
        {
            _timerDisposable.Dispose();
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            CurrencyButton = gameObject.GetComponent<Button>();
            ButtonAnimator = CurrencyButton.GetComponent<Animator>();
            PoofVfxParticleSystem = PoofVFXGameObject.GetComponent<ParticleSystem>();
            _indicator = gameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.INDICATOR)).gameObject;
        }

        private void RegisterButtonsListeners()
        {
            CurrencyButton.OnClickAsObservable()
                .Subscribe(HandleEnergyRefillButtonEvent)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            PlayerData.PersistentData.NumberOfMenuStarts
                .TakeWhile(numberOfMenuStarts => numberOfMenuStarts == 1)
                .Subscribe(_ => PlayerData.PersistentData.CurrentEnergy.Value = RemoteConfigClient.Instance.Energy)
                .AddTo(this);
            PlayerData.PersistentData.CurrentEnergy
                .SubscribeToText(_currentEnergyText)
                .AddTo(this);
            PlayerData.PersistentData.CurrentEnergy
                .Subscribe(currentEnergyAmount =>
                    {
                        SetIndicator(currentEnergyAmount < Constants.Energy.ENERGY_AMOUNT_FOR_PLAY);
                        AdjustBatteryLevel(currentEnergyAmount);
                        AdjustEnergyFillImage(currentEnergyAmount);
                    })
                .AddTo(this);
            
            // Animiramo objekat samo kada se energija dopuni
            PlayerData.PersistentData.CurrentEnergy
                .Zip(PlayerData.PersistentData.CurrentEnergy.Skip(1), (previous, current) => new {previous, current})
                .Where(energy => energy.current > energy.previous)
                .Throttle(TimeSpan.FromMilliseconds(250f))
                .Subscribe(energy =>
                    {
                        if (energy.current >= Constants.Energy.MAX_ENERGY)
                        {
                            _timerText.gameObject.SetActive(false);
                            _timerDisposable.Dispose();
                        }
                    })
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingToTopMenu)
                .Subscribe(HandleEnergiesStartedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingToTopMenu)
                .Subscribe(HandleEnergiesFinishedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingFromTopMenu)
                .Subscribe(HandleEnergiesStartedMovingFromTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingFromTopMenu)
                .Subscribe(HandleEnergiesFinishedMovingFromTopMenu)
                .AddTo(this);
        }

        private void StartEnergyRefillProcess()
        {
            DateTime currentTime = DateTime.Now;
            if (IsTimerAvailable(currentTime))
            {
                _timerText.gameObject.SetActive(true);
                int seconds = SecondsToNextRefill(currentTime);
                StartTimer(seconds);
                int offlineTime = (int)(currentTime - PlayerData.PersistentData.TimeSinceLastRefill.Value).TotalSeconds;
                int amountOfEnergy = offlineTime / Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY;
                if (amountOfEnergy > 0)
                {
                    RefillEnergy(amountOfEnergy);
                    SetRefillTime(currentTime);
                }
            }
            else
            {
                _timerText.gameObject.SetActive(false);
                if (PlayerData.PersistentData.CurrentEnergy.Value < Constants.Energy.MAX_ENERGY)
                {
                    RefillEnergy(Constants.Energy.MAX_ENERGY - PlayerData.PersistentData.CurrentEnergy.Value);
                }
                SetRefillTime(currentTime);
            }
        }

        /// <summary>
        /// Metoda koja proverava da li su ispunjeni uslovi za pokretanje tajmera
        /// 1. uslov jeste da energija nije puna
        /// 2. uslov je da je vreme koje je igrac proveo van igre manje od vremena potrebnog za dopunu celokupne energije
        /// WHY: Zato sto samo pod ova dva uslova ima smisla pokrenuti tajmer
        /// </summary>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        private bool IsTimerAvailable(DateTime currentTime)
        {
            if (PlayerData.PersistentData.CurrentEnergy.Value >= Constants.Energy.MAX_ENERGY) return false;
            return currentTime - PlayerData.PersistentData.TimeSinceLastRefill.Value <
                   TimeSpan.FromSeconds((Constants.Energy.MAX_ENERGY - PlayerData.PersistentData.CurrentEnergy.Value) * 
                                        Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY);
        }

        /// <summary>
        /// Metoda koja izracunava broj sekundi do sledece dopune energije
        /// WHY: Zato sto je to vreme koje treba pokazati u tajmeru
        /// </summary>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        private int SecondsToNextRefill(DateTime currentTime)
        {
            return ((Constants.Energy.MAX_ENERGY - PlayerData.PersistentData.CurrentEnergy.Value) *
                    Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY
                    - (int) (currentTime - PlayerData.PersistentData.TimeSinceLastRefill.Value).TotalSeconds)
                   % Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY;
        }

        #endregion





        #region Event Listeners

        private void HandleEnergyRefillButtonEvent(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new EnergyRefillMenuCalled());
        }

        private void SetIndicator(bool value)
        {
            _indicator.SetActive(value);
        }

        private void AdjustBatteryLevel(int currentEnergyAmount)
        {
            if (currentEnergyAmount >= Constants.Energy.MAX_ENERGY)
            {
                _fullEnergyImage.SetActive(true);
                _mediumEnergyImage.SetActive(true);
                _emptyEnergyImage.SetActive(true);
            }
            else if (currentEnergyAmount is >= 10 and < Constants.Energy.MAX_ENERGY)
            {
                _fullEnergyImage.SetActive(false);
                _mediumEnergyImage.SetActive(true);
                _emptyEnergyImage.SetActive(true);
            }
            else if (currentEnergyAmount is >= Constants.Energy.ENERGY_AMOUNT_FOR_PLAY and < 10)
            {
                _fullEnergyImage.SetActive(false);
                _mediumEnergyImage.SetActive(false);
                _emptyEnergyImage.SetActive(true);
            }
            else
            {
                _fullEnergyImage.SetActive(false);
                _mediumEnergyImage.SetActive(false);
                _emptyEnergyImage.SetActive(false);
            }
        }

        private void AdjustEnergyFillImage(int currentEnergyAmount)
        {
            _energyFillImage.fillAmount = (float) currentEnergyAmount / Constants.Energy.MAX_ENERGY;
        }

        private void StartTimer(int seconds)
        {
            CreateCountDownObservable(seconds)
                .Subscribe(
                    time => _timerText.text = TimeSpan.FromSeconds(time).ToString(@"mm\:ss"),
                    () =>
                    {
                        _timerText.gameObject.SetActive(false);
                        StartEnergyRefillProcess();
                    })
                .AddTo(_timerDisposable);
        }

        /// <summary>
        /// Metoda koja dopunjuje energiju igracu
        /// WHY: Zato sto je neophodno uraditi to posle odredjenog vremena
        /// </summary>
        /// <param name="amount"></param>
        private void RefillEnergy(int amount)
        {
            PlayerData.PersistentData.CurrentEnergy.Value += amount * Constants.Energy.ENERGY_REFILL_AMOUNT;
        }
        
        /// <summary>
        /// Metoda koja postavlja trenutno vreme kao vreme kada se dopunila energija
        /// WHY: Zato sto je potrebno pratiti to vreme za buduca podesavanja tajmera
        /// </summary>
        /// <param name="currentTime"></param>
        private void SetRefillTime(DateTime currentTime)
        {
            PlayerData.PersistentData.TimeSinceLastRefill.Value = currentTime;
        }

        protected override void Animate()
        {
            ButtonAnimator.enabled = false;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, SCALE_DURATION)
                .setLoopPingPong(3)
                .setOnComplete(() => ButtonAnimator.enabled = true);
        }

        private void HandleEnergiesStartedMovingToTopMenu(CurrenciesStartedMovingToTopMenu energiesStartedMovingToTopMenu)
        {
            if (energiesStartedMovingToTopMenu.CurrencyType == Constants.CurrencyType.Energy)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                SetPoofParticlePosition();
                AnimateScaling();
                AnimateChangingCurrencyAmount(energiesStartedMovingToTopMenu.CurrenciesWon);
            }
        }

        private void SetPoofParticlePosition()
        {
            PoofVFXGameObject.transform.localPosition = transform.localPosition;
        }

        private void AnimateScaling()
        {
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, 0.05f)
                .setFrom(Vector3.one)
                .setLoopPingPong(8)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(PlayVFXEffects);
        }

        private void AnimateChangingCurrencyAmount(int amount, bool isIncreasing = true)
        {
            var currentEnergy = PlayerData.PersistentData.CurrentEnergy.Value;
            LeanTween.value(gameObject,
                value => PlayerData.PersistentData.CurrentEnergy.Value = (int) value,
                currentEnergy,
                isIncreasing ? currentEnergy + amount : currentEnergy - amount,
                0.8f);
        }

        private void PlayVFXEffects()
        {
            MessageBroker.Default.Publish(_energiesAddingToTopMenuAnimation);
            PoofVfxParticleSystem.Play();
        }

        private void HandleEnergiesFinishedMovingToTopMenu(CurrenciesFinishedMovingToTopMenu energiesFinishedMovingToTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }

        private void HandleEnergiesStartedMovingFromTopMenu(CurrenciesStartedMovingFromTopMenu currenciesStartedMovingFromTopMenu)
        {
            if (currenciesStartedMovingFromTopMenu.CurrencyType == Constants.CurrencyType.Energy)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                Animate();
                AnimateChangingCurrencyAmount(currenciesStartedMovingFromTopMenu.CurrenciesSpent, false);
            }
        }

        private void HandleEnergiesFinishedMovingFromTopMenu(CurrenciesFinishedMovingFromTopMenu energiesFinishedMovingFromTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }

        #endregion
    }
}
