using System.Linq;
using Core.Events;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: TokensInfo
    /// DESCRIPTION: Klasa koja kontrolise objekat koji pokazuje korisniku koliko ima tokena u svakom trenutku
    /// </summary>
    public sealed class TokensInfo : CurrencyInfo
    {
        #region Private Fields

        private readonly CurrenciesAddingToTopMenuAnimation _tokensAddingToTopMenuAnimation = new CurrenciesAddingToTopMenuAnimation
        {
            CurrencyType = Constants.CurrencyType.Token
        };

        #endregion
        
        
        
        

        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            PlayerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            PoofVFXGameObject = gameObject.AfterSelf().First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        protected override void Start()
        {
            SetInitialValues();
            RegisterObservableListeners();
            RegisterEventListeners();
        }

        protected override void OnDestroy()
        {
            
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            CurrencyAmountText = GetComponentInChildren<TMP_Text>();
            CurrencyButton = GetComponent<Button>();
            ButtonAnimator = GetComponent<Animator>();
            PoofVfxParticleSystem = PoofVFXGameObject.GetComponent<ParticleSystem>();
        }

        private void RegisterObservableListeners()
        {
            PlayerData.PersistentData.Tokens
                .DistinctUntilChanged()
                .SubscribeToText(CurrencyAmountText)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingToTopMenu)
                .Subscribe(ListenToTokensStartedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingToTopMenu)
                .Subscribe(ListenToTokensFinishedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingFromTopMenu)
                .Subscribe(HandleTokensStartedMovingFromTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingFromTopMenu)
                .Subscribe(HandleTokensFinishedMovingFromTopMenu)
                .AddTo(this);
        }

        private void RegisterEventListeners()
        {
            CurrencyButton.OnClickAsObservable()
                .Subscribe(_ => HandleAddTokensButton())
                .AddTo(this);
        }

        protected override void Animate()
        {
            ButtonAnimator.enabled = false;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, SCALE_DURATION)
                .setLoopPingPong(3)
                .setOnComplete(() => ButtonAnimator.enabled = true);
        }

        #endregion





        #region Event Listeners

        private void ListenToTokensStartedMovingToTopMenu(CurrenciesStartedMovingToTopMenu currenciesStartedMovingToTopMenu)
        {
            if (currenciesStartedMovingToTopMenu.CurrencyType == Constants.CurrencyType.Token)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                SetPoofParticlePosition();
                AnimateScaling();
                AnimateChangingCurrencyAmount(currenciesStartedMovingToTopMenu.CurrenciesWon);
            }
        }

        private void SetPoofParticlePosition()
        {
            PoofVFXGameObject.transform.localPosition = transform.localPosition;   
        }

        private void AnimateScaling()
        {
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, 0.05f)
                .setFrom(Vector3.one)
                .setLoopPingPong(8)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(PlayVFXEffects);
        }

        private void AnimateChangingCurrencyAmount(int amount, bool isIncreasing = true)
        {
            LeanTween.value(gameObject,
                value => PlayerData.PersistentData.Tokens.Value = (int) value,
                PlayerData.PersistentData.Tokens.Value,
                isIncreasing ? PlayerData.PersistentData.Tokens.Value + amount : PlayerData.PersistentData.Tokens.Value - amount,
                0.8f);
        }

        private void ListenToTokensFinishedMovingToTopMenu(CurrenciesFinishedMovingToTopMenu tokensFinishedMovingToTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }

        private void PlayVFXEffects()
        {
            MessageBroker.Default.Publish(_tokensAddingToTopMenuAnimation);
            PoofVfxParticleSystem.Play();
        }

        private void HandleTokensStartedMovingFromTopMenu(CurrenciesStartedMovingFromTopMenu currenciesStartedMovingFromTopMenu)
        {
            if (currenciesStartedMovingFromTopMenu.CurrencyType == Constants.CurrencyType.Token)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                Animate();
                AnimateChangingCurrencyAmount(currenciesStartedMovingFromTopMenu.CurrenciesSpent, false);
            }
        }

        private void HandleTokensFinishedMovingFromTopMenu(CurrenciesFinishedMovingFromTopMenu tokensFinishedMovingFromTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }
        
        private void HandleAddTokensButton()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new TokensMenuCalled());
        }

        #endregion
    }
}