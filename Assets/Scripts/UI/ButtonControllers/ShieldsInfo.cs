﻿using System.Linq;
using Core.Events;
using TMPro;
using Tunnel.Audio;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/TopButtons/ShieldsInfo
    /// DESCRIPTION: Klasa koja kontrolise ponasanje objekta koji sluzi za prikazivanje informacija o trenutnom broju stitova
    /// </summary>
    public sealed class ShieldsInfo : CurrencyInfo
    {
        #region Private Fields

        private readonly CurrenciesAddingToTopMenuAnimation _shieldsAddingToTopMenuAnimation = new CurrenciesAddingToTopMenuAnimation
        {
            CurrencyType = Constants.CurrencyType.Shield
        };

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            PlayerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            PoofVFXGameObject = gameObject.AfterSelf().First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        protected override void Start()
        {
            SetInitialValues();
            RegisterObservableListeners();
            RegisterButtonListeners();
        }

        protected override void OnDestroy()
        {
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            CurrencyAmountText = GetComponentInChildren<TMP_Text>();
            CurrencyButton = GetComponent<Button>();
            ButtonAnimator = GetComponent<Animator>();
            PoofVfxParticleSystem = PoofVFXGameObject.GetComponent<ParticleSystem>();
        }

        private void RegisterObservableListeners()
        {
            PlayerData.PersistentData.Shields
                .DistinctUntilChanged()
                .SubscribeToText(CurrencyAmountText)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingToTopMenu)
                .Subscribe(ListenToShieldsStartedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingToTopMenu)
                .Subscribe(ListenToShieldsFinishedMovingToTopMenu)
                .AddTo(this);
        }

        private void RegisterButtonListeners()
        {
            CurrencyButton.OnClickAsObservable()
                .Subscribe(_ => ShieldsRefillButtonEventListener())
                .AddTo(this);
        }

        /// <summary>
        /// Metoda koja animira objekte u UI koji prikazuju vrednosti nekih elemenata koji mogu da se dopune za npr RV (trenutno su to stitovi i energija)
        /// WHY: Zato sto za sve te objekte koristimo istu animaciju i na ovaj nacin postujemo DRY princip
        /// </summary>
        protected override void Animate()
        {
            ButtonAnimator.enabled = false;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, SCALE_DURATION)
                .setLoopPingPong(3)
                .setOnComplete(() => ButtonAnimator.enabled = true);
        }

        #endregion





        #region Event Listeners

        private void ListenToShieldsStartedMovingToTopMenu(CurrenciesStartedMovingToTopMenu shieldsStartedMovingToTopMenu)
        {
            if (shieldsStartedMovingToTopMenu.CurrencyType == Constants.CurrencyType.Shield)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                AnimateReceivingShields();
                AnimateIncreasingCurrencyValue(shieldsStartedMovingToTopMenu.CurrenciesWon);
            }
        }

        private void AnimateReceivingShields()
        {
            PoofVFXGameObject.transform.localPosition = transform.localPosition;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, 0.05f)
                .setFrom(Vector3.one)
                .setLoopPingPong(8)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(PlayVFXEffects);
        }

        private void AnimateIncreasingCurrencyValue(int shieldsBought)
        {
            LeanTween.value(gameObject,
                value => PlayerData.PersistentData.Shields.Value = (int) value,
                PlayerData.PersistentData.Shields.Value,
                PlayerData.PersistentData.Shields.Value + shieldsBought,
                0.8f);
        }

        private void ListenToShieldsFinishedMovingToTopMenu(CurrenciesFinishedMovingToTopMenu shieldsFinishedMovingToTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }

        private void PlayVFXEffects()
        {
            MessageBroker.Default.Publish(_shieldsAddingToTopMenuAnimation);
            PoofVfxParticleSystem.Play();
        }

        private void ShieldsRefillButtonEventListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new GetShieldsMenuCalled());
        }

        #endregion
    }
}
