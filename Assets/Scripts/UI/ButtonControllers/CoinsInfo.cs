﻿using System;
using System.Linq;
using Core.Events;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: CoinsInfo
    /// DESCRIPTION: Klasa koja kontrolise objekat koji pokazuje korisniku koliko ima koina u svakom trenutku
    /// </summary>
    public sealed class CoinsInfo : CurrencyInfo
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private GameObject _indicator;
        private readonly CurrenciesAddingToTopMenuAnimation _currenciesAddingToTopMenuAnimation = new CurrenciesAddingToTopMenuAnimation
        {
            CurrencyType = Constants.CurrencyType.Coin
        };

        #endregion
        
        
        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            PlayerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            PoofVFXGameObject = gameObject.AfterSelf().First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif
        
        protected override void Start()
        {
            SetInitialValues();
            RegisterObservableListeners();
            RegisterButtonsListeners();
        }

        protected override void OnDestroy()
        {
            
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            CurrencyAmountText = GetComponentInChildren<TMP_Text>();
            CurrencyButton = GetComponent<Button>();
            ButtonAnimator = GetComponent<Animator>();
            PoofVfxParticleSystem = PoofVFXGameObject.GetComponent<ParticleSystem>();
            _indicator = gameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.INDICATOR)).gameObject;
        }

        private void RegisterObservableListeners()
        {
            PlayerData.PersistentData.Coins
                .DistinctUntilChanged()
                .SubscribeToText(CurrencyAmountText)
                .AddTo(this);
            PlayerData.PersistentData.BuyCoinsForAdsDailyAttempts
                .Where(_ => SceneManager.GetActiveScene().name.Equals(Constants.Scenes.MENU_SCENE_NAME))
                .Subscribe(buyCoinsForAdsDailyAttempts => SetIndicatorActive(buyCoinsForAdsDailyAttempts > 0))
                .AddTo(this);
            PlayerData.PersistentData.BuyCoinsForAdsLastAttemptDay
                .SkipWhile(_ => DateTime.Today.Subtract(PlayerData.PersistentData.DataSaveTime.Value).Days == 0)
                .Where(lastAttemptDay => DateTime.Today.Subtract(lastAttemptDay).Days >= 1)
                .Subscribe(_ => PlayerData.PersistentData.BuyCoinsForAdsDailyAttempts.Value = 3)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesStartedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesStartedMovingToTopMenu)
                .Subscribe(ListenToCoinsStartedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesFinishedMovingToTopMenu)
                .Select(eventType => eventType as CurrenciesFinishedMovingToTopMenu)
                .Subscribe(ListenToCoinsFinishedMovingToTopMenu)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesAnimationStarted)
                .Select(eventType => eventType as CurrenciesAnimationStarted)
                .Subscribe(HandleCurrenciesAnimationsStarted)
                .AddTo(this);
        }

        private void RegisterButtonsListeners()
        {
            CurrencyButton.OnClickAsObservable()
                .Subscribe(_ => AddCoinsButtonListener())
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void AddCoinsButtonListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ShopMenuCalled());
        }

        protected override void Animate()
        {
            ButtonAnimator.enabled = false;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, SCALE_DURATION)
                .setLoopPingPong(3)
                .setOnComplete(() => ButtonAnimator.enabled = true);
        }

        private void SetIndicatorActive(bool isActive)
        {
            _indicator.SetActive(isActive);
        }

        private void ListenToCoinsStartedMovingToTopMenu(CurrenciesStartedMovingToTopMenu currenciesStartedMovingToTopMenu)
        {
            if (currenciesStartedMovingToTopMenu.CurrencyType == Constants.CurrencyType.Coin)
            {
                ButtonAnimator.enabled = false;
                CurrencyButton.interactable = false;
                AnimateReceivingCoins();
                AnimateChangingCurrencyAmount(currenciesStartedMovingToTopMenu.CurrenciesWon);
            }
        }

        private void AnimateReceivingCoins()
        {
            PoofVFXGameObject.transform.localPosition = transform.localPosition;
            LeanTween.scale(gameObject, Vector3.one * SCALE_FACTOR, 0.05f)
                .setFrom(Vector3.one)
                .setLoopPingPong(8)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(PlayVFXEffects);
        }

        private void AnimateChangingCurrencyAmount(int amount, bool isIncreasing = true)
        {
            LeanTween.value(gameObject,
                value => PlayerData.PersistentData.Coins.Value = (int) value,
                PlayerData.PersistentData.Coins.Value,
                isIncreasing ? PlayerData.PersistentData.Coins.Value + amount : PlayerData.PersistentData.Coins.Value - amount,
                0.8f);
        }

        private void ListenToCoinsFinishedMovingToTopMenu(CurrenciesFinishedMovingToTopMenu currenciesFinishedMovingToTopMenu)
        {
            ButtonAnimator.enabled = true;
            CurrencyButton.interactable = true;
        }

        private void HandleCurrenciesAnimationsStarted(CurrenciesAnimationStarted currenciesAnimationStarted)
        {
            AnimateChangingCurrencyAmount(currenciesAnimationStarted.CurrenciesSpent, false);
        }

        private void PlayVFXEffects()
        {
            MessageBroker.Default.Publish(_currenciesAddingToTopMenuAnimation);
            PoofVfxParticleSystem.Play();
        }

        #endregion
    }
}
