﻿using System.Linq;
using Core.Events;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/TopButtons/SettingsButton
    /// DESCRIPTION: Klasa koja kontrolise ponasanje dugmeta settings u menu sceni
    /// </summary>
    public sealed class SettingsButton : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _settingsButton;
        private GameObject _indicator;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetInitialValues();
            RegisterEventListeners();
        }

        private void SetInitialValues()
        {
            _settingsButton = GetComponent<Button>();
            _indicator = gameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.INDICATOR)).gameObject;
        }

        private void RegisterEventListeners()
        {
            _settingsButton.OnClickAsObservable()
                .Subscribe(SettingsButtonEventHandler)
                .AddTo(this);
            _playerData.PersistentData.Coins
                .Select(_ => !_playerData.PersistentData.IsFacebookLiked.Value ||
                             !_playerData.PersistentData.IsTwitterFollowed.Value ||
                             !_playerData.PersistentData.IsInstagramFollowed.Value ||
                             !_playerData.PersistentData.IsDiscordJoined.Value)
                .Subscribe(SetIndicatorActive)
                .AddTo(this);
            _playerData.PersistentData.Tokens
                .Select(_ => !_playerData.PersistentData.IsApplicationRated.Value ||
                             !_playerData.PersistentData.IsFeedbackSubmitted.Value)
                .Subscribe(SetIndicatorActive)
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void SettingsButtonEventHandler(Unit obj)
        { 
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new SettingsMenuCalled());
        }

        private void SetIndicatorActive(bool isActive)
        {
            _indicator.SetActive(isActive);
        }

        #endregion
    }
}
