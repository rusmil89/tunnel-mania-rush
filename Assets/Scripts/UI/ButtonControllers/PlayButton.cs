﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using RSG;
using TMPro;
using Tunnel.Animations;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home/PlayButton
    /// DESCRIPTION: Klasa koja kontrolise ponasanje dugmeta Play u home meniju
    /// </summary>
    public sealed class PlayButton : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Button _playButton;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _topMenuEnergyPosition;
        [SerializeField] private RectTransform _energy;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;
        [SerializeField] private GameObject _poofVFXGameObject;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencySpentAnimations _energiesSpentAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private List<RectTransform> _energies;
        private TMP_Text _currencyAmountText;
        private ParticleSystem _poofVfxParticleSystem;
        private int _neededAmountOfEnergyForLevel;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Energy;

        public int CurrencyAmountEarned => default;

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms => _energies;

        public RectTransform CurrencyAnimationStartObject => _topMenuEnergyPosition;

        public RectTransform CurrencyAnimationEndObject => _playButton.GetComponent<RectTransform>();

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;
        

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _energy = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Energy"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            var descendantsEnumerable = gameObject.Parent().Parent().Descendants();
            _animationsElements = descendantsEnumerable.First(x => x.name.Contains("AnimationsElements"));
            _topMenuEnergyPosition = descendantsEnumerable.First(x => x.name.Contains("EnergyPosition")).GetComponent<RectTransform>();
            _poofVFXGameObject = descendantsEnumerable.First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                .Enter(HandleEnteringInitializationState)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                .Enter(HandleEnteringAnimationsPlayingState)
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState(Constants.MenusStatesTypes.Normal normal)
        {
            SetInitialValues();
            RegisterObservableListeners();
            InstantiateEnergyObjects();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void SetInitialValues()
        {
            _playButton = GetComponent<Button>();
            _energies = new List<RectTransform>();
            _currencyMenuLogic = new CurrencyMenuLogic();
            _energiesSpentAnimations = new CurrencySpentAnimations(this);
            _poofVfxParticleSystem = _poofVFXGameObject.GetComponent<ParticleSystem>();
            _neededAmountOfEnergyForLevel = Constants.Energy.ENERGY_AMOUNT_FOR_PLAY;
        }

        private void RegisterObservableListeners()
        {
            _playButton.OnClickAsObservable()
                .Subscribe(ListenPlayButtonClick)
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesRemovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesRemovingFromTopMenu)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Energy)
                .Subscribe(HandleCurrencyRemovingFromTopMenu)
                .AddTo(this);
        }

        private void InstantiateEnergyObjects()
        {
            if (_energies == null || _energies.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_energy, _animationsElements)
                    .Subscribe(energies => _energies = energies)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringAnimationsPlayingState(Constants.MenusStatesTypes.Normal normal)
        {
            Observable.FromCoroutine(PrepareAnimation)
                .SelectMany(_energiesSpentAnimations.Animate)
                .Delay(TimeSpan.FromSeconds(0.5d))
                .Subscribe(HandleFinishingAnimation)
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.CurrentEnergy), _neededAmountOfEnergyForLevel);
#endif
        }
        
        private IEnumerator PrepareAnimation()
        {
            CurrencyAmountSpent = _neededAmountOfEnergyForLevel;
            SetButtonsActivity(false);
            yield return null;
        }

        private void SetButtonsActivity(bool value)
        {
            _playButton.interactable = value;
            _playButton.GetComponent<Animator>().enabled = value;
        }

        private void HandleFinishingAnimation(Unit unit)
        {
            SetButtonsActivity(true);
            MessageBroker.Default.Publish(new ShouldLoadLevelScene());
        }

        #endregion





        #region Event Listeners

        private void ListenPlayButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playButton.interactable = false;
            if (_playerData.PersistentData.CurrentEnergy.Value - Constants.Energy.ENERGY_AMOUNT_FOR_PLAY >= 0)
            {
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new EnergyRefillMenuCalled());
            }
        }

        private void HandleCurrencyRemovingFromTopMenu(object o)
        {
            _poofVfxParticleSystem.Play();
        }

        #endregion
    }
}
