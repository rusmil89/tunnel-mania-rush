﻿using System;
using Core.Events;
using Tunnel.UI;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New_Menu", menuName = "ScriptableObjects/Menus/New_Menu", order = 1)]
public class MenuSO : ScriptableObject
{
    #region Private Fields

    /// <summary>
    /// Non-Serializable fields
    /// </summary>
    private MenuIsShown _menuIsShown = new MenuIsShown();
    private MenuIsHidden _menuIsHidden = new MenuIsHidden();

    #endregion
    
    
    
    
    
    #region Properties

    public Menu Menu { get; set; }
    public bool IsMenuShown { get; protected set; }

    #endregion





    #region Monobehaviour Events

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManagerOnSceneLoaded;
    }

    #endregion





    #region Custom Methods

    public virtual void Show()
    {
        IsMenuShown = true;
        MessageBroker.Default.Publish(_menuIsShown);
        Application.targetFrameRate = 30;
        Menu.MenuCanvas.enabled = true;
        Menu.MenuGraphicRaycaster.enabled = true;
        Menu.PanelGameObject.SetActive(true);
        LeanTween.scale(Menu.PanelGameObject, Vector3.one, Menu.MENU_ANIMATION_DURATION)
            .setFrom(Vector3.zero);
    }

    public virtual void Hide()
    {
        IsMenuShown = false;
        MessageBroker.Default.Publish(_menuIsHidden);
        Application.targetFrameRate = 60;
        if (!Menu.PanelGameObject.activeInHierarchy) return;
        Menu.MenuCanvas.enabled = true;
        Menu.MenuGraphicRaycaster.enabled = true;
        LeanTween.scale(Menu.PanelGameObject, Vector3.zero, Menu.MENU_ANIMATION_DURATION)
            .setFrom(Vector3.one)
            .setOnComplete(() => Menu.PanelGameObject.SetActive(false));
    }

    #endregion





    #region Event Listeners

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        IsMenuShown = false;
    }

    #endregion
}
