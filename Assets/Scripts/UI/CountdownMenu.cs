﻿using System.Linq;
using Core.Events;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using UnityEditor;
using UnityEngine;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: CountdownMenu/Panel
    /// DESCRIPTION: Klasa koja kontrolise meni koji prikazuje korisniku odbrojavanje 3,2,1 posle pauze, kako bi se korisnik spremio za pocetak
    /// </summary>
    public class CountdownMenu : MonoBehaviour
    {
        #region Private Fields

        private const int INITIAL_COUNTDOWN_NUMBER = 3;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private RectTransform _numberTextRectTransform;
        [SerializeField] private GameData _gameData;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private TMP_Text _numberText;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _numberTextRectTransform = transform.GetChild(0).GetChild(0).GetComponent<RectTransform>();
            _gameData = AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void OnEnable()
        {
            SetInitialValues();
            ScaleDownLeanTween();
        }

        private void SetInitialValues()
        {
            _numberText = _numberTextRectTransform.GetComponent<TMP_Text>();
        }

        private void ScaleDownLeanTween()
        {
            MessageBroker.Default.Publish(new CountdownFired());
            int countdownNumber = INITIAL_COUNTDOWN_NUMBER;
            
            MessageBroker.Default.Publish(new CountdownFired());
            _numberText.text = countdownNumber.ToString();
            
            LeanTween.scale(_numberTextRectTransform, Vector3.zero, 1f)
                .setRepeat(countdownNumber)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(() =>
                {
                    if (countdownNumber > 1)
                    {
                        MessageBroker.Default.Publish(new CountdownFired());
                        countdownNumber--;
                        _numberText.text = countdownNumber.ToString();
                        return;
                    }

                    _numberTextRectTransform.localScale = Vector3.one;
                    _numberText.text = "go";
                    MessageBroker.Default.Publish(new CountdownGoFired());
                    
                    LeanTween.delayedCall(1f, () =>
                    {
                        MessageBroker.Default.Publish(new CountdownFinished());
                    });
                });
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<CountdownMenu>
        {
            
        }

        #endregion
    }
}