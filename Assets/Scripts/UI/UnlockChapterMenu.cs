﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using TMPro;
using Tunnel.Ads;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    public sealed class ChapterUnlockedWithToken {}

    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Unlock_Menu
    /// DESCRIPTION: Klasa koja kontroliše ponašanje Unlock menija
    /// </summary>
    public class UnlockChapterMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Game Objects")]
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _freeButton;
        [SerializeField] private Button _tokenButton;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _freeButtonText;
        [SerializeField] private TMP_Text _tokensText;

        [Header("SO")] 
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private RectTransform _token;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;
        [SerializeField] private GameObject _poofVFXGameObject;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencySpentAnimations _tokensSpentAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private List<RectTransform> _tokens;
        private TMP_Text _currencyAmountText;
        private ParticleSystem _poofVfxParticleSystem;
        private int _unlockChapterCostRemoteValue;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Token;

        public int CurrencyAmountEarned => default;

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms => _tokens;

        public RectTransform CurrencyAnimationStartObject => _topMenuTokensPosition;

        public RectTransform CurrencyAnimationEndObject => _tokenButton.GetComponent<RectTransform>();

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;
        

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            const string freeButton = "FreeButton";
            const string tokenButton = "TokenButton";
            const string tokenButtonText = "TokenButtonText";

            var descendantsEnumerable = gameObject.Descendants();
            var allButtons = descendantsEnumerable.OfComponent<Button>();
            _freeButton = allButtons.First(x => x.name.Contains(freeButton));
            _tokenButton = allButtons.First(x => x.name.Contains(tokenButton));
            _tokensText = descendantsEnumerable.First(x => x.name.Contains(tokenButtonText)).GetComponent<TMP_Text>();
            _title = descendantsEnumerable.First(x => x.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _description = descendantsEnumerable.First(x => x.name.Contains("DescriptionText")).GetComponent<TMP_Text>();
            _freeButtonText = descendantsEnumerable.First(x => x.name.Contains("FreeButtonText")).GetComponent<TMP_Text>();
            _exitButton = descendantsEnumerable.First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = descendantsEnumerable.First(x => x.name.Contains("AnimationsElements"));
            _topMenuTokensPosition = descendantsEnumerable.First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _poofVFXGameObject = descendantsEnumerable.First(x => x.name.Contains("CFX_Poof"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        // Start is called before the first frame update
        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                .Enter(_ => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                .Enter(_ => HandleEnteringAnimationsPlayingState())
                .End()
                .Build();
        }

        #endregion





        #region Custom Methods

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonListeners();
            RegisterObservableListeners();
            InstantiateTokens();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void SetInitialValues()
        {
            _tokens = new List<RectTransform>();
            _currencyMenuLogic = new CurrencyMenuLogic();
            _tokensSpentAnimations = new CurrencySpentAnimations(this);
            _poofVfxParticleSystem = _poofVFXGameObject.GetComponent<ParticleSystem>();
            _unlockChapterCostRemoteValue = RemoteConfigClient.Instance.UnlockChapterCost;
            _tokensText.text = _unlockChapterCostRemoteValue.ToString();
        }

        private void SetLocalization()
        {
            var currentLanguage = _languageClient.CurrentLanguage;
            _title.SetText(currentLanguage.Unlock.Value);
            _description.SetText(currentLanguage.UnlockNewChapter.Value);
            _freeButtonText.SetText(currentLanguage.Free.Value);
        }

        private void RegisterButtonListeners()
        {
            _freeButton.OnClickAsObservable()
                .Subscribe(HandleFreeButtonClick)
                .AddTo(this);
            _tokenButton.OnClickAsObservable()
                .Subscribe(HandleTokenButtonClick)
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(HandleExitButtonClick)
                .AddTo(this);
        }
        
        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesRemovingFromTopMenu)
                .Select(eventType => eventType as CurrenciesRemovingFromTopMenu)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Token)
                .Subscribe(HandleCurrencyRemovingFromTopMenu)
                .AddTo(this);
        }

        private void InstantiateTokens()
        {
            if (_tokens == null || _tokens.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(tokens => _tokens = tokens)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            Observable.FromCoroutine(PrepareAnimation)
                .SelectMany(_tokensSpentAnimations.Animate)
                .Subscribe(HandleFinishingAnimation)
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.Tokens), _unlockChapterCostRemoteValue);
#endif
        }

        private IEnumerator PrepareAnimation()
        {
            CurrencyAmountSpent = _unlockChapterCostRemoteValue;
            SetButtonsActivity(false);
            yield return null;
        }

        private void SetButtonsActivity(bool value)
        {
            _tokenButton.GetComponent<Animator>().enabled = value;
            _exitButton.interactable = value;
            _freeButton.interactable = value;
            _tokenButton.interactable = value;
        }

        private void HandleFinishingAnimation(Unit unit)
        {
            SetButtonsActivity(true);
            MessageBroker.Default.Publish(new ChapterUnlockedWithToken());
        }

        #endregion





        #region Event Listeners

        private void HandleFreeButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            AdsClient.Instance.ShowRewardedVideo(AdPlacement.PlacementWithName(Constants.AdsPlacements.UNLOCK_CHAPTER));
        }

        private void HandleTokenButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (_playerData.PersistentData.Tokens.Value >= _unlockChapterCostRemoteValue)
            {
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new TokensMenuCalled());
            }
        }

        private void HandleExitButtonClick(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        //NOTE: obratiti paznju da li moze ovo nekako elegantnije da se resi
        private void HandleCurrencyRemovingFromTopMenu(object eventType)
        {
            _poofVfxParticleSystem.Play();
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<UnlockChapterMenu>
        {
            
        }

        #endregion
    }
}
