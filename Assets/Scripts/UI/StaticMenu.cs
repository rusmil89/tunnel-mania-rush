﻿using UnityEngine;

namespace Tunnel.UI
{
    public class StaticMenu : MonoBehaviour
    {
        public StaticMenuSO staticMenuSo;

        protected void Awake()
        {
            staticMenuSo.staticMenu = this;
        }
    }
}