﻿using TMPro;
using UnityEngine;

namespace Tunnel.UI
{
    public class Value : MonoBehaviour
    {
        public ValueSO metersSO;
        private TMP_Text metersText;
    
        void Awake()
        {
            metersText = GetComponent<TMP_Text>();
        }

        void Update()
        {
            metersText.text = metersSO.value.ToString("F");
        }
    }
}