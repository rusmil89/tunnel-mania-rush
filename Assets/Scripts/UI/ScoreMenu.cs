﻿using System.Linq;
using Core.Events;
using Localization;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: ScoreMenu
    /// DESCRIPTION: Klasa koja kontrolise score menu koji se pojavljuje posle game over menija kada igrac vise ne zeli da nastavi dalje
    /// </summary>
    public sealed class ScoreMenu : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;

        [Header("UI")] 
        [SerializeField] private Button _continueButton;
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _chapterNumberText;
        [SerializeField] private TMP_Text _levelText;
        [SerializeField] private TMP_Text _chapterText;
        [SerializeField] private TMP_Text _continueText;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            _continueButton = gameObject.Descendants().OfComponent<Button>().First(s => s.name.Contains("Continue"));
            var allTextGameObjects = gameObject.Descendants().OfComponent<TMP_Text>();
            _titleText = allTextGameObjects.First(s => s.name.Contains("Title"));
            _chapterNumberText = allTextGameObjects.First(s => s.name.Contains("ChapterNumber"));
            _levelText = allTextGameObjects.First(s => s.name.Contains("LevelNumber"));
            _continueText = allTextGameObjects.First(s => s.name.Contains("ButtonText"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterObservableListeners();
            RegisterButtonListeners();
        }

        private void SetLocalization()
        {
            _titleText.SetText(_languageClient.CurrentLanguage.Score.Value);
            _chapterText.SetText(_languageClient.CurrentLanguage.Chapter.Value);
            _continueText.SetText(_languageClient.CurrentLanguage.Continue.Value);
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<ScoreMenuCalled>()
                .Subscribe(SetValues)
                .AddTo(this);
        }

        private void SetValues(ScoreMenuCalled scoreMenuCalled)
        {
            if (_playerData.PersistentData.BestScoreChapter.Value < _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value])
            {
                _playerData.PersistentData.BestScoreChapter.Value = _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value];
                _playerData.PersistentData.BestScoreLevel.Value = 1;
            }
            else if (_playerData.PersistentData.BestScoreLevel.Value < _playerData.PersistentData.CurrentLevel.Value)
            {
                _playerData.PersistentData.BestScoreLevel.Value = _playerData.PersistentData.CurrentLevel.Value;
            }

            _chapterNumberText.text = _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value].ToString();
            _levelText.text = _playerData.PersistentData.CurrentLevel.Value.ToString();
        }
        
        private void RegisterButtonListeners()
        {
            _continueButton.OnClickAsObservable()
                .Subscribe(_ => ContinueButtonListener())
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private static void ContinueButtonListener()
        {
            MessageBroker.Default.Publish(new ShouldLoadMainMenuScene());
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<ScoreMenu>
        {
            
        }

        #endregion
    }
}
