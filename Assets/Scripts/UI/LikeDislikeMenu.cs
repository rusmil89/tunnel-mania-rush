﻿using System.Linq;
using Core.Events;
using Localization;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME OBJECT: LikeDislikeMenu
    /// DESCRIPTION: Klasa koja kontrolise ponasanje menija koji pita korisnika da li mu se igra svidja ili ne
    /// </summary>
    public sealed class LikeDislikeMenu : MonoBehaviour
    {
        #region private fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Buttons")] 
        [SerializeField] private Button _noButton;
        [SerializeField] private Button _yesButton;

        [Header("Localization")] 
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _yesButtonText;
        [SerializeField] private TMP_Text _noButtonText;

        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Transform _panel;
        private LanguageClient _languageClient;

        #endregion

        
        
        
        
        #region Monobehaviour functions

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            var allButtons = gameObject.Descendants().OfComponent<Button>();
            _noButton = allButtons.First(x => x.name.Contains("NoButton"));
            _yesButton = allButtons.First(x => x.name.Contains("YesButton"));
            var allTexts = gameObject.Descendants().OfComponent<TMP_Text>();
            _title = allTexts.First(x => x.name.Contains("TitleText"));
            _description = allTexts.First(x => x.name.Contains("Description"));
            _noButtonText = allTexts.First(x => x.name.Contains("NoButtonText"));
            _yesButtonText = allTexts.First(x => x.name.Contains("YesButtonText"));
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetInitialValues();
            SetLocalization();
            OnMenuShowed();
            RegisterButtonsListeners();
            RegisterObservableListeners();
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.LikeTheGameTitle.Value);
            _description.SetText(_languageClient.CurrentLanguage.LikeTheGameDescription.Value);
            _yesButtonText.SetText(_languageClient.CurrentLanguage.Yes.Value);
            _noButtonText.SetText(_languageClient.CurrentLanguage.No.Value);
        }

        private void RegisterButtonsListeners()
        {
            _noButton.OnClickAsObservable()
                .Subscribe(_ => NoButtonListener())
                .AddTo(this);
            _yesButton.OnClickAsObservable()
                .Subscribe(_ => YesButtonListener())
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => OnMenuShowed())
                .AddTo(this);
        }

        #endregion

        
        
        
        
        #region Event Listeners

        private void OnMenuShowed()
        {
            MessageBroker.Default.Publish(new LikeDislikeMenuOpened());
        }

        private void NoButtonListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.SetLikeDislikeStatus(2);
            MessageBroker.Default.Publish(new LikeMenuNoButtonClicked());
        }

        private void YesButtonListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.SetLikeDislikeStatus(1);
            MessageBroker.Default.Publish(new LikeMenuYesButtonClicked());
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            MessageBroker.Default.Publish(new HomeAndTopButtonsMenuCalled());
#endif
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<LikeDislikeMenu>
        {
            
        }

        #endregion
    }
}