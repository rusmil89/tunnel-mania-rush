﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tunnel.Animations;
using Core.Events;
using Localization;
using RSG;
using TMPro;
using Tunnel.Audio;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu/Level
    /// GAME_OBJECT: GetShieldsMenu
    /// DESCRIPTION: Klasa koja kontroliše meni za dopunjavanje štitova kada igrač ostane bez istih
    /// </summary>
    public sealed class GetShieldsMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Objects")]
        [SerializeField] private Transform _panel;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _shieldsPack01Button;
        [SerializeField] private Button _shieldsPack02Button;
        [SerializeField] private Button _shieldsPack03Button;
        [SerializeField] private TMP_Text _shieldsPack01CostText;
        [SerializeField] private TMP_Text _shieldsPack02CostText;
        [SerializeField] private TMP_Text _shieldsPack03CostText;
        [SerializeField] private TMP_Text _shieldsPack01RewardText;
        [SerializeField] private TMP_Text _shieldsPack02RewardText;
        [SerializeField] private TMP_Text _shieldsPack03RewardText;
        [SerializeField] private TMP_Text _title;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _shield;
        [SerializeField] private RectTransform _topMenuShieldsPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencyBoughtAnimations _shieldsMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Shield;

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject => _topMenuShieldsPosition;

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion




        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var shieldsPackObjects = gameObject.Descendants().Where(x => x.name.Contains("ShieldsPack")).ToArray();
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            _title = gameObject.Descendants().First(x => x.name.Contains("TitleText")).GetComponent<TMP_Text>();
            List<Button> getButtons = shieldsPackObjects.Select(energyPackGameObject => energyPackGameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.REFILL_BUTTON)).GetComponent<Button>()).ToList();
            List<TMP_Text> shieldsCostText = shieldsPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("RefillButtonText")).GetComponent<TMP_Text>()).ToList();
            List<TMP_Text> shieldsRewardText = shieldsPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("AmountText")).GetComponent<TMP_Text>()).ToList();
            _shieldsPack01Button = getButtons[0];
            _shieldsPack02Button = getButtons[1];
            _shieldsPack03Button = getButtons[2];
            _shieldsPack01CostText = shieldsCostText[0];
            _shieldsPack02CostText = shieldsCostText[1];
            _shieldsPack03CostText = shieldsCostText[2];
            _shieldsPack01RewardText = shieldsRewardText[0];
            _shieldsPack02RewardText = shieldsRewardText[1];
            _shieldsPack03RewardText = shieldsRewardText[2];
            _shield = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Shield"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuShieldsPosition = gameObject.Descendants().First(x => x.name.Contains("ShieldsPosition")).GetComponent<RectTransform>();
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            InstantiateShields();
            Observable.Timer(TimeSpan.FromMilliseconds(500))
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
        }

        private void SetInitialValues()
        {
            _shieldsMenuAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            CurrenciesTransforms = new List<RectTransform>();
            _shieldsPack01CostText.text = RemoteConfigClient.Instance.ShieldsPack01Cost.ToString();
            _shieldsPack02CostText.text = RemoteConfigClient.Instance.ShieldsPack02Cost.ToString();
            _shieldsPack03CostText.text = RemoteConfigClient.Instance.ShieldsPack03Cost.ToString();
            _shieldsPack01RewardText.text = RemoteConfigClient.Instance.ShieldsPack01Reward.ToString();
            _shieldsPack02RewardText.text = RemoteConfigClient.Instance.ShieldsPack02Reward.ToString();
            _shieldsPack03RewardText.text = RemoteConfigClient.Instance.ShieldsPack03Reward.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.GetShields.Value);
        }

        private void RegisterButtonsListeners()
        {
            _shieldsPack01Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingShieldsPack(RemoteConfigClient.Instance.ShieldsPack01Cost, RemoteConfigClient.Instance.ShieldsPack01Reward))
                .AddTo(this);
            _shieldsPack02Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingShieldsPack(RemoteConfigClient.Instance.ShieldsPack02Cost, RemoteConfigClient.Instance.ShieldsPack02Reward))
                .AddTo(this);
            _shieldsPack03Button.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingShieldsPack(RemoteConfigClient.Instance.ShieldsPack03Cost, RemoteConfigClient.Instance.ShieldsPack03Reward))
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(ExitButtonEventListener)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _rootState.Exit())
                .AddTo(this);
        }

        private void InstantiateShields()
        {
            if (CurrenciesTransforms == null || CurrenciesTransforms.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_shield, _animationsElements)
                    .Subscribe(shields => CurrenciesTransforms = shields)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            Observable.FromCoroutine(_shieldsMenuAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new GetShieldsMenuCalled()))
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogEarnVirtualCurrency(nameof(_playerData.PersistentData.Shields), CurrencyAmountEarned);
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.Coins), CurrencyAmountSpent);
#endif
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion





        #region Event Listeners

        private void HandleBuyingShieldsPack(int cost, int reward)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (_playerData.PersistentData.Coins.Value >= cost)
            {
                CurrencyAmountEarned = reward;
                CurrencyAmountSpent = cost;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new ShopMenuCalled());
            }
        }

        private void ExitButtonEventListener(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<GetShieldsMenu>
        {
            
        }

        #endregion
    }
}
