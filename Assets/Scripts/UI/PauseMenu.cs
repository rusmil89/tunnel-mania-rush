﻿using System.Linq;
using Core.Events;
using Localization;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: PauseMenu
    /// Služi za kontrolisanje pause menija
    /// </summary>
    public sealed class PauseMenu : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Consts
        /// </summary>
        private const string STRING_FORMAT_F1 = Constants.StringFormats.F1;
        private const float GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE = Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE;
        private const float ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE = Constants.Gyroscope.ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Buttons")]
        [SerializeField] private Button _mainMenuButton;
        [SerializeField] private Button _continueButton;
        [SerializeField] private Toggle _gyroscopeControlsToggle;
        
        [Header("Texts")]
        [SerializeField] private TMP_Text _levelNumberText;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _gyroscopeControls;
        [SerializeField] private TMP_Text _sensitivityText;
        [SerializeField] private TMP_Text _rotationAccelerationText;
        [SerializeField] private TMP_Text _mainMenuText;
        [SerializeField] private TMP_Text _continueText;
        
        [Space] [Header("Scriptable Objects")]
        [SerializeField] private GameData _gameData;
        [SerializeField] private global::PlayerData _playerData;
        [SerializeField] private MetersSO _meters;
        
        [Space] [Header("Gyroscope Elements")]
        [SerializeField] private GameObject _sensitivityHolder;
        [SerializeField] private GameObject _rotationAccelerationHolder;
        [SerializeField] private Slider _sensitivitySlider;
        [SerializeField] private Slider _rotationAccelerationSlider;
        [SerializeField] private TMP_Text _sensitivityValueText;
        [SerializeField] private TMP_Text _rotationAccelerationValueText;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _meters = AssetDatabase.FindAssets(Constants.Editor.Filters.METERS, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<MetersSO>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var allRectTransforms = gameObject.Descendants().OfComponent<RectTransform>();
            _mainMenuButton = allRectTransforms.First(s => s.name.Equals("MainMenuButton")).GetComponent<Button>();
            _continueButton = allRectTransforms.First(s => s.name.Equals("ContinueButton")).GetComponent<Button>();
            _levelNumberText = allRectTransforms.First(s => s.name.Equals("LevelNumberText")).GetComponent<TMP_Text>();
            _gyroscopeControlsToggle = allRectTransforms.First(s => s.name.Equals("GyroscopeToggle")).GetComponent<Toggle>();
            _sensitivityHolder = allRectTransforms.First(s => s.name.Equals("SensitivityHolder")).gameObject;
            _rotationAccelerationHolder = allRectTransforms.First(s => s.name.Equals("RotationAccelerationHolder")).gameObject;
            _sensitivitySlider = allRectTransforms.First(s => s.name.Equals("SensitivitySlider")).GetComponent<Slider>();
            _rotationAccelerationSlider = allRectTransforms.First(s => s.name.Equals("RotationAccelerationSlider")).GetComponent<Slider>();
            _sensitivityValueText = allRectTransforms.First(s => s.name.Equals("SensitivityValueText")).GetComponent<TMP_Text>();
            _rotationAccelerationValueText = allRectTransforms.First(s => s.name.Equals("RotationAccelerationValueText")).GetComponent<TMP_Text>();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            SetInitialValues();
        }

        private void SetInitialValues()
        {
            // set gyroscope values
            _gyroscopeControlsToggle.isOn = _playerData.PersistentData.IsGyroscopeEnabled.Value;
            _sensitivitySlider.value = (GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE - _playerData.PersistentData.GyroscopeSensitivity.Value) *
                                       Constants.Gyroscope.GYROSCOPE_SENSITIVITY_SLIDER_COMPONENT_FACTOR;
            _rotationAccelerationSlider.value = _playerData.PersistentData.RotationAccelerationFactor.Value * Constants.Gyroscope.ROTATION_ACCELERATION_SLIDER_COMPONENT_FACTOR;
            _sensitivityValueText.text = (_sensitivitySlider.value * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
            _rotationAccelerationValueText.text = (_rotationAccelerationSlider.value * Constants.Gyroscope.ROTATION_ACCELERATION_VALUE_TEXT_FACTOR).ToString(STRING_FORMAT_F1);
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterButtonsListeners();
            RegisterObservableListeners();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.Pause.Value);
            _description.SetText(_languageClient.CurrentLanguage.Level.Value);
            _gyroscopeControls.SetText(_languageClient.CurrentLanguage.GyroscopeControls.Value);
            _sensitivityText.SetText(_languageClient.CurrentLanguage.Sensitivity.Value);
            _rotationAccelerationText.SetText(_languageClient.CurrentLanguage.RotationAcceleration.Value);
            _mainMenuText.SetText(_languageClient.CurrentLanguage.MainMenu.Value);
            _continueText.SetText(_languageClient.CurrentLanguage.Continue.Value);
        }

        private void RegisterButtonsListeners()
        {
            _mainMenuButton.OnClickAsObservable()
                .Subscribe(MainMenuButtonEventListener)
                .AddTo(this);
            _continueButton.OnClickAsObservable()
                .Subscribe(ContinueButtonEventListener)
                .AddTo(this);
            _gyroscopeControlsToggle.OnValueChangedAsObservable()
                .Subscribe(OnGyroscopeControlsListener)
                .AddTo(this);
            _sensitivitySlider.OnValueChangedAsObservable()
                .Subscribe(ListenToGyroscopeSensitivitySlider)
                .AddTo(this);
            _rotationAccelerationSlider.OnValueChangedAsObservable()
                .Subscribe(ListenToRotationAccelerationSlider)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<GamePaused>()
                .Subscribe(OnGamePausedEventHandler)
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void MainMenuButtonEventListener(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ShouldLoadMainMenuScene());
        }

        private void ContinueButtonEventListener(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new CountdownMenuCalled());
        }

        private void OnGyroscopeControlsListener(bool value)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsGyroscopeEnabled.Value = value;
            _sensitivityHolder.SetActive(value);
            _rotationAccelerationHolder.SetActive(value);
        }

        private void ListenToGyroscopeSensitivitySlider(float sliderValue)
        {
            _playerData.PersistentData.GyroscopeSensitivity.Value = sliderValue < Constants.Gyroscope.SLIDER_MAX_VALUE
                ? (_sensitivitySlider.maxValue - sliderValue) * Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_VALUE_FACTOR
                : Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_MAX_VALUE;
            _sensitivityValueText.text = (sliderValue * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
        }

        private void ListenToRotationAccelerationSlider(float sliderValue)
        {
            _playerData.PersistentData.RotationAccelerationFactor.Value = sliderValue > Constants.Gyroscope.SLIDER_MIN_VALUE
                ? sliderValue * ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE
                : ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE;
            _rotationAccelerationValueText.text = (sliderValue * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
        }

        private void OnGamePausedEventHandler(GamePaused gamePaused)
        {
            _levelNumberText.text = _playerData.PersistentData.CurrentLevel.Value.ToString();
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<PauseMenu>
        {
            
        }

        #endregion
    }
}
