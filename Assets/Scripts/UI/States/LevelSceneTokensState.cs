using UnityEngine;

namespace Tunnel.UI
{
    public class LevelSceneTokensState : BaseMenuState
    {
        private UIInLevelScene _uiInLevelScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInLevelScene uiInLevelScene)
        {
            _uiInLevelScene = uiInLevelScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInLevelScene.TokensMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInLevelScene.TokensMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInLevelScene.TokensFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInLevelScene.transform, false);
            }
        }
    }
}