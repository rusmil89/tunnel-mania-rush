using UnityEngine;

namespace Tunnel.UI
{
    public class LevelSceneShopState : BaseMenuState
    {
        private UIInLevelScene _uiInLevelScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInLevelScene uiInLevelScene)
        {
            _uiInLevelScene = uiInLevelScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInLevelScene.ShopMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInLevelScene.ShopMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInLevelScene.ShopFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInLevelScene.transform, false);
            }
        }
    }
}