using RSG;

namespace Tunnel.UI
{
    public class BaseMenuState : AbstractState
    {
        protected void ShowMenu(MenuSO menu)
        {
            if (!menu.IsMenuShown)
            {
                menu.Show();
            }
        }

        protected void HideMenu(MenuSO menu)
        {
            if (menu.IsMenuShown)
            {
                menu.Hide();
            }
        }
    }
}