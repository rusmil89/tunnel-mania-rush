using UnityEngine;

namespace Tunnel.UI
{
    public class LevelSceneInitialState : BaseMenuState
    {
        private UIInLevelScene _uiInLevelScene;
        
        public void OnStateEnter(UIInLevelScene uiInLevelScene)
        {
            _uiInLevelScene = uiInLevelScene;
            Application.targetFrameRate = 60;
            ShowMenu(_uiInLevelScene.ControlsMenu);
            ShowMenu(_uiInLevelScene.TopMenu);
        }

        public void OnStateExit()
        {
            Application.targetFrameRate = 30;
            HideMenu(_uiInLevelScene.ControlsMenu);
            HideMenu(_uiInLevelScene.TopMenu);
        }
    }
}