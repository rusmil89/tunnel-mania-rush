using UnityEngine;

namespace Tunnel.UI
{
    public class MenuSceneTokensState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            _uiInMenuScene = uiInMenuScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.TopButtonsMenu);
            HideMenu(_uiInMenuScene.TokensMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInMenuScene.TopButtonsMenu);
            ShowMenu(_uiInMenuScene.TokensMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInMenuScene.TokensFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInMenuScene.transform, false);
            }
        }
    }
}