namespace Tunnel.UI
{
    public class MenuSceneTopButtonsState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        
        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            _uiInMenuScene = uiInMenuScene;
            ShowMenu(_uiInMenuScene.TopButtonsMenu);
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.TopButtonsMenu);
        }
    }
}