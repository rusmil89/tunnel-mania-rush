using UnityEngine;

namespace Tunnel.UI
{
    public class MenuSceneInitialMenuState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        
        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            Application.targetFrameRate = 30;
            _uiInMenuScene = uiInMenuScene;
            ShowMenu(_uiInMenuScene.HomeMenu);
            ShowMenu(_uiInMenuScene.TopButtonsMenu);
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.HomeMenu);
            HideMenu(_uiInMenuScene.TopButtonsMenu);
        }
    }
}