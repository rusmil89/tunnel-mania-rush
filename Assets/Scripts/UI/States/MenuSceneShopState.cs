using UnityEngine;

namespace Tunnel.UI
{
    public class MenuSceneShopState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            _uiInMenuScene = uiInMenuScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.TopButtonsMenu);
            HideMenu(_uiInMenuScene.ShopMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInMenuScene.TopButtonsMenu);
            ShowMenu(_uiInMenuScene.ShopMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInMenuScene.ShopFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInMenuScene.transform, false);
            }
        }
    }
}