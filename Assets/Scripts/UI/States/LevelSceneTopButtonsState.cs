using System.Linq;
using Unity.Linq;
using UnityEngine;

namespace Tunnel.UI
{
    public class LevelSceneTopButtonsState : BaseMenuState
    {
        private UIInLevelScene _uiInLevelScene;
        private GameObject _instantiatedMenu;
        
        public void OnStateEnter(UIInLevelScene uiInLevelScene)
        {
            _uiInLevelScene = uiInLevelScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInLevelScene.TopButtonsMenu);
        }

        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            SetUpTopButtonsMenu();
            ShowMenu(_uiInLevelScene.TopButtonsMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInLevelScene.TopButtonsFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInLevelScene.transform, false);
            }
        }

        private void SetUpTopButtonsMenu()
        {
            if (_instantiatedMenu != null)
            {
                var descendantsEnumerable = _instantiatedMenu.Descendants();
                descendantsEnumerable.First(x => x.name.Equals("SettingsButton")).SetActive(false);
                descendantsEnumerable.First(x => x.name.Equals("EnergyInfo")).SetActive(false);
                descendantsEnumerable.First(x => x.name.Equals("LevelsDifficultyButton")).SetActive(false);
            }
        }
    }
}