using UnityEngine;

namespace Tunnel.UI
{
    public class LevelSceneScoreState : BaseMenuState
    {
        private UIInLevelScene _uiInLevelScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInLevelScene uiInLevelScene)
        {
            _uiInLevelScene = uiInLevelScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInLevelScene.ScoreMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInLevelScene.ScoreMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInLevelScene.ScoreFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInLevelScene.transform, false);
            }
        }
    }
}