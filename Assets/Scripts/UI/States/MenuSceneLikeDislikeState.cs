using UnityEngine;

namespace Tunnel.UI
{
    public class MenuSceneLikeDislikeState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        private GameObject _instantiatedMenu;
        
        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            _uiInMenuScene = uiInMenuScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.LikeDislikeMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInMenuScene.LikeDislikeMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInMenuScene.LikeDislikeFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInMenuScene.transform, false);
            }
        }
    }
}