using UnityEngine;

namespace Tunnel.UI
{
    public class MenuSceneExitState : BaseMenuState
    {
        private UIInMenuScene _uiInMenuScene;
        private GameObject _instantiatedMenu;

        public void OnStateEnter(UIInMenuScene uiInMenuScene)
        {
            _uiInMenuScene = uiInMenuScene;
            SetUpMenu();
        }

        public void OnStateExit()
        {
            HideMenu(_uiInMenuScene.ExitMenu);
        }
        
        private void SetUpMenu()
        {
            InstantiateMenuPrefab();
            ShowMenu(_uiInMenuScene.ExitMenu);
        }

        private void InstantiateMenuPrefab()
        {
            if (_instantiatedMenu == null)
            {
                _instantiatedMenu = _uiInMenuScene.ExitFactory.Create().gameObject;
                _instantiatedMenu.transform.SetParent(_uiInMenuScene.transform, false);
            }
        }
    }
}