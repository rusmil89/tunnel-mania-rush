﻿using UnityEngine;

[CreateAssetMenu(fileName = "ValueName", menuName = "ScriptableObjects/Levels/Values", order = 1)]
public class ValueSO : ScriptableObject
{
    public int value;

    public void ResetValue()
    {
        value = 0;
    }
}
