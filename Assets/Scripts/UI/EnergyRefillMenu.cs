﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tunnel.Animations;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using TMPro;
using Tunnel.Ads;
using Tunnel.Audio;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: EnergyRefillMenu
    /// DESCRIPTION: Klasa koja kontrolise meni za dopunjavanje energije
    /// </summary>
    public sealed class EnergyRefillMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Objects")]
        [SerializeField] private Transform _panel;
        [SerializeField] private GameObject _indicator;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _energyPack01RefillButton;
        [SerializeField] private Button _energyPack02RefillButton;
        [SerializeField] private Button _energyPack03RefillButton;
        [SerializeField] private TMP_Text _energyPack01CostText;
        [SerializeField] private TMP_Text _energyPack02CostText;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _freeButtonText;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _energy;
        [SerializeField] private RectTransform _topMenuEnergyPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private CurrencyBoughtAnimations _energyMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;
        private const int ENERGY_AMOUNT_FOR_PLAY = Constants.Energy.ENERGY_AMOUNT_FOR_PLAY;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Energy;

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject => _topMenuEnergyPosition;

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion




        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();

            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            var energyPackObjects = gameObject.Descendants().Where(x => x.name.Contains("EnergyPack")).ToArray();
            List<Button> refillButtons = energyPackObjects.Select(energyPackGameObject => energyPackGameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.REFILL_BUTTON)).GetComponent<Button>()).ToList();
            List<TMP_Text> energyCostText = energyPackObjects.Select(energyPackObject => energyPackObject.Descendants().First(x => x.name.Equals("RefillButtonText")).GetComponent<TMP_Text>()).ToList();
            _energyPack01RefillButton = refillButtons[0];
            _energyPack02RefillButton = refillButtons[1];
            _energyPack03RefillButton = refillButtons[2];
            _freeButtonText = energyCostText[0];
            _energyPack01CostText = energyCostText[1];
            _energyPack02CostText = energyCostText[2];
            _title = gameObject.Descendants().First(x => x.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _energy = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Energy"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuEnergyPosition = gameObject.Descendants().First(x => x.name.Contains("EnergyPosition")).GetComponent<RectTransform>();
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            _indicator = gameObject.Descendants().First(x => x.name.Equals(Constants.GameObjectsNames.INDICATOR)).gameObject;
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                    .Enter(state => HandleEnteringShownState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            InstantiateEnergies();
            Observable.Timer(TimeSpan.FromMilliseconds(500f))
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
        }

        private void SetInitialValues()
        {
            _energyMenuAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            CurrenciesTransforms = new List<RectTransform>();
            _energyPack01CostText.text = RemoteConfigClient.Instance.EnergyPack01Cost.ToString();
            _energyPack02CostText.text = RemoteConfigClient.Instance.EnergyPack02Cost.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.RefillYourEnergy.Value);
            _freeButtonText.SetText(_languageClient.CurrentLanguage.Free.Value);
        }

        private void RegisterButtonsListeners()
        {
            _exitButton.OnClickAsObservable()
                .Subscribe(HandleExitButtonEvent)
                .AddTo(this);
            _energyPack01RefillButton.OnClickAsObservable()
                .Subscribe(HandleEnergyPack01RefillButtonEvent)
                .AddTo(this);
            _energyPack02RefillButton.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingEnergyPack(RemoteConfigClient.Instance.EnergyPack01Cost, 1))
                .AddTo(this);
            _energyPack03RefillButton.OnClickAsObservable()
                .Subscribe(_ => HandleBuyingEnergyPack(RemoteConfigClient.Instance.EnergyPack02Cost, 3))
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _rootState.Exit())
                .AddTo(this);
            _playerData.PersistentData.CurrentEnergy
                .Subscribe(currentEnergy => SetIndicatorActive(currentEnergy < ENERGY_AMOUNT_FOR_PLAY))
                .AddTo(this);
            MessageBroker.Default.Receive<EnergyRefilledWithAd>()
                .Throttle(TimeSpan.FromMilliseconds(100f))
                .Subscribe(EnergyRefillListener)
                .AddTo(this);
        }

        private void SetIndicatorActive(bool isActive)
        {
            _indicator.SetActive(isActive);
        }

        private void InstantiateEnergies()
        {
            if (CurrenciesTransforms == null || CurrenciesTransforms.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_energy, _animationsElements)
                    .Subscribe(energies => CurrenciesTransforms = energies)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringShownState()
        {
            AdsClient.Instance.LoadRewardedAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.ENERGY_REFILL));
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            Observable.FromCoroutine(_energyMenuAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new EnergyRefillMenuCalled()))
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogEarnVirtualCurrency(nameof(_playerData.PersistentData.CurrentEnergy), CurrencyAmountEarned);
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.Coins), CurrencyAmountSpent);
#endif
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion





        #region Event Listeners

        private void HandleExitButtonEvent(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        private void HandleEnergyPack01RefillButtonEvent(Unit unit)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            AdsClient.Instance.ShowRewardedVideo(AdPlacement.PlacementWithName(Constants.AdsPlacements.ENERGY_REFILL));
        }

        private void HandleBuyingEnergyPack(int cost, int rewardFactor)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (_playerData.PersistentData.Coins.Value >= cost)
            {
                CurrencyAmountEarned = rewardFactor * ENERGY_AMOUNT_FOR_PLAY;
                CurrencyAmountSpent = cost;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new ShopMenuCalled());
            }
        }

        private void EnergyRefillListener(EnergyRefilledWithAd energyRefilledWithAd)
        {
            CurrencyAmountEarned = ENERGY_AMOUNT_FOR_PLAY;
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<EnergyRefillMenu>
        {
            
        }

        #endregion
    }
}
