﻿using System.Linq;
using Core.Events;
using Localization;
using TMPro;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAMEOBJECT: ExitMenu
    /// DESCRIPTION: Klasa koja kontrolise ponasanje exit menija
    /// </summary>
    public sealed class ExitMenu : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Space] [Header("Buttons")]
        [SerializeField] private Button _yesButton;
        [SerializeField] private Button _noButton;

        [Header("Text")]
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _yesButtonText;
        [SerializeField] private TMP_Text _noButtonText;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _yesButton = gameObject.Descendants().First(x => x.name.Contains("YesButton")).GetComponent<Button>();
            _noButton = gameObject.Descendants().First(x => x.name.Contains("NoButton")).GetComponent<Button>();
            _description = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("Description"));
            _title = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("TitleText"));
            _yesButtonText = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("ButtonText"));
            _noButtonText = gameObject.Descendants().Where(text => text.name.Contains("ButtonText")).OfComponent<TMP_Text>().ToArray()[1];
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterObservableListeners();
        }

        #endregion





        #region Custom Methods

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.ExitTitle.Value);
            _description.SetText(_languageClient.CurrentLanguage.ExitMenuDescription.Value);
            _yesButtonText.SetText(_languageClient.CurrentLanguage.Yes.Value);
            _noButtonText.SetText(_languageClient.CurrentLanguage.No.Value);
        }

        private void RegisterObservableListeners()
        {
            _yesButton.OnClickAsObservable()
                .Subscribe(_ => YesButtonEventHandler())
                .AddTo(this);
            _noButton.OnClickAsObservable()
                .Subscribe(_ => NoButtonEventHandler())
                .AddTo(this);
        }

        public void Exit(bool yes)
        {
            if (yes)
            {
                Application.Quit();
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#endif
            }
            else
            {
                MessageBroker.Default.Publish(new HomeAndTopButtonsMenuCalled());
            }
        }

        #endregion





        #region Event Listeners

        private void YesButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            Exit(true);
        }

        private void NoButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            Exit(false);
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<ExitMenu>
        {
            
        }

        #endregion
    }
}