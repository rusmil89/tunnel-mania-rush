﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIEffects;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using TMPro;
using Tunnel.Ads;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.PlayerData;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: Home
    /// DESCRIPTION: Klasa koja kontrolise ponasanje home menija u menu sceni
    /// </summary>
    public sealed class HomeMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Consts
        /// </summary>
        private readonly Color _levelsImageLockedColor = new(0.4811f, 0.4811f, 0.4811f, 0.3921f);
        private readonly Color _levelsImageUnlockedColor = new(0f, 0.8509f, 1f, 0.3921f);
        private readonly Color _levelsImageFullColor = new(0f, 0.8509f, 1f, 1f);
        private readonly WorldScrollSnapSelectionPageChanged _worldScrollSnapSelectionPageChanged = new();
        private readonly ChapterScrollSnapSelectionPageChanged _chapterScrollSnapSelectionPageChanged = new();
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Animations elements
        /// </summary>
        private CurrencyBoughtAnimations _currencyBoughtAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private int _shareRewardRemoteValue;
        private IState _rootState;
        private List<RectTransform> _coins;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("UI")]
        [SerializeField] private Button _achievementsButton;
        [SerializeField] private Button _dailyRewardsButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _unlockButton;
        [SerializeField] private Button _shareButton;
        [SerializeField] private TMP_Text _loadingText;
        [SerializeField] private TMP_Text _playButtonText;
        [SerializeField] private TMP_Text _unlockButtonText;

        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("World & Chapter Elements")]
        [SerializeField] private TMP_Text _worldText;
        [SerializeField] private HorizontalScrollSnap _chaptersHorizontalScrollSnap;
        [SerializeField] private HorizontalScrollSnap _worldsHorizontalScrollSnap;
        [SerializeField] private Image[] _chaptersImages;
        [SerializeField] private Image[] _levelsImages;
        [SerializeField] private Image[] _worldsImages;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _coin;
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;
        
        [Header("Levels Data")] 
        [SerializeField] private WorldData[] _allWorldData;
        
        #endregion





        #region Properties
        
        private IObservable<int> WorldHorizontalScrollSnapChange
        {
            get
            {
                return _worldsHorizontalScrollSnap.ObserveEveryValueChanged(scrollSnap => scrollSnap.CurrentPage)
                    .Throttle(TimeSpan.FromMilliseconds(250f));
            }
        }
        
        private IObservable<int> ChapterHorizontalScrollSnapChange
        {
            get
            {
                return _chaptersHorizontalScrollSnap.ObserveEveryValueChanged(scrollSnap => scrollSnap.CurrentPage)
                    .Throttle(TimeSpan.FromMilliseconds(260f));
            }
        }
        
        public Constants.CurrencyType CurrencyType { get; private set; }
        public int CurrencyAmountEarned { get; private set; }
        public int CurrencyAmountSpent => default;
        public List<RectTransform> CurrenciesTransforms { get; private set; }
        public RectTransform CurrencyAnimationStartObject => default;
        public RectTransform CurrencyAnimationEndObject { get; private set; }
        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion
        
        
        


        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            const string achievementValue = "Achievement";
            const string dailyRewards = "DailyRewards";
            const string playValue = "PlayButton";
            const string unlockButtonValue = "UnlockButton";
            const string shareButtonValue = "Share";
            
            // buttons
            var descendantsEnumerable = gameObject.Descendants();
            var allButtons = descendantsEnumerable.OfComponent<Button>();
            _achievementsButton = allButtons.First(x => x.name.Contains(achievementValue));
            _dailyRewardsButton = allButtons.First(x => x.name.Contains(dailyRewards));
            _playButton = allButtons.First(x => x.name.Contains(playValue));
            _unlockButton = allButtons.First(x => x.name.Contains(unlockButtonValue));
            _shareButton = allButtons.First(x => x.name.Contains(shareButtonValue));

            // texts
            var allTextMeshProTexts = descendantsEnumerable.OfComponent<TMP_Text>();
            _loadingText = allTextMeshProTexts.First(x => x.name.Contains("LoadingText"));
            _worldText = allTextMeshProTexts.First(x => x.name.Contains("WorldText"));
            _playButtonText = allTextMeshProTexts.First(x => x.name.Contains("PlayButtonText"));
            _unlockButtonText = allTextMeshProTexts.First(x => x.name.Contains("UnlockButtonText"));
            
            // images
            var allImages = descendantsEnumerable.OfComponent<Image>();
            _chaptersImages = allImages.Where(i => i.name.StartsWith("Chapter0") && i.name.EndsWith("Image")).ToArray();
            _levelsImages = descendantsEnumerable.OfComponent<HorizontalLayoutGroup>().SelectMany(h => h.GetComponentsInChildren<Image>()).ToArray();
            _worldsImages = allImages.Where(i => i.name.Contains("World0")).ToArray();

            // horizontal scroll snaps
            _chaptersHorizontalScrollSnap = descendantsEnumerable.OfComponent<HorizontalScrollSnap>().First(x => x.name.Contains("Chapter"));
            _worldsHorizontalScrollSnap = descendantsEnumerable.OfComponent<HorizontalScrollSnap>().First(x => x.name.Contains("World"));
            
            // scriptable objects
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            var allWorldDataGUIDs = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.WORLD_DATA, new[] {Constants.Editor.Paths.SCRIPTABLE_OBJECTS});
            _allWorldData = allWorldDataGUIDs
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<WorldData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .Where(s => !s.name.Contains("Tutorial"))
                .ToArray();
            
            // animations elements
            _coin = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = descendantsEnumerable.First(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = descendantsEnumerable.First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }
        
        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                .Enter(HandleEnteringInitializationState)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                .Enter(HandleEnteringAnimationsPlayingState)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                .Enter(HandleEnteringHiddenState)
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState(Constants.MenusStatesTypes.Normal normal)
        {
            SetInitialValues();
            SetTexts();
            StartCoroutine(DelayInitializationWhileScrollSnapGameObjectsAreNotActive());
            InstantiateCurrencies();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void HandleEnteringAnimationsPlayingState(Constants.MenusStatesTypes.Normal state)
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            CurrenciesTransforms = _coins;
            CurrencyAnimationEndObject = _topMenuCoinsPosition;
            Observable.FromCoroutine(_currencyBoughtAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new HomeAndTopButtonsMenuCalled()))
                .AddTo(this);
        }
        
        private void HandleEnteringHiddenState(Constants.MenusStatesTypes.Normal state)
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        private void OnDisable()
        {
            _rootState.Exit();
            _worldsHorizontalScrollSnap.OnSelectionPageChangedEvent.RemoveAllListeners();
            _chaptersHorizontalScrollSnap.OnSelectionPageChangedEvent.RemoveAllListeners();
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            _playButton.gameObject.SetActive(false);
            _unlockButton.gameObject.SetActive(false);
            _currencyBoughtAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            _shareRewardRemoteValue = RemoteConfigClient.Instance.ShareReward;
            _shareButton.GetComponentInChildren<TMP_Text>().SetText(_shareRewardRemoteValue.ToString());
        }

        private void SetTexts()
        {
            var currentLanguage = _languageClient.CurrentLanguage;
            _playButtonText.SetText(currentLanguage.Play.Value);
            _loadingText.SetText(currentLanguage.Loading.Value);
            _unlockButtonText.SetText(currentLanguage.Unlock.Value);
            _worldText.SetText(currentLanguage.World.Value);
            _playButtonText.SetText(currentLanguage.Play.Value);
            _unlockButtonText.SetText(currentLanguage.Unlock.Value);
        }

        // NOTE: zato sto se start metoda poziva pre Awake metode iz ScrollSnapBase klase
        // jer objekat na kojoj se nalazi ta klasa se inicijalizuje nakon sto se inicijalizuje objekat na kome se nalazi ova klasa
        // NOTE: zbog toga kada pozovem ChagenPage() iz ScrollSnapBase klase izbacuje null ref ex
        // zato sto ta metoda zahteva referencu na skrol koja ne postoji jer se ona dobija tek u Awake metodi koja nije jos pozvana
        private IEnumerator DelayInitializationWhileScrollSnapGameObjectsAreNotActive()
        {
            while (!_worldsHorizontalScrollSnap.gameObject.activeInHierarchy && !_chaptersHorizontalScrollSnap.gameObject.activeInHierarchy)
            {
                yield return null;
            }
            SetButtons();
            RegisterObservableListeners();
            RegisterEventListeners();
        }
        
        private void InstantiateCurrencies()
        {
            if (_coins == null || _coins.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                    .Subscribe(list => _coins = list)
                    .AddTo(this);
            }

            if(_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<ChapterUnlockedWithAd>()
                .Subscribe(_ => HandleUnlockingChapter())
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterUnlockedWithToken>()
                .Subscribe(_ => HandleUnlockingChapter())
                .AddTo(this);
            WorldHorizontalScrollSnapChange
                .SkipWhile(_ => !_worldsHorizontalScrollSnap.gameObject.activeInHierarchy && !_chaptersHorizontalScrollSnap.gameObject.activeInHierarchy)
                .Subscribe(HandleWorldScrollSnapping)
                .AddTo(this);
            ChapterHorizontalScrollSnapChange
                .SkipWhile(_ => !_worldsHorizontalScrollSnap.gameObject.activeInHierarchy && !_chaptersHorizontalScrollSnap.gameObject.activeInHierarchy)
                .Subscribe(SetPlayAndUnlockButton)
                .AddTo(this);
            _playerData.PersistentData.ChaptersProgress[_playerData.PersistentData.CurrentWorld.Value].ObserveEveryValueChanged(states => states)
                .Where(state => state.Contains(PersistentData.ChapterState.IsReadyForUnlock))
                .Subscribe(_ => AdsClient.Instance.LoadRewardedAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.UNLOCK_CHAPTER)))
                .AddTo(this);
            _playerData.PersistentData.IsPlayerSignedInGoogleGameServices
                .Subscribe(PlayerSignedInGoogleGameServicesHandler)
                .AddTo(this);
            _playerData.PersistentData.DataSaveTime
                .Where(lastDataSaveTime => DateTime.Today.Subtract(lastDataSaveTime).Days < 1)
                .Subscribe(_ => _dailyRewardsButton.gameObject.SetActive(false))
                .AddTo(this);
            _shareButton.OnClickAsObservable()
                .Subscribe(HandleShareButtonClick)
                .AddTo(this);
        }

        private void RegisterEventListeners()
        {
            _worldsHorizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(HandleWorldsScrollPageChanged);
            _chaptersHorizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(HandleChaptersScrollPageChanged);
        }

        #endregion
        
        
        


        #region Event Listeners

        private void HandleChaptersScrollPageChanged(int currentScreen) => MessageBroker.Default.Publish(_chapterScrollSnapSelectionPageChanged);

        private void HandleWorldsScrollPageChanged(int currentScreen) => MessageBroker.Default.Publish(_worldScrollSnapSelectionPageChanged);
        
        private void HandleWorldScrollSnapping(int currentWorld)
        {
            ResetProgress();
            SetChaptersProgress();
            SetLevelsLook();
            SetLevelsProgress();
            SetCurrentWorld();
            SetPlayAndUnlockButton(_playerData.PersistentData.CurrentChapterInWorld[currentWorld + 1] - 1);
        }

        private void SetPlayAndUnlockButton(int currentChapter)
        {
            switch (_playerData.PersistentData.ChaptersProgress[_worldsHorizontalScrollSnap.CurrentPage + 1][currentChapter])
            {
                case PersistentData.ChapterState.Locked:
                {
                    SetPlayButtonState(false);
                    SetUnlockButton(false);
                } 
                    break;
                case PersistentData.ChapterState.IsReadyForUnlock:
                {
                    SetCurrentChapter();
                    SetPlayButtonState(false);
                    SetUnlockButton(true);
                }
                    break;
                case PersistentData.ChapterState.Unlocked:
                {
                    SetCurrentChapter();
                    SetPlayButtonState(true);
                    SetUnlockButton(false);
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void HandleUnlockingChapter()
        {
            var currentWorld = _playerData.PersistentData.CurrentWorld.Value;
            _playerData.PersistentData.ChaptersProgress[currentWorld][
                _playerData.PersistentData.CurrentChapterInWorld[currentWorld] - 1] = PersistentData.ChapterState.Unlocked;
            ResetProgress();
            SetChaptersProgress();
            SetLevelsLook();
            SetLevelsProgress();
            SetPlayButtonState(true);
            SetUnlockButton(false);
        }

        private void PlayerSignedInGoogleGameServicesHandler(bool isPlayerSignedInGoogleGameServices)
        {
            _achievementsButton.gameObject.SetActive(isPlayerSignedInGoogleGameServices);
#if UNITY_ANDROID
            if (isPlayerSignedInGoogleGameServices)
            {
                _loadingText.gameObject.SetActive(false);
                _playButton.gameObject.SetActive(true);
            }
#endif
        }
        
        private void HandleShareButtonClick(Unit obj)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            CurrencyAmountEarned = _shareRewardRemoteValue;
            CurrencyType = Constants.CurrencyType.Coin;
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            Share();
        }
        
        private void Share()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
        Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.NativeShare);
        Sharing.ShareURL(Constants.Links.ANDROID);
#elif UNITY_IOS
        Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.NativeShare);
        Sharing.ShareURL(Constants.Links.IOS);
#endif
        }

        /// <summary>
        /// Metoda koja kontrolise prikaz svih dugmica u home meniju u zavisnosti od ispunjenih uslova
        /// WHY: zato sto se neki dugmici pale ili gase samo pod odredjenim uslovima i na taj nacin omogucavamo da UI bude drugaciji u zavisnosti od uslova
        /// </summary>
        private void SetButtons()
        {
            SetProgress();
            _loadingText.gameObject.SetActive(false);
            _playButton.gameObject.SetActive(true);
        }

        /// <summary>
        /// Metoda koja kontrolise izgled UI-a koji pokazuje progress igraca kroz svetove, chaptere i levele
        /// WHY: Zato sto na samom pocetku obavestavamo igraca kako izgleda njegov progres
        /// </summary>
        private void SetProgress()
        {
            ResetProgress();
            SetWorldsProgress();
            SetChaptersProgress();
            SetLevelsLook();
            SetLevelsProgress();
        }

        /// <summary>
        /// Metoda koja resetuje izgled UI-a na podrazumevane vrednosti
        /// WHY: Zato sto je neophodno uraditi to svaki put kada igrac promeni svet u UI-u da bismo mogli da postavimo nove vrednosti za novi svet
        /// </summary>
        private void ResetProgress()
        {
            foreach (var chaptersImage in _chaptersImages)
            {
                chaptersImage.GetComponent<UIEffect>().effectMode = EffectMode.Grayscale;
                chaptersImage.gameObject.Child("LockImage").GetComponent<Image>().enabled = true;
            }

            foreach (var levelsImage in _levelsImages)
            {
                levelsImage.color = _levelsImageLockedColor;
            }
        }

        /// <summary>
        /// Metoda koja postavlja izgled progresa u UI za svetove
        /// WHY: Zato sto se razlikuju otkljucani od zakljucanih svetova
        /// </summary>
        private void SetWorldsProgress()
        {
            var unlockedWorldsArray = _playerData.PersistentData.WorldProgress.Where(isWorldUnlocked => isWorldUnlocked == PersistentData.WorldState.Unlocked).ToArray();
            for (int i = 0; i < unlockedWorldsArray.Length; i++)
            {
                var worldImageGameObject = _worldsImages[i].gameObject;
                var lockImagesEnumerable = worldImageGameObject.Children().OfComponent<Image>().Where(x => x.name.Contains("Lock"));
                var lockImagesArray = lockImagesEnumerable as Image[] ?? lockImagesEnumerable.ToArray();
                foreach (var lockImage in lockImagesArray)
                {
                    lockImage.enabled = false;
                }

                var worldNumberTextEnumerable = worldImageGameObject.Children().OfComponent<TMP_Text>();
                foreach (var worldNumberText in worldNumberTextEnumerable)
                {
                    worldNumberText.alpha = 1f;
                }

                worldImageGameObject.ChildrenAndSelf().OfComponent<UIEffect>().First().effectMode = EffectMode.None;
            }

            var currentWorld = _playerData.PersistentData.CurrentWorld.Value - 1;
            _worldsHorizontalScrollSnap.ChangePage(currentWorld);
        }

        /// <summary>
        /// Metoda koja postavlja izgled progresa u UI za chaptere
        /// WHY: Zato sto se razlikuje broj otkljucanih chaptera od sveta do sveta
        /// </summary>
        private void SetChaptersProgress()
        {
            var unlockedChaptersArray = _playerData.PersistentData.ChaptersProgress[_worldsHorizontalScrollSnap.CurrentPage + 1]
                .Where(chapterState => chapterState == PersistentData.ChapterState.Unlocked)
                .ToArray();
            for (int i = 0; i < unlockedChaptersArray.Length; i++)
            {
                var chapterImageGameObject = _chaptersImages[i].gameObject;
                var lockImagesEnumerable = chapterImageGameObject.Children().OfComponent<Image>().Where(x => x.name.Contains("Lock"));
                var lockImagesArray = lockImagesEnumerable as Image[] ?? lockImagesEnumerable.ToArray();
                foreach (var lockImage in lockImagesArray)
                {
                    lockImage.enabled = false;
                }
                
                chapterImageGameObject.ChildrenAndSelf().OfComponent<UIEffect>().First().effectMode = EffectMode.None;
            }

            var currentChapter = _playerData.PersistentData.CurrentChapterInWorld[_worldsHorizontalScrollSnap.CurrentPage + 1] - 1;
            _chaptersHorizontalScrollSnap.ChangePage(currentChapter);
        }
        
        /// <summary>
        /// Metoda koja postavlja izgled UI-a za levele na osnovu broja levela u nekom chapteru nekog sveta
        /// WHY: Zato sto moze da se desi da 1 chapter ima manje od 5 levela i onda je potrebno to vizuelno predstaviti korisniku
        /// </summary>
        private void SetLevelsLook()
        {
            var chapters = _allWorldData[_worldsHorizontalScrollSnap.CurrentPage].Chapters;
            for (int i = 0; i < chapters.Count; i++)
            {
                var levelsImages = _chaptersImages[i].gameObject.AfterSelf().Children().OfComponent<Image>().ToArray();
                var levelsCount = chapters[i].Levels.Count;
                if (levelsCount == 5)
                {
                    foreach (var levelsImage in levelsImages)
                    {
                        levelsImage.gameObject.SetActive(true);
                    }
                }
                else
                {
                    for (int j = levelsImages.Length - 1; j >= levelsCount; j--)
                    {
                        levelsImages[j].gameObject.SetActive(false);
                    }
                }
            }
        }

        /// <summary>
        /// Metoda koja postavlja izgled progresa u UI za levele
        /// WHY: Zato sto se razlikuje broj otkljucanih levela od sveta do sveta
        /// </summary>
        private void SetLevelsProgress()
        {
            var unlockedChaptersArray = _playerData.PersistentData.ChaptersProgress[_worldsHorizontalScrollSnap.CurrentPage + 1]
                .Where(chapterState => chapterState == PersistentData.ChapterState.Unlocked || chapterState == PersistentData.ChapterState.IsReadyForUnlock)
                .ToArray();
            for (int i = 0; i < unlockedChaptersArray.Length; i++)
            {
                var levelsImagesEnumerable = _chaptersImages[i].gameObject.AfterSelf().Children().OfComponent<Image>();
                if (i == unlockedChaptersArray.Length - 1 && unlockedChaptersArray.Length < _playerData.PersistentData.ChaptersProgress[_worldsHorizontalScrollSnap.CurrentPage + 1].Count)
                {
                    foreach (var levelsImage in levelsImagesEnumerable)
                    {
                        levelsImage.color = _levelsImageUnlockedColor;
                    }
                }
                else
                {
                    foreach (var levelsImage in levelsImagesEnumerable)
                    {
                        levelsImage.color = _levelsImageFullColor;
                    }
                }
            }
        }

        /// <summary>
        /// Metoda koja postavlja vrednost trenutnog sveta u zavisnosti od toga kako igrac menja svetove u UI-u
        /// Zato sto je to neophodno da bi igrac mogao da ucita i pokrene zeljeni svet
        /// </summary>
        private void SetCurrentWorld() => _playerData.PersistentData.CurrentWorld.Value = _worldsHorizontalScrollSnap.CurrentPage + 1;

        /// <summary>
        /// Metoda koja postavlja vrednost trenutnog chaptera u zavisnosti od toga kako igrac menja svetove u UI-u i u zavisnosti od toga kako menja chaptere isto u UI-u
        /// Zato sto je to neophodno da bi igrac mogao da ucita i pokrene zeljeni chapter u zeljenom svetu
        /// </summary>
        private void SetCurrentChapter() => _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value] = _chaptersHorizontalScrollSnap.CurrentPage + 1;

        /// <summary>
        /// Metoda koja pali i gasi play dugme u zavisnosti od stanja u kome se nalazi trenutni chapter (otkljucan, spreman da se otkljuca, zakljucan)
        /// WHY: Zato sto svaki chapter ima svoje stanje i od tog stanja zavisi da li se prikazuje ili ne play dugme
        /// </summary>
        /// <param name="isEnabled">parametar na osnovu koga palimo i gasimo dugme</param>
        private void SetPlayButtonState(bool isEnabled) => _playButton.gameObject.SetActive(isEnabled);

        /// <summary>
        /// Metoda koja od Play dugmeta pravi Unlock dugme kako bi naterala korisnika da odgleda reklamu i nastavi dalje
        /// WHY: Zato sto zelimo da usporimo progres igraca, i ujedno da mu ponudimo resenje da ubrza progres
        /// </summary>
        /// <param name="isEnabled"></param>
        private void SetUnlockButton(bool isEnabled) => _unlockButton.gameObject.SetActive(isEnabled);

        #endregion
    }
}