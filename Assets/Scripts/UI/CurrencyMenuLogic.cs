using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UniRx;
using UnityEngine;

namespace Tunnel.UI
{
    public sealed class CurrencyMenuLogic
    {
        public IObservable<GameObject> GetInstantiatedCurrencyAmountElements(GameObject amountPrefab, GameObject animationsElements)
        {
            return Observable.FromCoroutine<GameObject>((observer, token) => InstantiateCurrencyAmountElements(observer, token, amountPrefab, animationsElements));
        }

        private IEnumerator InstantiateCurrencyAmountElements(IObserver<GameObject> observer, CancellationToken cancellationToken, GameObject amountPrefab, GameObject animationsElements)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                observer.OnError(new Exception(cancellationToken.ToString()));
                yield break;
            }
            
            var currencyAmountPrefab = UnityEngine.Object.Instantiate(amountPrefab, animationsElements.transform);
            currencyAmountPrefab.SetActive(false);
            yield return null;
            
            observer.OnNext(currencyAmountPrefab);
            observer.OnCompleted();
        }

        public IObservable<List<RectTransform>> GetInstantiatedAnimationsElements(RectTransform currencyObject, GameObject animationsElements)
        {
            return Observable.FromCoroutine<List<RectTransform>>((observer, token) => InstantiateAnimationElement(observer, token, currencyObject, animationsElements));
        }

        private IEnumerator InstantiateAnimationElement(IObserver<List<RectTransform>> observer, CancellationToken cancellationToken, RectTransform currencyObject, GameObject animationsElements)
        {
            List<RectTransform> temporaryInstantiatedAnimationElementsList = new List<RectTransform>();
            if (cancellationToken.IsCancellationRequested)
            {
                observer.OnError(new Exception(cancellationToken.ToString()));
                yield break;
            }
            
            for (int i = 0; i < 10; i++)
            {
                var instantiatedCurrencyObject = UnityEngine.Object.Instantiate(currencyObject, animationsElements.transform);
                instantiatedCurrencyObject.gameObject.SetActive(false);
                temporaryInstantiatedAnimationElementsList.Add(instantiatedCurrencyObject);
                yield return null;
            }
            observer.OnNext(temporaryInstantiatedAnimationElementsList);
            observer.OnCompleted();
        }
    }
}