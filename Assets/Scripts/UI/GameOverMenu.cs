﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using EasyMobile;
using Localization;
using RSG;
using Tunnel.Ads;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    #region Events
    
    public sealed class GameOverOneSecondFrame {}
    public sealed class GameOverTimerFinished {}
    public sealed class PlayerRevivedWithToken {}

    #endregion
    
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: GameOverMenu
    /// Skripta koja kontroliše ponašanje game over menija i njegovih dugmića
    /// </summary>
    public sealed class GameOverMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("UI Elements")]
        [SerializeField] private Transform _panel;
        [SerializeField] private Button _continueWithRVButton;
        [SerializeField] private Button _continueWithTokensButton;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Image _timerImage;
        [SerializeField] private TMP_Text _tokensAmountText;
        [SerializeField] private TMP_Text _secondsText;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _free;
        [SerializeField] private TMP_Text _noThanks;

        [Header("Scriptable Objects")]
        [SerializeField] private MetersSO _metersSo;
        [SerializeField] private GameData _gameData;
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private RectTransform _token;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;
        [SerializeField] private GameObject _poofVFXGameObject;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IConnectableObservable<long> _timerConnectableObservable;
        private IDisposable _connectionDisposable;
        private IState _rootState;
        private CurrencySpentAnimations _tokensSpentAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private List<RectTransform> _tokens;
        private TMP_Text _currencyAmountText;
        private ParticleSystem _poofVfxParticleSystem;
        private int _continueAfterDeathCostRemoteValue;

        /// <summary>
        /// Consts
        /// </summary>
        private readonly GameOverTimerFinished _gameOverTimerFinished = new GameOverTimerFinished();
        private readonly GameOverOneSecondFrame _gameOverOneSecondFrame = new GameOverOneSecondFrame();
        private const float TIMER_PERIOD = 1f;
        private const float TIMER_IMAGE_FILL_AMOUNT = 1f;
        private const long TIME_SHOWING_RESTART_BUTTON = 2;
        private const float FILL_AMOUNT_FACTOR = 0.2f;
        private const int SECONDS_FOR_TIMER = 5;
        private const float DELAY_PLAYER_REVIVING_WITH_TOKENS = 0.5f;
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType => Constants.CurrencyType.Token;

        public int CurrencyAmountEarned => default;

        public int CurrencyAmountSpent { get; private set; }

        public List<RectTransform> CurrenciesTransforms => _tokens;

        public RectTransform CurrencyAnimationStartObject => _topMenuTokensPosition;

        public RectTransform CurrencyAnimationEndObject => _continueWithTokensButton.GetComponent<RectTransform>();

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        private static IObservable<RewardAdsArgs> RewardedAdSkippedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<RewardedAdNetwork, AdPlacement>, RewardAdsArgs>(
                    h => (network, placement) => h(new RewardAdsArgs {RewardedAdNetwork = network, AdPlacement = placement}),
                    h => Advertising.RewardedAdSkipped += h, h => Advertising.RewardedAdSkipped -= h);
            }
        }

        private IObservable<long> Timer
        {
            get
            {
                return Observable
                    .Timer(TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(TIMER_PERIOD))
                    .Skip(1)
                    .Select(secondsPassed => SECONDS_FOR_TIMER - secondsPassed)
                    .TakeWhile(seconds => seconds >= 0);
            }
        }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _continueWithRVButton = gameObject.Descendants().OfComponent<Button>().First(s => s.name.Contains("ContinueWithRVButton"));
            _continueWithTokensButton = gameObject.Descendants().OfComponent<Button>().First(s => s.name.Contains("ContinueWithTokenButton"));
            _restartButton = gameObject.Descendants().OfComponent<Button>().First(s => s.name.Contains("RestartButton"));
            _tokensAmountText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("ContinueWithTokenButtonText"));
            _secondsText = gameObject.Descendants().OfComponent<TMP_Text>().First(s => s.name.Contains("Seconds"));
            _timerImage = gameObject.Descendants().OfComponent<Image>().First(s => s.name.Contains("Timer"));
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _poofVFXGameObject = gameObject.Descendants().First(x => x.name.Contains("CFX_Poof"));
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            _title = gameObject.Descendants().First(s => s.name.Contains("TitleText")).GetComponent<TMP_Text>();
            _free = gameObject.Descendants().First(s => s.name.Contains("ContinueButtonText")).GetComponent<TMP_Text>();
            _noThanks = gameObject.Descendants().First(s => s.name.Contains("RestartButtonText")).GetComponent<TMP_Text>();
            
            _metersSo = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.METERS, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<MetersSO>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        private void OnDisable()
        {
            _rootState.Exit();
        }

        #endregion





        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                    .Enter(state => HandleEnteringShownMenuState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            InstantiateCurrencies();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void HandleEnteringShownMenuState()
        {
            _restartButton.gameObject.SetActive(false);
            SetupTimerElements();
            SetupTimerStream();
            RegisterTimerListener(Unit.Default);
            PlayTimer();
            AdsClient.Instance.LoadRewardedAdsForPlacement(AdPlacement.Default);
            AdsClient.Instance.LoadInterstitialAdsForPlacement(AdPlacement.GameOver);
            AdsClient.Instance.LoadInterstitialAdsForPlacement(AdPlacement.PlacementWithName(Constants.AdsPlacements.LEVEL_RESTART));
        }

        private void SetInitialValues()
        {
            _tokens = new List<RectTransform>();
            _currencyMenuLogic = new CurrencyMenuLogic();
            _tokensSpentAnimations = new CurrencySpentAnimations(this);
            _poofVfxParticleSystem = _poofVFXGameObject.GetComponent<ParticleSystem>();
            _continueAfterDeathCostRemoteValue = RemoteConfigClient.Instance.ContinueAfterDeathCost;
            _tokensAmountText.text = _continueAfterDeathCostRemoteValue.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.Continue.Value);
            _free.SetText(_languageClient.CurrentLanguage.Free.Value);
            _noThanks.SetText(_languageClient.CurrentLanguage.NoThanks.Value);
        }

        private void RegisterButtonsListeners()
        {
            _continueWithRVButton.OnClickAsObservable()
                .Subscribe(HandleContinueWithAdButtonEvent)
                .AddTo(this);
            _continueWithTokensButton.OnClickAsObservable()
                .Subscribe(HandleContinueWithTokenButtonEvent)
                .AddTo(this);
            _restartButton.OnClickAsObservable().
                Subscribe(HandleRestartButtonEvent)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            RewardedAdSkippedAsObservable
                .Subscribe(rewardAdsArgs => HandleRewardedAdSkippedEvent(rewardAdsArgs.RewardedAdNetwork, rewardAdsArgs.AdPlacement))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesRemovingFromTopMenu)
                .Subscribe(HandleCurrencyRemovingFromTopMenu)
                .AddTo(this);
        }

        private void InstantiateCurrencies()
        {
            if (_tokens == null || _tokens.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(tokens => _tokens = tokens)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            Observable.FromCoroutine(PrepareAnimation)
                .SelectMany(_tokensSpentAnimations.Animate)
                .SelectMany(HandleFinishingAnimation)
                .Subscribe()
                .AddTo(this);

#if UNITY_ANDROID || UNITY_IOS
            Analytics.WarmAppGamesAnalytics.LogSpentVirtualCurrency(nameof(_playerData.PersistentData.Tokens), _continueAfterDeathCostRemoteValue);
#endif
        }

        private IEnumerator PrepareAnimation()
        {
            CurrencyAmountSpent = _continueAfterDeathCostRemoteValue;
            SetButtonActivity(false);
            yield return null;
        }

        private void SetButtonActivity(bool value)
        {
            _continueWithTokensButton.GetComponent<Animator>().enabled = value;
            _continueWithTokensButton.interactable = value;
            _continueWithRVButton.interactable = value;
            _restartButton.interactable = value;
        }

        private IEnumerator HandleFinishingAnimation()
        {
            SetButtonActivity(true);
            yield return new WaitForSeconds(DELAY_PLAYER_REVIVING_WITH_TOKENS);
            _playerData.IsPlayerDead = false;
        }

        /// <summary>
        /// Metoda koja izvrsava osnovne funkcije kada se klikne na bilo koje dugme u ovom meniju
        /// WHY: Postovanje DRY principa
        /// </summary>
        private void ButtonsBaseFunctions()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _metersSo.Value = 0;
            PauseTimer();
        }

        #endregion





        #region Event Listeners

        /// <summary>
        /// Metoda koja se izvrsava kada igrac pritisne dugme za nastavak igre uz pomoc RV-a
        /// WHY: Mnogo bolja kontrola nad tim sta se poziva pritiskom na neko dugme
        /// </summary>
        /// <param name="unit"></param>
        private void HandleContinueWithAdButtonEvent(Unit unit)
        {
            ButtonsBaseFunctions();
            AdsClient.Instance.ShowRewardedVideo(AdPlacement.Default);
        }

        /// <summary>
        /// Metoda koja se izvrsava kada igrac pritisne dugme za nastavak igre uz pomoc tokena
        /// WHY: Mnogo bolja kontrola nad tim sta se poziva pritiskom na neko dugme
        /// </summary>
        /// <param name="unit"></param>
        private void HandleContinueWithTokenButtonEvent(Unit unit)
        {
            ButtonsBaseFunctions();
            CalculateTokens();
        }

        private void CalculateTokens()
        {
            if (_playerData.PersistentData.Tokens.Value >= _continueAfterDeathCostRemoteValue)
            {
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            else
            {
                MessageBroker.Default.Publish(new TokensMenuCalled());
            }
        }

        /// <summary>
        /// Metoda koja stopira Observable stream za timer da emituje elemente
        /// WHY: Zato sto to treba da se desi kada korisnik izabere jedno od dva dugmeta
        /// </summary>
        private void PauseTimer() => _connectionDisposable.Dispose();

        /// <summary>
        /// Metoda koja se izvrsava kada igrac pritisne dugme "No Thanks"
        /// WHY: Mnogo bolja kontrola nad tim sta se poziva pritiskom na neko dugme
        /// </summary>
        /// <param name="unit"></param>
        private void HandleRestartButtonEvent(Unit unit)
        {
            ButtonsBaseFunctions();
            AdsClient.Instance.ShowInterstitialAds(AdPlacement.PlacementWithName(Constants.AdsPlacements.LEVEL_RESTART));
            MessageBroker.Default.Publish(new ScoreMenuCalled());
        }

        /// <summary>
        /// Metoda koja postavlja pocetne vrednosti za UI elemente
        /// WHY: Zato sto odvajamo UI od logike
        /// </summary>
        private void SetupTimerElements()
        {
            _secondsText.text = SECONDS_FOR_TIMER.ToString();
            _timerImage.fillAmount = TIMER_IMAGE_FILL_AMOUNT;
        }

        /// <summary>
        /// Metoda koja potavlja Observable stream za timer
        /// WHY: Zato sto je to mehanizam koji koristimo za odbrojavanje vremena kada korisnik ostane potpuno bez stitova i pogine
        /// </summary>
        private void SetupTimerStream() => _timerConnectableObservable = Timer.Publish();

        /// <summary>
        /// Metoda koja registruje osluskivac za Observable stream za timer
        /// WHY: Zato sto se tu obradjuju akcije nakon isteka svake sekunde u timeru
        /// </summary>
        /// <param name="unit"></param>
        private void RegisterTimerListener(Unit unit)
        {
            _timerConnectableObservable
                .Subscribe(HandleTimerBehaviour,
                    () => HandleRestartButtonEvent(Unit.Default));
        }

        /// <summary>
        /// Metoda koja pokrece Observable stream za timer
        /// WHY: Zato sto je stream samo pripremljen prethodno, i potrebno ga je pustiti
        /// Takodje, ova metoda omogucava i da se stream pauzira
        /// </summary>
        private void PlayTimer() => _connectionDisposable = _timerConnectableObservable.Connect();

        private void HandleTimerBehaviour(long seconds)
        {
            AnimateTimer(seconds);
            FireTimerEvents(seconds);
            if (seconds == TIME_SHOWING_RESTART_BUTTON)
            {
                _restartButton.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Metoda koja animira sekunde na tajmeru
        /// WHY: Zato sto moramo korisniku da damo validan feedback o tome sta se desava
        /// </summary>
        /// <param name="seconds"></param>
        private void AnimateTimer(long seconds)
        {
            _secondsText.text = seconds.ToString();
            _timerImage.fillAmount = seconds * FILL_AMOUNT_FACTOR;
        }

        /// <summary>
        /// Metoda koja podesava vrednosti svojstava koja koristimo kao evente koji obavestavaju klasu za kontrolu zvuka da pusti odgovarajuce zvuke
        /// Zato sto zelim da postujem SRP princip i zato sto se razlikuju zvuci koji se pustaju i zato je potrebno imati i razlicite evente koji se okidaju pod uslovom 
        /// </summary>
        /// <param name="seconds">Parametar koji kontrolise uslove pod kojima se okidaju odgovarajuci eventi</param>
        private void FireTimerEvents(long seconds)
        {
            if (seconds == 0)
            {
                MessageBroker.Default.Publish(_gameOverTimerFinished);
            }
            else
            {
                MessageBroker.Default.Publish(_gameOverOneSecondFrame);
            }
        }

        /// <summary>
        /// Metoda koja reaguje na dogadjaj kada igrac prekine gledanje rewarded videa
        /// </summary>
        /// <param name="rewardedAdNetwork">koja ad network je u pitanju</param>
        /// <param name="adPlacement">koji ad placement je u pitanju</param>
        private void HandleRewardedAdSkippedEvent(RewardedAdNetwork rewardedAdNetwork, AdPlacement adPlacement) => PlayTimer();
        
        private void HandleCurrencyRemovingFromTopMenu(object eventType)
        {
            _poofVfxParticleSystem.Play();
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<GameOverMenu>
        {
            
        }

        #endregion
    }
}
