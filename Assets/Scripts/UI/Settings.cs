﻿using System.Collections.Generic;
using System.Linq;
using Core.Events;
using EasyFeedback;
using Localization;
using RSG;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME OBJECT: SettingsMenu
    /// DESCRIPTION: Klasa koja kontrolise ponasanje settings menija
    /// </summary>
    public sealed class Settings : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Sprite _highlightedButtonSprite;
        private Sprite _normalButtonSprite;
        private SpriteState _spriteState;
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Animations elements
        /// </summary>
        private CurrencyBoughtAnimations _currencyBoughtAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private int _socialNetworksRewardRemoteValue;
        private IState _rootState;
        private List<RectTransform> _coins;
        private List<RectTransform> _tokens;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string STRING_FORMAT_F1 = Constants.StringFormats.F1;
        private const string INDICATOR_NAME = Constants.GameObjectsNames.INDICATOR;
        private const float GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE = Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE;
        private const float ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE = Constants.Gyroscope.ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE;
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Space] [Header("Toggles")] 
        [SerializeField] private Toggle _audioToggle;
        [SerializeField] private Toggle _notificationsToggle;
        [SerializeField] private Toggle _vibrationToggle;
        [SerializeField] private Toggle _gyroscopeControlsToggle;

        [Space] [Header("UI Top Buttons")] 
        [SerializeField] private Button _generalSettingsButton;
        [SerializeField] private Button _supportButton;
        [SerializeField] private Button _privacyPolicyButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private GameObject _panel;
        [SerializeField] private GameObject _facebookIndicator;
        [SerializeField] private GameObject _twitterIndicator;
        [SerializeField] private GameObject _instagramIndicator;
        [SerializeField] private GameObject _discordIndicator;
        [SerializeField] private GameObject _rateUsIndicator;
        [SerializeField] private GameObject _supportIndicator;
        [SerializeField] private GameObject _supportSubmitIndicator;

        [Space] [Header("UI General Settings Buttons")]
        [SerializeField] private Button _rateUsButton;
        [SerializeField] private TMP_Text _rateUsText;
        [SerializeField] private GameObject _rateUsReward;
        [SerializeField] private Button _signInOutGoogleGameServicesButton;

        [Space] [Header("UI Support Buttons")] 
        [SerializeField] private Button _supportSubmitButton;
        [SerializeField] private TMP_Text _supportSubmitText;
        [SerializeField] private GameObject _supportSubmitReward;

        [Space] [Header("UI Privacy Policy Buttons")] 
        [SerializeField] private Button _privacyPolicyLinkButton;
        [SerializeField] private Button _termsOfServiceButton;
        [SerializeField] private Button _gdprButton;

        [Space] [Header("Social Media Buttons")]
        [SerializeField] private Button _facebookButton;
        [SerializeField] private Button _twitterButton;
        [SerializeField] private Button _instagramButton;
        [SerializeField] private Button _discordButton;
        [SerializeField] private GameObject _facebookReward;
        [SerializeField] private GameObject _twitterReward;
        [SerializeField] private GameObject _instagramReward;
        [SerializeField] private GameObject _discordReward;
        [SerializeField] private TMP_Text _facebookRewardText;
        [SerializeField] private TMP_Text _twitterRewardText;
        [SerializeField] private TMP_Text _instagramRewardText;
        [SerializeField] private TMP_Text _discordRewardText;

        [Space] [Header("Buttons Invoke Elements")] 
        [SerializeField] private GameObject _generalSettingsGameObject;
        [SerializeField] private GameObject _supportGameObject;
        [SerializeField] private GameObject _privacyPolicyGameObject;
        [SerializeField] private FeedbackForm _feedbackForm;
        
        [Space] [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Space] [Header("Gyroscope Elements")]
        [SerializeField] private GameObject _sensitivityHolder;
        [SerializeField] private GameObject _rotationAccelerationHolder;
        [SerializeField] private Slider _sensitivitySlider;
        [SerializeField] private Slider _rotationAccelerationSlider;
        [SerializeField] private TMP_Text _sensitivityValueText;
        [SerializeField] private TMP_Text _rotationAccelerationValueText;

        [Space] [Header("Localization Elements")] 
        [SerializeField] private TMP_Text _settingsMenuTitle;
        [SerializeField] private TMP_Text _generalTabTitle;
        [SerializeField] private TMP_Text _supportTabTitle;
        [SerializeField] private TMP_Text _privacyTabTitle;
        [SerializeField] private TMP_Text _audioOption;
        [SerializeField] private TMP_Text _notificationOption;
        [SerializeField] private TMP_Text _vibrationOption;
        [SerializeField] private TMP_Text _gyroscopeOption;
        [SerializeField] private TMP_Text _sensitivityOption;
        [SerializeField] private TMP_Text _rotationAccelerationOption;
        [SerializeField] private TMP_Text _summaryText;
        [SerializeField] private TMP_Text _summaryPlaceholderText;
        [SerializeField] private TMP_Text _emailText;
        [SerializeField] private TMP_Text _emailPlaceholderText;
        [SerializeField] private TMP_Text _messageText;
        [SerializeField] private TMP_Text _messagePlaceholderText;
        [SerializeField] private TMP_Text _submitText;
        [SerializeField] private TMP_Text _successfullySubmittedText;
        [SerializeField] private TMP_Text _privacyPolicyText;
        [SerializeField] private TMP_Text _termsOfServicesText;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _coin;
        [SerializeField] private RectTransform _token;
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType { get; private set; }

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent => default;

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject { get; private set; }

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion
        

        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            var allRectTransforms = gameObject.Descendants().OfComponent<RectTransform>();
            var allTMPTexts = gameObject.Descendants().OfComponent<TMP_Text>();
            _audioToggle = allRectTransforms.First(s => s.name.Equals("AudioToggle")).GetComponent<Toggle>();
            _notificationsToggle = allRectTransforms.First(s => s.name.Equals("NotificationToggle")).GetComponent<Toggle>();
            _vibrationToggle = allRectTransforms.First(s => s.name.Equals("VibrationToggle")).GetComponent<Toggle>();
            _gyroscopeControlsToggle = allRectTransforms.First(s => s.name.Equals("GyroscopeToggle")).GetComponent<Toggle>();
            _generalSettingsButton = allRectTransforms.First(s => s.name.Equals("GeneralButton")).GetComponent<Button>();
            _supportButton= allRectTransforms.First(s => s.name.Equals("SupportButton")).GetComponent<Button>();
            _privacyPolicyButton = allRectTransforms.First(s => s.name.Equals("PrivacyPolicyButton")).GetComponent<Button>();
            _rateUsButton = allRectTransforms.First(s => s.name.Equals("RateUs")).GetComponent<Button>();
            _rateUsText = allRectTransforms.First(s => s.name.Equals("RateUsText")).GetComponent<TMP_Text>();
            _rateUsReward = allRectTransforms.First(s => s.name.Equals("RateUsReward")).gameObject;
            _signInOutGoogleGameServicesButton = allRectTransforms.First(s => s.name.Contains("SignInOut")).GetComponent<Button>();
            _supportSubmitButton = allRectTransforms.First(s => s.name.Contains("SubmitButton")).GetComponent<Button>();
            _supportSubmitText = allRectTransforms.First(s => s.name.Contains("SubmitButtonText")).GetComponent<TMP_Text>();
            _supportSubmitReward = allRectTransforms.First(s => s.name.Contains("SupportReward")).gameObject;
            _privacyPolicyLinkButton = allRectTransforms.First(s => s.name.Contains("PrivacyPolicyLinkButton")).GetComponent<Button>();
            _termsOfServiceButton = allRectTransforms.First(s => s.name.Contains("TermsOfServiceLinkButton")).GetComponent<Button>();
            _gdprButton = allRectTransforms.First(s => s.name.Contains("GDPRMenuButton")).GetComponent<Button>();
            _facebookButton = allRectTransforms.First(s => s.name.Contains("Facebook")).GetComponent<Button>();
            _twitterButton = allRectTransforms.First(s => s.name.Contains("Twitter")).GetComponent<Button>();
            _instagramButton = allRectTransforms.First(s => s.name.Contains("Instagram")).GetComponent<Button>();
            _discordButton = allRectTransforms.First(s => s.name.Contains("Discord")).GetComponent<Button>();
            _facebookReward = allRectTransforms.First(s => s.name.Contains("FacebookReward")).gameObject;
            _twitterReward = allRectTransforms.First(s => s.name.Contains("TwitterReward")).gameObject;
            _instagramReward = allRectTransforms.First(s => s.name.Contains("InstagramReward")).gameObject;
            _discordReward = allRectTransforms.First(s => s.name.Contains("DiscordReward")).gameObject;
            _facebookRewardText = _facebookReward.GetComponentInChildren<TMP_Text>();
            _twitterRewardText = _twitterReward.GetComponentInChildren<TMP_Text>();
            _instagramRewardText = _instagramReward.GetComponentInChildren<TMP_Text>();
            _discordRewardText = _discordReward.GetComponentInChildren<TMP_Text>();
            _generalSettingsGameObject = allRectTransforms.First(s => s.name.Equals("GeneralSettings")).gameObject;
            _supportGameObject = allRectTransforms.First(s => s.name.Equals("Support")).gameObject;
            _privacyPolicyGameObject = allRectTransforms.First(s => s.name.Equals("Privacy")).gameObject;
            _feedbackForm = allRectTransforms.First(s => s.name.Equals("Support")).GetComponent<FeedbackForm>();
            _sensitivityHolder = allRectTransforms.First(s => s.name.Equals("SensitivityHolder")).gameObject;
            _rotationAccelerationHolder = allRectTransforms.First(s => s.name.Equals("RotationAccelerationHolder")).gameObject;
            _sensitivitySlider = allRectTransforms.First(s => s.name.Equals("SensitivitySlider")).GetComponent<Slider>();
            _rotationAccelerationSlider = allRectTransforms.First(s => s.name.Equals("RotationAccelerationSlider")).GetComponent<Slider>();
            _sensitivityValueText = allRectTransforms.First(s => s.name.Equals("SensitivityValueText")).GetComponent<TMP_Text>();
            _rotationAccelerationValueText = allRectTransforms.First(s => s.name.Equals("RotationAccelerationValueText")).GetComponent<TMP_Text>();
            _settingsMenuTitle = allTMPTexts.First(x => x.name.Contains("TitleText"));
            _generalTabTitle = allTMPTexts.First(x => x.name.Contains("GeneralButtonText"));
            _supportTabTitle = allTMPTexts.First(x => x.name.Contains("SupportButtonText"));
            _privacyTabTitle = allTMPTexts.First(x => x.name.Contains("PrivacyPolicyButtonText"));
            _audioOption = allTMPTexts.First(x => x.name.Contains("AudioToggleText"));
            _notificationOption = allTMPTexts.First(x => x.name.Contains("NotificationToggleText"));
            _vibrationOption = allTMPTexts.First(x => x.name.Contains("VibrationToggleText"));
            _gyroscopeOption = allTMPTexts.First(x => x.name.Contains("GyroscopeToggleText"));
            _sensitivityOption = allTMPTexts.First(x => x.name.Contains("SensitivityTitleText"));
            _rotationAccelerationOption = allTMPTexts.First(x => x.name.Contains("RotationAccelerationTitleText"));
            _summaryText = allTMPTexts.First(x => x.name.Contains("SummaryText"));
            _summaryPlaceholderText = allTMPTexts.First(x => x.name.Contains("SummaryPlaceholderText"));
            
            _emailText = allTMPTexts.First(x => x.name.Contains("EmailText"));
            _emailPlaceholderText = allTMPTexts.First(x => x.name.Contains("EmailPlaceholderText"));
            _messageText = allTMPTexts.First(x => x.name.Contains("MessageText"));
            _messagePlaceholderText = allTMPTexts.First(x => x.name.Contains("MessagePlaceholderText"));
            _submitText = allTMPTexts.First(x => x.name.Contains("SubmitButtonText"));
            _successfullySubmittedText = allTMPTexts.First(x => x.name.Contains("FeedbackDescription"));
            _privacyPolicyText = allTMPTexts.First(x => x.name.Contains("PrivacyPolicyLinkButtonText"));
            _termsOfServicesText = allTMPTexts.First(x => x.name.Contains("TermsOfServiceLinkButtonText"));
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _coin = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = gameObject.Descendants().First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _facebookIndicator = _facebookButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _twitterIndicator = _twitterButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _instagramIndicator = _instagramButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _discordIndicator = _discordButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _rateUsIndicator = _rateUsButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _supportIndicator = _supportButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _supportSubmitIndicator = _supportSubmitButton.gameObject.Descendants().First(x => x.name.Equals(INDICATOR_NAME)).gameObject;
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion
        
        
        


        #region Custom Methods

        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .Enter(state => HandleEnteringShownState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            InitialState();
            SetTheElements();
            SetLocalization();
            RegisterObservableListeners();
            RegisterToggleListeners();
            RegisterButtonListeners();
            RegisterSliderListeners();
            InstantiateCurrencies();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void InitialState()
        {
            _currencyBoughtAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            _socialNetworksRewardRemoteValue = RemoteConfigClient.Instance.SocialNetworksReward;
            
            // set toggle buttons
            _audioToggle.isOn = _playerData.PersistentData.IsAudioEnabled.Value;
            _notificationsToggle.isOn = _playerData.PersistentData.AreNotificationsEnabled.Value;
            _vibrationToggle.isOn = _playerData.PersistentData.IsVibrationEnabled.Value;
            _gyroscopeControlsToggle.isOn = _playerData.PersistentData.IsGyroscopeEnabled.Value;

            // get sprites
            _spriteState = _generalSettingsButton.spriteState;
            _highlightedButtonSprite = _spriteState.highlightedSprite;
            _normalButtonSprite = _spriteState.disabledSprite;
            
            // set gyroscope values
            _sensitivitySlider.value = (GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE - _playerData.PersistentData.GyroscopeSensitivity.Value) *
                                       Constants.Gyroscope.GYROSCOPE_SENSITIVITY_SLIDER_COMPONENT_FACTOR;
            _rotationAccelerationSlider.value = _playerData.PersistentData.RotationAccelerationFactor.Value * Constants.Gyroscope.ROTATION_ACCELERATION_SLIDER_COMPONENT_FACTOR;
            _sensitivityValueText.text = (_sensitivitySlider.value * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
            _rotationAccelerationValueText.text = (_rotationAccelerationSlider.value * Constants.Gyroscope.ROTATION_ACCELERATION_VALUE_TEXT_FACTOR).ToString(STRING_FORMAT_F1);
            
            // set rewards
            _facebookRewardText.text = _twitterRewardText.text = _instagramRewardText.text = _discordRewardText.text = _socialNetworksRewardRemoteValue.ToString();
        }

        private void SetTheElements()
        { 
            if (_supportGameObject.activeInHierarchy)
            {
                _generalSettingsButton.image.sprite = _normalButtonSprite;
                _supportButton.image.sprite = _highlightedButtonSprite;
                _privacyPolicyButton.image.sprite = _normalButtonSprite;
            }
            else if (_privacyPolicyGameObject.activeInHierarchy)
            {
                _generalSettingsButton.image.sprite = _normalButtonSprite;
                _supportButton.image.sprite = _normalButtonSprite;
                _privacyPolicyButton.image.sprite = _highlightedButtonSprite;
            }
            else
            {
                _generalSettingsButton.image.sprite = _highlightedButtonSprite;
                _supportButton.image.sprite = _normalButtonSprite;
                _privacyPolicyButton.image.sprite = _normalButtonSprite;
            }
        }

        private void SetLocalization()
        {
            _settingsMenuTitle.SetText(_languageClient.CurrentLanguage.Settings.Value);
            _generalTabTitle.SetText(_languageClient.CurrentLanguage.General.Value);
            _supportTabTitle.SetText(_languageClient.CurrentLanguage.Support.Value);
            _privacyTabTitle.SetText(_languageClient.CurrentLanguage.Privacy.Value);
            _audioOption.SetText(_languageClient.CurrentLanguage.Audio.Value);
            _notificationOption.SetText(_languageClient.CurrentLanguage.Notification.Value);
            _vibrationOption.SetText(_languageClient.CurrentLanguage.Vibration.Value);
            _gyroscopeOption.SetText(_languageClient.CurrentLanguage.GyroscopeControls.Value);
            _sensitivityOption.SetText(_languageClient.CurrentLanguage.Sensitivity.Value);
            _rotationAccelerationOption.SetText(_languageClient.CurrentLanguage.RotationAcceleration.Value);
            _summaryText.SetText(_languageClient.CurrentLanguage.Summary.Value);
            _summaryPlaceholderText.SetText(_languageClient.CurrentLanguage.EnterText.Value);
            _emailText.SetText(_languageClient.CurrentLanguage.Email.Value);
            _emailPlaceholderText.SetText(_languageClient.CurrentLanguage.EnterText.Value);
            _messageText.SetText(_languageClient.CurrentLanguage.Message.Value);
            _messagePlaceholderText.SetText(_languageClient.CurrentLanguage.EnterText.Value);
            _submitText.SetText(_languageClient.CurrentLanguage.Submit.Value);
            _successfullySubmittedText.SetText(_languageClient.CurrentLanguage.FeedbackSubmitted.Value + _languageClient.CurrentLanguage.ThankYou.Value);
            _privacyPolicyText.SetText(_languageClient.CurrentLanguage.PrivacyPolicy.Value);
            _termsOfServicesText.SetText(_languageClient.CurrentLanguage.TermsOfService.Value);
            _rateUsText.SetText(_languageClient.CurrentLanguage.RateUs.Value);
            // set sign in (out) button
            _signInOutGoogleGameServicesButton.GetComponentInChildren<TMP_Text>()
                .text = _playerData.PersistentData.IsPlayerSignedInGoogleGameServices.Value
                ? _languageClient.CurrentLanguage.SignOut.Value
                : _languageClient.CurrentLanguage.SignIn.Value;
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .Subscribe(HandleMenuShowedEvent)
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => _rootState.Exit())
                .AddTo(this);
            MessageBroker.Default.Receive<SettingsMenuFeedbackCalled>()
                .Subscribe(FeedbackCalledEventListener)
                .AddTo(this);
            MessageBroker.Default.Receive<FormSuccessfullySubmitted>()
                .Subscribe(ListenToFeedbackSubmittedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<ApplicationRated>()
                .Subscribe(ListenToApplicationRatedEvent)
                .AddTo(this);
            _playerData.PersistentData.IsFacebookLiked
                .Subscribe(isFacebookLiked => SetSocialNetworkButtonRewardAndIndicator(_facebookReward, _facebookIndicator, !isFacebookLiked))
                .AddTo(this);
            _playerData.PersistentData.IsTwitterFollowed
                .Subscribe(isTwitterFollowed => SetSocialNetworkButtonRewardAndIndicator(_twitterReward, _twitterIndicator, !isTwitterFollowed))
                .AddTo(this);
            _playerData.PersistentData.IsInstagramFollowed
                .Subscribe(isInstagramFollowed => SetSocialNetworkButtonRewardAndIndicator(_instagramReward, _instagramIndicator, !isInstagramFollowed))
                .AddTo(this);
            _playerData.PersistentData.IsDiscordJoined
                .Subscribe(isDiscordJoined => SetSocialNetworkButtonRewardAndIndicator(_discordReward, _discordIndicator, !isDiscordJoined))
                .AddTo(this);
            _playerData.PersistentData.IsApplicationRated
                .Subscribe(SetRateUsActivity)
                .AddTo(this);
            _playerData.PersistentData.IsFeedbackSubmitted
                .Subscribe(SetSupportActivity)
                .AddTo(this);
        }

        private void RegisterToggleListeners()
        {
            _audioToggle.OnValueChangedAsObservable()
                .Subscribe(ListenToAudioToggleEvent)
                .AddTo(this);
            _notificationsToggle.OnValueChangedAsObservable()
                .Subscribe(ListenToNotificationToggleEvent)
                .AddTo(this);
            _vibrationToggle.OnValueChangedAsObservable()
                .Subscribe(ListenToVibrationToggleEvent)
                .AddTo(this);
            _gyroscopeControlsToggle.OnValueChangedAsObservable()
                .Subscribe(ListenToGyroscopeToggleEvent)
                .AddTo(this);
        }

        private void RegisterButtonListeners()
        {
            _generalSettingsButton.OnClickAsObservable()
                .Subscribe(_ => ListenToGeneralSettingsAreaButtonEvent())
                .AddTo(this);
            _supportButton.OnClickAsObservable()
                .Subscribe(_ => ListenToSupportAreaButtonEvent())
                .AddTo(this);
            _privacyPolicyButton.OnClickAsObservable()
                .Subscribe(_ => ListenToPrivacyPolicyAreaButtonEvent())
                .AddTo(this);
            _rateUsButton.OnClickAsObservable()
                .Subscribe(_ => ListenToRateUsButtonEvent())
                .AddTo(this);
            _signInOutGoogleGameServicesButton.OnClickAsObservable()
                .Subscribe(_ => ListenToLoginGoogleGameServicesButtonEvent())
                .AddTo(this);
            _supportSubmitButton.OnClickAsObservable()
                .Subscribe(_ => ListenToSupportSubmitButtonEvent())
                .AddTo(this);
            _privacyPolicyLinkButton.OnClickAsObservable()
                .Subscribe(_ => ListenToPrivacyPolicyButtonEvent())
                .AddTo(this);
            _termsOfServiceButton.OnClickAsObservable()
                .Subscribe(_ => ListenToTermsOfServiceButtonEvent())
                .AddTo(this);
            _gdprButton.OnClickAsObservable()
                .Subscribe(_ => ListenToGdprButtonEvent())
                .AddTo(this);
            _facebookButton.OnClickAsObservable()
                .Subscribe(_ => ListenToFacebookButtonEvent())
                .AddTo(this);
            _twitterButton.OnClickAsObservable()
                .Subscribe(_ => ListenToTwitterButtonEvent())
                .AddTo(this);
            _instagramButton.OnClickAsObservable()
                .Subscribe(_ => ListenToInstagramButtonEvent())
                .AddTo(this);
            _discordButton.OnClickAsObservable()
                .Subscribe(_ => ListenToDiscordButtonEvent())
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(_ => ListenToExitButtonEvent())
                .AddTo(this);
        }

        private void RegisterSliderListeners()
        {
            _sensitivitySlider.OnValueChangedAsObservable()
                .Subscribe(ListenToGyroscopeSensitivitySlider)
                .AddTo(this);
            _rotationAccelerationSlider.OnValueChangedAsObservable()
                .Subscribe(ListenToRotationAccelerationSlider)
                .AddTo(this);
        }

        private void InstantiateCurrencies()
        {
            if (_coins == null || _coins.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                    .Subscribe(list => _coins = list)
                    .AddTo(this);
            }
            if (_tokens == null || _tokens.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(list => _tokens = list)
                    .AddTo(this);
            }

            if(_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void HandleEnteringShownState()
        {
            HandleMenuShowedEvent(Unit.Default);
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            switch (CurrencyType)
            {
                case Constants.CurrencyType.Coin:
                    CurrenciesTransforms = _coins;
                    CurrencyAnimationEndObject = _topMenuCoinsPosition;
                    break;
                case Constants.CurrencyType.Token:
                    CurrenciesTransforms = _tokens;
                    CurrencyAnimationEndObject = _topMenuTokensPosition;
                    break;
            }
            Observable.FromCoroutine(_currencyBoughtAnimations.Animate)
                .Subscribe(_ => MessageBroker.Default.Publish(new SettingsMenuCalled()))
                .AddTo(this);
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion
        
        
        


        #region Event Listeners

        private void HandleMenuShowedEvent(Unit unit)
        {
            MessageBroker.Default.Publish(new SettingsMenuOpened());
        }

        private void ListenToAudioToggleEvent(bool value)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsAudioEnabled.Value = value;
        }

        private void ListenToNotificationToggleEvent(bool value)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.AreNotificationsEnabled.Value = value;
        }

        private void ListenToVibrationToggleEvent(bool value)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsVibrationEnabled.Value = value;
        }

        private void ListenToGyroscopeToggleEvent(bool value)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsGyroscopeEnabled.Value = value;
            _sensitivityHolder.SetActive(value);
            _rotationAccelerationHolder.SetActive(value);
        }

        private void ListenToGeneralSettingsAreaButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _generalSettingsGameObject.SetActive(true);
            _supportGameObject.SetActive(false);
            _privacyPolicyGameObject.SetActive(false);
            SetTheElements();
        }

        private void ListenToSupportAreaButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _generalSettingsGameObject.SetActive(false);
            _supportGameObject.SetActive(true);
            _privacyPolicyGameObject.SetActive(false);
            SetTheElements();
        }

        private void ListenToPrivacyPolicyAreaButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _generalSettingsGameObject.SetActive(false);
            _supportGameObject.SetActive(false);
            _privacyPolicyGameObject.SetActive(true);
            SetTheElements();
        }

        private void ListenToRateUsButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            MessageBroker.Default.Publish(new RateUsButtonClicked());
#elif UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            ListenToApplicationRatedEvent(null);
#endif
        }

        private void ListenToLoginGoogleGameServicesButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (_playerData.PersistentData.IsPlayerSignedInGoogleGameServices.Value)
            {
                _signInOutGoogleGameServicesButton.GetComponentInChildren<TMP_Text>().text = _languageClient.CurrentLanguage.SignIn.Value;
                _playerData.PersistentData.IsPlayerSignedInGoogleGameServices.Value = false;
                _playerData.PersistentData.IsPlayerManuallySignedOut.Value = true;
            }
            else
            {
                _signInOutGoogleGameServicesButton.GetComponentInChildren<TMP_Text>().text = _languageClient.CurrentLanguage.SignOut.Value;
                _playerData.PersistentData.IsPlayerSignedInGoogleGameServices.Value = true;
                _playerData.PersistentData.IsPlayerManuallySignedOut.Value = false;
            }
        }

        private void ListenToSupportSubmitButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _feedbackForm.Submit();
        }

        private void ListenToPrivacyPolicyButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            OpenPrivacyPolicy();
        }

        private void ListenToTermsOfServiceButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            OpenTermsOfService();
        }

        private void ListenToGdprButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new GDPRMenuCalled());
        }

        private void ListenToFacebookButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (!_playerData.PersistentData.IsFacebookLiked.Value)
            {
                _playerData.PersistentData.IsFacebookLiked.Value = true;
                CurrencyAmountEarned = _socialNetworksRewardRemoteValue;
                CurrencyType = Constants.CurrencyType.Coin;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            OpenFB();
        }

        private void ListenToTwitterButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (!_playerData.PersistentData.IsTwitterFollowed.Value)
            {
                _playerData.PersistentData.IsTwitterFollowed.Value = true;
                CurrencyAmountEarned = _socialNetworksRewardRemoteValue;
                CurrencyType = Constants.CurrencyType.Coin;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            OpenTwitter();
        }

        private void ListenToInstagramButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (!_playerData.PersistentData.IsInstagramFollowed.Value)
            {
                _playerData.PersistentData.IsInstagramFollowed.Value = true;
                CurrencyAmountEarned = _socialNetworksRewardRemoteValue;
                CurrencyType = Constants.CurrencyType.Coin;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            OpenInstagram();
        }

        private void ListenToDiscordButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            if (!_playerData.PersistentData.IsDiscordJoined.Value)
            {
                _playerData.PersistentData.IsDiscordJoined.Value = true;
                CurrencyAmountEarned = _socialNetworksRewardRemoteValue;
                CurrencyType = Constants.CurrencyType.Coin;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
            OpenDiscord();
        }

        private void ListenToExitButtonEvent()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        private void ListenToGyroscopeSensitivitySlider(float sliderValue)
        {
            _playerData.PersistentData.GyroscopeSensitivity.Value = sliderValue < Constants.Gyroscope.SLIDER_MAX_VALUE
                ? (_sensitivitySlider.maxValue - sliderValue) * Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_VALUE_FACTOR
                : Constants.Gyroscope.GYROSCOPE_SENSITIVITY_BACKGROUND_MAX_VALUE;
            _sensitivityValueText.text = (sliderValue * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
        }

        private void ListenToRotationAccelerationSlider(float sliderValue)
        {
            _playerData.PersistentData.RotationAccelerationFactor.Value = sliderValue > Constants.Gyroscope.SLIDER_MIN_VALUE
                ? sliderValue * ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE
                : ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE;
            _rotationAccelerationValueText.text = (sliderValue * GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE).ToString(STRING_FORMAT_F1);
        }

        private void ListenToFeedbackSubmittedEvent(FormSuccessfullySubmitted formSuccessfullySubmitted)
        {
            if (!_playerData.PersistentData.IsFeedbackSubmitted.Value)
            {
                _supportSubmitReward.SetActive(false);
                _playerData.PersistentData.IsFeedbackSubmitted.Value = true;
                CurrencyAmountEarned = RemoteConfigClient.Instance.FeedbackReward;
                CurrencyType = Constants.CurrencyType.Token;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
        }

        private void ListenToApplicationRatedEvent(ApplicationRated applicationRated)
        {
            if (!_playerData.PersistentData.IsApplicationRated.Value)
            {
                _rateUsReward.SetActive(false);
                _playerData.PersistentData.IsApplicationRated.Value = true;
                CurrencyAmountEarned = RemoteConfigClient.Instance.RateUsReward;
                CurrencyType = Constants.CurrencyType.Token;
                _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
            }
        }

        private void SetSocialNetworkButtonRewardAndIndicator(GameObject reward, GameObject indicator, bool value)
        {
            reward.SetActive(value);
            indicator.SetActive(value);
        }

        private void SetRateUsActivity(bool isApplicationRated)
        {
            _rateUsReward.SetActive(!isApplicationRated);
            _rateUsIndicator.SetActive(!isApplicationRated);
        }

        private void SetSupportActivity(bool isFeedbackSubmitted)
        {
            _supportSubmitReward.SetActive(!isFeedbackSubmitted);
            _supportIndicator.SetActive(!isFeedbackSubmitted);
            _supportSubmitIndicator.SetActive(!isFeedbackSubmitted);
        }

        #endregion
        
        
        


        #region Custom Methods

        public void OpenFB()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.Facebook);
#endif
            Application.OpenURL(Constants.Links.FACEBOOK);
        }

        public void OpenTwitter()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.Twitter);
#endif
            Application.OpenURL(Constants.Links.TWITTER);
        }

        public void OpenInstagram()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.Instagram);
#endif
            Application.OpenURL(Constants.Links.INSTAGRAM);
        }

        public void OpenDiscord()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogOpenLinkEvent(Analytics.SocialNetworks.Discord);
#endif
            Application.OpenURL(Constants.Links.DISCORD);
        }

        public void OpenPrivacyPolicy()
        {
            Application.OpenURL(Constants.Links.PRIVACY_POLICY);
        }

        public void OpenTermsOfService()
        {
            Application.OpenURL(Constants.Links.TERMS_OF_SERVICE);
        }

        #endregion





        #region Event Listeners

        private void FeedbackCalledEventListener(SettingsMenuFeedbackCalled settingsMenuFeedbackCalled)
        {
            ListenToSupportAreaButtonEvent();
        }

        #endregion





	#region Factory

        public class Factory : PlaceholderFactory<Settings>
        {
            
        }

        #endregion
    }
}
