﻿using System.Collections.Generic;
using System.Linq;
using Core.Events;
using RSG;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Utilities;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: Canvas
    /// DESCRIPTION: Klasa koja kontrolise ponasanje prikaza menija u zavisnosti od stanja u kome se igrac trenutno nalazi
    /// </summary>
    public class UIInTutorialScene : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Menus")]
        [SerializeField] private List<MenuSO> _allMenus;
        [SerializeField] private List<GameObject> _allMenusPrefabs;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IState _rootState;
        private StaticMenuSO _topButtonsMenu;
        private GameObject _topButtonsMenuPrefab;
        private GameObject _instantiatedTopButtonsMenu;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE = Constants.MenusStatesNames.INITIALIZATION;
        private const string INITIAL_MENU_STATE = "InitialState";
        private const string TOP_BUTTONS_MENU_NAME = Constants.Menus.TOP_BUTTONS;

        #endregion
        
        
        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _allMenus = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.MENU, new[] {Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<MenuSO>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToList();
            _allMenusPrefabs = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new[] {Constants.Editor.Paths.MENUS_PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToList();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Start()
        {
            SetMenusStates();
            _rootState.ChangeState(INITIALIZATION_STATE);
        }

        private void OnDestroy()
        {
            _rootState.Exit();
        }
        
        #endregion





        #region Custom Methods

        

        private void SetMenusStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE)
                .Enter(_ => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(INITIAL_MENU_STATE)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(TOP_BUTTONS_MENU_NAME)
                .Enter(_ => SetUpMenu(_topButtonsMenu, _topButtonsMenuPrefab, ref _instantiatedTopButtonsMenu))
                .Exit(_ => HideMenu(_topButtonsMenu))
                .End()
                .Build();
        }

        /// <summary>
        /// Metoda koja instancira menu ukoliko nije instanciran
        /// WHY: Zato sto instanciramo meni samo ako korisnik izrazi potrebu za njim i ukoliko je menu vec instanciran, onda nema potrebe ponovo instancirati isti meni
        /// </summary>
        /// <param name="menu">meni koji pozivamo</param>
        /// <param name="menuPrefab">objekat menija koji instanciramo i prikazujemo korisniku</param>
        /// <param name="instantiatedMenuPrefab">referenca na instancirani objekat menija</param>
        private void SetUpMenu(MenuSO menu, GameObject menuPrefab, ref GameObject instantiatedMenuPrefab)
        {
            Assert.IsNotNull(menu);
            Assert.IsNotNull(menuPrefab);
            InstantiateMenuPrefab(menuPrefab, ref instantiatedMenuPrefab);
            if (menu == _topButtonsMenu)
            {
                SetUpTopButtonsMenu(instantiatedMenuPrefab);
            }
            ShowMenu(menu);
        }

        private void InstantiateMenuPrefab(GameObject menuPrefab, ref GameObject instantiatedMenuPrefab)
        {
            if (instantiatedMenuPrefab == null)
            {
                instantiatedMenuPrefab = Instantiate(menuPrefab, transform);
            }
        }

        private void SetUpTopButtonsMenu(GameObject instantiatedTopButtonsMenu)
        {
            var descendantsEnumerable = instantiatedTopButtonsMenu.Descendants();
            descendantsEnumerable.First(x => x.name.Equals("SettingsButton")).SetActive(false);
            descendantsEnumerable.First(x => x.name.Equals("EnergyInfo")).SetActive(false);
            descendantsEnumerable.First(x => x.name.Equals("LevelsDifficultyButton")).SetActive(false);
        }

        private void ShowMenu(MenuSO menu)
        {
            if (!menu.IsMenuShown)
            {
                menu.Show();
            }
        }

        private void HideMenu(MenuSO menu)
        {
            if (menu.IsMenuShown)
            {
                menu.Hide();
            }
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            RegisterObservableListeners();
        }

        private void SetInitialValues()
        {
            _topButtonsMenu = _allMenus.Find(x => x.name.Contains(TOP_BUTTONS_MENU_NAME)) as StaticMenuSO;
            _topButtonsMenuPrefab = _allMenusPrefabs.Find(x => x.name.Contains(TOP_BUTTONS_MENU_NAME));
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<TopButtonsMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(TOP_BUTTONS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ShouldLoadMainMenuScene>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
        }

        #endregion
    }
}
