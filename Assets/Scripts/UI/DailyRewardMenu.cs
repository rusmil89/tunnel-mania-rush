﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIEffects;
using Core.Events;
using Localization;
using RSG;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.PlayerData;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using Unity.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: DailyRewardMenu
    /// DESCRIPTION: Klasa koja kontrolise ponasanje menija koji dodeljuje korisniku nagrade svaki dan kada se bude vratio u igru
    /// </summary>
    public class DailyRewardMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scriptable Objects")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("UI Objects")]
        [SerializeField] private Transform _panel;
        [SerializeField] private TMP_Text _day01AmountText;
        [SerializeField] private TMP_Text _day02AmountText;
        [SerializeField] private TMP_Text _day03AmountText;
        [SerializeField] private TMP_Text _day04AmountText;
        [SerializeField] private TMP_Text _day05AmountText;
        [SerializeField] private TMP_Text _day06AmountText;
        [SerializeField] private TMP_Text _day07Amount01Text;
        [SerializeField] private TMP_Text _day07Amount02Text;
        [SerializeField] private TMP_Text _day07Amount03Text;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _day01Title;
        [SerializeField] private TMP_Text _day02Title;
        [SerializeField] private TMP_Text _day03Title;
        [SerializeField] private TMP_Text _day04Title;
        [SerializeField] private TMP_Text _day05Title;
        [SerializeField] private TMP_Text _day06Title;
        [SerializeField] private TMP_Text _day07Title;
        [SerializeField] private Button _exitButton;
        [SerializeField] private List<GameObject> _dayXPack;

        [Header("Assets")] 
        [SerializeField] private Sprite _collectedDaySprite;
        [SerializeField] private Sprite _activeDaySprite;
        [SerializeField] private UIShiny _uiShinyPreset;
        
        [Header("Animations Elements")]
        [SerializeField] private RectTransform _coin;
        [SerializeField] private RectTransform _token;
        [SerializeField] private RectTransform _energy;
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private RectTransform _topMenuEnergyPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IDisposable _activeButtonDisposable;
        private DailyRewardMenuAnimations _dailyRewardMenuAnimations;
        private CurrencyBoughtAnimations _currencyBoughtAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private List<Button> _dayXButton;
        private List<Image> _dayXImage;
        
        /// <summary>
        /// Animations elements
        /// </summary>
        private IState _rootState;

        private List<RectTransform> _tokens;
        private List<RectTransform> _energies;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const int DAY_1_INDEX = 0;
        private const int DAY_2_INDEX = 1;
        private const int DAY_3_INDEX = 2;
        private const int DAY_4_INDEX = 3;
        private const int DAY_5_INDEX = 4;
        private const int DAY_6_INDEX = 5;
        private const int DAY_7_INDEX = 6;
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;

        #endregion





        #region Properties

        public Constants.CurrencyType CurrencyType { get; private set; }

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent => default;

        public List<RectTransform> CurrenciesTransforms { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject { get; private set; }

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _collectedDaySprite = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.SPRITE, new[] {Constants.Editor.Paths.TEXTURES})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(s => s.name.Contains("button03_normal"));
            _activeDaySprite = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.SPRITE, new[] {Constants.Editor.Paths.TEXTURES})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(s => s.name.Contains("button02_hover"));
            _uiShinyPreset = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new[] {Constants.Editor.Paths.PREFABS})
                    .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                    .First(s => s.name.Contains("UIShiny")).GetComponent<UIShiny>();
            
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            _dayXPack = gameObject.Descendants().Where(x => x.name.StartsWith("Day0")).ToList();
            _token = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _coin = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _energy = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Energy"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = gameObject.Descendants().First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _topMenuEnergyPosition = gameObject.Descendants().First(x => x.name.Contains("EnergyPosition")).GetComponent<RectTransform>();
            _panel = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL)).transform;
            _day01AmountText = _dayXPack[DAY_1_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            _day02AmountText = _dayXPack[DAY_2_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            _day03AmountText = _dayXPack[DAY_3_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            _day04AmountText = _dayXPack[DAY_4_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            _day05AmountText = _dayXPack[DAY_5_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            _day06AmountText = _dayXPack[DAY_6_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("AmountText"));
            var day07Texts = _dayXPack[DAY_7_INDEX].Descendants().OfComponent<TMP_Text>().Where(x => x.name.Contains("AmountText")).ToArray();
            _day07Amount01Text = day07Texts[0];
            _day07Amount02Text = day07Texts[1];
            _day07Amount03Text = day07Texts[2];
            _day01Title = _dayXPack[DAY_1_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day02Title = _dayXPack[DAY_2_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day03Title = _dayXPack[DAY_3_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day04Title = _dayXPack[DAY_4_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day05Title = _dayXPack[DAY_5_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day06Title = _dayXPack[DAY_6_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _day07Title = _dayXPack[DAY_7_INDEX].Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("DayText"));
            _title = gameObject.Descendants().OfComponent<TMP_Text>().First(x => x.name.Contains("TitleText"));
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            GetRequiredComponents();
            AssertRequiredComponents();
        }

        private void GetRequiredComponents()
        {
            _dayXButton = new List<Button>(_dayXPack.Count);
            _dayXImage = new List<Image>(_dayXPack.Count);
            for (int i = 0; i < _dayXPack.Count; i++)
            {
                _dayXButton.Add(_dayXPack[i].GetComponent<Button>());
                _dayXImage.Add(_dayXPack[i].GetComponent<Image>());
            }
        }

        private void AssertRequiredComponents()
        {
            Assert.IsNotNull(_dayXButton);
            foreach (var button in _dayXButton)
            {
                Assert.IsNotNull(button);
            }
            Assert.IsNotNull(_dayXImage);
            foreach (var image in _dayXImage)
            {
                Assert.IsNotNull(image);
            }
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetDailyRewardMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        #endregion





        #region Custom Methods

        private void SetDailyRewardMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                    .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                    .Enter(state => HandleEnteringShownState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                    .Enter(state => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                    .Enter(state => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonListeners();
            RegisterObservableListeners();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void SetInitialValues()
        {
            _playerData.PersistentData.IsTodayRewardCollected.Value = false;
            _dailyRewardMenuAnimations = new DailyRewardMenuAnimations();
            _currencyBoughtAnimations = new CurrencyBoughtAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            _day01AmountText.text = RemoteConfigClient.Instance.Day1Reward.ToString();
            _day02AmountText.text = RemoteConfigClient.Instance.Day2Reward.ToString();
            _day03AmountText.text = RemoteConfigClient.Instance.Day3Reward.ToString();
            _day04AmountText.text = RemoteConfigClient.Instance.Day4Reward.ToString();
            _day05AmountText.text = RemoteConfigClient.Instance.Day5Reward.ToString();
            _day06AmountText.text = RemoteConfigClient.Instance.Day6Reward.ToString();
            _day07Amount01Text.text = RemoteConfigClient.Instance.Day7Reward1.ToString();
            _day07Amount02Text.text = RemoteConfigClient.Instance.Day7Reward2.ToString();
            _day07Amount03Text.text = RemoteConfigClient.Instance.Day7Reward3.ToString();
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.DailyRewards.Value);
            _day01Title.SetText(_languageClient.CurrentLanguage.Day.Value + "1");
            _day02Title.SetText(_languageClient.CurrentLanguage.Day.Value + "2");
            _day03Title.SetText(_languageClient.CurrentLanguage.Day.Value + "3");
            _day04Title.SetText(_languageClient.CurrentLanguage.Day.Value + "4");
            _day05Title.SetText(_languageClient.CurrentLanguage.Day.Value + "5");
            _day06Title.SetText(_languageClient.CurrentLanguage.Day.Value + "6");
            _day07Title.SetText(_languageClient.CurrentLanguage.Day.Value + "7");
        }

        private void RegisterButtonListeners()
        {
            _exitButton.OnClickAsObservable()
                .Subscribe(ListenToExitButtonEvent)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _panel.OnEnableAsObservable()
                .TakeWhile(_ => !_playerData.PersistentData.IsTodayRewardCollected.Value)
                .Subscribe(_ => _rootState.ChangeState(SHOWN_STATE_NAME))
                .AddTo(this);
            _panel.OnDisableAsObservable()
                .Subscribe(_ => HandlePanelDisabling())
                .AddTo(this);
            this.OnDisableAsObservable()
                .Subscribe(_ => _dailyRewardMenuAnimations.DisposeAllAnimations())
                .AddTo(this);
        }

        private void HandlePanelDisabling()
        {
            _activeButtonDisposable.Dispose();
            _rootState.Exit();
        }

        private void HandleEnteringShownState()
        {
            if (_playerData.PersistentData.DailyRewardStates.All(state => state == PersistentData.EDailyRewardState.Collected))
            {
                for (int i = 0; i < _playerData.PersistentData.DailyRewardStates.Count; i++)
                {
                    _playerData.PersistentData.DailyRewardStates[i] = PersistentData.EDailyRewardState.NotCollected;
                }
            }

            var indexOfFirstNotCollectedDay = _playerData.PersistentData.DailyRewardStates.IndexOf(_playerData.PersistentData.DailyRewardStates.First(state => state == PersistentData.EDailyRewardState.NotCollected));
            switch (indexOfFirstNotCollectedDay)
            {
                case DAY_1_INDEX:
                    SetInteractivityFor(_dayXButton[DAY_1_INDEX]);
                    MainThreadDispatcher.StartCoroutine(ApplyShinyEffectTo(_dayXPack[DAY_1_INDEX]));
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuCoinsPosition;
                            CurrencyType = Constants.CurrencyType.Coin;
                        })
                        .AddTo(this);
                    break;
                case DAY_2_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_2_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuTokensPosition;
                            CurrencyType = Constants.CurrencyType.Token;
                        })
                        .AddTo(this);
                    break;
                case DAY_3_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_3_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_energy, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuEnergyPosition;
                            CurrencyType = Constants.CurrencyType.Energy;
                        })
                        .AddTo(this);
                    break;
                case DAY_4_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_4_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuCoinsPosition;
                            CurrencyType = Constants.CurrencyType.Coin;
                        })
                        .AddTo(this);
                    break;
                case DAY_5_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_5_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuTokensPosition;
                            CurrencyType = Constants.CurrencyType.Token;
                        })
                        .AddTo(this);
                    break;
                case DAY_6_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_6_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_energy, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuEnergyPosition;
                            CurrencyType = Constants.CurrencyType.Energy;
                        })
                        .AddTo(this);
                    break;
                case DAY_7_INDEX:
                    SetButtonLookForDay(_dayXPack[DAY_7_INDEX]);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                        .Subscribe(list =>
                        {
                            CurrenciesTransforms = list;
                            CurrencyAnimationEndObject = _topMenuCoinsPosition;
                            CurrencyType = Constants.CurrencyType.Coin;
                        })
                        .AddTo(this);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                        .Subscribe(list => _tokens = list)
                        .AddTo(this);
                    _currencyMenuLogic.GetInstantiatedAnimationsElements(_energy, _animationsElements)
                        .Subscribe(list => _energies = list)
                        .AddTo(this);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }

            void SetButtonLookForDay(GameObject activeDay)
            {
                ApplyCollectedDaysEffectBefore(activeDay);
                ApplyActiveEffectTo(activeDay.GetComponent<Image>());
                SetInteractivityFor(activeDay.GetComponent<Button>());
                RemoveGrayscaleEffectFrom(activeDay);
                MainThreadDispatcher.StartCoroutine(ApplyShinyEffectTo(activeDay));
            }
        }

        private void ApplyCollectedDaysEffectBefore(GameObject todayPackObject)
        {
            foreach (var collectedDayGameObject in _dayXPack.Where(currentDayPack => _dayXPack.IndexOf(currentDayPack) < _dayXPack.IndexOf(todayPackObject)))
            {
                ApplyCollectedDayEffectFor(collectedDayGameObject);
            }
        }

        private void ApplyCollectedDayEffectFor(GameObject collectedDayGameObject)
        {
            collectedDayGameObject.GetComponent<Image>().sprite = _collectedDaySprite;
            if (collectedDayGameObject.GetComponent<UIEffect>() != null)
            {
                Destroy(collectedDayGameObject.GetComponent<UIEffect>());
            }
        }

        private void SetInteractivityFor(Button activeButton)
        {
            activeButton.interactable = true;
            activeButton.animator.enabled = false;
            _activeButtonDisposable = activeButton.OnClickAsObservable()
                .Subscribe(_ => ListenToActiveDayButton(activeButton))
                .AddTo(this);
        }

        private void ApplyActiveEffectTo(Image activeImage)
        {
            activeImage.sprite = _activeDaySprite;
        }

        private void RemoveGrayscaleEffectFrom(GameObject activeGameObject)
        {
            if (activeGameObject.GetComponent<UIEffect>() != null)
            {
                Destroy(activeGameObject.GetComponent<UIEffect>());
            }
        }

        private IEnumerator ApplyShinyEffectTo(GameObject activeGameObject)
        {
            yield return null;
            var dayXUiShiny = activeGameObject.GetComponent<UIShiny>() != null ? activeGameObject.GetComponent<UIShiny>() : activeGameObject.AddComponent<UIShiny>();
            dayXUiShiny.effectFactor = _uiShinyPreset.effectFactor;
            dayXUiShiny.width = _uiShinyPreset.width;
            dayXUiShiny.rotation = _uiShinyPreset.rotation;
            dayXUiShiny.softness = _uiShinyPreset.softness;
            dayXUiShiny.brightness = _uiShinyPreset.brightness;
            dayXUiShiny.gloss = _uiShinyPreset.gloss;
            dayXUiShiny.effectArea = _uiShinyPreset.effectArea;
            dayXUiShiny.effectPlayer.play = _uiShinyPreset.effectPlayer.play;
            dayXUiShiny.effectPlayer.duration = _uiShinyPreset.effectPlayer.duration;
            dayXUiShiny.effectPlayer.initialPlayDelay = _uiShinyPreset.effectPlayer.initialPlayDelay;
            dayXUiShiny.effectPlayer.loop = _uiShinyPreset.effectPlayer.loop;
            dayXUiShiny.effectPlayer.loopDelay = _uiShinyPreset.effectPlayer.loopDelay;
            dayXUiShiny.effectPlayer.updateMode = _uiShinyPreset.effectPlayer.updateMode;
            _dailyRewardMenuAnimations.AnimateDayXButton(activeGameObject);
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            if (CurrenciesTransforms != null && CurrenciesTransforms.Count != 0)
            {
                var indexOfFirstNotCollectedDay = _playerData.PersistentData.DailyRewardStates.IndexOf(_playerData.PersistentData.DailyRewardStates.First(state => state == PersistentData.EDailyRewardState.NotCollected));
                if (indexOfFirstNotCollectedDay == DAY_7_INDEX)
                {
                    Observable.FromCoroutine(_currencyBoughtAnimations.Animate)
                        .SelectMany(PrepareTokensAnimation)
                        .SelectMany(_currencyBoughtAnimations.Animate)
                        .SelectMany(PrepareEnergyAnimation)
                        .SelectMany(_currencyBoughtAnimations.Animate)
                        .Subscribe(_ => FinishAnimating(indexOfFirstNotCollectedDay))
                        .AddTo(this);
                    return;
                }
                
                Observable.FromCoroutine(_currencyBoughtAnimations.Animate)
                    .Subscribe(_ => FinishAnimating(indexOfFirstNotCollectedDay))
                    .AddTo(this);
            }
        }

        private IEnumerator PrepareTokensAnimation()
        {
            CurrenciesTransforms = _tokens;
            CurrencyAnimationEndObject = _topMenuTokensPosition;
            CurrencyAmountEarned = RemoteConfigClient.Instance.Day7Reward2;
            CurrencyType = Constants.CurrencyType.Token;
            yield return null;
        }

        private IEnumerator PrepareEnergyAnimation()
        {
            CurrenciesTransforms = _energies;
            CurrencyAnimationEndObject = _topMenuEnergyPosition;
            CurrencyAmountEarned = RemoteConfigClient.Instance.Day7Reward3;
            CurrencyType = Constants.CurrencyType.Energy;
            yield return null;
        }

        private void FinishAnimating(int indexOfFirstNotCollectedDay)
        {
            _playerData.PersistentData.DailyRewardStates[indexOfFirstNotCollectedDay] = PersistentData.EDailyRewardState.Collected;
            _playerData.PersistentData.IsTodayRewardCollected.Value = true;
            MessageBroker.Default.Publish(new DailyRewardMenuCalled());
        }

        private static void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion





        #region Event Listeners

        private void ListenToActiveDayButton(Button activeButton)
        {
            activeButton.interactable = false;
            ApplyCollectedDayEffectFor(activeButton.gameObject);
            _dailyRewardMenuAnimations.AnimateClick(activeButton.gameObject);
            _dailyRewardMenuAnimations.CancelAllAnimations();
            switch (_dayXButton.IndexOf(activeButton))
            {
                case 0:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day1Reward;
                    break;
                case 1:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day2Reward;
                    break;
                case 2:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day3Reward;
                    break;
                case 3:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day4Reward;
                    break;
                case 4:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day5Reward;
                    break;
                case 5:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day6Reward;
                    break;
                case 6:
                    CurrencyAmountEarned = RemoteConfigClient.Instance.Day7Reward1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        private void ListenToExitButtonEvent(Unit obj)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _dailyRewardMenuAnimations.CancelAllAnimations();
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<DailyRewardMenu>
        {
            
        }

        #endregion
    }
}
