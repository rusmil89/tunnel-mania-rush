﻿using System.Linq;
using Tunnel.Utilities;
using Unity.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Tunnel.UI
{
    public class Menu : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] protected MenuSO _menuSO;

        /// <summary>
        /// Consts
        /// </summary>
        public const float MENU_ANIMATION_DURATION = 0.225f;
        
        #endregion





        #region Properties

        public GameObject PanelGameObject { get; private set; }
        public Canvas MenuCanvas { get; private set; }
        public GraphicRaycaster MenuGraphicRaycaster { get; private set; }

        #endregion





        #region Monobehaviour Events

        // Start is called before the first frame update
        protected void Awake()
        {
            GetRequiredComponents();
            _menuSO.Menu = this;
        }
        
        #endregion





        #region Custom Methods

        private void GetRequiredComponents()
        {
            MenuCanvas = GetComponent<Canvas>();
            MenuGraphicRaycaster = GetComponent<GraphicRaycaster>();
            PanelGameObject = gameObject.Descendants().First(component => component.name.Contains(Constants.GameObjectsNames.PANEL));
            Assert.IsNotNull(MenuCanvas);
            Assert.IsNotNull(MenuGraphicRaycaster);
            Assert.IsNotNull(PanelGameObject);
        }
        
        #endregion
    }
}
