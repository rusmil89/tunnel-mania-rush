﻿using System.Linq;
using Core.Events;
using EasyMobile;
using Localization;
using TMPro;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    public class GDPRMenu : Menu
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        [SerializeField] private global::PlayerData _playerData;

        [Space] [Header("UI")]
        [SerializeField] private Button acceptButton;
        [SerializeField] private Button declineButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _acceptButtonText;
        [SerializeField] private TMP_Text _declineButtonText;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            acceptButton = gameObject.Descendants().First(x => x.name.Contains("AcceptButton")).GetComponent<Button>();
            declineButton = gameObject.Descendants().First(x => x.name.Contains("DeclineButton")).GetComponent<Button>();
            _exitButton = gameObject.Descendants().First(x => x.name.Contains(Constants.GameObjectsNames.EXIT_BUTTON)).GetComponent<Button>();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        protected new void Awake()
        {
            base.Awake();
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            if (_playerData.PersistentData.NumberOfMenuStarts.Value < 2) return;
            if (_playerData.PersistentData.IsGDPREnabled.Value)
            {
                Advertising.GrantDataPrivacyConsent();
            }
            else
            {
                Advertising.RevokeDataPrivacyConsent();
            }
        }

        #endregion





        #region Custom Methods

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.Gdpr.Value);
            _description.SetText(_languageClient.CurrentLanguage.GdprDescription.Value);
            _acceptButtonText.SetText(_languageClient.CurrentLanguage.Yes.Value);
            _declineButtonText.SetText(_languageClient.CurrentLanguage.No.Value);
        }

        private void RegisterButtonsListeners()
        {
            acceptButton.OnClickAsObservable()
                .Subscribe(_ => AcceptButtonEventHandler())
                .AddTo(this);
            declineButton.OnClickAsObservable()
                .Subscribe(_ => DeclineButtonEventHandler())
                .AddTo(this);
            _exitButton.OnClickAsObservable()
                .Subscribe(_ => ExitButtonEventListener())
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            _playerData.PersistentData.IsGDPREnabled
                .Where(isGdprEnabled => isGdprEnabled)
                .Subscribe(_ => Advertising.GrantDataPrivacyConsent())
                .AddTo(this);
            
            _playerData.PersistentData.IsGDPREnabled
                .Where(isGdprEnabled => !isGdprEnabled)
                .Subscribe(_ => Advertising.RevokeDataPrivacyConsent())
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void AcceptButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsGDPREnabled.SetValueAndForceNotify(true);
        }

        private void DeclineButtonEventHandler()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _playerData.PersistentData.IsGDPREnabled.SetValueAndForceNotify(false);
        }

        private void ExitButtonEventListener()
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            MessageBroker.Default.Publish(new ExitCurrentMenuCalled());
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<GDPRMenu>
        {
            
        }

        #endregion
    }
}