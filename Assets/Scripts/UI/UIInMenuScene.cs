﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using RSG;
using Tunnel.Ads;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME OBJECT: Canvas
    /// DESCRIPTION: Klasa koja kontrolise ponasanje prikaza menija u zavisnosti od stanja u kome se igrac trenutno nalazi
    /// </summary>
    public sealed class UIInMenuScene : MonoBehaviour
    {
        #region private fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("SO")]
        [SerializeField] private global::PlayerData _playerData;
        
        [Header("Menus")]
        [SerializeField] private List<MenuSO> _allMenus;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private string _remoteAppVersion;
        private IState _rootState;
        [Inject] private Settings.Factory _settingsFactory;
        [Inject] private UpdateMenu.Factory _updateFactory;
        [Inject] private LikeDislikeMenu.Factory _likeDislikeFactory;
        [Inject] private ExitMenu.Factory _exitFactory;
        [Inject] private UnlockChapterMenu.Factory _unlockFactory;
        [Inject] private EnergyRefillMenu.Factory _energyRefillFactory;
        [Inject] private GetShieldsMenu.Factory _shieldsFactory;
        [Inject] private TokensMenu.Factory _tokensFactory;
        [Inject] private ShopMenu.Factory _shopFactory;
        [Inject] private DailyRewardMenu.Factory _dailyRewardFactory;
        [Inject] private GDPRMenu.Factory _gdprFactory;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE = Constants.MenusStatesNames.INITIALIZATION;
        private const string INITIAL_MENU_STATE = "InitialState";
        private const string SETTINGS_FEEDBACK_STATE = "SettingsFeedbackState";
        private const string HOME_MENU_NAME = Constants.Menus.HOME;
        private const string TOP_BUTTONS_MENU_NAME = Constants.Menus.TOP_BUTTONS;
        private const string GDPR_MENU_NAME = Constants.Menus.GDPR;
        private const string UPDATE_MENU_NAME = Constants.Menus.UPDATE;
        private const string LIKE_DISLIKE_MENU_NAME = Constants.Menus.LIKE_DISLIKE;
        private const string SETTINGS_MENU_NAME = Constants.Menus.SETTINGS;
        private const string EXIT_MENU_NAME = Constants.Menus.EXIT;
        private const string UNLOCK_CHAPTER_MENU_NAME = Constants.Menus.UNLOCK_CHAPTER_MENU;
        private const string ENERGY_REFILL_MENU_NAME = Constants.Menus.ENERGY_REFILL;
        private const string GET_SHIELDS_MENU_NAME = Constants.Menus.GET_SHIELDS_MENU;
        private const string TOKENS_MENU_NAME = Constants.Menus.TOKENS_MENU;
        private const string SHOP_MENU_NAME = Constants.Menus.SHOP;
        private const string DAILY_REWARD_MENU_NAME = Constants.Menus.DAILY_REWARD;

        #endregion





        #region Properties

        public Settings.Factory SettingsFactory => _settingsFactory;
        public UpdateMenu.Factory UpdateFactory => _updateFactory;
        public LikeDislikeMenu.Factory LikeDislikeFactory => _likeDislikeFactory;
        public ExitMenu.Factory ExitFactory => _exitFactory;
        public UnlockChapterMenu.Factory UnlockFactory => _unlockFactory;
        public EnergyRefillMenu.Factory EnergyRefillFactory => _energyRefillFactory;
        public GetShieldsMenu.Factory ShieldsFactory => _shieldsFactory;
        public TokensMenu.Factory TokensFactory => _tokensFactory;
        public ShopMenu.Factory ShopFactory => _shopFactory;
        public DailyRewardMenu.Factory DailyRewardFactory => _dailyRewardFactory;
        public GDPRMenu.Factory GdprFactory => _gdprFactory;
        public StaticMenuSO HomeMenu { get; private set; }
        public StaticMenuSO TopButtonsMenu { get; private set; }
        public MenuSO UpdateMenu { get; private set; }
        public MenuSO LikeDislikeMenu { get; private set; }
        public MenuSO SettingsMenu { get; private set; }
        public MenuSO ExitMenu { get; private set; }
        public MenuSO UnlockChapterMenu { get; private set; }
        public MenuSO EnergyRefillMenu { get; private set; }
        public MenuSO ShieldsMenu { get; private set; }
        public MenuSO TokensMenu { get; private set; }
        public MenuSO ShopMenu { get; private set; }
        public MenuSO DailyRewardMenu { get; private set; }
        public MenuSO GDPRMenu { get; private set; }

        #endregion
        
        
        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _allMenus = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.MENU, new[] {Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<MenuSO>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Start()
        {
            SetMenusStates();
            _rootState.ChangeState(INITIALIZATION_STATE);

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
    Analytics.WarmAppGamesAnalytics.LogAppOpen();
#endif
        }

        private void OnDestroy()
        {
            _rootState.Exit();
        }

        #endregion


        
        
        
        #region Custom Methods

        /// <summary>
        /// Metoda koja pokrece i konfigurise finite state masinu koja kontrolise stanja u kojima se meniji nalaze
        /// WHY: Zato sto svaki od meniju u menu sceni moze da se nadje u nekom svom stanju i to treba kontrolisati na 1 mestu
        /// </summary>
        private void SetMenusStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE)
                .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<MenuSceneInitialMenuState>(INITIAL_MENU_STATE)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneLikeDislikeState>(LIKE_DISLIKE_MENU_NAME)
                .Enter(state => state.OnStateEnter( this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneSettingsState>(SETTINGS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Event(SETTINGS_FEEDBACK_STATE, state => MainThreadDispatcher.StartCoroutine(DelayFeedbackInvokingEnumerator()))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneUpdateState>(UPDATE_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneExitState>(EXIT_MENU_NAME)
                .Enter(state =>
                {
                    state.OnStateEnter(this);
                    MessageBroker.Default.Publish(new ButtonClicked());
                })
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneUnlockChapterState>(UNLOCK_CHAPTER_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneEnergyState>(ENERGY_REFILL_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneShieldsState>(GET_SHIELDS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneTokensState>(TOKENS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneShopState>(SHOP_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneDailyRewardState>(DAILY_REWARD_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .State<MenuSceneGDPRState>(GDPR_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            _playerData.PersistentData.NumberOfMenuStarts.Value++;
            SetInitialValues();
            RegisterObservableListeners();
            _rootState.ChangeState(INITIAL_MENU_STATE);
            if (IsGDPRMenuShowing()) return;
            if (IsDailyRewardMenuShowing()) return;
            if (Application.internetReachability == NetworkReachability.NotReachable) return;
            if (IsUpdateMenuShowing()) return;
            LikeDislikeMenuShowing();
        }

        private void SetInitialValues()
        {
            HomeMenu = _allMenus.First(x => x.name.Contains(HOME_MENU_NAME)) as StaticMenuSO;
            TopButtonsMenu = _allMenus.First(x => x.name.Contains(TOP_BUTTONS_MENU_NAME)) as StaticMenuSO;
            UpdateMenu = _allMenus.First(x => x.name.Contains(UPDATE_MENU_NAME));
            LikeDislikeMenu = _allMenus.First(x => x.name.Contains(LIKE_DISLIKE_MENU_NAME));
            SettingsMenu = _allMenus.First(x => x.name.Contains(SETTINGS_MENU_NAME));
            ExitMenu = _allMenus.First(x => x.name.Contains(EXIT_MENU_NAME));
            UnlockChapterMenu = _allMenus.First(x => x.name.Contains(UNLOCK_CHAPTER_MENU_NAME));
            EnergyRefillMenu = _allMenus.First(x => x.name.Contains(ENERGY_REFILL_MENU_NAME));
            ShieldsMenu = _allMenus.First(x => x.name.Contains(GET_SHIELDS_MENU_NAME));
            TokensMenu = _allMenus.First(x => x.name.Contains(TOKENS_MENU_NAME));
            ShopMenu = _allMenus.First(x => x.name.Contains(SHOP_MENU_NAME));
            DailyRewardMenu = _allMenus.First(x => x.name.Contains(DAILY_REWARD_MENU_NAME));
            GDPRMenu = _allMenus.First(x => x.name.Contains(GDPR_MENU_NAME));
        }

        private void RegisterObservableListeners()
        {
            RemoteConfigClient.Instance.Ready
                .Where(isReady => isReady)
                .Subscribe(_ => _remoteAppVersion = RemoteConfigClient.Instance.AppVersion)
                .AddTo(this);
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyUp(KeyCode.Escape) && !ExitMenu.IsMenuShown && !UpdateMenu.IsMenuShown && !DailyRewardMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(EXIT_MENU_NAME))
                .AddTo(this);
            _playerData.PersistentData.IsGDPREnabled
                .SkipWhile(_ => _rootState == null)
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<LikeMenuNoButtonClicked>()
                .Subscribe(_ =>
                {
                    _rootState.ChangeState(SETTINGS_MENU_NAME);
                    _rootState.TriggerEvent(SETTINGS_FEEDBACK_STATE);
                })
                .AddTo(this);
            MessageBroker.Default.Receive<ApplicationRated>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<HomeAndTopButtonsMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<TopButtonsMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(TOP_BUTTONS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<GDPRMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(GDPR_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<SettingsMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(SETTINGS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<UnlockChapterMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(UNLOCK_CHAPTER_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterUnlockedWithToken>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterUnlockedWithAd>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<ShopMenuCalled>()
                .Where(_ => !ShopMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(SHOP_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<TokensMenuCalled>()
                .Where(_ => !TokensMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(TOKENS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<GetShieldsMenuCalled>()
                .Where(_ => !ShieldsMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(GET_SHIELDS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<EnergyRefillMenuCalled>()
                .Where(_ => !EnergyRefillMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(ENERGY_REFILL_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<DailyRewardMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(DAILY_REWARD_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ExitCurrentMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
        }

        /// <summary>
        /// Korutina koja ceka 1 frejm da se settings menu instancira i registruje na event SettingsMenuFeedbackCalled, pa tek onda salje taj event settings meniju
        /// WHY: Zato sto menije instanciramo u runtime-u samo po potrebi i onda je nemoguce registrovati sve evente pre nego da se oni okinu
        /// </summary>
        /// <returns>IEnumerator</returns>
        private static IEnumerator DelayFeedbackInvokingEnumerator()
        {
            yield return null;
            MessageBroker.Default.Publish(new SettingsMenuFeedbackCalled());
        }

        private bool IsGDPRMenuShowing()
        {
#if DEVELOPMENT_BUILD
            EasyMobile.NativeUI.ShowToast("IsGDPRMenuShowing - canShowGDPR: " + _playerData.CanShowGDPR);
#endif
            if (!_playerData.CanShowGDPR) return false;
            if (_playerData.PersistentData.NumberOfMenuStarts.Value != 2) return false;
            _rootState.ChangeState(GDPR_MENU_NAME);
            return true;
        }

        private bool IsDailyRewardMenuShowing()
        {
            if (DateTime.Today.Subtract(_playerData.PersistentData.DataSaveTime.Value).Days >= 1 && !_playerData.PersistentData.IsTodayRewardCollected.Value)
            {
                _rootState.ChangeState(DAILY_REWARD_MENU_NAME);
                return true;
            }

            return false;
        }

        private void LikeDislikeMenuShowing()
        {
            switch (_playerData.PersistentData.LikeDislikeOption)
            {
                case PlayerData.PersistentData.LikeDislikeOptions.NotChosen:
                    CheckIfShouldShowLikeDislikeMenu(true);
                    break;
                case PlayerData.PersistentData.LikeDislikeOptions.Dislike:
                    CheckIfShouldShowLikeDislikeMenu();
                    break;
                default:
                    return;
            }
        }

        private void CheckIfShouldShowLikeDislikeMenu(bool isFirstTime = false)
        {
            if (isFirstTime)
            {
                if (_playerData.PersistentData.NumberOfAppStarts > 4 && _playerData.PersistentData.NumberOfAppStarts % 2 == 1)
                {
                    _rootState.ChangeState(LIKE_DISLIKE_MENU_NAME);
                }
            }
            else
            {
                if (_playerData.PersistentData.NumberOfAppStarts > 10 && _playerData.PersistentData.NumberOfAppStarts % 5 == 1)
                {
                    _rootState.ChangeState(LIKE_DISLIKE_MENU_NAME);
                }
            }
        }

        private bool IsUpdateMenuShowing()
        {
            if (_remoteAppVersion.Equals(Application.version)) return false;
            _rootState.ChangeState(UPDATE_MENU_NAME);
            return true;
        }

        #endregion
    }
}
