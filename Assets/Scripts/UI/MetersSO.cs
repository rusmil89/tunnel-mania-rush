﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Meters", menuName = "ScriptableObjects/Levels/Meters", order = 1)]
public class MetersSO : ScriptableObject
{
    [SerializeField] private float _value;

    public float Value
    {
        get => _value;
        set
        {
            if (value >= 0)
            {
                _value = value;
            }
            else
            {
                throw new Exception("MetersSO.Value ne sme biti manji od 0");
            }
        }
    }

}