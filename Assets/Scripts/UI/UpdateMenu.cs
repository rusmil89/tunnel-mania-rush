﻿using System;
using System.Linq;
using System.Text;
using Core.Events;
using Localization;
using TMPro;
using Tunnel.Controllers;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Menu
    /// GAME_OBJECT: UpdateMenu
    /// </summary>
    public sealed class UpdateMenu : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private Button _updateButton;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _updateButtonText;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const char DASH_CHARACTER = '-';
        private const string NEW_LINE_CHARACTER = "\n";

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _updateButton = gameObject.Descendants().OfComponent<Button>().First();
            _description = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("Description"));
            _title = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("TitleText"));
            _updateButtonText = gameObject.Descendants().OfComponent<TMP_Text>().First(text => text.name.Contains("ButtonText"));

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetInitialValues();
            SetLocalization();
            RegisterButtonsListeners();
        }

        private void SetInitialValues()
        {
            StringBuilder updateText = new StringBuilder();
            var updateDescriptionRemoteValue = RemoteConfigClient.Instance.UpdateMenuDescription.Split(new[] {DASH_CHARACTER}, StringSplitOptions.None);
            foreach (var sentence in updateDescriptionRemoteValue)
            {
                if (Array.IndexOf(updateDescriptionRemoteValue, sentence) == 0)
                {
                    updateText.AppendLine(sentence);
                    updateText.Append(NEW_LINE_CHARACTER);
                }
                else
                {
                    updateText.Append(DASH_CHARACTER);
                    updateText.Append(string.Empty);
                    updateText.AppendLine(sentence);
                }
            }
            _description.SetText(updateText.ToString());
        }

        private void SetLocalization()
        {
            _title.SetText(_languageClient.CurrentLanguage.UpdateMenuTitle.Value);
            _description.SetText(_languageClient.CurrentLanguage.UpdateMenuDescription.Value);
            _updateButtonText.SetText(_languageClient.CurrentLanguage.UpdateButton.Value);
        }

        private void RegisterButtonsListeners()
        {
            _updateButton.OnClickAsObservable()
                .Subscribe(HandleUpdateButtonClick)
                .AddTo(this);
        }

        #endregion
        
        
        
        
        
        #region Custom Methods

        private static void HandleUpdateButtonClick(Unit unit)
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            Application.OpenURL(Constants.Links.ANDROID);
            MessageBroker.Default.Publish(new HomeAndTopButtonsMenuCalled());
#elif UNITY_ANDROID
            Application.OpenURL(Constants.Links.ANDROID);
            Application.Quit();
#endif
        }

        #endregion





        #region Factory

        public class Factory : PlaceholderFactory<UpdateMenu>
        {
            
        }

        #endregion
    }
}
