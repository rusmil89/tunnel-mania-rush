﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Events;
using RSG;
using Tunnel.Ads;
using Tunnel.Utilities;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: Canvas
    /// DESCRIPTION: Klasa koja kontrolise prikazivanje menija preko FSM-a
    /// </summary>
    public class UIInLevelScene : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("SO")]
        [SerializeField] private global::PlayerData _playerData;
        [SerializeField] private GameData _gameData;
        
        [Header("Menus")]
        [SerializeField] private List<MenuSO> _allMenus;

        /// <summary>
        /// Serializable fields
        /// </summary>
        private IState _rootState;
        [Inject] private PauseMenu.Factory _pauseFactory;
        [Inject] private GameOverMenu.Factory _gameOverFactory;
        [Inject] private FinishMenu.Factory _finishFactory;
        [Inject] private ScoreMenu.Factory _scoreFactory;
        [Inject] private ChapterClearedMenu.Factory _chapterClearedFactory;
        [Inject] private GetShieldsMenu.Factory _shieldsFactory;
        [Inject] private TokensMenu.Factory _tokensFactory;
        [Inject] private ShopMenu.Factory _shopFactory;
        [Inject] private CountdownMenu.Factory _countdownFactory;
        [Inject] private TopButtonsMenu.Factory _topButtonsFactory;

        /// <summary>
        /// Consts
        /// </summary>
        private const float ANIMATION_DURATION = 0.225f;
        private readonly Color _defaultColor = new Color(0f, 0f, 0f, 175/255f);
        private const string INITIALIZATION_STATE = Constants.MenusStatesNames.INITIALIZATION;
        private const string INITIAL_MENU_STATE = "InitialState";
        private const string TOP_MENU_NAME = Constants.Menus.TOP;
        private const string TOP_BUTTONS_MENU_NAME = Constants.Menus.TOP_BUTTONS;
        private const string CONTROLS_MENU_NAME = Constants.Menus.CONTROLS;
        private const string GAME_OVER_MENU_NAME = Constants.Menus.GAME_OVER;
        private const string PAUSE_MENU_NAME = Constants.Menus.PAUSE;
        private const string COUNTDOWN_MENU_NAME = Constants.Menus.COUNTDOWN;
        private const string FINISH_MENU_NAME = Constants.Menus.FINISH;
        private const string SCORE_MENU_NAME = Constants.Menus.SCORE;
        private const string CHAPTER_CLEARED_MENU_NAME = Constants.Menus.CHAPTER_CLEARED;
        private const string GET_SHIELDS_MENU_NAME = Constants.Menus.GET_SHIELDS_MENU;
        private const string TOKENS_MENU_NAME = Constants.Menus.TOKENS_MENU;
        private const string SHOP_MENU_NAME = Constants.Menus.SHOP;

        #endregion





        #region Properties

        public Image BackgroundPanelImage { get; private set; }
        public PauseMenu.Factory PauseFactory => _pauseFactory;
        public GameOverMenu.Factory GameOverFactory => _gameOverFactory;
        public FinishMenu.Factory FinishFactory => _finishFactory;
        public ScoreMenu.Factory ScoreFactory => _scoreFactory;
        public ChapterClearedMenu.Factory ChapterClearedFactory => _chapterClearedFactory;
        public GetShieldsMenu.Factory ShieldsFactory => _shieldsFactory;
        public TokensMenu.Factory TokensFactory => _tokensFactory;
        public ShopMenu.Factory ShopFactory => _shopFactory;
        public CountdownMenu.Factory CountdownFactory => _countdownFactory;
        public TopButtonsMenu.Factory TopButtonsFactory => _topButtonsFactory;
        public StaticMenuSO TopMenu { get; private set; }
        public StaticMenuSO TopButtonsMenu { get; private set; }
        public StaticMenuSO ControlsMenu { get; private set; }
        public MenuSO PauseMenu { get; private set; }
        public MenuSO GameOverMenu { get; private set; }
        public MenuSO CountdownMenu { get; private set; }
        public MenuSO FinishMenu { get; private set; }
        public MenuSO ScoreMenu { get; private set; }
        public MenuSO ChapterClearedMenu { get; private set; }
        public MenuSO ShieldsMenu { get; private set; }
        public MenuSO TokensMenu { get; private set; }
        public MenuSO ShopMenu { get; private set; }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<global::PlayerData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _allMenus = AssetDatabase.FindAssets(Constants.Editor.Filters.MENU, new[] {Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<MenuSO>(AssetDatabase.GUIDToAssetPath(s)))
                .ToList();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Start()
        {
            SetMenusStates();
            _rootState.ChangeState(INITIALIZATION_STATE);
        }

        #endregion





        #region Custom Methods

        private void SetInitialValues()
        {
            BackgroundPanelImage = transform.GetChild(0).GetComponent<Image>();
            TopMenu = _allMenus.Find(x => x.name.Contains(TOP_MENU_NAME)) as StaticMenuSO;
            TopButtonsMenu = _allMenus.Find(x => x.name.Contains(TOP_BUTTONS_MENU_NAME)) as StaticMenuSO;
            ControlsMenu = _allMenus.Find(x => x.name.Contains(CONTROLS_MENU_NAME)) as StaticMenuSO;
            PauseMenu = _allMenus.Find(x => x.name.Contains(PAUSE_MENU_NAME));
            GameOverMenu = _allMenus.Find(x => x.name.Contains(GAME_OVER_MENU_NAME));
            CountdownMenu = _allMenus.Find(x => x.name.Contains(COUNTDOWN_MENU_NAME));
            FinishMenu = _allMenus.Find(x => x.name.Contains(FINISH_MENU_NAME));
            ScoreMenu = _allMenus.Find(x => x.name.Contains(SCORE_MENU_NAME));
            ChapterClearedMenu = _allMenus.Find(x => x.name.Contains(CHAPTER_CLEARED_MENU_NAME));
            ShieldsMenu = _allMenus.Find(x => x.name.Contains(GET_SHIELDS_MENU_NAME));
            TokensMenu = _allMenus.Find(x => x.name.Contains(TOKENS_MENU_NAME));
            ShopMenu = _allMenus.Find(x => x.name.Contains(SHOP_MENU_NAME));
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<ExitCurrentMenuCalled>()
                .Subscribe(HandleExitCurrentMenuEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuCalled>()
                .Delay(TimeSpan.FromSeconds(1f))
                .Subscribe(_ => _rootState.ChangeState(CHAPTER_CLEARED_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<PauseMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(PAUSE_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<CountdownMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(COUNTDOWN_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<CountdownFinished>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<ScoreMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(SCORE_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<GameOverMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(GAME_OVER_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<FinishMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(FINISH_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerRevived>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<FinishMenuContinueButtonClicked>()
                .Subscribe(_ => _rootState.ChangeState(INITIAL_MENU_STATE))
                .AddTo(this);
            MessageBroker.Default.Receive<ShopMenuCalled>()
                .Where(_ => !ShopMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(SHOP_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<GetShieldsMenuCalled>()
                .Where(_ => !ShieldsMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(GET_SHIELDS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<TokensMenuCalled>()
                .Where(_ => !TokensMenu.IsMenuShown)
                .Subscribe(_ => _rootState.ChangeState(TOKENS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<TopButtonsMenuCalled>()
                .Subscribe(_ => _rootState.ChangeState(TOP_BUTTONS_MENU_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuClaimButtonClicked>()
                .Subscribe(HandleChapterClearedMenuClaimButtonClickedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<RewardedVideoInChapterClearedCompleted>()
                .Subscribe(HandleRewardedVideoInChapterClearedCompleted)
                .AddTo(this);
        }

        private void SetMenusStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE)
                .Enter(state => HandleEnteringInitializationState())
                .End()
                .State<LevelSceneInitialState>(INITIAL_MENU_STATE)
                .Enter(state =>
                {
                    PauseGame(false);
                    state.OnStateEnter(this);
                })
                .Exit(state => state.OnStateExit())
                .End()
                .State<LevelScenePauseState>(PAUSE_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .End()
                .State<LevelSceneCountdownState>(COUNTDOWN_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .End()
                .State<LevelSceneShopState>(SHOP_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.PushState(TOP_BUTTONS_MENU_NAME);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .State<LevelSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .End()
                .State<LevelSceneScoreState>(SCORE_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .End()
                .State<LevelSceneGameOverState>(GAME_OVER_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.PushState(TOP_BUTTONS_MENU_NAME);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .State<LevelSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .End()
                .State<LevelSceneFinishState>(FINISH_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .End()
                .State<LevelSceneChapterClearedState>(CHAPTER_CLEARED_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    state.OnStateEnter(this);
                })
                .Exit(state => state.OnStateExit())
                .End()
                .State<LevelSceneShieldsState>(GET_SHIELDS_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.PushState(TOP_BUTTONS_MENU_NAME);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .State<LevelSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .End()
                .State<LevelSceneTokensState>(TOKENS_MENU_NAME)
                .Enter(state =>
                {
                    PauseGame(true);
                    AnimateBackground(true);
                    state.PushState(TOP_BUTTONS_MENU_NAME);
                    state.OnStateEnter(this);
                })
                .Exit(state =>
                {
                    AnimateBackground();
                    state.OnStateExit();
                })
                .State<LevelSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .End()
                .State<LevelSceneTopButtonsState>(TOP_BUTTONS_MENU_NAME)
                .Enter(state => state.OnStateEnter(this))
                .Exit(state => state.OnStateExit())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            RegisterObservableListeners();
            _rootState.ChangeState(INITIAL_MENU_STATE);
        }

        private void AnimateBackground(bool isFadingOut = false)
        {
            if (isFadingOut)
            {
                LeanTween.value(BackgroundPanelImage.gameObject, a => BackgroundPanelImage.color = a, Color.clear, _defaultColor, ANIMATION_DURATION);
            }
            else
            {
                LeanTween.value(BackgroundPanelImage.gameObject, a => BackgroundPanelImage.color = a, _defaultColor, Color.clear, ANIMATION_DURATION);
            }
        }

        private void PauseGame(bool value)
        {
            _gameData.IsGamePaused = value;
        }

        #endregion





        #region Event Listeners

        private void HandleExitCurrentMenuEvent(ExitCurrentMenuCalled exitCurrentMenuCalled)
        {
            if (_playerData.IsPlayerDead)
            {
                _rootState.ChangeState(GAME_OVER_MENU_NAME);
                return;
            }

            _rootState.ChangeState(INITIAL_MENU_STATE);
        }

        private void HandleChapterClearedMenuClaimButtonClickedEvent(ChapterClearedMenuClaimButtonClicked chapterClearedMenuClaimButtonClicked)
        {
            _rootState.ChangeState(TOP_BUTTONS_MENU_NAME);
        }

        private void HandleRewardedVideoInChapterClearedCompleted(RewardedVideoInChapterClearedCompleted rewardedVideoInChapterClearedCompleted)
        {
            _rootState.ChangeState(TOP_BUTTONS_MENU_NAME);
        }

        #endregion
    }
}