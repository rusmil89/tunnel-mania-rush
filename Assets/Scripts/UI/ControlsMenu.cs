﻿using System;
using System.Linq;
using System.Text;
using Localization;
using TMPro;
using Tunnel.LevelLoad;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEngine;
using Utilities;
using Zenject;

namespace Tunnel.UI
{
    public class ControlsMenu : StaticMenu
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private CompositeDisposable _compositeDisposable;

        private StringBuilder _stringBuilder;
        private LanguageClient _languageClient;
        private int _numberOfLevels;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private global::PlayerData _playerData;

        [SerializeField] private TMP_Text _tutorialText;
        [SerializeField] private TMP_Text _currentLevelText;

        [Header("Scriptable Objects")] [SerializeField]
        private WorldData[] _allWorldData;

        #endregion


        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase
                .FindAssets(Constants.Editor.Filters.PLAYER_DATA, new[] { Constants.Editor.Paths.SCRIPTABLE_OBJECTS })
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _tutorialText = gameObject.Descendants().First(component => component.name.Contains("TutorialText")).GetComponent<TMP_Text>();
            _currentLevelText = gameObject.Descendants().First(component => component.name.Contains("CurrentLevelText")).GetComponent<TMP_Text>();
            var allWorldDataGUIDs =
                UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.WORLD_DATA, new[] { Constants.Editor.Paths.SCRIPTABLE_OBJECTS });
            _allWorldData = allWorldDataGUIDs
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<WorldData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .Where(s => !s.name.Contains("Tutorial"))
                .ToArray();

            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        // Start is called before the first frame update
        private void Awake()
        {
            _compositeDisposable = new CompositeDisposable();
            _stringBuilder = new StringBuilder();
            staticMenuSo.staticMenu = this;
            _numberOfLevels = _allWorldData[_playerData.PersistentData.CurrentWorld.Value - 1]
                .Chapters[_playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value] - 1].Levels.Count;
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetLocalization();
            RegisterObservableListeners();
        }

        private void SetLocalization()
        {
            _tutorialText.SetText(_languageClient.CurrentLanguage.Level.Value);
        }

        private void RegisterObservableListeners()
        {
            _playerData.PersistentData.CurrentLevel
                .Subscribe(SetLevelNumberText)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(HandlePlayerDiedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerRevived>()
                .Subscribe(HandlePlayerRevivedEvent)
                .AddTo(this);
        }

        private void OnDestroy()
        {
            _compositeDisposable.Dispose();
        }

        #endregion


        #region Event Listeners

        private void SetLevelNumberText(int currentLevel)
        {
            _tutorialText.gameObject.SetActive(true);
            _stringBuilder.Clear();
            _stringBuilder.Append(currentLevel);
            _stringBuilder.Append("/");
            _stringBuilder.Append(_numberOfLevels);
            _currentLevelText.text = _stringBuilder.ToString();
            _tutorialText.color = _currentLevelText.color = Color.white;
            Observable.Timer(TimeSpan.FromSeconds(5f))
                .Subscribe(_ => AnimateFading())
                .AddTo(_compositeDisposable);
        }

        private void AnimateFading()
        {
            LeanTween.value(gameObject, value =>
            {
                _tutorialText.color = value;
                _currentLevelText.color = value;
            }, Color.white, Color.clear, 0.5f);
        }

        private void HandlePlayerDiedEvent(PlayerDied playerDied)
        {
            _tutorialText.gameObject.SetActive(false);
            _compositeDisposable.Dispose();
        }

        private void HandlePlayerRevivedEvent(PlayerRevived playerRevived)
        {
            _compositeDisposable = new CompositeDisposable();
        }

        #endregion
    }
}