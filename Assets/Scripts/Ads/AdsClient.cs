﻿using System;
using System.Collections;
using EasyMobile;
using Tunnel.Analytics;
using Tunnel.Controllers;
using Tunnel.LevelLoad;
using Tunnel.PlayerData;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;

namespace Tunnel.Ads
{
    #region Events

    public sealed class RewardVideoAfterDeathCompleted {}
    public sealed class RewardedVideoInChapterClearedCompleted {}
    public sealed class ChapterUnlockedWithAd {}
    public sealed class EnergyRefilledWithAd {}
    public sealed class CoinsBought {}

    #endregion
    
    
    
    
    
    
    public sealed class RewardAdsArgs
    {
        public RewardedAdNetwork RewardedAdNetwork { get; set; }
        public AdPlacement AdPlacement { get; set; }
    }
    
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: / 
    /// DESCRIPTION: Sigleton klasa koja kontrolise sve vezano za reklame
    /// </summary>
    public sealed class AdsClient
    {
        #region Singleton

        private static AdsClient _instance = new AdsClient();
        public static AdsClient Instance => _instance;

        private AdsClient()
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            _areInterstitialAdsLoadingAfterDeath = RemoteConfigClient.Instance.IsLoadingAdsAfterDeath;
            _interstitialAfterDeathFrequency = RemoteConfigClient.Instance.InterstitialAfterDeathFrequency;
            _interstitialAdPlacementDefaultOrder = RemoteConfigClient.Instance.InterstitialAdPlacementDefaultOrder;
            _interstitialAdPlacementGameOverOrder = RemoteConfigClient.Instance.InterstitialAdPlacementGameOverOrder;
            _rewardedVideoAdPlacementDefaultOrder = RemoteConfigClient.Instance.RewardedVideoAdPlacementDefaultOrder;
            Advertising.Initialize();
            Advertising.AdMobClient.Init();
            Advertising.UnityAdsClient.Init();
            RegisterObservableListeners();
#endif
        }

        #endregion
    
    
    
    
    
        #region Private fields
        
        private CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private PersistentData _playerData;
        private int _interstitialAfterDeathFrequency;

        // rewarded video
        private bool _isAdMobRewardedVideoReady;
        private bool _isUnityAdsRewardedVideoReady;
    
        // interstitial
        private bool _isAdMobInterstitialReady;
        private bool _isUnityAdsInterstitialReady;
        private bool _isAdMobNoVideoInterstitialReady;
        private bool _isUnityAdsNoVideoInterstitialReady;

        // remote settings values
        private bool _areInterstitialAdsLoadingAfterDeath;
        private string _interstitialAdPlacementDefaultOrder;
        private string _interstitialAdPlacementGameOverOrder;
        private string _rewardedVideoAdPlacementDefaultOrder;

        // consts
        private const string CHAPTER_CLEARED_DOUBLE_REWARD = Constants.AdsPlacements.CHAPTER_CLEARED_DOUBLE_REWARD;
        private const string UNLOCK_CHAPTER_REWARDED_VIDEO = Constants.AdsPlacements.UNLOCK_CHAPTER;
        private const string ENERGY_REFILL = Constants.AdsPlacements.ENERGY_REFILL;
        private const string BUY_COINS_REWARDED_VIDEO = Constants.AdsPlacements.BUY_COINS;
        private const string INTERSTITIAL_ADPLACEMENT_DEFAULT_ADMOB_ORDER = "1";
        private const string INTERSTITIAL_ADPLACEMENT_GAMEOVER_ADMOB_ORDER = "1";
        private const string REWARDEDVIDEO_ADPLACEMENT_DEFAULT_ADMOB_ORDER = "1";
        private const string INTERSTITIAL_ADPLACEMENT_DEFAULT_UNITY_ORDER = "2";
        private const string INTERSTITIAL_ADPLACEMENT_GAMEOVER_UNITY_ORDER = "2";
        private const string REWARDEDVIDEO_ADPLACEMENT_DEFAULT_UNITY_ORDER = "2";

        #endregion





        #region Properties

        private static IObservable<RewardAdsArgs> RewardedAdCompletedAsObservable
        {
            get
            {
                return Observable.FromEvent<Action<RewardedAdNetwork, AdPlacement>, RewardAdsArgs>(
                    h => (network, placement) => h(new RewardAdsArgs {RewardedAdNetwork = network, AdPlacement = placement}),
                    h => Advertising.RewardedAdCompleted += h,
                    h => Advertising.RewardedAdCompleted -= h);
            }
        }

        #endregion
    
    
    


        #region Initialization

        /// <summary>
        /// Metoda koja inicijalizuje početne vrednosti
        /// WHY: Te vrednosti su neophodne za funkcionisanje ove klase
        /// </summary>
        /// <param name="playerData"></param>
        public IEnumerator Init(PersistentData playerData)
        {
            Advertising.Initialize();
            Advertising.AdMobClient.Init();
            Advertising.UnityAdsClient.Init();
            _playerData = playerData;
            _areInterstitialAdsLoadingAfterDeath = RemoteConfigClient.Instance.IsLoadingAdsAfterDeath;
            _interstitialAfterDeathFrequency = RemoteConfigClient.Instance.InterstitialAfterDeathFrequency;
            _interstitialAdPlacementDefaultOrder = RemoteConfigClient.Instance.InterstitialAdPlacementDefaultOrder;
            _interstitialAdPlacementGameOverOrder = RemoteConfigClient.Instance.InterstitialAdPlacementGameOverOrder;
            _rewardedVideoAdPlacementDefaultOrder = RemoteConfigClient.Instance.RewardedVideoAdPlacementDefaultOrder;
            RegisterObservableListeners();
            yield return new WaitUntil(Advertising.IsInitialized);
        }

        /// <summary>
        /// Prijavljivanje osluskivaca na Observable stream
        /// WHY: Da bi klasa mogla adekvatno da odgovori
        /// </summary>
        private void RegisterObservableListeners()
        {
            _playerData.ObserveEveryValueChanged(data => data.IsTutorialPlayed)
                .Where(isTutorialPlayed => isTutorialPlayed.Value)
                .Take(1)
                .Subscribe(_ => MessageBroker.Default.Receive<PlayerDied>().Subscribe(PlayerDiedEventHandler).AddTo(_compositeDisposable))
                .AddTo(_compositeDisposable);
            MessageBroker.Default.Receive<LevelStarted>()
                .Subscribe(_ => LevelStartedEventHandler())
                .AddTo(_compositeDisposable);
            RewardedAdCompletedAsObservable
                .ThrottleFirst(TimeSpan.FromSeconds(1d))
                .Subscribe(args => RewardedAdCompletedHandler(args.RewardedAdNetwork, args.AdPlacement))
                .AddTo(_compositeDisposable);
        }
        
        /// <summary>
        /// Odjavljivanje osluškivača sa događaja
        /// WHY: Neophodno da bi događaji i osluškivači normalno funkcionisali i da bi smo oslobodili memoriju
        /// </summary>
        public void UnregisterListeners()
        {
            _compositeDisposable.Dispose();
        }

        #endregion

    
    
    

        #region Interstitial

        /// <summary>
        /// Metoda koja prikazuje interstitial reklamu u zavisnosti od placementa
        /// WHY: Zato što je to jedan od načina zarade
        /// </summary>
        public void ShowInterstitialAds(AdPlacement adPlacement)
        {
#if UNITY_ANDROID
            _isUnityAdsInterstitialReady = Advertising.IsInterstitialAdReady(InterstitialAdNetwork.UnityAds, adPlacement);
            _isAdMobInterstitialReady = Advertising.IsInterstitialAdReady(InterstitialAdNetwork.AdMob, adPlacement);

            if (_interstitialAdPlacementDefaultOrder.Equals(INTERSTITIAL_ADPLACEMENT_DEFAULT_ADMOB_ORDER))
            {
                if (_isAdMobInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.AdMob, adPlacement);
                }
                else if (_isUnityAdsInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.UnityAds, adPlacement);
                }
            }
            else if (_interstitialAdPlacementDefaultOrder.Equals(INTERSTITIAL_ADPLACEMENT_DEFAULT_UNITY_ORDER))
            {
                if (_isUnityAdsInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.UnityAds, adPlacement);
                }
                else if (_isAdMobInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.AdMob, adPlacement);
                }
            }
#endif
        }

        #endregion

    
    
    

        #region Rewarded Video

        /// <summary>
        /// Metoda koja prikazuje rewarded video reklame
        /// WHY: Zato što je to jedan od načina zarade u igri
        /// </summary>
        /// <param name="adPlacement">Parametar na osnovu kog određujemo koja reklama će biti prikazana</param>
        public void ShowRewardedVideo(AdPlacement adPlacement)
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            ShowRewardedVideoInEditor(adPlacement);
#endif
            
#if UNITY_ANDROID
            ShowRewardedVideoOnAndroid(adPlacement);
#endif
        }

        /// <summary>
        /// Metoda koja simulira prikaz rewarded video reklama u Unity Editor
        /// WHY: Zato što je u njemu nemoguće prikazati reklame, a bitno nam je da to simuliramo kako bismo dalje mogli da testiramo igru
        /// </summary>
        /// <param name="adPlacement">Parametar na osnovu kog određujemo koja reklama će biti prikazana</param>
        private void ShowRewardedVideoInEditor(AdPlacement adPlacement)
        {
            FireEventsAfterRewardedAdCompleted(adPlacement);
        }

        /// <summary>
        /// Metoda koja prikazuje rewarded video reklame na Android uređajima
        /// WHY: Zato što hoćemo da razdvojimo prikazivanje ovih reklama na osnovu platforme za koju radimo
        /// </summary>
        /// <param name="adPlacement">Parametar na osnovu kog određujemo koja reklama će biti prikazana</param>
        private void ShowRewardedVideoOnAndroid(AdPlacement adPlacement)
        {
            _isAdMobRewardedVideoReady = Advertising.IsRewardedAdReady(RewardedAdNetwork.AdMob, adPlacement);
            _isUnityAdsRewardedVideoReady = Advertising.IsRewardedAdReady(RewardedAdNetwork.UnityAds, adPlacement);

            if (_rewardedVideoAdPlacementDefaultOrder.Equals(REWARDEDVIDEO_ADPLACEMENT_DEFAULT_ADMOB_ORDER))
            {
                if (_isAdMobRewardedVideoReady)
                {
                    Advertising.ShowRewardedAd(RewardedAdNetwork.AdMob, adPlacement);
                }
                else if (_isUnityAdsRewardedVideoReady)
                {
                    Advertising.ShowRewardedAd(RewardedAdNetwork.UnityAds, adPlacement);
                }
                else
                {
                    ShowNoVideoInterstitial();
                    LoadRewardedAdsForPlacement(adPlacement);
                    NativeUI.ShowToast("Video is not ready! \n Try again");
                }
            }
            else if (_rewardedVideoAdPlacementDefaultOrder.Equals(REWARDEDVIDEO_ADPLACEMENT_DEFAULT_UNITY_ORDER))
            {

                if (_isUnityAdsRewardedVideoReady)
                {
                    Advertising.ShowRewardedAd(RewardedAdNetwork.UnityAds, adPlacement);
                }
                else if (_isAdMobRewardedVideoReady)
                {
                    Advertising.ShowRewardedAd(RewardedAdNetwork.AdMob, adPlacement);
                }
                else
                {
                    ShowNoVideoInterstitial();
                    LoadRewardedAdsForPlacement(adPlacement);
                    NativeUI.ShowToast("Video is not ready! \n Try again");
                }
            }
        }

        /// <summary>
        /// Metoda koja prikazuje interstitial reklamu kada korisnik želi da nastavi igru sa mesta na kom je poginuo, ali mu sistem ne pušta nijednu reklamu
        /// WHY: Zato što hoćemo da pružimo mogućnost korisniku da nastavi dalje, i da mi dobijemo šansu da zaradimo novac
        /// </summary>
        private void ShowNoVideoInterstitial()
        {
            _isAdMobNoVideoInterstitialReady = Advertising.IsInterstitialAdReady(InterstitialAdNetwork.AdMob, AdPlacement.GameOver);
            _isUnityAdsNoVideoInterstitialReady = Advertising.IsInterstitialAdReady(InterstitialAdNetwork.UnityAds, AdPlacement.GameOver);

#if UNITY_ANDROID || UNITY_IOS
            WarmAppGamesAnalytics.LogVideoNotReadyEvent();
#endif

            if (_interstitialAdPlacementGameOverOrder.Equals(INTERSTITIAL_ADPLACEMENT_GAMEOVER_ADMOB_ORDER))
            {
        
                if (_isAdMobNoVideoInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.AdMob, AdPlacement.GameOver);
                }
                else if (_isUnityAdsNoVideoInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.UnityAds, AdPlacement.GameOver);
                }
            }
            else if (_interstitialAdPlacementGameOverOrder.Equals(INTERSTITIAL_ADPLACEMENT_GAMEOVER_UNITY_ORDER))
            {
                if (_isUnityAdsNoVideoInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.UnityAds, AdPlacement.GameOver);
                }
                else if (_isAdMobNoVideoInterstitialReady)
                {
                    Advertising.ShowInterstitialAd(InterstitialAdNetwork.AdMob, AdPlacement.GameOver);
                }
            }
        }

        /// <summary>
        /// Osluškivač događaja da je rewarded video reklama uspešno odgledana do kraja
        /// WHY: Zato što je to preduslov da igrač nastavi igru sa mesta na kome je poginuo 
        /// </summary>
        /// <param name="adNetwork">Parametar koji definiše koji provajder reklama je u pitanju</param>
        /// <param name="adPlacement">Parametar koji definiše poziciju na kojoj se rewarded video reklama prikazuje</param>
        private void RewardedAdCompletedHandler(RewardedAdNetwork adNetwork, AdPlacement adPlacement)
        {
            FireEventsAfterRewardedAdCompleted(adPlacement);
        }

        private void FireEventsAfterRewardedAdCompleted(AdPlacement adPlacement)
        {
            if (adPlacement.Equals(AdPlacement.Default))
            {
                MessageBroker.Default.Publish(new RewardVideoAfterDeathCompleted());
#if UNITY_ANDROID || UNITY_IOS
                LoadInterstitialAdsForPlacement(AdPlacement.Default);
#endif
            }
            else if (adPlacement.Equals(AdPlacement.PlacementWithName(CHAPTER_CLEARED_DOUBLE_REWARD)))
            {
                MessageBroker.Default.Publish(new RewardedVideoInChapterClearedCompleted());
            }
            else if (adPlacement.Equals(AdPlacement.PlacementWithName(UNLOCK_CHAPTER_REWARDED_VIDEO)))
            {
                MessageBroker.Default.Publish(new ChapterUnlockedWithAd());
            }
            else if (adPlacement.Equals(AdPlacement.PlacementWithName(ENERGY_REFILL)))
            {
                MessageBroker.Default.Publish(new EnergyRefilledWithAd());
            }
            else if (adPlacement.Equals(AdPlacement.PlacementWithName(BUY_COINS_REWARDED_VIDEO)))
            {
                MessageBroker.Default.Publish(new CoinsBought());
            }
        }

        #endregion

    




        #region Event Listeners

        /// <summary>
        /// Metoda koja osluškuje kada igrač pogine
        /// WHY: Zato što se tada najviše pustaju reklame i jednostavno moramo da znamo taj trenutak
        /// </summary>
        /// <param name="eventArgs">parametar u sebi sadrži podatak o broju puta koliko je igrač do tada poginuo</param>
        private void PlayerDiedEventHandler(PlayerDied eventArgs)
        {
            //TODO: ovaj uslov je ovde potpuno nepotreban i nesinhronizovan
//            if (_areInterstitialAdsLoadingAfterDeath)
//            {
//                LoadInterstitialAdsForPlacement(AdPlacement.Default);
//            }
        
            if (eventArgs.NumberOfDeaths % _interstitialAfterDeathFrequency == 0)
            {
                ShowInterstitialAds(AdPlacement.Default);
            }
        }

        /// <summary>
        /// Metoda koja osluškuje događaj koji se okida kada počne neki novi level, nebitno da li je to prvi put ili u toku igranja nakon uspešnog završetka prethodnog levela
        /// WHY: Zato što tada ponovo učitavamo sve interstitial i rewarded video reklame koje su nam neophodne
        /// </summary>
        private void LevelStartedEventHandler()
        {
            LoadInterstitialAdsForPlacement(AdPlacement.Default);
        }

        #endregion





        #region Custom Methods

        /// <summary>
        /// Univerzalna metoda za ucitavanje rewarded video reklama za odredjeni placement koja mora da bude public zato sto AdMob dozvoljava ucitavanje samo 1 RV reklame u trenutku i ona mora da bude konzumirana pre ucitavanja sledece
        /// WHY: Zato sto se u ucitavanju svih rewarded video reklama razlikuje samo placement
        /// </summary>
        /// <param name="adPlacement">Placement za koji ucitavamo reklamu</param>
        public void LoadRewardedAdsForPlacement(AdPlacement adPlacement)
        {
            if (!Advertising.IsRewardedAdReady(RewardedAdNetwork.AdMob, adPlacement))
            {
                Advertising.LoadRewardedAd(RewardedAdNetwork.AdMob, adPlacement);
            }

            if (!Advertising.IsRewardedAdReady(RewardedAdNetwork.UnityAds, adPlacement))
            {
                Advertising.LoadRewardedAd(RewardedAdNetwork.UnityAds, adPlacement);
            }
        }
        
        /// <summary>
        /// Univerzalna metoda za ucitavanje interstitial reklama za odredjeni placement
        /// WHY: Zato sto se u ucitavanju svih interstitial reklama razlikuje samo placement
        /// </summary>
        /// <param name="adPlacement">Placement za koji ucitavamo reklamu</param>
        public void LoadInterstitialAdsForPlacement(AdPlacement adPlacement)
        {
            if (!Advertising.IsInterstitialAdReady(InterstitialAdNetwork.AdMob, adPlacement))
            {
                Advertising.LoadInterstitialAd(InterstitialAdNetwork.AdMob, adPlacement);
            }

            if (!Advertising.IsInterstitialAdReady(InterstitialAdNetwork.UnityAds, adPlacement))
            {
                Advertising.LoadInterstitialAd(InterstitialAdNetwork.UnityAds, adPlacement);
            }
        }

        #endregion
    }
}