﻿using System;
using UniRx;
using UnityEngine;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// Statička klasa koja kontroliše komande za updavljanje unutar igre preko observable-a (UniRx)
    /// </summary>
    public static class Drag
    {
#if UNITY_EDITOR
        public static IObservable<long> LeftKeyArrowTapStream()
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.GetKey(KeyCode.LeftArrow))
                .DistinctUntilChanged();
        }
        
        public static IObservable<long> RightKeyArrowTapStream()
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.GetKey(KeyCode.RightArrow))
                .DistinctUntilChanged();
        }

        public static IObservable<long> LeftKeyArrowUpStream()
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.GetKeyUp(KeyCode.LeftArrow))
                .DistinctUntilChanged();
        }
        
        public static IObservable<long> RightKeyArrowUpStream()
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.GetKeyUp(KeyCode.RightArrow))
                .DistinctUntilChanged();
        }
#endif
        
#if UNITY_ANDROID || UNITY_IOS

        public static IObservable<Touch> LeftTapStream(float middleOfTheScreen)
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.touchCount > 0)
                .Select(_ => Input.GetTouch(0))
                .Where(touch => touch.position.x < middleOfTheScreen);
        }

        public static IObservable<Touch> RightTapStream(float middleOfTheScreen)
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.touchCount > 0)
                .Select(_ => Input.GetTouch(0))
                .Where(touch => touch.position.x > middleOfTheScreen);
        }

        public static IObservable<Touch> TapEndedStream()
        {
            return Observable.EveryUpdate()
                .Where(_ => Input.touchCount > 0)
                .Select(_ => Input.GetTouch(0))
                .Where(touch => touch.phase is TouchPhase.Ended or TouchPhase.Canceled);
        }
#endif
    }
}