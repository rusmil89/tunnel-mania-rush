﻿using UnityEngine;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE:
    /// GAME_OBJECT:
    /// </summary>
    public static class ColorExtensions
    {
        public static Color GetComplementary(this Color source)
        {
            Color.RGBToHSV(source, out float H, out float S, out float V);
            float oppositeH = (H + 0.5f) % 1f;
            return Color.HSVToRGB(oppositeH, S, V);
        }
    }
}