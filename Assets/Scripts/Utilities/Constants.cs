﻿using RSG;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: / 
    /// GAME_OBJECT: / 
    /// DESCRIPTION: Klasa koja čuva često korišćene konstante koje su potrebne u celoj aplikaciji
    /// </summary>
    public static class Constants
    {
        public enum CurrencyType
        {
            Coin,
            Token,
            Shield,
            Energy
        }
        /// <summary>
        /// Klasa koja čuva konstante koje se odnose na reklame
        /// </summary>
        public static class AdsPlacements
        {
            internal const string CHAPTER_CLEARED_CLAIM = "Chapter_Cleared_Claim_Button_Interstitial";
            internal const string CHAPTER_CLEARED_DOUBLE_REWARD = "Chapter_Cleared_Double_Reward";
            internal const string UNLOCK_CHAPTER = "Unlock_Chapter_Rewarded_Video";
            internal const string ENERGY_REFILL = "Energy_Refill";
            internal const string FINISH_MENU = "Finish_Menu_Continue_Interstitial";
            internal const string LEVEL_RESTART = "Level_Restart_Interstitial";
            internal const string BUY_COINS = "Buy_Coins_Rewarded_Video";
        }
        
        /// <summary>
        /// Klasa koja čuva konstante koje se odnose na scene
        /// </summary>
        public static class Scenes
        {
            internal const string STARTUP = "Startup";
            internal const string LOCALIZATION = "Localization";
            internal const string AUDIO = "Audio";
            internal const string SPLASH_SCENE_NAME = "Splash";
            internal const string TUTORIAL_SCENE_NAME = "Tutorial";
            internal const string MENU_SCENE_NAME = "Menu";
            internal const string LEVEL_SCENE_NAME = "Level";
        }

        /// <summary>
        /// Klasa koja cuva sve nazive tagova u igri
        /// </summary>
        public static class Tags
        {
            internal const string TUBE = "Tube";
            internal const string NEW_HIGH_SCORE = "NewHighScore";
            internal const string TUTORIAL_AVOIDED_TRIGGER = "TutorialAvoidedTrigger";
        }
        
        /// <summary>
        /// Klasa koja cuva sve linkove u igri
        /// </summary>
        public static class Links
        {
            internal const string ANDROID = "https://play.google.com/store/apps/details?id=com.warmAppGames.tunnelRushMania";
            internal const string IOS = "";
            internal const string FACEBOOK = "https://www.facebook.com/WarmAppGames/";
            internal const string INSTAGRAM = "https://www.instagram.com/warm_app_games/";
            internal const string DISCORD = "https://discord.gg/wKze6SP";
            internal const string TWITTER = "https://twitter.com/WarmappGames";
            internal const string PRIVACY_POLICY = "https://drive.google.com/open?id=1G_lAVVwro3Ai--wzl4TVzHMErHWRTcun_qbit5OSZok";
            internal const string TERMS_OF_SERVICE = "https://drive.google.com/open?id=1OvNr5OAqXGaf8zv0Qw4Dni8A_pdgf8I-FPGinmUq9rQ";
        }
        
        /// <summary>
        /// Klasa koja cuva nazive svih menija
        /// </summary>
        public static class Menus
        {
            internal const string CHAPTER_CLEARED = "ChapterClearedMenu";
            internal const string CONTROLS = "ControlsMenu";
            internal const string COUNTDOWN = "CountdownMenu";
            internal const string DAILY_REWARD = "DailyRewardMenu";
            internal const string ENERGY_REFILL = "EnergyRefillMenu";
            internal const string EXIT = "ExitMenu";
            internal const string FINISH = "FinishMenu";
            internal const string GAME_OVER = "GameOverMenu";
            internal const string GDPR = "GDPRMenu";
            internal const string GET_SHIELDS_MENU = "GetShieldsMenu";
            internal const string HOME = "HomeMenu";
            internal const string LIKE_DISLIKE = "LikeDislikeMenu";
            internal const string PAUSE = "PauseMenu";
            internal const string SCORE = "ScoreMenu";
            internal const string SETTINGS = "SettingsMenu";
            internal const string SHOP = "ShopMenu";
            internal const string TOKENS_MENU = "TokensMenu";
            internal const string TOP = "TopMenu";
            internal const string TOP_BUTTONS = "TopButtonsMenu";
            internal const string TUTORIAL = "TutorialMenu";
            internal const string UI_PANEL = "UIPanel";
            internal const string UNLOCK_CHAPTER_MENU = "UnlockChapterMenu";
            internal const string UPDATE = "UpdateMenu";
        }
        
        /// <summary>
        /// Klasa koja cuva nazive svih stanja koja se koriste u FSM za menije
        /// </summary>
        public static class MenusStatesNames
        {
            internal const string INITIALIZATION = "Initialization";
            internal const string SHOWN = "Shown";
            internal const string HIDDEN = "Hidden";
            internal const string ANIMATIONS_PLAYING = "AnimationsPlaying";
        }
        
        /// <summary>
        /// Klasa koja cuva sve tipove stanja koji se koriste u FSM za menije
        /// </summary>
        public static class MenusStatesTypes
        {
            public class Normal : AbstractState {}
        }

        /// <summary>
        /// Klasa koja cuva nazive cesto koriscenih objekata iz scena
        /// </summary>
        public static class GameObjectsNames
        {
            internal const string PANEL = "Panel";
            internal const string EXIT_BUTTON = "ExitButton";
            internal const string REFILL_BUTTON = "RefillButton";
            internal const string INDICATOR = "Indicator";
        }
        
        /// <summary>
        /// Klasa koja cuva tipove formata koje koristimo za formatiranje stringova
        /// </summary>
        public static class StringFormats
        {
            internal const string F1 = "F1";
        }
        
        public static class Gyroscope
        {
            public const int SLIDER_MIN_VALUE = 0;
            public const int SLIDER_MAX_VALUE = 10;
            public const float GYROSCOPE_SENSITIVITY_BACKGROUND_MAX_VALUE = 0.01f;
            public const float GYROSCOPE_SENSITIVITY_BACKGROUND_MIN_VALUE = 0.2f;
            public const int GYROSCOPE_SENSITIVITY_SLIDER_COMPONENT_FACTOR = 50;
            public const float GYROSCOPE_SENSITIVITY_BACKGROUND_VALUE_FACTOR = 0.02f;
            public const float ROTATION_ACCELERATION_BACKGROUND_MAX_VALUE = 2000f;
            public const float ROTATION_ACCELERATION_BACKGROUND_MIN_VALUE = 200f;
            public const float ROTATION_ACCELERATION_SLIDER_COMPONENT_FACTOR = 0.005f;
            public const float ROTATION_ACCELERATION_VALUE_TEXT_FACTOR = 0.001f;
        }
        
        public static class Energy
        {
            public const int MAX_ENERGY = 15;
            public const int ENERGY_AMOUNT_FOR_PLAY = 5;
            public const int ENERGY_REFILL_AMOUNT = 1;
            public const int SECONDS_TO_REFILL_ONE_ENERGY = 720;
        }

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        /// <summary>
        /// Klasa koja cuva sve konstante koje se koriste u Editoru
        /// </summary>
        public static class Editor
        {
            /// <summary>
            /// Klasa koja cuva sve konstante za filtere po kojima se pretrazuju aseti
            /// </summary>
            public static class Filters
            {
                internal const string AUDIO_CLIP = "t:AudioClip";
                internal const string GAME_CONTROLLER = "t:GameData";
                internal const string METERS = "t:MetersSO";
                internal const string PLAYER_DATA = "t:PlayerData";
                internal const string AUDIO_DATA = "t:AudioData";
                internal const string STATIC_MENU = "t:StaticMenuSO";
                internal const string MENU = "t:MenuSO";
                internal const string PREFAB = "t:prefab";
                internal const string LEVEL_DATA = "t:LevelData";
                internal const string WORLD_DATA = "t:WorldData";
                internal const string GAME_EVENT = "t:GameEventSO";
                internal const string SPRITE = "t:sprite";
                internal const string PRESET = "t:preset";
            }
            
            /// <summary>
            /// Klasa koja cuva sve konstante za putanje na kojima se nalaze odredjeni aseti
            /// </summary>
            public static class Paths
            {
                internal const string AUDIO = "Assets/Audio";
                internal const string MENU_MUSIC = "Assets/Audio/Music/Menu";
                internal const string GAME_MUSIC = "Assets/Audio/Music/Game";
                internal const string SCRIPTABLE_OBJECTS = "Assets/ScriptableObjects";
                internal const string PREFAB_INSIDE_OBSTACLES = "Assets/Prefabs/Level/Obstacles/InsideObstacles";
                internal const string PREFAB_OUTSIDE_OBSTACLES = "Assets/Prefabs/Level/Obstacles/OutsideObstacles";
                internal const string MENUS_PREFABS = "Assets/Prefabs/UI/Menus";
                internal const string PREFABS = "Assets/Prefabs";
                internal const string TEXTURES = "Assets/Textures";
                internal const string PRESETS = "Assets/Presets";
            }
        }
#endif
    }
}