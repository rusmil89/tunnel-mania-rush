﻿using System;
using System.Linq;
using Tunnel.Controllers;
using Tunnel.VFX;
using UniRx;
using Unity.Linq;
using UnityEngine;
using Utilities;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: MainCamera
    /// Klasa koja kontrolise rotaciju kamere kako korisnik upravlja
    /// </summary>
    public class RotationSimulation : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] protected global::PlayerData _playerData;
        [SerializeField] protected GameData _gameData;
        [SerializeField] private Transform _rotationTarget;

        private Player _player;
        private CameraShakingEffect _cameraShakingEffect;

        protected bool _isCameraShaking;
        protected float _middleOfScreen;
        protected float _rotationSpeed;
        protected readonly Vector3 _insidePosition = new Vector3(0, -1.5f, 0);

        protected const float MINIMUM_ROTATION_SPEED = 0f;
        private float _rotationTapFactor = 250;

        #endregion
        
        
        


        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _rotationTarget = gameObject.Parent().transform;
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            SetInitialValues();
        }

        protected virtual void SetInitialValues()
        {
            _player = GetComponentInChildren<Player>();
            _cameraShakingEffect = GetComponent<CameraShakingEffect>();
            _middleOfScreen = Screen.width / 2;
            _rotationSpeed = MINIMUM_ROTATION_SPEED;
            _rotationTapFactor = RemoteConfigClient.Instance.RotationSpeedFactor;
        }

        private void OnEnable()
        {
            _cameraShakingEffect.IsCameraShaking
                .Subscribe(isCameraShaking => _isCameraShaking = isCameraShaking)
                .AddTo(this);
        }

        // Start is called before the first frame update
        private void Start()
        {
            Screen.sleepTimeout = _playerData.PersistentData.IsGyroscopeEnabled.Value ? SleepTimeout.NeverSleep : SleepTimeout.SystemSetting;
            PerformSimulation();
        }

        private void PerformSimulation()
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            PerformSimulationInEditor();
#endif
            
#if UNITY_ANDROID || UNITY_IOS
            PerformSimulationOnDevice();
#endif
        }

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void PerformSimulationInEditor()
        {
            // observable stream koji osluškuje konstantan pririsak na levu strelicu kako bismo postavili smer kretanja igrača na levo
            Drag.LeftKeyArrowTapStream()
                .Where(_ => !_playerData.IsPlayerDead && !_gameData.IsGamePaused && !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(_player.IsOutside ? 1 : -1))
                .AddTo(this);

            // observable stream koji osluškuje konstantan pririsak na desnu strelicu kako bismo postavili smer kretanja igrača na desno
            Drag.RightKeyArrowTapStream()
                .Where(_ => !_playerData.IsPlayerDead && !_gameData.IsGamePaused && !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(_player.IsOutside ? -1 : 1))
                .AddTo(this);

            Drag.LeftKeyArrowUpStream()
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);

            Drag.RightKeyArrowUpStream()
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);
        }
#endif
        
#if UNITY_ANDROID || UNITY_IOS
        private void PerformSimulationOnDevice()
        {
            // observable stream zTa tap na levu stranu ekrana koji postavlja smer kretanja igrača
            Drag.LeftTapStream(_middleOfScreen)
                .Where(_ => !_playerData.IsPlayerDead && !_gameData.IsGamePaused && !_isCameraShaking && !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                .Subscribe(_ => RotatePlayer(_player.IsOutside ? 1 : -1))
                .AddTo(this);

            // observable stream za tap na desnu stranu ekrana koji postavlja smer kretanja igrača
            Drag.RightTapStream(_middleOfScreen)
                .Where(_ => !_playerData.IsPlayerDead && !_gameData.IsGamePaused && !_isCameraShaking && !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                .Subscribe(_ => RotatePlayer(_player.IsOutside ? -1 : 1))
                .AddTo(this);

            Drag.TapEndedStream()
                .DistinctUntilChanged()
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);

            Observable.EveryUpdate()
                .Where(_ => _playerData.PersistentData.IsGyroscopeEnabled.Value && !_playerData.IsPlayerDead && !_gameData.IsGamePaused && !_isCameraShaking)
                .Where(_ => Math.Abs(Input.acceleration.x) >= _playerData.PersistentData.GyroscopeSensitivity.Value)
                .Subscribe(_ => RotatePlayer(_player.IsOutside ? -Input.acceleration.x : Input.acceleration.x))
                .AddTo(this);

            Observable.EveryUpdate()
                .Where(_ => _playerData.PersistentData.IsGyroscopeEnabled.Value && _rotationSpeed > MINIMUM_ROTATION_SPEED)
                .Where(_ => Math.Abs(Input.acceleration.x) < _playerData.PersistentData.GyroscopeSensitivity.Value)
                .DistinctUntilChanged()
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);
        }
        
#endif

        #endregion
        
        
        


        #region Custom Methods

        /// <summary>
        /// Metoda koja rotira igraca na osnovu odgovarajucih uslova
        /// WHY: Zato sto je to osnovni nacin izbegavanja prepreka u igri
        /// </summary>
        /// <param name="direction">Smer rotacije koji se podesava u zavisnosti od toga da li je igrac unutar ili van tunela</param>
        protected void RotatePlayer(float direction)
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            _rotationSpeed = Mathf.MoveTowards(_rotationSpeed, _gameData.PlayerRotationSpeed, _rotationTapFactor * Time.deltaTime);
#endif
            
#if UNITY_ANDROID || UNITY_IOS
            _rotationSpeed = _playerData.PersistentData.IsGyroscopeEnabled.Value
                ? Mathf.MoveTowards(_rotationSpeed, _gameData.PlayerRotationSpeed, _playerData.PersistentData.RotationAccelerationFactor.Value * Math.Abs(Input.acceleration.x) * Time.deltaTime)
                : Mathf.MoveTowards(_rotationSpeed, _gameData.PlayerRotationSpeed, _rotationTapFactor * Time.deltaTime);
#endif
            transform.RotateAround(_rotationTarget.position, direction * _rotationTarget.forward, _rotationSpeed * Time.deltaTime);
        }

        /// <summary>
        /// Metoda koja resetuje brzinu rotacije igraca kada on prestane da se rotira
        /// WHY: Zato sto zelimo da simuliramo rotaciju koja krece iz neke manje brzine i povecava se postepeno ka vecoj
        /// </summary>
        protected void ResetRotationSpeed()
        {
            _rotationSpeed = MINIMUM_ROTATION_SPEED;
        }
        
        #endregion
    }
}