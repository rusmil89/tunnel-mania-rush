using System;
using System.Collections;
using Tunnel.Controllers;
using UniRx;
using Unity.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Tunnel.Utilities
{
    internal sealed class SceneArgs
    {
        public Scene Scene { get; internal set; }
        public LoadSceneMode LoadSceneMode { get; internal set; }
    }
    
    
    
    
    
    
    public class FadingEffect
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly CompositeDisposable _disposable;
        private readonly BoolReactiveProperty _isFadeIn;
        private float _lastTime;
        private bool _isLoadingScene;
        private float _alpha;
        private float _fadeDamp;
        private GameObject _init;
        private CanvasGroup _myCanvasGroup;

        #endregion





        #region Properties

        private IObservable<SceneArgs> SceneLoaded =>
            Observable.FromEvent<UnityAction<Scene, LoadSceneMode>, SceneArgs>(
                h => (scene, mode) => h(new SceneArgs { Scene = scene, LoadSceneMode = mode }),
                h => SceneManager.sceneLoaded += h,
                h => SceneManager.sceneLoaded -= h);

        #endregion

        
        
        
        
        #region Custom Methods

        public FadingEffect()
        {
            _disposable = new CompositeDisposable();
            _isFadeIn = new BoolReactiveProperty(false);
            RegisterObservableListeners();
        }

        private void RegisterObservableListeners()
        {
            SceneLoaded
                .SkipWhile(_ => _alpha < 0.9f)
                .Subscribe(HandleSceneLoaded)
                .AddTo(_disposable);
            _isFadeIn
                .Where(isFadeIn => isFadeIn)
                .Delay(TimeSpan.FromMilliseconds(500d))
                .Subscribe(HandleFadingIn)
                .AddTo(_disposable);
            Observable.OnceApplicationQuit()
                .Subscribe(_ => _disposable.Dispose())
                .AddTo(_disposable);
        }

        private void HandleFadingIn(bool isFadingIn)
        {
            Observable.FromCoroutine(FadeItIn)
                .SelectMany(FinishFading)
                .Subscribe()
                .AddTo(_disposable);
        }

        private void HandleSceneLoaded(SceneArgs args)
        {
            _isFadeIn.Value = true;
        }
        
        public IObservable<Unit> Fade(Color col, float multiplier)
        {
            return Observable.FromCoroutine<Unit>((observer, token) => AsyncFade(col, multiplier, observer));
        }

        private IEnumerator AsyncFade(Color color, float multiplier, IObserver<Unit> observer)
        {
            _init = new GameObject();
            SceneManager.MoveGameObjectToScene(_init, SceneManager.GetSceneByName(Constants.Scenes.STARTUP));
            _init.name = "Fader";
            Canvas myCanvas = _init.AddComponent<Canvas>();
            myCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            myCanvas.sortingOrder = 100;
            _myCanvasGroup = _init.AddComponent<CanvasGroup>();
            _myCanvasGroup.alpha = 0f;
            Image bgImage = _init.AddComponent<Image>();
            bgImage.color = color;
            _fadeDamp = multiplier;
            Observable.FromCoroutine(FadeItOut)
                .Subscribe()
                .AddTo(_disposable);

            yield return new WaitUntil(() => _isLoadingScene);
            observer.OnNext(default);
            observer.OnCompleted();
        }

        private IEnumerator FadeItOut()
        {
            _lastTime = Time.time;
            float coDelta = _lastTime;
            bool hasFadedOut = false;

            while (!hasFadedOut)
            {
                coDelta = Time.time - _lastTime;
                //Fade out
                _alpha = NewAlpha(coDelta, 1, _alpha);
                if (_alpha == 1 && !_isLoadingScene)
                {
                    _isLoadingScene = true;
                    hasFadedOut = true;
                }
                
                _lastTime = Time.time;
                _myCanvasGroup.alpha = _alpha;
                yield return null;
            }
        }

        private IEnumerator FadeItIn()
        {
            _lastTime = Time.time;
            float coDelta = _lastTime;
            bool hasFadedIn = false;
            
            while (!hasFadedIn)
            {
                coDelta = Time.time - _lastTime;
                _alpha = NewAlpha(coDelta, 0, _alpha);
                if (_alpha == 0)
                {
                    hasFadedIn = true;
                }
                
                _lastTime = Time.time;
                _myCanvasGroup.alpha = _alpha;
                yield return null;
            }
        }

        private IEnumerator FinishFading()
        {
            _isLoadingScene = false;
            _isFadeIn.Value = false;
            yield return null;
            _init.Destroy();
        }

        private float NewAlpha(float delta, int to, float currentAlpha)
        {
            switch (to)
            {
                case 0:
                    currentAlpha -= _fadeDamp * delta;
                    if (currentAlpha <= 0)
                        currentAlpha = 0;

                    break;
                case 1:
                    currentAlpha += _fadeDamp * delta;
                    if (currentAlpha >= 1)
                        currentAlpha = 1;

                    break;
            }

            return currentAlpha;
        }

        #endregion
    }
}