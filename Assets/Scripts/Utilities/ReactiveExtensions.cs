﻿using System;
using TMPro;
using UniRx;
using Utilities;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: / 
    /// DESCRIPTION: Staticka klasa koja sluzi za extension metode koje su potrebne iskljucivo za ovu igru
    /// </summary>
    public static class ReactiveExtensions
    {
        /// <summary>
        /// Metoda koja uzima svaki n-ti element asinhronog Observable stream-a
        /// </summary>
        /// <param name="source">Observable stream na kojim primenjujemo ovu metodu</param>
        /// <param name="step">n-ti element koji uzimamo u obzir. Podrazumevana vrednost je 1, sto znaci da svaki elemenat uzimamo</param>
        /// <typeparam name="T">Genericki tip koji nam pokazuje da je ova metoda primenljiva nad observable streamom bilo kog tipa</typeparam>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static IObservable<T> TakeEvery<T>(this IObservable<T> source, int step = 1)
        {
            if (step < 1) throw new ArgumentOutOfRangeException(nameof(step));
            return Observable.Defer(() =>
            {
                int index = -1;
                return source.Where(value => (index = ++index % step) == 0);
            });
        }

        /// <summary>
        /// Metoda koja zamenjuje standardni Where operator
        /// WHY: Da bih vezbao pisanje takvih metoda i da bih naucio nesto novo
        /// </summary>
        /// <param name="source">Observable stream nad kojim se poziva ova metoda</param>
        /// <param name="predicate">Uslov koji treba da bude ispunjen</param>
        /// <typeparam name="T">Genericki tip podataka koji nam omogucava da ovu metodu primenimo na Observable stream sa bilo kojim tipom podataka u sebi</typeparam>
        /// <returns></returns>
        public static IObservable<T> CustomWhere<T>(this IObservable<T> source, Func<T, bool> predicate)
        {
            return source.SelectMany(item => predicate(item) ? Observable.Return(item) : Observable.Empty<T>());
        }
        
        /// <summary>
        /// Metoda koja prosiruje mogucnost prijavljivanja promene vrednosti na TMP text polje
        /// WHY: Zato sto ja koristim tu komponentu umesto standardne Unity UI Text komponente
        /// </summary>
        /// <param name="source">Observable Stream nad kojim se poziva metoda</param>
        /// <param name="text">TMP Text polje u koje upisujemo vrednosti iz source</param>
        /// <returns></returns>
        public static IDisposable SubscribeToText<T>(this IObservable<T> source, TMP_Text text)
        {
            return source.SubscribeWithState(text, (x, t) => t.text = x.ToString());
        }
        
        //TODO: napraviti i custom skip
        //TODO: napraviti custom take
        
        public static IObservable<T> Debug<T>(this IObservable<T> source, string label = null)
        {
#if DEBUG || DEVELOPMENT_BUILD || ENABLE_LOG
            var l = label == null ? "" : "[" + label + "]";
            return source.Materialize()
                .Do(x => Logging.Log(l + x))
                .Dematerialize()
                .DoOnCancel(() => Logging.Log(l + "OnCancel"))
                .DoOnSubscribe(() => Logging.Log(l + "OnSubscribe"));

#else
            return source;
#endif
        }

        /// <summary>
        /// Debug helper of observbale stream. Works for only DEBUG symbol.
        /// </summary>
        public static IObservable<T> Debug<T>(this IObservable<T> source, UniRx.Diagnostics.Logger logger)
        {
#if DEBUG || DEVELOPMENT_BUILD || ENABLE_LOG
            return source.Materialize()
                .Do(x => logger.Debug(x.ToString()))
                .Dematerialize()
                .DoOnCancel(() => logger.Debug("OnCancel"))
                .DoOnSubscribe(() => logger.Debug("OnSubscribe"));

#else
            return source;
#endif
        }
    }
}
