﻿using System;
using UniRx;
using Unity.Linq;
using UnityEngine;
using Utilities;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: LevelBuilder
    /// GAME_OBJECT: MainCamera
    /// DESCRIPTION: Kontrolise okretanje kamere zbog testiranja prepreka
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class LevelBuilderRotationSimulator : MonoBehaviour
    {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        #region Private Fields
        
        private Transform _rotationTarget;
        private bool _isOutside;
        private readonly Vector3 _outsidePosition = new Vector3(0, 2.2f, 0);

        #endregion
        
        
        
        
        
        #region Monobehaviour Events

        private void Awake()
        {
            _rotationTarget = gameObject.Parent().transform;
            _isOutside = Math.Abs(Camera.main.transform.position.y - _outsidePosition.y) < 0.1f;
        }

        // Start is called before the first frame update
        private void Start()
        {
            Drag.LeftKeyArrowTapStream()
                .Subscribe(_ => RotatePlayer(_isOutside ? 1 : -1))
                .AddTo(this);

            // observable stream koji osluškuje konstantan pririsak na desnu strelicu kako bismo postavili smer kretanja igrača na desno
            Drag.RightKeyArrowTapStream()
                .Subscribe(_ => RotatePlayer(_isOutside ? -1 : 1))
                .AddTo(this);
        }

        private void RotatePlayer(int direction)
        {
            transform.RotateAround(_rotationTarget.position, direction * _rotationTarget.forward, 160f * Time.deltaTime);
        }

        #endregion
#endif
    }
}
