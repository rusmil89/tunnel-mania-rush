using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Utilities
{
    public static class ReferencesClient
    {
        public static void CheckReferences(MonoBehaviour monoBehaviour, GameObject referencedObject)
        {
            var declaredSerializedFields = monoBehaviour
                .GetType()
                .GetTypeInfo()
                .DeclaredFields
                .Where(info => info.CustomAttributes.Any(data => data.AttributeType == typeof(SerializeField)));
            foreach (var field in declaredSerializedFields)
            {
                var valueOfField = field.GetValue(monoBehaviour);
                Debug.Assert(valueOfField != null, referencedObject);
            }
        }
        
        public static void CheckReferences(ScriptableObject scriptableObject)
        {
            var declaredSerializedFields = scriptableObject
                .GetType()
                .GetTypeInfo()
                .DeclaredFields
                .Where(info => info.CustomAttributes.Any(data => data.AttributeType == typeof(SerializeField)));
            foreach (var field in declaredSerializedFields)
            {
                var valueOfField = field.GetValue(scriptableObject);
                Debug.Assert(valueOfField != null, scriptableObject);
            }
        }
    }
}