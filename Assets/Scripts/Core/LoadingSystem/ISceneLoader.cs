﻿using System;
using UnityEngine.SceneManagement;

namespace Core
{
    public interface ISceneLoader
    {
        IObservable<float> LoadScene(string sceneName);
        IObservable<float> UnloadScene(string sceneName);
        IObservable<float> UnloadScene(Scene sceneName);
    }
}
