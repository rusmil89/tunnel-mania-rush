﻿using System;
using System.Collections;
using System.Threading;
using Tunnel.Controllers;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.LoadingSystem
{
    public class AsyncSceneLoader : ISceneLoader
    {
        public IObservable<float> LoadScene(string sceneName)
        {
            return Observable.FromCoroutine<float>((observer,
                cancellationToken) => AsynchronousLoad(sceneName,
                observer,
                cancellationToken));
        }

        public IObservable<float> UnloadScene(string sceneName) =>
            Observable.FromCoroutine<float>((observer,
                cancellationToken) => AsynchronousLoad(sceneName,
                observer,
                cancellationToken,
                true));

        private IEnumerator AsynchronousLoad(string sceneName, IObserver<float> observer, CancellationToken cancellationToken, bool unload = false)
        {
            yield return null;
            var asyncOperation = !unload ? SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive) : SceneManager.UnloadSceneAsync(sceneName);
            
            while (!asyncOperation.isDone && !cancellationToken.IsCancellationRequested)
            {
                // [0, 0.9] > [0, 1]
                float loadProgress = Mathf.Clamp01(asyncOperation.progress);
                observer.OnNext(loadProgress);
                Utilities.Logging.Log($"{this} , Async load progress = {loadProgress}");
                yield return null;
            }

            if (!cancellationToken.IsCancellationRequested)
            {
                Utilities.Logging.Log($"{this} , Async load complete");
                asyncOperation.allowSceneActivation = true;
                observer.OnNext(asyncOperation.progress);
                observer.OnCompleted();
            }
        }

        public IObservable<float> UnloadScene(Scene scene) =>
            Observable.FromCoroutine<float>((observer,
                cancellationToken) => AsynchronousUnload(scene,
                observer,
                cancellationToken));

        private IEnumerator AsynchronousUnload(Scene scene, IObserver<float> observer, CancellationToken cancellationToken)
        {
            yield return null;
            AsyncOperation asyncOperation = SceneManager.UnloadSceneAsync(scene);

            while (!asyncOperation.isDone && !cancellationToken.IsCancellationRequested)
            {
                // [0, 0.9] > [0, 1]
                float loadProgress = Mathf.Clamp01(asyncOperation.progress);
                observer.OnNext(loadProgress);
                Utilities.Logging.Log($"{this} , Async unload progress = {loadProgress}");
                yield return null;
            }

            if (!cancellationToken.IsCancellationRequested)
            {
                Utilities.Logging.Log($"{this} , Async unload complete");
                observer.OnNext(asyncOperation.progress);
                observer.OnCompleted();
            }
        }
    }
}