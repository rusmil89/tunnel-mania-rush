using System;
using System.Linq;
using Core.Events;
using UniRx;
using UnityEngine.SceneManagement;

namespace Core.LoadingSystem
{
    public class UnloadAllScenesExceptCommand : IDisposable
    {
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private readonly ISceneLoader _sceneLoader;

        public UnloadAllScenesExceptCommand(ISceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
            RegisterEventListeners();
        }

        private void RegisterEventListeners()
        {
            AsyncMessageBroker.Default
                .Subscribe<UnloadAllScenesExcept>(HandleUnloadingScenes)
                .AddTo(_disposable);
        }
        
        private IObservable<Unit> HandleUnloadingScenes(UnloadAllScenesExcept unloadAllParams)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(unloadAllParams.Scenes[0]));

            IObservable<float> lastObservable = null;

            int count = SceneManager.sceneCount;

            for (int i = 0; i < count; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);

                if (scene.isLoaded && !unloadAllParams.Scenes.Contains(scene.name))
                {
                    lastObservable = lastObservable != null
                        ? lastObservable.Merge(_sceneLoader.UnloadScene(scene.name))
                        : _sceneLoader.UnloadScene(scene.name);
                }
            }

            return lastObservable.AsUnitObservable();
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}