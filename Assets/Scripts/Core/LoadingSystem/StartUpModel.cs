using UniRx;

namespace Core.LoadingSystem
{
    public class StartUpModel
    {
        public enum ELoadingProgress
        {
            NotLoaded = -1,
            Audio = 30,
            Localization = 60,
            Splash = 100
        }

        public ReactiveProperty<ELoadingProgress> LoadingProgress { get; }

        public StartUpModel()
        {
            LoadingProgress = new ReactiveProperty<ELoadingProgress>(ELoadingProgress.Audio);
        }
    }
}