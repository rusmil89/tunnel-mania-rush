using System;
using Core.Events;
using UniRx;

namespace Core.LoadingSystem
{
    public class LoadUnloadScenesCommand : IDisposable
    {
        private readonly CompositeDisposable _disposable = new CompositeDisposable();
        private readonly ISceneLoader _sceneLoader;

        public LoadUnloadScenesCommand(ISceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
            RegisterEventListeners();
        }

        private void RegisterEventListeners()
        {
            AsyncMessageBroker.Default
                .Subscribe<LoadUnloadScenes>(HandleLoadingUnloadingScenes)
                .AddTo(_disposable);
        }

        private IObservable<Unit> HandleLoadingUnloadingScenes(LoadUnloadScenes arg)
        {
            IObservable<float> lastObservable = null;
            if (arg.LoadScenes != null)
            {
                foreach (var scene in arg.LoadScenes)
                {
                    lastObservable = lastObservable != null
                        ? lastObservable.Merge(_sceneLoader.LoadScene(scene))
                        : _sceneLoader.LoadScene(scene);
                }
            }

            if (arg.UnloadScenes != null)
            {
                foreach (var scene in arg.UnloadScenes)
                {
                    lastObservable = lastObservable != null
                        ? lastObservable.Merge(_sceneLoader.UnloadScene(scene))
                        : _sceneLoader.UnloadScene(scene);
                }
            }

            return lastObservable.AsUnitObservable();
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}
