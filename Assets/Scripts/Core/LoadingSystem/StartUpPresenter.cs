using Core.Events;
using RSG;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Core.LoadingSystem
{
    public class StartUpPresenter : MonoBehaviour
    {
        private const float FADING_MULTIPLIER = 2f;
        private IState _rootState;
        private StartUpModel _startUpModel;
        private FadingEffect _fadingEffect;

        [Inject]
        private void InjectDependencies(StartUpModel startUpModel, FadingEffect fadingEffect)
        {
            _startUpModel = startUpModel;
            _fadingEffect = fadingEffect;
        }

        private void Start()
        {
            SetStartupStates();
            RegisterObservableListeners();
        }

        private void RegisterObservableListeners()
        {
            _startUpModel.LoadingProgress
                .Subscribe(loadingProgress => _rootState.ChangeState(loadingProgress.ToString()))
                .AddTo(this);
            MessageBroker.Default.Receive<ShouldLoadTutorialScene>()
                .Subscribe(_ => _rootState.ChangeState(Constants.Scenes.TUTORIAL_SCENE_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ShouldLoadMainMenuScene>()
                .Subscribe(_ => _rootState.ChangeState(Constants.Scenes.MENU_SCENE_NAME))
                .AddTo(this);
            MessageBroker.Default.Receive<ShouldLoadLevelScene>()
                .Subscribe(_ => _rootState.ChangeState(Constants.Scenes.LEVEL_SCENE_NAME))
                .AddTo(this);
        }

        private void SetStartupStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.AUDIO)
                .Enter(OnEnterLoadingAudio)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.LOCALIZATION)
                .Enter(OnEnterLoadingLocalization)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.SPLASH_SCENE_NAME)
                .Enter(OnEnterLoadingSplash)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.TUTORIAL_SCENE_NAME)
                .Enter(OnEnterLoadingTutorial)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.MENU_SCENE_NAME)
                .Enter(OnEnterLoadingMainMenu)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(Constants.Scenes.LEVEL_SCENE_NAME)
                .Enter(OnEnterLoadingLevel)
                .End()
                .Build();
        }

        private void OnEnterLoadingAudio(Constants.MenusStatesTypes.Normal args)
        {
            LoadUnloadScenes loadUnloadScenes = new LoadUnloadScenes(new[] { Constants.Scenes.AUDIO }, null);
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(OnLoadingAudioFinished)
                .AddTo(this);
        }

        private void OnLoadingAudioFinished(Unit obj)
        {
            _startUpModel.LoadingProgress.Value = StartUpModel.ELoadingProgress.Localization;
        }

        private void OnEnterLoadingLocalization(Constants.MenusStatesTypes.Normal obj)
        {
            LoadUnloadScenes loadUnloadScenes = new LoadUnloadScenes(new[] { Constants.Scenes.LOCALIZATION }, null);
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(OnLoadingLocalizationFinished)
                .AddTo(this);
        }

        private void OnLoadingLocalizationFinished(Unit obj)
        {
            _startUpModel.LoadingProgress.Value = StartUpModel.ELoadingProgress.Splash;
        }

        private void OnEnterLoadingSplash(Constants.MenusStatesTypes.Normal obj)
        {
            LoadUnloadScenes loadUnloadScenes = new LoadUnloadScenes(new[] { Constants.Scenes.SPLASH_SCENE_NAME }, null);
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(_ => SetActiveScene(Constants.Scenes.SPLASH_SCENE_NAME))
                .AddTo(this);
        }

        private void OnEnterLoadingTutorial(Constants.MenusStatesTypes.Normal obj)
        {
            _fadingEffect.Fade(Color.black, FADING_MULTIPLIER)
                .Subscribe(LoadTutorial)
                .AddTo(this);
        }

        private void LoadTutorial(Unit obj)
        {
            LoadUnloadScenes loadUnloadScenes =
                new LoadUnloadScenes(new[] { Constants.Scenes.TUTORIAL_SCENE_NAME }, new[] { Constants.Scenes.SPLASH_SCENE_NAME });
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(_ => SetActiveScene(Constants.Scenes.TUTORIAL_SCENE_NAME))
                .AddTo(this);
        }

        private void OnEnterLoadingMainMenu(Constants.MenusStatesTypes.Normal obj)
        {
            _fadingEffect.Fade(Color.black, FADING_MULTIPLIER)
                .Subscribe(LoadMainMenu)
                .AddTo(this);
        }

        private void LoadMainMenu(Unit obj)
        {
            LoadUnloadScenes loadUnloadScenes =
                new LoadUnloadScenes(new[] { Constants.Scenes.MENU_SCENE_NAME }, null);
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(OnMenuSceneLoaded)
                .AddTo(this);
        }

        private void OnMenuSceneLoaded(Unit obj)
        {
            UnloadAllScenesExcept unloadAllScenesExcept = new UnloadAllScenesExcept(new[]
            {
                Constants.Scenes.STARTUP,
                Constants.Scenes.LOCALIZATION,
                Constants.Scenes.AUDIO,
                Constants.Scenes.MENU_SCENE_NAME
            });
            AsyncMessageBroker.Default.PublishAsync(unloadAllScenesExcept)
                .Subscribe(_ => SetActiveScene(Constants.Scenes.MENU_SCENE_NAME))
                .AddTo(this);
        }

        private void OnEnterLoadingLevel(Constants.MenusStatesTypes.Normal obj)
        {
            _fadingEffect.Fade(Color.black, FADING_MULTIPLIER)
                .Subscribe(LoadLevel)
                .AddTo(this);
        }

        private void LoadLevel(Unit obj)
        {
            LoadUnloadScenes loadUnloadScenes =
                new LoadUnloadScenes(new[] { Constants.Scenes.LEVEL_SCENE_NAME }, new [] { Constants.Scenes.MENU_SCENE_NAME });
            AsyncMessageBroker.Default.PublishAsync(loadUnloadScenes)
                .Subscribe(_ => SetActiveScene(Constants.Scenes.LEVEL_SCENE_NAME))
                .AddTo(this);
        }

        private void SetActiveScene(string sceneName)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        }
    }
}