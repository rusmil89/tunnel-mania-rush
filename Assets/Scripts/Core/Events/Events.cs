namespace Core.Events
{
    #region Load/Unload Events

    public class LoadUnloadScenes
    {
        public string[] LoadScenes { get; }
        public string[] UnloadScenes { get; }

        public LoadUnloadScenes(string[] loadScenes, string[] unloadScenes)
        {
            LoadScenes = loadScenes;
            UnloadScenes = unloadScenes;
        }
    }

    public class UnloadAllScenesExcept
    {
        public string[] Scenes { get; }

        public UnloadAllScenesExcept(string[] scenes)
        {
            Scenes = scenes;
        }
    }
    
    public class ShouldLoadTutorialScene {}
    public class ShouldLoadMainMenuScene {}
    public class ShouldLoadLevelScene {}

    #endregion




    #region Localization Events
    
    public enum ELanguage
    {
        English,
        Chinese
    }

    public class ChangeLanguage
    {
        public ELanguage Language { get; }

        public ChangeLanguage(ELanguage language)
        {
            Language = language;
        }
    }

    #endregion
}