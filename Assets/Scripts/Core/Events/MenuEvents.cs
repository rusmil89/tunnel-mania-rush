﻿namespace Core.Events
{
    #region Menus Calling Events

    internal sealed class ChapterClearedMenuCalled {}
    internal sealed class CountdownMenuCalled {}
    internal sealed class DailyRewardMenuCalled {}
    internal sealed class EnergyRefillMenuCalled {}
    internal sealed class FinishMenuCalled {}
    internal sealed class GameOverMenuCalled {}
    internal sealed class GDPRMenuCalled {}
    internal sealed class GetShieldsMenuCalled {}
    internal sealed class HomeAndTopButtonsMenuCalled {}
    internal sealed class PauseMenuCalled {}
    internal sealed class ScoreMenuCalled {}
    internal sealed class SettingsMenuCalled {}
    internal sealed class SettingsMenuFeedbackCalled {}
    internal sealed class ShopMenuCalled {}
    internal sealed class TopButtonsMenuCalled {}
    internal sealed class TopMenuCalled {}
    internal sealed class TokensMenuCalled {}
    internal sealed class UnlockChapterMenuCalled {}
    internal sealed class ExitCurrentMenuCalled {}

    #endregion

    



    #region Buttons Clicking Events
    
    internal sealed class LikeMenuYesButtonClicked { }
    internal sealed class LikeMenuNoButtonClicked { }
    internal sealed class FinishMenuContinueButtonClicked {}
    internal sealed class ChapterClearedMenuClaimButtonClicked {}
    internal sealed class ChapterClearedMenuDoubleRewardButtonClicked {}
    internal sealed class RateUsButtonClicked {}
    internal sealed class ButtonClicked {}
    internal sealed class CountdownFired {}
    internal sealed class CountdownGoFired {}

    #endregion





    #region Currency Events

    internal sealed class CurrenciesStartedMovingToTopMenu
    {
        public int CurrenciesWon { get; set; }
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    internal sealed class CurrenciesFinishedMovingToTopMenu 
    {
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    internal sealed class CurrenciesAnimationStarted
    {
        public int CurrenciesSpent { get; set; }
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    internal sealed class CurrenciesAddingToTopMenuAnimation 
    {
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    internal sealed class CurrenciesStartedMovingFromTopMenu
    {
        public int CurrenciesSpent { get; set; }
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    internal sealed class CurrenciesFinishedMovingFromTopMenu {}

    internal sealed class CurrenciesRemovingFromTopMenu
    {
        public Tunnel.Utilities.Constants.CurrencyType CurrencyType { get; set; }
    }

    #endregion





    #region Other Events
    
    internal sealed class WorldScrollSnapSelectionPageChanged {}
    internal sealed class ChapterScrollSnapSelectionPageChanged {}
    internal sealed class CountdownFinished {}
    internal sealed class SettingsMenuOpened {}
    internal sealed class LikeDislikeMenuOpened {}
    internal sealed class MenuIsShown {}
    internal sealed class MenuIsHidden {}
    internal sealed class ChapterClearedMenuStartedSpinningWheel {}
    internal sealed class ChapterClearedMenuFinishedSpinningWheel {}
    internal sealed class ChapterClearedMenuPresentRotated {}
    internal sealed class AnimateEnablingSpinTheWheel {}

    #endregion
}
