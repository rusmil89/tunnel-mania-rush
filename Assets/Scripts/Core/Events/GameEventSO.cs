﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Tunnel.Events
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "ScriptableObjects/Events", order = 0)]
    public class GameEventSO : ScriptableObject
    {
        #region Private Fields

        private bool isDispatching;
        
        // readonly
        private readonly List<GameEventListener> gameEventListeners = new List<GameEventListener>();

        #endregion





        #region Custom Methods

        public void Dispatch()
        {
            isDispatching = true;
            for (int i = 0; i < gameEventListeners.Count; i++)
            {
                gameEventListeners[i].OnEventRaised();
            }

            isDispatching = false;
        }

        public void RegisterListener(GameEventListener gameEventListener)
        {
            Assert.IsFalse(isDispatching);
            if (!gameEventListeners.Contains(gameEventListener))
            {
                gameEventListeners.Add(gameEventListener);
            }
        }

        public void UnregisterListener(GameEventListener gameEventListener)
        {
            Assert.IsFalse(isDispatching);
            if (gameEventListeners.Contains(gameEventListener))
            {
                gameEventListeners.Remove(gameEventListener);
            }
        }

        #endregion
    }
}