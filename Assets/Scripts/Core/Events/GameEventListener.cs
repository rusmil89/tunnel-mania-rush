﻿using UnityEngine;
using UnityEngine.Events;

namespace Tunnel.Events
{
    public class GameEventListener : MonoBehaviour
    {
        #region Private Fields

        [Tooltip("Event to register with.")]
        [SerializeField]
        private GameEventSO gameEvent;

        [Tooltip("Response to invoke when Event is raised.")]
        [SerializeField]
        private UnityEvent response;

        #endregion





        #region Monobehaviour Events

        private void OnEnable()
        {
            gameEvent.RegisterListener(this);
        }

        private void OnDisable()
        {
            gameEvent.UnregisterListener(this);
        }

        #endregion





        #region Custom Methods

        public void OnEventRaised()
        {
            response.Invoke();
        }

        #endregion
    }
}