using Core.LoadingSystem;
using Tunnel.Controllers;
using Tunnel.Utilities;
using Zenject;

namespace Core.Installers
{
    public class StartUpInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<StartUpModel>().AsSingle();
            Container.Bind<FadingEffect>().AsSingle();
            Container.BindInterfacesAndSelfTo<LoadUnloadScenesCommand>().AsSingle();
            Container.BindInterfacesAndSelfTo<UnloadAllScenesExceptCommand>().AsSingle();
            Container.BindInterfacesTo<AsyncSceneLoader>().AsSingle();
            Container.Bind<RemoteConfigClient>().AsSingle().NonLazy();
        }
    }
}