using Tunnel.UI;
using UnityEngine;
using Utilities;
using Zenject;

namespace Core.Installers
{
    public class MainMenuInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _updateMenuPrefab;
        [SerializeField] private GameObject _likeDislikeMenuPrefab;
        [SerializeField] private GameObject _settingsMenuPrefab;
        [SerializeField] private GameObject _exitMenuPrefab;
        [SerializeField] private GameObject _unlockChapterMenuPrefab;
        [SerializeField] private GameObject _energyRefillMenuPrefab;
        [SerializeField] private GameObject _shieldsMenuPrefab;
        [SerializeField] private GameObject _tokensMenuPrefab;
        [SerializeField] private GameObject _shopMenuPrefab;
        [SerializeField] private GameObject _dailyRewardMenuPrefab;
        [SerializeField] private GameObject _gdprMenuPrefab;

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif
        
        public override void InstallBindings()
        {
            Container.BindFactory<UpdateMenu, UpdateMenu.Factory>().FromComponentInNewPrefab(_updateMenuPrefab).AsSingle();
            Container.BindFactory<LikeDislikeMenu, LikeDislikeMenu.Factory>().FromComponentInNewPrefab(_likeDislikeMenuPrefab).AsSingle();
            Container.BindFactory<Settings, Settings.Factory>().FromComponentInNewPrefab(_settingsMenuPrefab).AsSingle();
            Container.BindFactory<ExitMenu, ExitMenu.Factory>().FromComponentInNewPrefab(_exitMenuPrefab).AsSingle();
            Container.BindFactory<UnlockChapterMenu, UnlockChapterMenu.Factory>().FromComponentInNewPrefab(_unlockChapterMenuPrefab).AsSingle();
            Container.BindFactory<EnergyRefillMenu, EnergyRefillMenu.Factory>().FromComponentInNewPrefab(_energyRefillMenuPrefab).AsSingle();
            Container.BindFactory<GetShieldsMenu, GetShieldsMenu.Factory>().FromComponentInNewPrefab(_shieldsMenuPrefab).AsSingle();
            Container.BindFactory<TokensMenu, TokensMenu.Factory>().FromComponentInNewPrefab(_tokensMenuPrefab).AsSingle();
            Container.BindFactory<ShopMenu, ShopMenu.Factory>().FromComponentInNewPrefab(_shopMenuPrefab).AsSingle();
            Container.BindFactory<DailyRewardMenu, DailyRewardMenu.Factory>().FromComponentInNewPrefab(_dailyRewardMenuPrefab).AsSingle();
            Container.BindFactory<GDPRMenu, GDPRMenu.Factory>().FromComponentInNewPrefab(_gdprMenuPrefab).AsSingle();
        }
    }
}