using Tunnel.UI;
using UnityEngine;
using Zenject;

namespace Core.Installers
{
    public class LevelInstaller : MonoInstaller
    {
        [SerializeField] private GameObject _pauseMenuPrefab;
        [SerializeField] private GameObject _gameOverMenuPrefab;
        [SerializeField] private GameObject _countdownMenuPrefab;
        [SerializeField] private GameObject _finishMenuPrefab;
        [SerializeField] private GameObject _scoreMenuPrefab;
        [SerializeField] private GameObject _chapterClearedMenuPrefab;
        [SerializeField] private GameObject _shieldsMenuPrefab;
        [SerializeField] private GameObject _tokensMenuPrefab;
        [SerializeField] private GameObject _shopMenuPrefab;
        [SerializeField] private GameObject _topButtonsMenuPrefab;
        
        public override void InstallBindings()
        {
            Container.BindFactory<PauseMenu, PauseMenu.Factory>().FromComponentInNewPrefab(_pauseMenuPrefab).AsSingle();
            Container.BindFactory<GameOverMenu, GameOverMenu.Factory>().FromComponentInNewPrefab(_gameOverMenuPrefab).AsSingle();
            Container.BindFactory<CountdownMenu, CountdownMenu.Factory>().FromComponentInNewPrefab(_countdownMenuPrefab).AsSingle();
            Container.BindFactory<FinishMenu, FinishMenu.Factory>().FromComponentInNewPrefab(_finishMenuPrefab).AsSingle();
            Container.BindFactory<ScoreMenu, ScoreMenu.Factory>().FromComponentInNewPrefab(_scoreMenuPrefab).AsSingle();
            Container.BindFactory<ChapterClearedMenu, ChapterClearedMenu.Factory>().FromComponentInNewPrefab(_chapterClearedMenuPrefab).AsSingle();
            Container.BindFactory<GetShieldsMenu, GetShieldsMenu.Factory>().FromComponentInNewPrefab(_shieldsMenuPrefab).AsSingle();
            Container.BindFactory<TokensMenu, TokensMenu.Factory>().FromComponentInNewPrefab(_tokensMenuPrefab).AsSingle();
            Container.BindFactory<ShopMenu, ShopMenu.Factory>().FromComponentInNewPrefab(_shopMenuPrefab).AsSingle();
            Container.BindFactory<TopButtonsMenu, TopButtonsMenu.Factory>().FromComponentInNewPrefab(_topButtonsMenuPrefab).AsSingle();
        }
    }
}