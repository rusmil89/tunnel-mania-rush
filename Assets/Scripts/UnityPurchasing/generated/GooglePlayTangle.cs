// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("kqivRqWuSob8+naWGEfAT4arBBDbEWYyb0wfEQVplfEsW2klqsPftjU+82RpK+A8TZ4YmZdZbOQFTqTfK5kaOSsWHRIxnVOd7BYaGhoeGxiHQvPYsUaGvdslDsdVg9aYs1bL5OELR6Fi7ZiEM7TclGIczzcQz+DcqlNJfgUmZO3kIq5g6imgDJsqdguZGhQbK5kaERmZGhobk+gJHuu9S1Zahav4RcQK0OAMyw//yeny7Y2NdYO5sEF4S7TJJvnXGdAm/ic6BaVu3S2Dq9oaf5cIigEHfxBDjmfOgtcn7TZdNmQihSORF+aXj8zcVZULQT+KEHHmMoqXCMKtHCriEpzp6RoB7zAyWTu84kUgn2Z4M0BUX1fTr77eakFWdn9wphkYGhsa");
        private static int[] order = new int[] { 5,13,2,5,5,10,9,13,11,9,12,12,13,13,14 };
        private static int key = 27;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
