// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("iI2OD4yCjb0PjIePD4yMjWkcJIQlUfOvuEeoWFSCW+ZZL6munHosIeoChTmtekYhoa3i/TuyjL0BOs5C6biumMaY1JA+GXp7ERNC3TdM1d36+qPs/f3h6KPu4uCi7P394eju7P3h6K3O6P/55Ovk7uz55OLjrcz4970PjPu9g4uO2JCCjIxyiYmOj4yLjtiQg4mbiZmmXeTKGfuEc3nmAL67173vvIa9hIuO2ImLno/Y3ryeimHwtA4G3q1etUk8MhfCh+ZypnGJi56P2N68nr2ci47YiYeeh8z9/f3h6K3f4uL5rc7MvZOagL27vbm/uL+8ub2+u9eagL64vb+9tL+8ub2tzsy9D4yvvYCLhKcLxQt6gIyMjOTr5O7s+eTi463M+Pnl4v/k+fS8ghCwfqbEpZdFc0M4NINU05FbRrCF070PjJyLjtiQrYkPjIW9D4yJvb2ci47YiYeeh8z9/eHorcTj7qO8OpYwHs+pn6dKgpA7wBHT7kXGDZqbvZmLjtiJjp6AzP394eit3+Li+f/s7vnk7uit/vns+ejg6OP5/qO9qW9mXDr9UoLIbKpHfOD1YGo4mpqhre7o//nk6+Tu7Pnorf3i4eTu9ICLhKcLxQt6gIyMiIiNjg+MjI3RuxTBoPU6YAEWUX76Fn/7X/q9wkzf6OHk7OPu6K3i46355eT+re7o/1S78kwK2FQqFDS/z3ZVWPwT8yzfor0MTouFpouMiIiKj4+9DDuXDD7v4eit/vns4+ns/+mt+ej/4P6t7A2Zpl3kyhn7hHN55gCjzSt6ysDyhaaLjIiIio+Mm5Pl+fn9/reiovqrvamLjtiJhp6QzP394eitzuj/+a3s4+mt7uj/+eTr5O7s+eTi4639sKvqrQe+53qAD0JTZi6idN7n1un55Ovk7uz56K3v9K3s4/St/ez/+fLMJRV0XEfrEanmnF0uNmmWp06SxFX7Er6Z6Cz6GUSgj46MjYwuD4wPjI2LhKcLxQt67umIjL0Mf72nizy91WHXib8B5T4CkFPo/nLq0+gx1CqIhPGazduck/leOgautsouWOImLvwfyt7YTCKizD51dm79QGsuwQaUBFN0xuF4iiavvY9llbN13YReOLcgeYKDjR+GPKybo/lYsYBW75uSHFaTyt1miGDT9AmgZrsv2sHYYRgT94EpygbWWZu6vkZJgsBDmeRc4+mt7uLj6eT55OLj/q3i6634/uijzSt6ysDyhdO9kouO2JCuiZW9m4u9gouO2JCejIxyiYi9joyMcr2QAv4M7UuW1oSiHz91ycV97bUTmHjI85LB5t0bzARJ+e+GnQ7MCr4HDKcLxQt6gIyMiIiNve+8hr2Ei47Y+eXi/+T59LybvZmLjtiJjp6AzP1ElP940INY8tIWf6iON9gCwNCAfJIIDgiWFLDKun8kFs0DoVk8HZ9VvQ+JNr0Pji4tjo+Mj4+Mj72Ai4St4uut+eXorfnl6OOt7P394eTu7E3uvvp6t4qh22ZXgqyDVzf+lMI4M3n+FmNf6YJG9MK5VS+zdPVy5kX0rez+/vjg6P6t7O7u6P357OPu6OHorcTj7qO8q72pi47YiYaekMz93ScHWFdpcV2Eiro9+Pis");
        private static int[] order = new int[] { 14,54,21,20,36,36,28,30,8,51,21,28,30,13,28,22,33,51,59,30,46,53,44,47,48,38,58,37,42,32,35,53,45,56,39,51,37,39,58,52,48,56,47,44,59,59,53,59,55,58,54,52,52,58,59,56,57,59,59,59,60 };
        private static int key = 141;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
