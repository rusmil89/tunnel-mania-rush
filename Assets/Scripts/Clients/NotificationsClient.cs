﻿using System;
using System.Collections;
using EasyMobile;
using Firebase.Messaging;
using Tunnel.Analytics;
using Tunnel.PlayerData;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;

namespace Tunnel.Controllers
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Klasa koja kontrolise ponasanje notifikacija
    /// </summary>
    public class NotificationsClient
    {
        #region Singleton

        private static readonly NotificationsClient _instance = new NotificationsClient();
        public static NotificationsClient Instance => _instance;

        private NotificationsClient()
        {
            
        }

        #endregion




        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private PersistentData _persistentData;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const string ONE_ENERGY_TITLE = "Energy refilled!";
        private const string FULL_ENERGY_TITLE = "Energy FULL!";
        private const string ONE_ENERGY_MESSAGE = "Ready for more fun? Unexplored tunnels are waiting for you.";
        private const string FULL_ENERGY_MESSAGE = "Ready to improve your focus today?";

        #endregion





        #region Custom Methods

        public IEnumerator Init(PersistentData persistentData)
        {
            _persistentData = persistentData;
            RegisterObservableListeners();
            yield return null;
        }

        private void RegisterObservableListeners()
        {
            _persistentData.AreNotificationsEnabled
                .SkipWhile(_ => !FirebaseClient.Instance.IsFirebaseReady.Value)
                .Subscribe(HandleNotifications)
                .AddTo(_compositeDisposable);
            Observable.EveryApplicationPause()
                .Where(applicationPaused => applicationPaused && _persistentData.AreNotificationsEnabled.Value)
                .Subscribe(_ => ScheduleLocalNotifications())
                .AddTo(_compositeDisposable);
            Observable.OnceApplicationQuit()
                .Where(_ => _persistentData.AreNotificationsEnabled.Value)
                .Subscribe(_ => ScheduleLocalNotifications())
                .AddTo(_compositeDisposable);
        }

        // Unsubscribes notification events.
        public void UnregisterEventListeners()
        {
            UnregisterObservableListeners();
        }

        private void UnregisterObservableListeners()
        {
            _compositeDisposable.Dispose();
        }

        #endregion





        #region Event Listeners

        private void HandleNotifications(bool areNotificationsEnabled)
        {
            if (areNotificationsEnabled)
            {
                InitializeNotifications();
            }
            else
            {
                DisableNotifications();
            }
        }

        private void InitializeNotifications()
        {
            if (!Notifications.IsInitialized())
            {
                Notifications.Init();
            }
        }

        private void DisableNotifications()
        {
            FirebaseMessaging.UnsubscribeAsync("default");
            Notifications.ClearAllDeliveredNotifications();
            Notifications.CancelAllPendingLocalNotifications();
        }

        private void ScheduleLocalNotifications()
        {
            Notifications.ClearAllDeliveredNotifications();
            Notifications.CancelAllPendingLocalNotifications();
            ScheduleOnePlayNotification();
            ScheduleEnergyFullNotification();
        }

        private void ScheduleOnePlayNotification()
        {
            if (_persistentData.CurrentEnergy.Value >= Constants.Energy.ENERGY_AMOUNT_FOR_PLAY)
            {
                return;
            }
            NotificationContent oneEnergyNotificationContent = new NotificationContent
            {
                title = ONE_ENERGY_TITLE,
                body = ONE_ENERGY_MESSAGE
            };

            var seconds = (Constants.Energy.ENERGY_AMOUNT_FOR_PLAY - _persistentData.CurrentEnergy.Value) * Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY;
            var oneEnergyDelayTime = TimeSpan.FromSeconds(seconds);
            Notifications.ScheduleLocalNotification(oneEnergyDelayTime, oneEnergyNotificationContent);
        }

        private void ScheduleEnergyFullNotification()
        {
            if (_persistentData.CurrentEnergy.Value >= Constants.Energy.MAX_ENERGY)
            {
                return;
            }
            NotificationContent fullEnergyNotificationContent = new NotificationContent
            {
                title = FULL_ENERGY_TITLE,
                body = FULL_ENERGY_MESSAGE
            };
            var seconds = (Constants.Energy.MAX_ENERGY - _persistentData.CurrentEnergy.Value) * Constants.Energy.SECONDS_TO_REFILL_ONE_ENERGY;
            var fullEnergyDelayTime = TimeSpan.FromSeconds(seconds);
            Notifications.ScheduleLocalNotification(fullEnergyDelayTime, fullEnergyNotificationContent);
        }

        #endregion
    }
}
