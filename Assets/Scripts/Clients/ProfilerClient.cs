﻿using System.IO;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;

namespace Tunnel.Utilities
{
    /// <summary>
    /// SCENE: Splash
    /// GAME_OBJECT: Controllers
    /// Koristimo je za kontrolisanje profilera na target uređajima kako bi mogli da snimamo podatke i kasnije ih analiziramo i uočimo potencijalne probleme
    /// </summary>
    public sealed class ProfilerClient : MonoBehaviour
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        [SerializeField] private GameObject _afpsCounter;
        [SerializeField] private bool _doesStartProfiler;

        #region Private Fields

        private const string SPLASH_SCENE_NAME = "Splash";
        private const string MENU_SCENE_NAME = "Menu";
        private const string LEVEL_SCENE_NAME = "Level";

        private const string SPLASH_PROFILE_NAME = "SplashSceneProfile";
        private const string MENU_PROFILE_NAME = "MenuSceneProfile";
        private const string LEVEL_PROFILE_NAME = "LevelSceneProfile";

        #endregion
        
        
        
        
        
        #region Monobehaviour Events

        // Start is called before the first frame update
        private void Awake()
        {
            Instantiate(_afpsCounter);
            
            if (!_doesStartProfiler) return;
            RegisterEventListeners();
        }

        private static void RegisterEventListeners()
        {
            SceneManager.sceneLoaded += SceneLoadedEventHandler;
            SceneManager.sceneUnloaded += SceneUnloadedEventHandler;
        }

        private void OnDestroy()
        {
            if (!_doesStartProfiler) return;
            SceneManager.sceneLoaded -= SceneLoadedEventHandler;
            SceneManager.sceneUnloaded -= SceneUnloadedEventHandler;
        }

        #endregion





        #region Event Listeners

        private static void SceneLoadedEventHandler(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (scene.name.Contains(SPLASH_SCENE_NAME))
            {
                StartProfiler(SPLASH_PROFILE_NAME);
            }
            else if (scene.name.Contains(MENU_SCENE_NAME))
            {
                StartProfiler(MENU_PROFILE_NAME);
            }
            else if (scene.name.Contains(LEVEL_SCENE_NAME))
            {
                StartProfiler(LEVEL_PROFILE_NAME);
            }
        }

        private static void SceneUnloadedEventHandler(Scene scene)
        {
            StopProfiler();
        }

        #endregion





        #region Custom Methods

        private static void StartProfiler(string logFileName)
        {
            var profilerDataFilePath = Path.Combine(Application.persistentDataPath, logFileName);
            Profiler.logFile = profilerDataFilePath;
            Profiler.enableBinaryLog = true;
            Profiler.enabled = true;
            Profiler.maxUsedMemory = 256 * 1024 * 1024;
        }

        private static void StopProfiler()
        {
            Profiler.enableBinaryLog = false;
            Profiler.enabled = false;
        }

        #endregion
#endif
    }
}
