﻿using System.Collections;
using Tunnel.DynamicObstacles;
using UniRx;
using UnityEngine;

namespace Tunnel.Controllers
{
    public class ObstacleAnimation : Obstacle
    {
        #region Private Fields

        private Animator _animator;
        private int _randomMultiplier;
        
        // static readonly
        private static readonly int _playTriggerStateHash = Animator.StringToHash("Play");

        #endregion




        
        #region Monobehaviour Events

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        
            // randomize between values -1 and 1, and these values are used for direction of rotation
            _randomMultiplier = Random.Range(0, 2) * 2 - 1;
        }

        #endregion




        
        #region Coroutines

        private IEnumerator DelayAnimation()
        {
            yield return new WaitForSeconds(delay);
            if (_animator == null) yield break;
            _animator.SetTrigger(_playTriggerStateHash);
        }

        #endregion
        




        #region Override Methods

        public override void Initialize()
        {
            _animator.SetFloat(Animator.StringToHash("randomMultiplier"), _randomMultiplier);
            _animator.Play("Idle");
            _animator.speed = speed;

            if (delay == 0)
            {
                _animator.SetTrigger(_playTriggerStateHash);
                return;
            }

            MainThreadDispatcher.StartCoroutine(DelayAnimation());
        }

        public override void Pause()
        {
            _animator.enabled = false;
        }

        public override void Resume()
        {
            _animator.enabled = true;
        }

        #endregion
    }
}