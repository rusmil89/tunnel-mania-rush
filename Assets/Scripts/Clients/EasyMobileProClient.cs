﻿using System;
using System.Collections;
using EasyMobile;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.Controllers
{
    public sealed class EeaRegionChecked
    {
        public bool IsEEARegion { get; set; }
    }
    
    /// <summary>
    /// Singleton klasa koja kontroliše elemente EasyMobilePro plugina
    /// </summary>
    public sealed class EasyMobileProClient
    {
        #region Private Fields

        private static readonly EasyMobileProClient _instance = new EasyMobileProClient();

        #endregion





        #region Public Fields

        public static EasyMobileProClient Instance => _instance;

        #endregion



        
        
        #region Custom Methods

        private EasyMobileProClient()
        {
        }

        public IEnumerator Init()
        {
            if (!RuntimeManager.IsInitialized())
            {
                try
                {
                    RuntimeManager.Init();
                }
                catch (Exception e)
                {
                    Logging.LogError($"Greska u inicijalizaciji EMP: {e.Message}");
                    throw;
                }
            }
            
            yield return new WaitUntil(RuntimeManager.IsInitialized);
            Privacy.IsInEEARegion(CheckEEARegionCallback);
        }
        
        #endregion




        
        #region Callbacks

        private void CheckEEARegionCallback(EEARegionStatus result)
        {
            bool isEeaRegion;
            switch (result)
            {
                case EEARegionStatus.InEEA:
                    isEeaRegion = true;
                    break;
                case EEARegionStatus.NotInEEA:
                    isEeaRegion = false;
                    break;
                case EEARegionStatus.Unknown:
                    isEeaRegion = false;
                    break;
                default:
                    Logging.Log("Result is Unknown: couldn't determine if we're in EEA region or not.");
                    isEeaRegion = false;
                    break;
            }
            
            MessageBroker.Default.Publish(new EeaRegionChecked
            {
                IsEEARegion = isEeaRegion
            });
        }

        #endregion
    }
}