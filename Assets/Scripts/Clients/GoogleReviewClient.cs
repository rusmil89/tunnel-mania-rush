﻿using System.Collections;
using Core.Events;
using Google.Play.Review;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.Controllers
{
    public sealed class ApplicationRated {}
    
    /// <summary>
    /// Singleton klasa koja kontroliše Goole Review API
    /// </summary>
    public sealed class GoogleReviewClient
    {
        #region Private Fields

        private static readonly GoogleReviewClient _instance = new GoogleReviewClient();

        /// <summary>
        /// Google Review Manager API fields
        /// </summary>
        private ReviewManager _reviewManager;
        private PlayReviewInfo _playReviewInfo;
        private CompositeDisposable _compositeDisposable = new CompositeDisposable();

        #endregion





        #region Public Fields
        
        public static GoogleReviewClient Instance => _instance;

        #endregion





        #region Monobehaviour Events

        private GoogleReviewClient()
        {
        }

        public IEnumerator Init()
        {
            RegisterObservableListeners();
            yield return null;
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<LikeMenuYesButtonClicked>()
                .Subscribe(HandleYesButtonClicked)
                .AddTo(_compositeDisposable);
            MessageBroker.Default.Receive<LikeDislikeMenuOpened>()
                .Subscribe(HandleLikeDislikeMenuOpened)
                .AddTo(_compositeDisposable);
            MessageBroker.Default.Receive<SettingsMenuOpened>()
                .Subscribe(HandleSettingsMenuOpened)
                .AddTo(_compositeDisposable);
            MessageBroker.Default.Receive<RateUsButtonClicked>()
                .Subscribe(HandleRateUsButton)
                .AddTo(_compositeDisposable);
        }

        public void UnregisterEventListeners()
        {
            _compositeDisposable.Dispose();
        }

        #endregion
        
        
        
        
        
        #region Coroutines

        private IEnumerator ReviewManagerInit()
        {
            _reviewManager = new ReviewManager();

            var requestFlowOperation = _reviewManager.RequestReviewFlow();
            yield return requestFlowOperation;
            if (requestFlowOperation.Error != ReviewErrorCode.NoError)
            {
                Logging.LogError("ReviewManagerInit error: " + requestFlowOperation.Error);
                yield break;
            }

            _playReviewInfo = requestFlowOperation.GetResult();
        }

        /// <summary>
        /// korutina koja poziva dijalog za ocenjivanje aplikacije direktno u samoj igrici
        /// </summary>
        /// <returns></returns>
        private IEnumerator LaunchInAppReview()
        {
            var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
            yield return launchFlowOperation;
            _playReviewInfo = null;
            if (launchFlowOperation.Error != ReviewErrorCode.NoError)
            {
                Logging.LogError("LaunchInAppReview error: " + launchFlowOperation.Error);
                yield break;
            }
            
            MessageBroker.Default.Publish(new ApplicationRated());
        }

        #endregion





        
        #region Event Listeners
        
        private void HandleYesButtonClicked(LikeMenuYesButtonClicked likeMenuYesButtonClicked)
        {
            MainThreadDispatcher.StartCoroutine(LaunchInAppReview());
        }
        
        private void HandleSettingsMenuOpened(SettingsMenuOpened settingsMenuOpened)
        {
            MainThreadDispatcher.StartCoroutine(ReviewManagerInit());
        }

        private void HandleRateUsButton(RateUsButtonClicked rateUsButtonClicked)
        {
            MainThreadDispatcher.StartCoroutine(LaunchInAppReview());
        }

        private void HandleLikeDislikeMenuOpened(LikeDislikeMenuOpened likeDislikeMenuOpened)
        {
            MainThreadDispatcher.StartCoroutine(ReviewManagerInit());
        }

        #endregion
    }
}