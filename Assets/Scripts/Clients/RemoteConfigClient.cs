﻿using Cysharp.Threading.Tasks;
using Tunnel.PlayerData;
using UniRx;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.RemoteConfig;
using UnityEditor;
using UnityEngine;

namespace Tunnel.Controllers
{
    /// <summary>
    /// Singleton klasa koja kontroliše ponašanje remote config servisa
    /// </summary>
    public sealed class RemoteConfigClient
    {
        #region Structs

        private struct UserAttributes { }

        private struct AppAttributes
        {
            public int currentWorld;
            public int currentChapter;
            public int numberOfAppStarts;
        }

        #endregion
        
        
        
        
        
        #region Private Fields

        /// <summary>
        /// statičke globalne promenljive
        /// </summary>
        private static RemoteConfigClient _instance;
        private PersistentData _persistentData;
        private readonly CompositeDisposable _compositeDisposable = new();

        /// <summary>
        /// gameplay globalne promenljive
        /// </summary>
        private const string DEFAULT_UPDATE_MENU_DESCRIPTION = "Tunnel Rush Mania has a new update:\n\n- test";
        private const float DEFAULT_EASY_MOVEMENT_SPEED = 10f;
        private const float DEFAULT_EASY_ROTATION_SPEED = 100f;
        private const float DEFAULT_MEDIUM_MOVEMENT_SPEED = 16f;
        private const float DEFAULT_MEDIUM_ROTATION_SPEED = 160f;
        private const float DEFAULT_HARD_MOVEMENT_SPEED = 22f;
        private const float DEFAULT_HARD_ROTATION_SPEED = 220f;
        private const int DEFAULT_SHIELDS_AMOUNT = 1;
        private const int DEFAULT_CONTINUE_AFTER_DEATH_COST = 50;
        private const int DEFAULT_FEEDBACK_REWARD = 10;
        private const int DEFAULT_RATE_US_REWARD = 10;
        private const int DEFAULT_SOCIAL_NETWORKS_REWARD = 500;
        private const int DEFAULT_INCREMENTAL_QUEST_REWARD = 1;
        private const int DEFAULT_NON_INCREMENTAL_QUEST_REWARD = 100;
        private const int DEFAULT_TUTORIAL_COMPLETED_REWARD_COINS = 5000;
        private const int DEFAULT_TUTORIAL_COMPLETED_REWARD_TOKENS = 200;
        private const int DEFAULT_TUTORIAL_COMPLETED_REWARD_SHIELDS = 10;
        private const int DEFAULT_UNLOCK_CHAPTER_COST = 20;
        private const int DEFAULT_ENERGY_PACK01_COST = 500;
        private const int DEFAULT_ENERGY_PACK02_COST = 1000;
        private const int DEFAULT_SHIELDS_PACK01_COST = 500;
        private const int DEFAULT_SHIELDS_PACK02_COST = 1000;
        private const int DEFAULT_SHIELDS_PACK03_COST = 2000;
        private const int DEFAULT_SHIELDS_PACK01_REWARD = 5;
        private const int DEFAULT_SHIELDS_PACK02_REWARD = 15;
        private const int DEFAULT_SHIELDS_PACK03_REWARD = 30;
        private const int DEFAULT_TOKENS_PACK01_COST = 500;
        private const int DEFAULT_TOKENS_PACK02_COST = 2000;
        private const int DEFAULT_TOKENS_PACK03_COST = 10000;
        private const int DEFAULT_TOKENS_PACK01_REWARD = 5;
        private const int DEFAULT_TOKENS_PACK02_REWARD = 30;
        private const int DEFAULT_TOKENS_PACK03_REWARD = 200;
        private const int DEFAULT_COINS_PACK01_REWARD = 500;
        private const int DEFAULT_COINS_PACK02_REWARD = 1000;
        private const int DEFAULT_COINS_PACK03_REWARD = 5000;
        private const int DEFAULT_COINS_PACK04_REWARD = 10000;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD01 = 10;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD02 = 20;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD03 = 500;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD04 = 1000;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD05 = 1500;
        private const int DEFAULT_SPIN_THE_WHEEL_REWARD06 = 2000;
        private const int DEFAULT_DAY1_REWARD = 500;
        private const int DEFAULT_DAY2_REWARD = 10;
        private const int DEFAULT_DAY3_REWARD = 5;
        private const int DEFAULT_DAY4_REWARD = 1000;
        private const int DEFAULT_DAY5_REWARD = 20;
        private const int DEFAULT_DAY6_REWARD = 10;
        private const int DEFAULT_DAY7_REWARD1 = 1000;
        private const int DEFAULT_DAY7_REWARD2 = 20;
        private const int DEFAULT_DAY7_REWARD3 = 10;
        private const int DEFAULT_ROTATION_SPEED_FACTOR = 250;
        private const int DEFAULT_SHARE_REWARD = 250;
        private const int DEFAULT_ENERGY = 15;

        /// <summary>
        /// globalne promenljive za reklame
        /// </summary>
        private const bool DEFAULT_ARE_INTERSTITIALS_LOADING_AFTER_DEATH = false;
        private const int DEFAULT_INTERSTITIAL_AFTER_DEATH_FREQUENCY = 1;
        private const int DEFAULT_FINISH_MENU_INTERSTITIAL_FREQUENCY = 1;

        /// <summary>
        /// privatna polja za medijaciju
        /// pravila:
        /// 1. ako pravilo počinje sa br 1, to znači da je AdMob prvi provajder
        /// 1a. ako pravilo sadrži i slovo pored broja jedan, to znači da se kombinacija provajdera posle AdMob-a menja, pa je 1a (npr. AdMob, UnityAds, Facebook,...), 1b (npr AdMob, Facebook, UnityAds,...)
        ///     i tako dalje, sve u zavisnosti koliko provajdera ima posle admoba. Svako dodavanje novog provajdera dodaje novo slovo u kombinaciji
        /// 2. ako pravilo počinje sa br 2, to znači da je UnityAds prvi provajder
        /// 2a. ponavlja se pravilo 1a, samo što je u glavnoj ulozi UnityAds
        /// </summary>
        private const string DEFAULT_INTERSTITIAL_AD_PLACEMENT_DEFAULT_ORDER = "1";
        private const string DEFAULT_INTERSTITIAL_AD_PLACEMENT_GAME_OVER_ORDER = "1";
        private const string DEFAULT_REWARDED_VIDEO_AD_PLACEMENT_DEFAULT_ORDER = "1";

        /// <summary>
        /// remote keys
        /// </summary>
        private const string UPDATE_MENU_DESCRIPTION_KEY = "Update_Menu_Description";
        private const string APP_VERSION_KEY = "AppVersion";
        private const string EASY_MOVEMENT_SPEED_KEY = "Gameplay_EasyMovementSpeed";
        private const string EASY_ROTATION_SPEED_KEY = "Gameplay_EasyRotationSpeed";
        private const string MEDIUM_MOVEMENT_SPEED_KEY = "Gameplay_MediumMovementSpeed";
        private const string MEDIUM_ROTATION_SPEED_KEY = "Gameplay_MediumRotationSpeed";
        private const string HARD_MOVEMENT_SPEED_KEY = "Gameplay_HardMovementSpeed";
        private const string HARD_ROTATION_SPEED_KEY = "Gameplay_HardRotationSpeed";
        private const string SHIELDS_AMOUNT_KEY = "Gameplay_ShieldsAmountPerLevel";
        private const string ARE_INTERSTITIALS_LOADING_AFTER_DEATH_KEY = "AreInterstitialsLoadingAfterDeath";
        private const string INTERSTITIAL_AFTER_DEATHS_FREQUENCY_KEY = "Ads_InterstitialAfterDeathFrequency";
        private const string FINISH_MENU_INTERSTITIAL_FREQUENCY_KEY = "Ads_FinishMenuInterstitialFrequency";
        private const string INTERSTITIAL_AD_PLACEMENT_DEFAULT_ORDER_KEY = "Ads_InterstitialAdPlacementDefaultOrder";
        private const string INTERSTITIAL_AD_PLACEMENT_GAME_OVER_ORDER_KEY = "Ads_InterstitialAdPlacementGameOverOrder";
        private const string REWARDED_VIDEO_AD_PLACEMENT_DEFAULT_ORDER_KEY = "Ads_RewardedVideoAdPlacementDefaultOrder";
        private const string CONTINUE_AFTER_DEATH_COST_KEY = "Gameplay_Continue_After_Death_Cost";
        private const string FEEDBACK_REWARD_KEY = "Gameplay_Feedback_Reward";
        private const string RATE_US_REWARD_KEY = "Gameplay_RateUs_Reward";
        private const string SOCIAL_NETWORKS_REWARD_KEY = "Gameplay_Social_Networks_Reward";
        private const string INCREMENTAL_QUEST_REWARD_KEY = "Gameplay_Incremental_Quest_Reward";
        private const string NON_INCREMENTAL_QUEST_REWARD_KEY = "Gameplay_NonIncremental_Quest_Reward";
        private const string TUTORIAL_COMPLETED_REWARD_COINS_KEY = "Gameplay_Tutorial_Completed_Reward_Coins";
        private const string TUTORIAL_COMPLETED_REWARD_TOKENS_KEY = "Gameplay_Tutorial_Completed_Reward_Tokens";
        private const string TUTORIAL_COMPLETED_REWARD_SHIELDS_KEY = "Gameplay_Tutorial_Completed_Reward_Shields";
        private const string UNLOCK_CHAPTER_COST_KEY = "Gameplay_Unlock_Chapter_Cost";
        private const string ENERGY_PACK_01_COST_KEY = "Gameplay_Energy_Refill_Pack_01_Cost";
        private const string ENERGY_PACK_02_COST_KEY = "Gameplay_Energy_Refill_Pack_02_Cost";
        private const string SHIELDS_PACK01_COST_KEY = "Gameplay_Shields_Pack01_Cost";
        private const string SHIELDS_PACK02_COST_KEY = "Gameplay_Shields_Pack02_Cost";
        private const string SHIELDS_PACK03_COST_KEY = "Gameplay_Shields_Pack03_Cost";
        private const string SHIELDS_PACK01_REWARD_KEY = "Gameplay_Shields_Pack01_Reward";
        private const string SHIELDS_PACK02_REWARD_KEY = "Gameplay_Shields_Pack02_Reward";
        private const string SHIELDS_PACK03_REWARD_KEY = "Gameplay_Shields_Pack03_Reward";
        private const string TOKENS_PACK01_COST_KEY = "Gameplay_Tokens_Pack01_Cost";
        private const string TOKENS_PACK02_COST_KEY = "Gameplay_Tokens_Pack02_Cost";
        private const string TOKENS_PACK03_COST_KEY = "Gameplay_Tokens_Pack03_Cost";
        private const string TOKENS_PACK01_REWARD_KEY = "Gameplay_Tokens_Pack01_Reward";
        private const string TOKENS_PACK02_REWARD_KEY = "Gameplay_Tokens_Pack02_Reward";
        private const string TOKENS_PACK03_REWARD_KEY = "Gameplay_Tokens_Pack03_Reward";
        private const string COINS_PACK01_REWARD_KEY = "Gameplay_Coins_Pack01_Reward";
        private const string COINS_PACK02_REWARD_KEY = "Gameplay_Coins_Pack02_Reward";
        private const string COINS_PACK03_REWARD_KEY = "Gameplay_Coins_Pack03_Reward";
        private const string COINS_PACK04_REWARD_KEY = "Gameplay_Coins_Pack04_Reward";
        private const string SPIN_THE_WHEEL_REWARD_01_KEY = "Gameplay_SpinTheWheel_Reward_01";
        private const string SPIN_THE_WHEEL_REWARD_02_KEY = "Gameplay_SpinTheWheel_Reward_02";
        private const string SPIN_THE_WHEEL_REWARD_03_KEY = "Gameplay_SpinTheWheel_Reward_03";
        private const string SPIN_THE_WHEEL_REWARD_04_KEY = "Gameplay_SpinTheWheel_Reward_04";
        private const string SPIN_THE_WHEEL_REWARD_05_KEY = "Gameplay_SpinTheWheel_Reward_05";
        private const string SPIN_THE_WHEEL_REWARD_06_KEY = "Gameplay_SpinTheWheel_Reward_06";
        private const string DAY_1_REWARD_KEY = "Gameplay_Day_1_Reward";
        private const string DAY_2_REWARD_KEY = "Gameplay_Day_2_Reward";
        private const string DAY_3_REWARD_KEY = "Gameplay_Day_3_Reward";
        private const string DAY_4_REWARD_KEY = "Gameplay_Day_4_Reward";
        private const string DAY_5_REWARD_KEY = "Gameplay_Day_5_Reward";
        private const string DAY_6_REWARD_KEY = "Gameplay_Day_6_Reward";
        private const string DAY_7_REWARD_1_KEY = "Gameplay_Day_7_Reward_1";
        private const string DAY_7_REWARD_2_KEY = "Gameplay_Day_7_Reward_2";
        private const string DAY_7_REWARD_3_KEY = "Gameplay_Day_7_Reward_3";
        private const string ROTATION_SPEED_FACTOR_KEY = "Gameplay_Rotation_Speed_Factor";
        private const string GAMEPLAY_SHARE_REWARD_KEY = "Gameplay_Share_Reward";
        private const string GAMEPLAY_ENERGY = "Gameplay_Energy";
        
        #endregion


        
        
        
        #region Public Fields

        public static RemoteConfigClient Instance => _instance;

        public BoolReactiveProperty Ready { get; private set; }
        public string UpdateMenuDescription { get; private set; }
        public float EasyMovementSpeed { get; private set; }
        public float EasyRotationSpeed { get; private set; }
        public float MediumMovementSpeed { get; private set; }
        public float MediumRotationSpeed { get; private set; }
        public float HardMovementSpeed { get; private set; }
        public float HardRotationSpeed { get; private set; }
        public string AppVersion { get; private set; }
        public int ShieldsAmountPerLevel { get; private set; }
        public bool IsLoadingAdsAfterDeath { get; private set; }
        public int InterstitialAfterDeathFrequency { get; private set; }
        public int FinishMenuInterstitialFrequency { get; private set; }
        public string InterstitialAdPlacementDefaultOrder { get; private set; }
        public string InterstitialAdPlacementGameOverOrder { get; private set; }
        public string RewardedVideoAdPlacementDefaultOrder { get; private set; }
        public int ContinueAfterDeathCost { get; private set; }
        public int FeedbackReward { get; private set; }
        public int RateUsReward { get; private set; }
        public int SocialNetworksReward { get; private set; }
        public int IncrementalQuestReward { get; private set; }
        public int NonIncrementalQuestReward { get; private set; }
        public int TutorialCompletedRewardCoins { get; private set; }
        public int TutorialCompletedRewardTokens { get; private set; }
        public int TutorialCompletedRewardShields { get; private set; }
        public int UnlockChapterCost { get; private set; }
        public int EnergyPack01Cost { get; private set; }
        public int EnergyPack02Cost { get; private set; }
        public int ShieldsPack01Cost { get; private set; }
        public int ShieldsPack02Cost { get; private set; }
        public int ShieldsPack03Cost { get; private set; }
        public int ShieldsPack01Reward { get; private set; }
        public int ShieldsPack02Reward { get; private set; }
        public int ShieldsPack03Reward { get; private set; }
        public int TokensPack01Cost { get; private set; }
        public int TokensPack02Cost { get; private set; }
        public int TokensPack03Cost { get; private set; }
        public int TokensPack01Reward { get; private set; }
        public int TokensPack02Reward { get; private set; }
        public int TokensPack03Reward { get; private set; }
        public int CoinsPack01Reward { get; private set; }
        public int CoinsPack02Reward { get; private set; }
        public int CoinsPack03Reward { get; private set; }
        public int CoinsPack04Reward { get; private set; }
        public int SpinTheWheelReward01 { get; private set; }
        public int SpinTheWheelReward02 { get; private set; }
        public int SpinTheWheelReward03 { get; private set; }
        public int SpinTheWheelReward04 { get; private set; }
        public int SpinTheWheelReward05 { get; private set; }
        public int SpinTheWheelReward06 { get; private set; }
        public int Day1Reward { get; private set; }
        public int Day2Reward { get; private set; }
        public int Day3Reward { get; private set; }
        public int Day4Reward { get; private set; }
        public int Day5Reward { get; private set; }
        public int Day6Reward { get; private set; }
        public int Day7Reward1 { get; private set; }
        public int Day7Reward2 { get; private set; }
        public int Day7Reward3 { get; private set; }
        public int RotationSpeedFactor { get; private set; }
        public int ShareReward { get; private set; }
        public int Energy { get; private set; }

        #endregion




        #region Monobehaviour Events

        private RemoteConfigClient(global::PlayerData playerData)
        {
            _instance = this;
            Ready = new BoolReactiveProperty(false);
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            RemoteConfigService.Instance.SetEnvironmentID("3eee70bd-50d6-473b-b482-fb7a1fa2b358");
#endif
        }

        public async UniTask Init(PersistentData persistentData)
        {
            _persistentData = persistentData;
            if (Unity.Services.RemoteConfig.Utilities.CheckForInternetConnection()) 
            {
                await InitializeRemoteConfigAsync();
            }
            RegisterEventListeners();
            FetchRemoteConfigs();
            await UniTask.WaitUntil(() => Ready.Value);
        }

        private async UniTask InitializeRemoteConfigAsync()
        {
            await UnityServices.InitializeAsync();

            if (!AuthenticationService.Instance.IsSignedIn)
            {
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }
        }

        private void RegisterEventListeners()
        {
            RemoteConfigService.Instance.FetchCompleted += ApplyRemoteSettings;
            _persistentData.CurrentChapterInWorld
                .ObserveReplace()
                .Where(_ => _persistentData.CurrentWorld.Value == 1)
                .Subscribe(_ => FetchRemoteConfigs())
                .AddTo(_compositeDisposable);
        }

        private void FetchRemoteConfigs()
        {
            RemoteConfigService.Instance.FetchConfigs(new UserAttributes(), new AppAttributes
            {
                currentWorld = _persistentData.CurrentWorld.Value,
                currentChapter = _persistentData.CurrentChapterInWorld[_persistentData.CurrentWorld.Value],
                numberOfAppStarts =  _persistentData.NumberOfAppStarts
            });
        }

        public void UnregisterEventListeners()
        {
            RemoteConfigService.Instance.FetchCompleted -= ApplyRemoteSettings;
        }

        #endregion





        #region Custom Methods

        private void SetDefaultValues()
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            AppVersion = PlayerSettings.bundleVersion;
#elif UNITY_ANDROID
            AppVersion = Application.version;
#endif
            UpdateMenuDescription = DEFAULT_UPDATE_MENU_DESCRIPTION;
            EasyMovementSpeed = DEFAULT_EASY_MOVEMENT_SPEED;
            EasyRotationSpeed = DEFAULT_EASY_ROTATION_SPEED;
            MediumMovementSpeed = DEFAULT_MEDIUM_MOVEMENT_SPEED;
            MediumRotationSpeed = DEFAULT_MEDIUM_ROTATION_SPEED;
            HardMovementSpeed = DEFAULT_HARD_MOVEMENT_SPEED;
            HardRotationSpeed = DEFAULT_HARD_ROTATION_SPEED;
            ShieldsAmountPerLevel = DEFAULT_SHIELDS_AMOUNT;
            IsLoadingAdsAfterDeath = DEFAULT_ARE_INTERSTITIALS_LOADING_AFTER_DEATH;
            InterstitialAfterDeathFrequency = DEFAULT_INTERSTITIAL_AFTER_DEATH_FREQUENCY;
            FinishMenuInterstitialFrequency = DEFAULT_FINISH_MENU_INTERSTITIAL_FREQUENCY;
            InterstitialAdPlacementDefaultOrder = DEFAULT_INTERSTITIAL_AD_PLACEMENT_DEFAULT_ORDER;
            InterstitialAdPlacementGameOverOrder = DEFAULT_INTERSTITIAL_AD_PLACEMENT_GAME_OVER_ORDER;
            RewardedVideoAdPlacementDefaultOrder = DEFAULT_REWARDED_VIDEO_AD_PLACEMENT_DEFAULT_ORDER;
            ContinueAfterDeathCost = DEFAULT_CONTINUE_AFTER_DEATH_COST;
            FeedbackReward = DEFAULT_FEEDBACK_REWARD;
            RateUsReward = DEFAULT_RATE_US_REWARD;
            SocialNetworksReward = DEFAULT_SOCIAL_NETWORKS_REWARD;
            IncrementalQuestReward = DEFAULT_INCREMENTAL_QUEST_REWARD;
            NonIncrementalQuestReward = DEFAULT_NON_INCREMENTAL_QUEST_REWARD;
            TutorialCompletedRewardCoins = DEFAULT_TUTORIAL_COMPLETED_REWARD_COINS;
            TutorialCompletedRewardTokens = DEFAULT_TUTORIAL_COMPLETED_REWARD_TOKENS;
            TutorialCompletedRewardShields = DEFAULT_TUTORIAL_COMPLETED_REWARD_SHIELDS;
            UnlockChapterCost = DEFAULT_UNLOCK_CHAPTER_COST;
            EnergyPack01Cost = DEFAULT_ENERGY_PACK01_COST;
            EnergyPack02Cost = DEFAULT_ENERGY_PACK02_COST;
            ShieldsPack01Cost = DEFAULT_SHIELDS_PACK01_COST;
            ShieldsPack02Cost = DEFAULT_SHIELDS_PACK02_COST;
            ShieldsPack03Cost = DEFAULT_SHIELDS_PACK03_COST;
            ShieldsPack01Reward = DEFAULT_SHIELDS_PACK01_REWARD;
            ShieldsPack02Reward = DEFAULT_SHIELDS_PACK02_REWARD;
            ShieldsPack03Reward = DEFAULT_SHIELDS_PACK03_REWARD;
            TokensPack01Cost = DEFAULT_TOKENS_PACK01_COST;
            TokensPack02Cost = DEFAULT_TOKENS_PACK02_COST;
            TokensPack03Cost = DEFAULT_TOKENS_PACK03_COST;
            TokensPack01Reward = DEFAULT_TOKENS_PACK01_REWARD;
            TokensPack02Reward = DEFAULT_TOKENS_PACK02_REWARD;
            TokensPack03Reward = DEFAULT_TOKENS_PACK03_REWARD;
            CoinsPack01Reward = DEFAULT_COINS_PACK01_REWARD;
            CoinsPack02Reward = DEFAULT_COINS_PACK02_REWARD;
            CoinsPack03Reward = DEFAULT_COINS_PACK03_REWARD;
            CoinsPack04Reward = DEFAULT_COINS_PACK04_REWARD;
            SpinTheWheelReward01 = DEFAULT_SPIN_THE_WHEEL_REWARD01;
            SpinTheWheelReward02 = DEFAULT_SPIN_THE_WHEEL_REWARD02;
            SpinTheWheelReward03 = DEFAULT_SPIN_THE_WHEEL_REWARD03;
            SpinTheWheelReward04 = DEFAULT_SPIN_THE_WHEEL_REWARD04;
            SpinTheWheelReward05 = DEFAULT_SPIN_THE_WHEEL_REWARD05;
            SpinTheWheelReward06 = DEFAULT_SPIN_THE_WHEEL_REWARD06;
            Day1Reward = DEFAULT_DAY1_REWARD;
            Day2Reward = DEFAULT_DAY2_REWARD;
            Day3Reward = DEFAULT_DAY3_REWARD;
            Day4Reward = DEFAULT_DAY4_REWARD;
            Day5Reward = DEFAULT_DAY5_REWARD;
            Day6Reward = DEFAULT_DAY6_REWARD;
            Day7Reward1 = DEFAULT_DAY7_REWARD1;
            Day7Reward2 = DEFAULT_DAY7_REWARD2;
            Day7Reward3 = DEFAULT_DAY7_REWARD3;
            RotationSpeedFactor = DEFAULT_ROTATION_SPEED_FACTOR;
            ShareReward = DEFAULT_SHARE_REWARD;
            Energy = DEFAULT_ENERGY;
        }

        private void SetRemoteValues()
        {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
            AppVersion = RemoteConfigService.Instance.appConfig.GetString(APP_VERSION_KEY, PlayerSettings.bundleVersion);
#elif UNITY_ANDROID
            AppVersion = RemoteConfigService.Instance.appConfig.GetString(APP_VERSION_KEY, Application.version);
#endif
            UpdateMenuDescription = RemoteConfigService.Instance.appConfig.GetString(UPDATE_MENU_DESCRIPTION_KEY, DEFAULT_UPDATE_MENU_DESCRIPTION);
            EasyMovementSpeed = RemoteConfigService.Instance.appConfig.GetFloat(EASY_MOVEMENT_SPEED_KEY, DEFAULT_EASY_MOVEMENT_SPEED);
            EasyRotationSpeed = RemoteConfigService.Instance.appConfig.GetFloat(EASY_ROTATION_SPEED_KEY, DEFAULT_EASY_ROTATION_SPEED);
            MediumMovementSpeed = RemoteConfigService.Instance.appConfig.GetFloat(MEDIUM_MOVEMENT_SPEED_KEY, DEFAULT_MEDIUM_MOVEMENT_SPEED);
            MediumRotationSpeed = RemoteConfigService.Instance.appConfig.GetFloat(MEDIUM_ROTATION_SPEED_KEY, DEFAULT_MEDIUM_ROTATION_SPEED);
            HardMovementSpeed = RemoteConfigService.Instance.appConfig.GetFloat(HARD_MOVEMENT_SPEED_KEY, DEFAULT_HARD_MOVEMENT_SPEED);
            HardRotationSpeed = RemoteConfigService.Instance.appConfig.GetFloat(HARD_ROTATION_SPEED_KEY, DEFAULT_HARD_ROTATION_SPEED);
            ShieldsAmountPerLevel = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_AMOUNT_KEY, DEFAULT_SHIELDS_AMOUNT);
            IsLoadingAdsAfterDeath = RemoteConfigService.Instance.appConfig.GetBool(ARE_INTERSTITIALS_LOADING_AFTER_DEATH_KEY, DEFAULT_ARE_INTERSTITIALS_LOADING_AFTER_DEATH);
            InterstitialAfterDeathFrequency = RemoteConfigService.Instance.appConfig.GetInt(INTERSTITIAL_AFTER_DEATHS_FREQUENCY_KEY, DEFAULT_INTERSTITIAL_AFTER_DEATH_FREQUENCY);
            FinishMenuInterstitialFrequency = RemoteConfigService.Instance.appConfig.GetInt(FINISH_MENU_INTERSTITIAL_FREQUENCY_KEY, DEFAULT_FINISH_MENU_INTERSTITIAL_FREQUENCY);
            InterstitialAdPlacementDefaultOrder = RemoteConfigService.Instance.appConfig.GetString(INTERSTITIAL_AD_PLACEMENT_DEFAULT_ORDER_KEY, DEFAULT_INTERSTITIAL_AD_PLACEMENT_DEFAULT_ORDER);
            InterstitialAdPlacementGameOverOrder = RemoteConfigService.Instance.appConfig.GetString(INTERSTITIAL_AD_PLACEMENT_GAME_OVER_ORDER_KEY, DEFAULT_INTERSTITIAL_AD_PLACEMENT_GAME_OVER_ORDER);
            RewardedVideoAdPlacementDefaultOrder = RemoteConfigService.Instance.appConfig.GetString(REWARDED_VIDEO_AD_PLACEMENT_DEFAULT_ORDER_KEY, DEFAULT_REWARDED_VIDEO_AD_PLACEMENT_DEFAULT_ORDER);
            ContinueAfterDeathCost = RemoteConfigService.Instance.appConfig.GetInt(CONTINUE_AFTER_DEATH_COST_KEY, DEFAULT_CONTINUE_AFTER_DEATH_COST);
            FeedbackReward = RemoteConfigService.Instance.appConfig.GetInt(FEEDBACK_REWARD_KEY, DEFAULT_FEEDBACK_REWARD);
            RateUsReward = RemoteConfigService.Instance.appConfig.GetInt(RATE_US_REWARD_KEY, DEFAULT_RATE_US_REWARD);
            SocialNetworksReward = RemoteConfigService.Instance.appConfig.GetInt(SOCIAL_NETWORKS_REWARD_KEY, DEFAULT_SOCIAL_NETWORKS_REWARD);
            IncrementalQuestReward = RemoteConfigService.Instance.appConfig.GetInt(INCREMENTAL_QUEST_REWARD_KEY, DEFAULT_INCREMENTAL_QUEST_REWARD);
            NonIncrementalQuestReward = RemoteConfigService.Instance.appConfig.GetInt(NON_INCREMENTAL_QUEST_REWARD_KEY, DEFAULT_NON_INCREMENTAL_QUEST_REWARD);
            TutorialCompletedRewardCoins = RemoteConfigService.Instance.appConfig.GetInt(TUTORIAL_COMPLETED_REWARD_COINS_KEY, DEFAULT_TUTORIAL_COMPLETED_REWARD_COINS);
            TutorialCompletedRewardTokens = RemoteConfigService.Instance.appConfig.GetInt(TUTORIAL_COMPLETED_REWARD_TOKENS_KEY, DEFAULT_TUTORIAL_COMPLETED_REWARD_TOKENS);
            TutorialCompletedRewardShields = RemoteConfigService.Instance.appConfig.GetInt(TUTORIAL_COMPLETED_REWARD_SHIELDS_KEY, DEFAULT_TUTORIAL_COMPLETED_REWARD_SHIELDS);
            UnlockChapterCost = RemoteConfigService.Instance.appConfig.GetInt(UNLOCK_CHAPTER_COST_KEY, DEFAULT_UNLOCK_CHAPTER_COST);
            EnergyPack01Cost = RemoteConfigService.Instance.appConfig.GetInt(ENERGY_PACK_01_COST_KEY, DEFAULT_ENERGY_PACK01_COST);
            EnergyPack02Cost = RemoteConfigService.Instance.appConfig.GetInt(ENERGY_PACK_02_COST_KEY, DEFAULT_ENERGY_PACK02_COST);
            ShieldsPack01Cost = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK01_COST_KEY, DEFAULT_SHIELDS_PACK01_COST);
            ShieldsPack02Cost = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK02_COST_KEY, DEFAULT_SHIELDS_PACK02_COST);
            ShieldsPack03Cost = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK03_COST_KEY, DEFAULT_SHIELDS_PACK03_COST);
            ShieldsPack01Reward = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK01_REWARD_KEY, DEFAULT_SHIELDS_PACK01_REWARD);
            ShieldsPack02Reward = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK02_REWARD_KEY, DEFAULT_SHIELDS_PACK02_REWARD);
            ShieldsPack03Reward = RemoteConfigService.Instance.appConfig.GetInt(SHIELDS_PACK03_REWARD_KEY, DEFAULT_SHIELDS_PACK03_REWARD);
            TokensPack01Cost = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK01_COST_KEY, DEFAULT_TOKENS_PACK01_COST);
            TokensPack02Cost = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK02_COST_KEY, DEFAULT_TOKENS_PACK02_COST);
            TokensPack03Cost = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK03_COST_KEY, DEFAULT_TOKENS_PACK03_COST);
            TokensPack01Reward = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK01_REWARD_KEY, DEFAULT_TOKENS_PACK01_REWARD);
            TokensPack02Reward = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK02_REWARD_KEY, DEFAULT_TOKENS_PACK02_REWARD);
            TokensPack03Reward = RemoteConfigService.Instance.appConfig.GetInt(TOKENS_PACK03_REWARD_KEY, DEFAULT_TOKENS_PACK03_REWARD);
            CoinsPack01Reward = RemoteConfigService.Instance.appConfig.GetInt(COINS_PACK01_REWARD_KEY, DEFAULT_COINS_PACK01_REWARD);
            CoinsPack02Reward = RemoteConfigService.Instance.appConfig.GetInt(COINS_PACK02_REWARD_KEY, DEFAULT_COINS_PACK02_REWARD);
            CoinsPack03Reward = RemoteConfigService.Instance.appConfig.GetInt(COINS_PACK03_REWARD_KEY, DEFAULT_COINS_PACK03_REWARD);
            CoinsPack04Reward = RemoteConfigService.Instance.appConfig.GetInt(COINS_PACK04_REWARD_KEY, DEFAULT_COINS_PACK04_REWARD);
            SpinTheWheelReward01 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_01_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD01);
            SpinTheWheelReward02 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_02_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD02);
            SpinTheWheelReward03 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_03_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD03);
            SpinTheWheelReward04 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_04_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD04);
            SpinTheWheelReward05 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_05_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD05);
            SpinTheWheelReward06 = RemoteConfigService.Instance.appConfig.GetInt(SPIN_THE_WHEEL_REWARD_06_KEY, DEFAULT_SPIN_THE_WHEEL_REWARD06);
            Day1Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_1_REWARD_KEY, DEFAULT_DAY1_REWARD);
            Day2Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_2_REWARD_KEY, DEFAULT_DAY2_REWARD);
            Day3Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_3_REWARD_KEY, DEFAULT_DAY3_REWARD);
            Day4Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_4_REWARD_KEY, DEFAULT_DAY4_REWARD);
            Day5Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_5_REWARD_KEY, DEFAULT_DAY5_REWARD);
            Day6Reward = RemoteConfigService.Instance.appConfig.GetInt(DAY_6_REWARD_KEY, DEFAULT_DAY6_REWARD);
            Day7Reward1 = RemoteConfigService.Instance.appConfig.GetInt(DAY_7_REWARD_1_KEY, DEFAULT_DAY7_REWARD1);
            Day7Reward2 = RemoteConfigService.Instance.appConfig.GetInt(DAY_7_REWARD_2_KEY, DEFAULT_DAY7_REWARD2);
            Day7Reward3 = RemoteConfigService.Instance.appConfig.GetInt(DAY_7_REWARD_3_KEY, DEFAULT_DAY7_REWARD3);
            RotationSpeedFactor = RemoteConfigService.Instance.appConfig.GetInt(ROTATION_SPEED_FACTOR_KEY, DEFAULT_ROTATION_SPEED_FACTOR);
            ShareReward = RemoteConfigService.Instance.appConfig.GetInt(GAMEPLAY_SHARE_REWARD_KEY, DEFAULT_SHARE_REWARD);
            Energy = RemoteConfigService.Instance.appConfig.GetInt(GAMEPLAY_ENERGY, DEFAULT_ENERGY);
            Ready.Value = true;
        }

        #endregion





        #region Event Listeners

        private void ApplyRemoteSettings(ConfigResponse configResponse)
        {
            switch (configResponse.requestOrigin)
            {
                case ConfigOrigin.Default:
                    SetDefaultValues();
                    break;
                case ConfigOrigin.Cached:
                    break;
                case ConfigOrigin.Remote:
                    SetRemoteValues();
                    break;
                default:
                    SetDefaultValues();
                    break;
            }
        }

        #endregion
    }
}