﻿using System;
using System.Linq;
using Tunnel.Tutorial;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.Utilities
{
    public struct PlayerTappedOnLeft {}
    public struct PlayerTappedOnRight {}
    public struct PlayerTiltedOnLeft {}
    public struct PlayerTiltedOnRight {}
    
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: MainCamera
    /// Klasa koja nasleđuje apstraktnu klasu RotationSimulation i kontroliše rotaciju igrača samo u tutorijalu
    /// </summary>
    public sealed class TutorialRotationSimulation : RotationSimulation
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private bool _isPlayerTappedOnLeft;
        private bool _isPlayerTappedOnRight;
        private bool _isPlayerTiltedOnLeft;
        private bool _isPlayerTiltedOnRight;
        private bool _isAccelerationChanged;
        private bool _areMissionCompletedAnimationsFinished;
        
        private const double DELAY_TUTORIAL_START = 3d;

        #endregion


        #region Properties

        private bool IsPlayerTappedOnLeft
        {
            get => _isPlayerTappedOnLeft;
            set
            {
                _isPlayerTappedOnLeft = value;
                if (_isPlayerTappedOnLeft)
                {
                    _isPlayerTappedOnRight = false;
                    MessageBroker.Default.Publish(new PlayerTappedOnLeft());
                }
            }
        }

        private bool IsPlayerTappedOnRight
        {
            get => _isPlayerTappedOnRight;
            set
            {
                _isPlayerTappedOnRight = value;
                if (_isPlayerTappedOnRight)
                {
                    MessageBroker.Default.Publish(new PlayerTappedOnRight());
                }
            }
        }

        private bool IsPlayerTiltedOnLeft
        {
            get => _isPlayerTiltedOnLeft;
            set
            {
                _isPlayerTiltedOnLeft = value;
                if (_isPlayerTiltedOnLeft)
                {
                    MessageBroker.Default.Publish(new PlayerTiltedOnLeft());
                }
            }
        }

        private bool IsPlayerTiltedOnRight
        {
            get => _isPlayerTiltedOnRight;
            set
            {
                _isPlayerTiltedOnRight = value;
                if (_isPlayerTiltedOnRight)
                {
                    MessageBroker.Default.Publish(new PlayerTiltedOnRight());
                }
            }
        }

#if UNITY_ANDROID || UNITY_IOS
        private IObservable<long> GyroscopeRotationObservable
        {
            get
            {
                return Observable.EveryUpdate()
                    .SkipWhile(_ => !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                    .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                    .Where(_ => Math.Abs(Input.acceleration.x) >= _playerData.PersistentData.GyroscopeSensitivity.Value && !_isCameraShaking);
            }
        }

        private IObservable<long> GyroscopeRotationStoppedObservable
        {
            get
            {
                return Observable.EveryUpdate()
                    .SkipWhile(_ => !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                    .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                    .Where(_ => Math.Abs(Input.acceleration.x) < _playerData.PersistentData.GyroscopeSensitivity.Value);
            }
        }
#endif

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase
                .FindAssets(Constants.Editor.Filters.PLAYER_DATA, new[] { Constants.Editor.Paths.SCRIPTABLE_OBJECTS })
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _gameData = UnityEditor.AssetDatabase
                .FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new[] { Constants.Editor.Paths.SCRIPTABLE_OBJECTS })
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            SetInitialValues();
        }

        protected override void SetInitialValues()
        {
            transform.localPosition = _insidePosition;
            transform.localEulerAngles = Vector3.zero;
            base.SetInitialValues();
        }

        // Start is called before the first frame update
        private void Start()
        {
            RegisterObservableStreamListeners();
        }

        private void RegisterObservableStreamListeners()
        {
            // observable stream koji osluškujemo tek posle vremena definisanog za odlaganje osluškivanja
            // zato što ne želimo pre toga nikakve akcije 
            Observable.Timer(TimeSpan.FromSeconds(DELAY_TUTORIAL_START))
                .Subscribe(_ => PerformSimulation())
                .AddTo(this);
            MessageBroker.Default.Receive<MissionCompletedAnimationFinished>()
                .Subscribe(_ => _areMissionCompletedAnimationsFinished = true)
                .AddTo(this);
        }

        private void PerformSimulation()
        {
#if UNITY_EDITOR
            PerformSimulationInEditor();
#endif
            
#if UNITY_ANDROID || UNITY_IOS
            if (SystemInfo.supportsGyroscope || SystemInfo.supportsAccelerometer)
            {
                PerformGyroSimulationOnDevice();
            }
            else
            {
                PerformTapSimulationOnDevice();
            }
#endif
        }
        
#if UNITY_EDITOR
        private void PerformSimulationInEditor()
        {
            // observable stream koji osluškuje konstantan pririsak na levu strelicu kako bismo postavili smer kretanja igrača na levo
            Drag.LeftKeyArrowTapStream()
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .Where(_ => !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(-1))
                .AddTo(this);

            // observable stream koji osluškuje konstantan pririsak na desnu strelicu kako bismo postavili smer kretanja igrača na desno
            Drag.RightKeyArrowTapStream()
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .Where(_ => !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(1))
                .AddTo(this);

            // observable stream koji osluškuje da li je igrač prekinuo pritisak na levu strelicu kako bismo poslali odgovarajući event
            // i obavestili osluškivače
            Drag.LeftKeyArrowUpStream()
                .Where(_ => !IsPlayerTappedOnLeft)
                .Take(1)
                .Subscribe(_ => IsPlayerTappedOnLeft = true)
                .AddTo(this);

            // observable stream koji osluškuje da li je igrač prekinuo pritisak na desnu strelicu kako bismo poslali odgovarajući event
            // i obavestili osluškivače
            // osluškivanje se preskače sve dok igrač ne pritisne levu strelicu (skipWhile)
            Drag.RightKeyArrowUpStream()
                .SkipWhile(_ => !_areMissionCompletedAnimationsFinished)
                .SkipWhile(_ => !IsPlayerTappedOnLeft)
                .Where(_ => !IsPlayerTappedOnRight)
                .Take(1)
                .Subscribe(_ => IsPlayerTappedOnRight = true)
                .AddTo(this);

            Drag.LeftKeyArrowUpStream()
                .SkipWhile(_ => !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .Where(_ => !IsPlayerTiltedOnLeft)
                .Take(1)
                .Subscribe(_ => IsPlayerTiltedOnLeft = true)
                .AddTo(this);

            Drag.RightKeyArrowUpStream()
                .SkipWhile(_ => !_playerData.PersistentData.IsGyroscopeEnabled.Value)
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .SkipWhile(_ => !_areMissionCompletedAnimationsFinished)
                .SkipWhile(_ => !IsPlayerTiltedOnLeft)
                .Where(_ => !IsPlayerTiltedOnRight)
                .Take(1)
                .Subscribe(_ => IsPlayerTiltedOnRight = true)
                .AddTo(this);
        }
#endif

#if UNITY_ANDROID || UNITY_IOS
        private void PerformGyroSimulationOnDevice()
        {
            PerformTapSimulationOnDevice();
            GyroscopeRotationObservable
                .Subscribe(_ => RotatePlayer(Input.acceleration.x))
                .AddTo(this);

            GyroscopeRotationStoppedObservable
                .SkipUntil(GyroscopeRotationObservable)
                .Where(_ => Input.acceleration.x < -0.08f && !IsPlayerTiltedOnLeft)
                .Take(1)
                .Subscribe(_ =>
                {
                    IsPlayerTiltedOnLeft = true;
                    ResetRotationSpeed();
                })
                .AddTo(this);

            GyroscopeRotationStoppedObservable
                .SkipUntil(GyroscopeRotationObservable)
                .SkipWhile(_ => !_areMissionCompletedAnimationsFinished)
                .SkipWhile(_ => !IsPlayerTiltedOnLeft)
                .Where(_ => Input.acceleration.x > 0.08f && !IsPlayerTiltedOnRight)
                .Take(1)
                .Subscribe(_ =>
                {
                    IsPlayerTiltedOnRight = true;
                    ResetRotationSpeed();
                })
                .AddTo(this);

            GyroscopeRotationStoppedObservable
                .SkipWhile(_ => !IsPlayerTiltedOnLeft && !IsPlayerTiltedOnRight)
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);
        }

        private void PerformTapSimulationOnDevice()
        {
            // observable stream za tap na levu stranu ekrana koji postavlja smer kretanja igrača
            Drag.LeftTapStream(_middleOfScreen)
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .Where(_ => !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(-1))
                .AddTo(this);

            // observable stream za tap na desnu stranu ekrana koji postavlja smer kretanja igrača
            Drag.RightTapStream(_middleOfScreen)
                .TakeWhile(_ => !_playerData.PersistentData.IsTutorialPlayed.Value)
                .Where(_ => !_isCameraShaking)
                .Subscribe(_ => RotatePlayer(1))
                .AddTo(this);

            // observable stream koji osluškuje da li je igrač završio klik na levu stranu ekrana zato što onda treba da
            // okinemo određeni event za to i pošaljemo poruku osluškivačima
            Drag.TapEndedStream()
                .Where(touch => touch.position.x < _middleOfScreen && !IsPlayerTappedOnLeft)
                .Take(1).Subscribe(_ =>
                {
                    IsPlayerTappedOnLeft = true;
                    ResetRotationSpeed();
                }).AddTo(this);

            // observable stream koji osluškuje da li je igrač završio klik na desnu stranu ekrana zato što onda treba da
            // okinemo određeni event za to i pošaljemo poruku osluškivačima
            // osluškivanje ne kreće sve dok igrač nije kliknuo na levu stranu (skipWhile)
            Drag.TapEndedStream()
                .SkipWhile(_ => !_areMissionCompletedAnimationsFinished)
                .SkipWhile(_ => !IsPlayerTappedOnLeft)
                .Where(touch => touch.position.x > _middleOfScreen && !IsPlayerTappedOnRight)
                .Take(1).Subscribe(_ =>
                {
                    IsPlayerTappedOnRight = true;
                    _areMissionCompletedAnimationsFinished = false;
                    ResetRotationSpeed();
                }).AddTo(this);
            Drag.TapEndedStream()
                .SkipWhile(_ => !IsPlayerTappedOnLeft && !IsPlayerTappedOnRight)
                .Subscribe(_ => ResetRotationSpeed())
                .AddTo(this);
        }
#endif

        #endregion
    }
}