﻿using Tunnel.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Tunnel.Tutorial
{
    public class PlayerAvoidedObstacle
    {
        public int NumberOfAvoidedObstacles { get; set; }
    }
    
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: Player
    /// Klasa koja kontrolise "smrt" igraca
    /// </summary>
    public sealed class TutorialPlayerDeath : MonoBehaviour
    {
        #region Private Fields

        private global::PlayerData _playerData;
        private int _counterOfAvoidedObstacles;

        #endregion





        #region Monobehaviour Events

        private void Awake()
        {
#if UNITY_EDITOR
            AssertReferences();
#endif
        }

        [Inject]
        private void InjectDependencies(global::PlayerData playerData)
        {
            _playerData = playerData;
        }

        private void AssertReferences()
        {
            Assert.IsNotNull(_playerData);
        }

        // Start is called before the first frame update
        private void Start()
        {
            this.OnTriggerEnterAsObservable()
                .Subscribe(OnTriggerEnterEventHandler)
                .AddTo(this);
        }

        private void OnTriggerEnterEventHandler(Collider otherCollider)
        {
            if (otherCollider.CompareTag(Constants.Tags.TUTORIAL_AVOIDED_TRIGGER))
            {
                ++_counterOfAvoidedObstacles;
                MessageBroker.Default.Publish(new PlayerAvoidedObstacle
                {
                    NumberOfAvoidedObstacles = _counterOfAvoidedObstacles
                });
                return;
            }

            _playerData.IsPlayerHitObstacle = true;
        }

        #endregion
    }
}
