﻿using Dreamteck.Splines;
using Tunnel.DynamicObstacles;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;

namespace Tunnel.Tutorial
{
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: Track
    /// Klasa koja kontrolise izgled levela
    /// </summary>
    public sealed class TutorialEnvironment : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] private ObstaclesHolderSO _insideObstacles;
        [SerializeField] private SplineFollower _playerSplineFollower;
        [SerializeField] private TubeGenerator _tubeGenerator;
        [SerializeField] private Light _tubeLight;
        [SerializeField] private Light _obstacleLight;
        [SerializeField] private LevelData _levelData;
        [SerializeField] private BoxCollider _firstAvoidedObstacleCollider;
        [SerializeField] private BoxCollider _secondAvoidedObstacleCollider;

        #endregion




        #region Monobehaviour Events

        private void Awake()
        {
#if UNITY_EDITOR
            AssertReferences();
#endif
        }

        private void AssertReferences()
        {
            Assert.IsNotNull(_insideObstacles);
            Assert.IsNotNull(_playerSplineFollower);
            Assert.IsNotNull(_tubeGenerator);
            Assert.IsNotNull(_tubeLight);
            Assert.IsNotNull(_obstacleLight);
            Assert.IsNotNull(_levelData);
            Assert.IsNotNull(_firstAvoidedObstacleCollider);
            Assert.IsNotNull(_secondAvoidedObstacleCollider);
        }

        // Start is called before the first frame update
        private void Start()
        {
            SetTutorialEnvironment();
            RegisterObservableStreamListeners();
        }

        private void SetTutorialEnvironment()
        {
            _tubeGenerator.flipFaces = true;
            _tubeGenerator.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = _levelData.tubeTexture;
            _tubeLight.color = _levelData.tubeAndObstacleInsideLightColor;
            _obstacleLight.color = _tubeLight.color.GetComplementary();
        }

        private void RegisterObservableStreamListeners()
        {
            MessageBroker.Default.Receive<GamePaused>()
                .Subscribe(HandleGamePaused)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTappedOnRight>()
                .Subscribe(HandlePlayerTappedOnRight)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTiltedOnRight>()
                .Subscribe(HandlePlayerTiledOnRight)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(HandlePlayerHitObstacleEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<TutorialFinished>()
                .Subscribe(HandleTutorialIsFinished)
                .AddTo(this);
        }

        #endregion


        #region Event Listeners

        private void HandleGamePaused(GamePaused gamePaused)
        {
            _insideObstacles.TooglePause(gamePaused.IsGamePaused);
        }

        private void HandlePlayerTappedOnRight(PlayerTappedOnRight playerTappedOnRight)
        {
            SetUpObstacles();
        }

        private void SetUpObstacles()
        {
            _insideObstacles.Initialize();
            var obstacles = _insideObstacles.obstaclesHolder.obstacles;
            for (int i = 0; i < obstacles.Length; i++)
            {
                obstacles[i].splineFollower.SetPercent(_playerSplineFollower.result.percent + (i + 1) * 0.1f);
            }
        }

        private void HandlePlayerTiledOnRight(PlayerTiltedOnRight obj)
        {
            SetUpObstacles();
        }

        private void HandlePlayerHitObstacleEvent(PlayerHitObstacle playerHitObstacle)
        {
            _playerSplineFollower.Restart();
            SetUpObstacles();
        }

        private void HandleTutorialIsFinished(TutorialFinished tutorialFinished)
        {
            _insideObstacles.Hide();
        }

        #endregion
    }
}