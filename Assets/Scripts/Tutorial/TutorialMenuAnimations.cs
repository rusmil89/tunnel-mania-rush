﻿using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Tunnel.Tutorial
{
    public struct MissionCompletedAnimationsPlaying {}
    public struct RibbonBlinkingAnimationPlaying {}
    
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: Tutorial Menu
    /// Klasa koja kontrolise animacije u tutorijal meniju
    /// </summary>
    public sealed class TutorialMenuAnimations
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly TutorialMenu _tutorialMenu;
        private bool _isAnimationFinished;

        /// <summary>
        /// Consts
        /// </summary>
        private readonly Color _blackPanelColor = new Color(0f, 0f, 0f, 175/255f);
        private const float MISSION_COMPLETED_EASING_DURATION = 0.5f;
        private const float MISSION_COMPLETED_EASING_OUT_DELAY = 2f;

        #endregion
        
        
        
        
        
        #region Custom Methods

        public TutorialMenuAnimations(TutorialMenu tutorialMenu)
        {
            _tutorialMenu = tutorialMenu;
        }

        /// <summary>
        /// Metoda koja animira objekte koji pokazuju korisniku na koji deo ekrana treba da tapne
        /// WHY: Zato sto animacija vise privlaci paznju igraca i daje mu bolji feedback
        /// </summary>
        public IEnumerator AnimateTapIcons()
        {
            var tutorialMenuOuterTapIcon = _tutorialMenu.TapIcon;
            var scaleVector = Vector3.one / 2;
            LeanTween.scale(tutorialMenuOuterTapIcon, scaleVector, 0.5f)
                .setFrom(Vector3.one)
                .setLoopPingPong();
            yield return null;
        }

        public IEnumerator AnimateTiltIcons()
        {
            var tutorialMenuTiltIcon = _tutorialMenu.TiltIcon;
            tutorialMenuTiltIcon.rotation = Quaternion.Euler(Vector3.zero);
            const float toAngle = 0f;
            LeanTween.rotateZ(tutorialMenuTiltIcon.gameObject, toAngle, 0.5f)
                .setFrom(-20f)
                .setLoopPingPong();
            yield return null;
        }

        /// <summary>
        /// Metoda koja animira tekst "MISSION COMPLETED"
        /// WHY: Zato sto animacija vise privlaci paznju igraca i daje mu bolji feedback
        /// </summary>
        public IEnumerator AnimateMissionCompleted()
        {
            _isAnimationFinished = false;
            MessageBroker.Default.Publish(new MissionCompletedAnimationsPlaying());
            AnimateMissionCompletedRibbonText();
            var tutorialMenuMissionRectTransform = _tutorialMenu.MissionRectTransform;
            var tutorialMenuCompletedRectTransform = _tutorialMenu.CompletedRectTransform;
            var missionRectTransformPositionX = tutorialMenuMissionRectTransform.anchoredPosition.x;
            var completedRectTransformPositionX = tutorialMenuCompletedRectTransform.anchoredPosition.x;
            LeanTween.moveX(tutorialMenuMissionRectTransform, 0f, MISSION_COMPLETED_EASING_DURATION)
                .setEase(LeanTweenType.easeSpring);
            LeanTween.moveX(tutorialMenuCompletedRectTransform, 0f, MISSION_COMPLETED_EASING_DURATION)
                .setEase(LeanTweenType.easeSpring);
            LeanTween.delayedCall(MISSION_COMPLETED_EASING_OUT_DELAY, () => MessageBroker.Default.Publish(new MissionCompletedAnimationsPlaying()));
            LeanTween.moveX(tutorialMenuMissionRectTransform, missionRectTransformPositionX, MISSION_COMPLETED_EASING_DURATION)
                .setEase(LeanTweenType.easeOutBack)
                .setDelay(MISSION_COMPLETED_EASING_OUT_DELAY);
            LeanTween.moveX(tutorialMenuCompletedRectTransform,
                    completedRectTransformPositionX,
                    MISSION_COMPLETED_EASING_DURATION)
                .setEase(LeanTweenType.easeOutBack)
                .setDelay(MISSION_COMPLETED_EASING_OUT_DELAY)
                .setOnComplete(() => _isAnimationFinished = true);
            
            yield return new WaitUntil(() => _isAnimationFinished);
        }

        /// <summary>
        /// Metoda koja animira precrtavanje teksta na traci nakon sto igrac uspesno zavrsi neki korak u tutorijalu
        /// WHY: Zato sto animacija vise privlaci paznju igraca i daje mu bolji feedback 
        /// </summary>
        private void AnimateMissionCompletedRibbonText()
        {
            _tutorialMenu.RibbonText.color = Color.gray;
            _tutorialMenu.RibbonText.fontStyle = FontStyles.Strikethrough;
        }

        /// <summary>
        /// Metoda koja animira traku na kojoj se ispisuju zadaci za igraca u tutorijalu
        /// WHY: Zato sto animacija vise privlaci paznju igraca i daje mu bolji feedback
        /// </summary>
        public void RibbonBlinking()
        {
            MessageBroker.Default.Publish(new RibbonBlinkingAnimationPlaying());
            var tutorialMenuRibbonImageRectTransform = _tutorialMenu.RibbonImageRectTransform;
            var ribbonAlphaValue = tutorialMenuRibbonImageRectTransform.GetComponent<Image>().color.a;
            var ribbonImageAnimationSequence = LeanTween.sequence();
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, 0f, 0.2f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, ribbonAlphaValue, 0.2f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, 0f, 0.1f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, ribbonAlphaValue, 0.025f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, 0f, 0.025f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, ribbonAlphaValue, 0.05f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, 0f, 0.15f));
            ribbonImageAnimationSequence.append(LeanTween.alpha(tutorialMenuRibbonImageRectTransform, ribbonAlphaValue, 0.15f));
        }

        /// <summary>
        /// Metoda koja ispisuje tekst i podesava polja za ispisivanje teksta "TUTORIAL COMPLETED"
        /// WHY: Zato sto animacija vise privlaci paznju igraca i daje mu bolji feedback
        /// </summary>
        /// <param name="missionRectTransform">Objekat koji sadrzi tekst "TUTORIAL"</param>
        /// <param name="completedRectTransform">Objekat koji sadrzi tekst "COMPLETED"</param>
        public void TutorialCompleted(RectTransform missionRectTransform, RectTransform completedRectTransform)
        {
            MessageBroker.Default.Publish(new MissionCompletedAnimationsPlaying());
            missionRectTransform.anchorMin = missionRectTransform.anchorMax = completedRectTransform.anchorMin = completedRectTransform.anchorMax = Vector2.one / 2;
            missionRectTransform.anchoredPosition = new Vector2(missionRectTransform.anchoredPosition.x, 65f);
            completedRectTransform.anchoredPosition = new Vector2(completedRectTransform.anchoredPosition.x, -65f);
            LeanTween.moveX(missionRectTransform, 0f, MISSION_COMPLETED_EASING_DURATION).setEase(LeanTweenType.easeSpring);
            LeanTween.moveX(completedRectTransform, 0f, MISSION_COMPLETED_EASING_DURATION).setEase(LeanTweenType.easeSpring);
        }

        /// <summary>
        /// Metoda koja skriva traku nakon uspesno zavrsenog tutorijala na kojoj pise TUTORIAL COMPLETED
        /// WHY: Zato sto preko nje treba da se pojavi prozor koji prikazuje igracu nagradu koju je dobio
        /// </summary>
        /// <param name="ribbonImageRectTransform">Objekat koji se animira</param>
        public void HideRibbon(RectTransform ribbonImageRectTransform)
        {
            LeanTween.alpha(ribbonImageRectTransform, 0f, MISSION_COMPLETED_EASING_DURATION);
        }

        /// <summary>
        /// Metoda koja gasi objekte za ispisvanje teksta TUTORIAL COMPLETED
        /// WHY: Zato sto preko nje treba da se pojavi prozor koji prikazuje igracu nagradu koju je dobio
        /// </summary>
        /// <param name="missionText">Objekat koji sadrzi tekst TUTORIAL</param>
        /// <param name="completedText">Objekat koji sadrzi tekst COMPLETED</param>
        public void HideText(GameObject missionText, GameObject completedText)
        {
            missionText.SetActive(false);
            completedText.SetActive(false);
        }

        /// <summary>
        /// Metoda koja menja boju panela koji se nalazi iza menija koji prikazuje korisniku koju je nagradu dobio na kraju tutorijala
        /// WHY: Zato sto preko nje treba da se pojavi prozor koji prikazuje igracu nagradu koju je dobio i tako fokus igraca bude na nagradu, a ne na tunel
        /// </summary>
        /// <param name="panelRectTransform">Objekat koji animiramo</param>
        public void BlackPanel(RectTransform panelRectTransform)
        {
            LeanTween.value(panelRectTransform.gameObject,
                color => panelRectTransform.GetComponent<Image>()
                    .color = color,
                Color.clear,
                _blackPanelColor,
                MISSION_COMPLETED_EASING_DURATION);
        }

        /// <summary>
        /// Metoda koja prikazuje meni za nagradu
        /// WHY: Zato sto zelimo da nagradimo igraca za njegov trud
        /// </summary>
        /// <param name="rewardPanel">Objekat koji se animira</param>
        public void ShowMenuWithReward(RectTransform rewardPanel)
        {
            rewardPanel.gameObject.SetActive(true);
            LeanTween.alpha(rewardPanel, 1f, MISSION_COMPLETED_EASING_DURATION)
                .setDelay(1f);
        }

        /// <summary>
        /// Metoda koja pokazuje dugme za prikupljanje nagrade
        /// WHY: Zato sto zelim da igrac sto vise interaguje sa igrom i na taj nacin se vise poveze sa njom umesto da bude samo nemi posmatrac
        /// </summary>
        /// <param name="claimRewardButton">Objekat koji se animira</param>
        public void ShowClaimRewardButton(Button claimRewardButton)
        {
            claimRewardButton.gameObject.SetActive(true);
            LeanTween.alpha(claimRewardButton.gameObject, 1f, MISSION_COMPLETED_EASING_DURATION)
                .setDelay(1f);
        }

        public void ShowControlsPanel(RectTransform controlsPanel)
        {
            controlsPanel.gameObject.SetActive(true);
            LeanTween.alpha(controlsPanel, 1f, MISSION_COMPLETED_EASING_DURATION)
                .setDelay(1f);
        }

        public void ShowTouchControlButton(Button touchControlButton)
        {
            touchControlButton.gameObject.SetActive(true);
            LeanTween.alpha(touchControlButton.gameObject, 1f, MISSION_COMPLETED_EASING_DURATION)
                .setDelay(1f);
        }

        public void ShowAccelerometerControlButton(Button accelerometerControlButton)
        {
            accelerometerControlButton.gameObject.SetActive(true);
            LeanTween.alpha(accelerometerControlButton.gameObject, 1f, MISSION_COMPLETED_EASING_DURATION)
                .setDelay(1f);
        }

        #endregion
    }
}
