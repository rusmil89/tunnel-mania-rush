﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Core.Events;
using Localization;
using RSG;
using TMPro;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.UI;
using Tunnel.Utilities;
using UniRx;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using Zenject;

namespace Tunnel.Tutorial
{
    public struct RewardMenuInTutorialCalled {}
    public struct TutorialFinished {}
    public struct MissionCompletedAnimationFinished {}
    public struct TutorialMenuClaimButtonClicked {}
    public struct TutorialMenuTouchControlsButtonClicked {}
    
    /// <summary>
    /// SCENE: Level
    /// GAME_OBJECT: TutorialMenu
    /// Klasa koja kontroliše ponašanje tutorijal menija
    /// </summary>
    public sealed class TutorialMenu : MonoBehaviour, ICurrencyAnimatingMenu
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private RectTransform _panelRectTransform;
        [SerializeField] private RectTransform _ribbonImageRectTransform;
        [SerializeField] private TMP_Text _ribbonText;
        [SerializeField] private RectTransform _missionRectTransform;
        [SerializeField] private RectTransform _completedRectTransform;
        [SerializeField] private RectTransform _tapIcon;
        [SerializeField] private RectTransform _tiltIcon;
        [SerializeField] private RectTransform _rewardPanel;
        [SerializeField] private RectTransform _controlsPanel;
        [SerializeField] private TMP_Text _coinsRewardAmountText;
        [SerializeField] private TMP_Text _tokensRewardAmountText;
        [SerializeField] private TMP_Text _shieldsRewardAmountText;
        [SerializeField] private GameObject _fireworksParticlesHolder;
        [SerializeField] private TMP_Text _rewardPanelTitleText;
        [SerializeField] private TMP_Text _controlsPanelTitleText;
        [SerializeField] private TMP_Text _controlsPanelDescriptionText;
        [SerializeField] private Button _claimRewardButton;
        [SerializeField] private TMP_Text _claimButtonText;
        [SerializeField] private Button _touchControlButton;
        [SerializeField] private Button _accelerometerControlButton;
        
        [Header("Animation Elements")]
        [SerializeField] private RectTransform _coin;
        [SerializeField] private RectTransform _token;
        [SerializeField] private RectTransform _shield;
        [SerializeField] private RectTransform _topMenuCoinsPosition;
        [SerializeField] private RectTransform _topMenuTokensPosition;
        [SerializeField] private RectTransform _topMenuShieldsPosition;
        [SerializeField] private GameObject _currencyAmountPrefab;
        [SerializeField] private GameObject _animationsElements;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private TutorialMenuAnimations _tutorialMenuAnimations;
        private CurrencyMenuLogic _currencyMenuLogic;
        private CurrencyBoughtAnimations _currencyBoughtAnimations;
        private TMP_Text _missionText;
        private TMP_Text _completedText;
        private LanguageClient _languageClient;
        
        /// <summary>
        /// Animations elements
        /// </summary>
        private IState _rootState;
        private List<RectTransform> _coins;
        private List<RectTransform> _tokens;
        private List<RectTransform> _shields;
        private TMP_Text _currencyAmountText;

        /// <summary>
        /// Consts
        /// </summary>
        private const string INITIALIZATION_STATE_NAME = Constants.MenusStatesNames.INITIALIZATION;
        private const string SHOWN_STATE_NAME = Constants.MenusStatesNames.SHOWN;
        private const string HIDDEN_STATE_NAME = Constants.MenusStatesNames.HIDDEN;
        private const string ANIMATIONS_PLAYING_STATE_NAME = Constants.MenusStatesNames.ANIMATIONS_PLAYING;
        private const double DELAY_TUTORIAL_START = 3d;
        private const float DELAY_TUTORIAL_COMPLETED = 0.1f;
        private const float DELAY_LOADING_MENU_SCENE = 0.25f;
        private const float DELAY_SHOWING_REWARD_MENU = 2f;
        private const float RIBBON_HEIGHT = 300f;
        private const float TAP_ICON_POSITION_VALUE = 200f;

        #endregion





        #region Properties
        

        public Constants.CurrencyType CurrencyType { get; private set; }

        public int CurrencyAmountEarned { get; private set; }

        public int CurrencyAmountSpent => default;

        public List<RectTransform> CurrenciesTransforms  { get; private set; }

        public RectTransform CurrencyAnimationStartObject => default;

        public RectTransform CurrencyAnimationEndObject { get; private set; }

        public GameObject CurrencyAmountPrefab => _currencyAmountPrefab;
        
        public RectTransform TapIcon => _tapIcon;
        
        public RectTransform TiltIcon => _tiltIcon;

        public RectTransform MissionRectTransform => _missionRectTransform;

        public RectTransform CompletedRectTransform => _completedRectTransform;

        public RectTransform RibbonImageRectTransform => _ribbonImageRectTransform;

        public TMP_Text RibbonText => _ribbonText;

        #endregion
        
        
        


        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            var allRectTransforms = gameObject.Descendants().OfComponent<RectTransform>();
            _panelRectTransform = allRectTransforms.First(s => s.name.Contains(Constants.GameObjectsNames.PANEL));
            _ribbonImageRectTransform = allRectTransforms.First(s => s.name.Contains("Ribbon"));
            _missionRectTransform = allRectTransforms.First(s => s.name.Contains("Mission"));
            _completedRectTransform = allRectTransforms.First(s => s.name.Contains("Completed"));
            _tapIcon = allRectTransforms.First(s => s.name.Contains("TapIcon"));
            _tiltIcon = allRectTransforms.First(s => s.name.Contains("TiltIcon"));
            _rewardPanel = allRectTransforms.First(s => s.name.Contains("RewardPanel"));
            _controlsPanel = allRectTransforms.First(s => s.name.Contains("ControlsPanel"));
            _rewardPanelTitleText = allRectTransforms.First(s => s.name.Contains("RewardPanelTitleText")).GetComponent<TMP_Text>();
            _controlsPanelTitleText = allRectTransforms.First(s => s.name.Contains("ControlsPanelTitleText")).GetComponent<TMP_Text>();
            _controlsPanelDescriptionText = _controlsPanel.gameObject.Child("Description").GetComponent<TMP_Text>();
            _coinsRewardAmountText = allRectTransforms.First(s => s.name.Contains("CoinsRewardText")).GetComponent<TMP_Text>();
            _tokensRewardAmountText = allRectTransforms.First(s => s.name.Contains("TokensRewardText")).GetComponent<TMP_Text>();
            _shieldsRewardAmountText = allRectTransforms.First(s => s.name.Contains("ShieldsRewardText")).GetComponent<TMP_Text>();
            _claimRewardButton = allRectTransforms.First(s => s.name.Contains("ClaimButton")).GetComponent<Button>();
            _claimButtonText = allRectTransforms.First(s => s.name.Contains("ClaimButtonText")).GetComponent<TMP_Text>();
            _touchControlButton = allRectTransforms.First(s => s.name.Contains("TouchButton")).GetComponent<Button>();
            _accelerometerControlButton = allRectTransforms.First(s => s.name.Contains("AccelerometerButton")).GetComponent<Button>();
            _ribbonText = allRectTransforms.First(s => s.name.Contains("RibbonText")).GetComponent<TMP_Text>();
            _fireworksParticlesHolder = allRectTransforms.First(s => s.name.Contains("Fireworks")).gameObject;
            _coin = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Coin"))
                .GetComponent<RectTransform>();
            _token = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Token"))
                .GetComponent<RectTransform>();
            _shield = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("Shield"))
                .GetComponent<RectTransform>();
            _currencyAmountPrefab = AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new []{Constants.Editor.Paths.PREFABS})
                .Select(s => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Contains("CurrencyAmountText"));
            _animationsElements = gameObject.Descendants().First(x => x.name.Contains("AnimationsElements"));
            _topMenuCoinsPosition = gameObject.Descendants().First(x => x.name.Contains("CoinsPosition")).GetComponent<RectTransform>();
            _topMenuTokensPosition = gameObject.Descendants().First(x => x.name.Contains("TokensPosition")).GetComponent<RectTransform>();
            _topMenuShieldsPosition = gameObject.Descendants().First(x => x.name.Contains("ShieldsPosition")).GetComponent<RectTransform>();
            
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogTutorialBegin();   
#endif
            
            GetRequiredComponents();
        }

        private void GetRequiredComponents()
        {
            _missionText = MissionRectTransform.GetComponent<TMP_Text>();
            _completedText = CompletedRectTransform.GetComponent<TMP_Text>();
        }

        [Inject]
        private void InjectDependencies(LanguageClient languageClient)
        {
            _languageClient = languageClient;
        }

        private void Start()
        {
            SetMenuStates();
            _rootState.ChangeState(INITIALIZATION_STATE_NAME);
        }

        private void OnDestroy()
        {
            _rootState.Exit();
        }
        
        #endregion





        #region Custom Methods
        
        private void SetMenuStates()
        {
            _rootState = new StateMachineBuilder()
                .State<Constants.MenusStatesTypes.Normal>(INITIALIZATION_STATE_NAME)
                .Enter(_ => HandleEnteringInitializationState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(SHOWN_STATE_NAME)
                .End()
                .State<Constants.MenusStatesTypes.Normal>(ANIMATIONS_PLAYING_STATE_NAME)
                .Enter(_ => HandleEnteringAnimationsPlayingState())
                .End()
                .State<Constants.MenusStatesTypes.Normal>(HIDDEN_STATE_NAME)
                .Enter(_ => HandleEnteringHiddenState())
                .End()
                .Build();
        }

        private void HandleEnteringInitializationState()
        {
            SetInitialValues();
            RegisterButtonsListeners();
            RegisterObservableListeners();
            InstantiateCoins();
            _rootState.ChangeState(SHOWN_STATE_NAME);
        }

        private void SetInitialValues()
        {
            _missionText.SetText(_languageClient.CurrentLanguage.Mission.Value);
            _completedText.SetText(_languageClient.CurrentLanguage.Completed.Value);
            _ribbonText.SetText(_languageClient.CurrentLanguage.Welcome.Value);
            _rewardPanelTitleText.SetText(_languageClient.CurrentLanguage.Reward.Value);
            _controlsPanelTitleText.SetText(_languageClient.CurrentLanguage.ControlsPanelTitle.Value);
            _controlsPanelDescriptionText.SetText(_languageClient.CurrentLanguage.ControlsPanelDescription.Value);
            _claimButtonText.SetText(_languageClient.CurrentLanguage.Claim.Value);
            _tutorialMenuAnimations = new TutorialMenuAnimations(this);
            _currencyMenuLogic = new CurrencyMenuLogic();
            _currencyBoughtAnimations = new CurrencyBoughtAnimations(this);
            _coinsRewardAmountText.text = new StringBuilder("x").Append(RemoteConfigClient.Instance.TutorialCompletedRewardCoins).ToString();
            _tokensRewardAmountText.text = new StringBuilder("x").Append(RemoteConfigClient.Instance.TutorialCompletedRewardTokens).ToString();
            _shieldsRewardAmountText.text = new StringBuilder("x").Append(RemoteConfigClient.Instance.TutorialCompletedRewardShields).ToString();
        }

        private void RegisterButtonsListeners()
        {
            _claimRewardButton.OnClickAsObservable()
                .Subscribe(HandleClaimRewardButton)
                .AddTo(this);
            _touchControlButton.OnClickAsObservable()
                .Subscribe(HandleTouchControlsButton)
                .AddTo(this);
            _accelerometerControlButton.OnClickAsObservable()
                .Subscribe(HandleAccelerometerControlsButton)
                .AddTo(this);
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<PlayerTappedOnLeft>()
                .Subscribe(HandlePlayerTappedOnLeftEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTappedOnRight>()
                .Subscribe(HandlePlayerTappedOnRightEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerAvoidedObstacle>()
                .Subscribe(HandlePlayerAvoidedObstacleEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(HandlePlayerHitObstacleEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTiltedOnLeft>()
                .Subscribe(HandlePlayerTiltedOnLeft)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTiltedOnRight>()
                .Subscribe(HandlePlayerTiltedOnRight)
                .AddTo(this);
            Observable.Timer(TimeSpan.FromSeconds(DELAY_TUTORIAL_START))
                .Subscribe(DelayStartingTutorial)
                .AddTo(this);
        }

        private void InstantiateCoins()
        {
            if (CurrenciesTransforms == null || CurrenciesTransforms.Count == 0)
            {
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_coin, _animationsElements)
                    .Subscribe(coins => _coins = coins)
                    .AddTo(this);
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_token, _animationsElements)
                    .Subscribe(tokens => _tokens = tokens)
                    .AddTo(this);
                _currencyMenuLogic.GetInstantiatedAnimationsElements(_shield, _animationsElements)
                    .Subscribe(shields => _shields = shields)
                    .AddTo(this);
            }

            if (_currencyAmountText == null)
            {
                _currencyMenuLogic.GetInstantiatedCurrencyAmountElements(_currencyAmountPrefab, _animationsElements)
                    .Subscribe(currencyAmountPrefab =>
                    {
                        _currencyAmountPrefab = currencyAmountPrefab;
                        _currencyAmountText = _currencyAmountPrefab.GetComponent<TMP_Text>();
                    })
                    .AddTo(this);
            }
        }

        private void DelayStartingTutorial(long l)
        {
            Observable.FromCoroutine(() => PrepareTapIcons(true))
                .SelectMany(_tutorialMenuAnimations.AnimateTapIcons)
                .SelectMany(SetRibbon(_languageClient.CurrentLanguage.TapOnTheLeft.Value))
                .Subscribe()
                .AddTo(this);
        }

        private IEnumerator PrepareTapIcons(bool isLeft)
        {
            if (isLeft)
            {
                _tapIcon.anchorMin = _tapIcon.anchorMax = Vector2.zero;
                _tapIcon.anchoredPosition = new Vector2(TAP_ICON_POSITION_VALUE, TAP_ICON_POSITION_VALUE);
            }
            else
            {
                _tapIcon.anchorMin = _tapIcon.anchorMax = Vector2.right;
                _tapIcon.anchoredPosition = new Vector2(-TAP_ICON_POSITION_VALUE, TAP_ICON_POSITION_VALUE);
            }
            SetTapIconsActivity(true);
            yield return null;
        }

        private void SetTapIconsActivity(bool value)
        {
            _tapIcon.gameObject.SetActive(value);
        }

        private void HandleEnteringAnimationsPlayingState()
        {
            _rootState.ChangeState(HIDDEN_STATE_NAME);
            var coinsRewardAmount = RemoteConfigClient.Instance.TutorialCompletedRewardCoins;
            var tokensRewardAmount = RemoteConfigClient.Instance.TutorialCompletedRewardTokens;
            var shieldsRewardAmount = RemoteConfigClient.Instance.TutorialCompletedRewardShields;
            Observable.FromCoroutine(() => PrepareAnimation(_coins, _topMenuCoinsPosition, coinsRewardAmount, Constants.CurrencyType.Coin))
                .SelectMany(_currencyBoughtAnimations.Animate)
                .SelectMany(PrepareAnimation(_tokens, _topMenuTokensPosition, tokensRewardAmount, Constants.CurrencyType.Token))
                .SelectMany(_currencyBoughtAnimations.Animate)
                .SelectMany(PrepareAnimation(_shields, _topMenuShieldsPosition, shieldsRewardAmount, Constants.CurrencyType.Shield))
                .SelectMany(_currencyBoughtAnimations.Animate)
                .Delay(TimeSpan.FromSeconds(DELAY_LOADING_MENU_SCENE))
                .Subscribe(_ => DelayLoadingMenuScene())
                .AddTo(this);
        }

        private IEnumerator PrepareAnimation(List<RectTransform> currencyTransforms, RectTransform topMenuPosition, int rewardAmount, Constants.CurrencyType currencyType)
        {
            CurrenciesTransforms = currencyTransforms;
            CurrencyAnimationEndObject = topMenuPosition;
            CurrencyAmountEarned = rewardAmount;
            CurrencyType = currencyType;
            yield return null;
        }

        private static void DelayLoadingMenuScene()
        {
            MessageBroker.Default.Publish(new ShouldLoadMainMenuScene());
        }

        private void HandleEnteringHiddenState()
        {
            MessageBroker.Default.Publish(new TopButtonsMenuCalled());
        }

        #endregion
        
        
        


        #region Event Listeners

        /// <summary>
        /// Metoda koja osluskuje i poziva se kada igrac klikne na CLAIM dugme nakon sto se pojavi meni u kome se prikazuje koju je nagradu dobio na kraju tutorijala
        /// WHY: Zato sto je neophodno da igrac dobije odmah feedback nakon klika na dugme
        /// </summary>
        private void HandleClaimRewardButton(Unit unit)
        {
            MessageBroker.Default.Publish(new TutorialMenuClaimButtonClicked());
            _fireworksParticlesHolder.SetActive(true);
            _claimRewardButton.gameObject.SetActive(false);
            _rewardPanel.gameObject.SetActive(false);
            _rootState.ChangeState(ANIMATIONS_PLAYING_STATE_NAME);
        }

        private void HandleTouchControlsButton(Unit obj)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _controlsPanel.gameObject.SetActive(false);
            MessageBroker.Default.Publish(new TutorialMenuTouchControlsButtonClicked());
            ShowRewardsPanel();
        }

        private void HandleAccelerometerControlsButton(Unit obj)
        {
            MessageBroker.Default.Publish(new ButtonClicked());
            _controlsPanel.gameObject.SetActive(false);
            ShowRewardsPanel();
        }

        /// <summary>
        /// Metoda koja osluskuje dogadjaj kada korisnik tapne na levu stranu u tutorijalu
        /// WHY: Zato sto je to prvi korak u tutorijalu i preduslov da se nastavi dalje sa njim
        /// </summary>
        private void HandlePlayerTappedOnLeftEvent(PlayerTappedOnLeft playerTappedOnLeft)
        {
            SetTapIconsActivity(false);
            Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                .SelectMany(PrepareTapIcons(false))
                .SelectMany(_tutorialMenuAnimations.AnimateTapIcons)
                .SelectMany(SetRibbon(_languageClient.CurrentLanguage.TapOnTheRight.Value))
                .Subscribe()
                .AddTo(this);
        }

        private IEnumerator SetRibbon(string message)
        {
            SetRibbonText(message);
            _tutorialMenuAnimations.RibbonBlinking();
            if (message.Equals(_languageClient.CurrentLanguage.TapOnTheRight.Value))
            {
                MessageBroker.Default.Publish(new MissionCompletedAnimationFinished());
                yield break;
            }
            
            if (message.Equals(_languageClient.CurrentLanguage.TiltOnTheRight.Value))
            {
                MessageBroker.Default.Publish(new MissionCompletedAnimationFinished());
            }
            yield return null;
        }

        /// <summary>
        /// Metoda koja osluskuje dogadjaj kada korisnik tapne na desno u tutorijalu
        /// WHY: Zato sto je to drugi korak u tutorijalu i preduslov da se nastavi dalje sa njim
        /// </summary>
        private void HandlePlayerTappedOnRightEvent(PlayerTappedOnRight playerTappedOnRight)
        {
            SetTapIconsActivity(false);
            Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                .SelectMany(SetRibbon(_languageClient.CurrentLanguage.AvoidObstacle.Value))
                .Subscribe()
                .AddTo(this);
        }

        private void HandlePlayerTiltedOnLeft(PlayerTiltedOnLeft obj)
        {
            SetTiltIconsActivity(false);
            Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                .SelectMany(PrepareTiltIcons(false))
                .SelectMany(_tutorialMenuAnimations.AnimateTiltIcons)
                .SelectMany(SetRibbon(_languageClient.CurrentLanguage.TiltOnTheRight.Value))
                .Subscribe()
                .AddTo(this);
        }
        
        private void HandlePlayerTiltedOnRight(PlayerTiltedOnRight obj)
        {
            SetTiltIconsActivity(false);
            Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                .SelectMany(SetRibbon(_languageClient.CurrentLanguage.AvoidObstacle.Value))
                .Subscribe()
                .AddTo(this);
        }

        /// <summary>
        /// Metoda koja osluskuje kada korisnik uspesno zaobidje sve prepreke
        /// WHY: Zato sto su to sledeci koraci u tutorijalu i preduslovi da se tutorijal nastavi dalje, ili da se zavrsi u zavisnosti od toga koju prepreku po redu je igrac presao
        /// </summary>
        /// <param name="playerAvoidedObstacle">Objekat iz koga dobijamo informaciju o broju prepreka koje je korisnik presao</param>
        private void HandlePlayerAvoidedObstacleEvent(PlayerAvoidedObstacle playerAvoidedObstacle)
        {
            if (playerAvoidedObstacle.NumberOfAvoidedObstacles == 1)
            {
                Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                    .SelectMany(SetRibbon(_languageClient.CurrentLanguage.Again.Value))
                    .Subscribe()
                    .AddTo(this);
                return;
            }

            if (SystemInfo.supportsAccelerometer || SystemInfo.supportsGyroscope)
            {
                switch (playerAvoidedObstacle.NumberOfAvoidedObstacles)
                {
                    case 2:
                        Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                                  .SelectMany(PrepareTiltIcons(true))
                                  .SelectMany(_tutorialMenuAnimations.AnimateTiltIcons)
                                  .SelectMany(SetRibbon(_languageClient.CurrentLanguage.TiltOnTheLeft.Value))
                                  .Subscribe()
                                  .AddTo(this);
                        return;
                    case 3:
                        Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                                  .SelectMany(SetRibbon(_languageClient.CurrentLanguage.Again.Value))
                                  .Subscribe()
                                  .AddTo(this);
                        return;
                }
            }

            ExecuteTutorialFinish();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogTutorialCompleted();
#endif
        }

        private void ExecuteTutorialFinish()
        {
            MessageBroker.Default.Publish(new TutorialFinished());
            Observable.FromCoroutine(_tutorialMenuAnimations.AnimateMissionCompleted)
                      .SelectMany(SetFinishMission)
                      .SelectMany(FinishTutorial)
                      .SelectMany(CheckForGyro)
                      .Subscribe()
                      .AddTo(this);
        }

        private IEnumerator CheckForGyro(Unit unit)
        {
            // Check if the device has a gyroscope/accelerometer
            if (SystemInfo.supportsGyroscope || SystemInfo.supportsAccelerometer)
            {
                // Continue with ShowControlsPanel if the condition is true
                return ShowControlsPanel();
            }
            else
            {
                // Skip ShowControlsPanel by returning an empty observable
                return EmptyCoroutine();
            }
        }

        private IEnumerator EmptyCoroutine()
        {
            yield return new WaitForSeconds(DELAY_SHOWING_REWARD_MENU);
            MessageBroker.Default.Publish(new TutorialMenuTouchControlsButtonClicked());
            _controlsPanel.gameObject.SetActive(false);
            _tutorialMenuAnimations.HideRibbon(RibbonImageRectTransform);
            _tutorialMenuAnimations.HideText(_missionText.gameObject, _completedText.gameObject);
            _tutorialMenuAnimations.BlackPanel(_panelRectTransform);
            ShowRewardsPanel();
        }

        private IEnumerator PrepareTiltIcons(bool isLeft)
        {
            if (!isLeft)
            {
                _tiltIcon.GetChild(0).localRotation = Quaternion.Euler(0f, 180f, 0f);
            }
            SetTiltIconsActivity(true);
            yield return null;
        }

        private void SetTiltIconsActivity(bool isActive)
        {
            _tiltIcon.gameObject.SetActive(isActive);
        }

        private IEnumerator SetFinishMission()
        {
            RibbonText.gameObject.SetActive(false);
            RibbonImageRectTransform.gameObject.SetActive(false);
            yield return null;
        }

        /// <summary>
        /// Metoda koja prikazuje poruku na ekranu "TUTORAIL COMPLETED"
        /// WHY: Zato sto korisnik mora da dobije feedback kada je tutorijal gotov
        /// </summary>
        private IEnumerator FinishTutorial()
        {
            yield return new WaitForSeconds(DELAY_TUTORIAL_COMPLETED);
            SetRibbonForTutorialCompleted();
            _missionText.text = _languageClient.CurrentLanguage.Tutorial.Value;
            _missionText.fontSize = _completedText.fontSize = 100f;
            CurrencyAmountEarned = RemoteConfigClient.Instance.TutorialCompletedRewardCoins;
            _tutorialMenuAnimations.TutorialCompleted(MissionRectTransform, CompletedRectTransform);
        }

        /// <summary>
        /// Metoda koja prikazuje prozor u kome se ispisuje korisniku koju nagradu je dobio nakon uspesno predjenog tutorijala
        /// WHY: Zato sto zelimo da nagradimo korisnika za njegov trud
        /// </summary>
        private void ShowRewardsPanel()
        {
            _tutorialMenuAnimations.ShowMenuWithReward(_rewardPanel);
            _tutorialMenuAnimations.ShowClaimRewardButton(_claimRewardButton);
        }

        private IEnumerator ShowControlsPanel()
        {
            yield return new WaitForSeconds(DELAY_SHOWING_REWARD_MENU);
            MessageBroker.Default.Publish(new RewardMenuInTutorialCalled());
            _tutorialMenuAnimations.HideRibbon(RibbonImageRectTransform);
            _tutorialMenuAnimations.HideText(_missionText.gameObject, _completedText.gameObject);
            _tutorialMenuAnimations.BlackPanel(_panelRectTransform);
            _tutorialMenuAnimations.ShowControlsPanel(_controlsPanel);
            _tutorialMenuAnimations.ShowTouchControlButton(_touchControlButton);
            _tutorialMenuAnimations.ShowAccelerometerControlButton(_accelerometerControlButton);
        }

        /// <summary>
        /// Metoda koja podesava crnu traku na odgovarajucu poziciju kada treba da se ispise poruka "TUTORIAL COMPLETED"
        /// WHY: Zato sto default podesavanje te trake ne odgovara potrebama za ovu poruku, a zelim da iskoristim vec postojeci objekat zbog cuvanja memorije
        /// </summary>
        private void SetRibbonForTutorialCompleted()
        {
            RibbonImageRectTransform.gameObject.SetActive(true);
            RibbonImageRectTransform.anchorMin = Vector2.one / 2;
            RibbonImageRectTransform.anchorMax = Vector2.one / 2;
            RibbonImageRectTransform.anchoredPosition = Vector2.zero;
            RibbonImageRectTransform.sizeDelta = new Vector2(Screen.width, RIBBON_HEIGHT);
        }

        /// <summary>
        /// Metoda koja osluskuje dogadja kada igrac udari u prepreku
        /// WHY: Zato sto se tada igracu ponovo ispisuje drugacija poruka na ekranu i daje mu se nova sansa da zaobidje prepreku
        /// </summary>
        /// <param name="playerHitObstacle"></param>
        private void HandlePlayerHitObstacleEvent(PlayerHitObstacle playerHitObstacle)
        {
            MainThreadDispatcher.StartCoroutine(SetRibbon(_languageClient.CurrentLanguage.AvoidObstacle.Value));
        }

        /// <summary>
        /// Metoda koja podesava izgled teksta i ispisuje taj tekst na traci koja prikazuje zadatke za igraca
        /// WHY: Zato sto je potrebno da korisnik vidi koji mu je trenutni zadatak u tutorijalu i to mora da bude jasno i lepo prikazano
        /// </summary>
        /// <param name="message"></param>
        private void SetRibbonText(string message)
        {
            RibbonText.color = Color.white;
            RibbonText.fontStyle = FontStyles.LowerCase;
            RibbonText.text = message;
        }

        #endregion
    }
}