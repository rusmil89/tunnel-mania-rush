﻿using System.Collections;
using Dreamteck.Splines;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;

namespace Tunnel.Tutorial
{
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: PlayerPathFollower
    /// Klasa koja kontrolise ponasanje igraca u tutorijalu
    /// </summary>
    public sealed class TutorialPlayer : MonoBehaviour
    {

        #region Private Fields

        [SerializeField] private SplineFollower _playerPathFollower;

        private const float INITIAL_SPEED = 5f;
        private const float MAX_SPEED = 10f;
        
        private global::PlayerData _playerData;

        #endregion
        
        
        


        #region Monobehaviour Events

        private void Awake()
        {
#if UNITY_EDITOR
            AssertReferences();
#endif
        }

        private void AssertReferences()
        {
            Assert.IsNotNull(_playerPathFollower);
        }

        [Inject]
        private void InjectDependencies(global::PlayerData playerData)
        {
            _playerData = playerData;
        }

        private void Start()
        {
            SetInitialValues();
            RegisterObservableStreamListeners();
        }

        private void SetInitialValues()
        {
            StartCoroutine(IncreasePlayerSpeed());
        }

        private void RegisterObservableStreamListeners()
        {
            MessageBroker.Default.Receive<TutorialFinished>()
                .Subscribe(HandleTutorialIsFinished)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerAvoidedObstacle>()
                .Subscribe(HandlePlayerAvoidedObstacle)
                .AddTo(this);
            MessageBroker.Default.Receive<TutorialMenuTouchControlsButtonClicked>()
                .Subscribe(HandleTouchControls)
                .AddTo(this);
        }

        #endregion
        
        
        


        #region Coroutines

        /// <summary>
        /// Korutina koja povecava brzinu igraca kroz tutorijal tokom vremena
        /// WHY: Zato sto ne zelimo da uplasimo igraca na samom pocetku ogromnom brzinom, vec zelimo da on pravovremeno reaguje
        /// </summary>
        /// <returns></returns>
        private IEnumerator IncreasePlayerSpeed()
        {
            WaitForSeconds delay = new WaitForSeconds(0.1f);
            _playerPathFollower.followSpeed = INITIAL_SPEED;
            while (_playerPathFollower.followSpeed < MAX_SPEED)
            {
                _playerPathFollower.followSpeed += Time.deltaTime;
                yield return delay;
            }
        }

        #endregion
        
        
        


        #region Event Listeners

        /// <summary>
        /// Metoda koja osluskuje dogadjaj kada je tutorijal zavrsen
        /// WHY: Zato sto tada podesava vrednosti odredjenih promenljivih neopgodnih za dalji rad igre
        /// </summary>
        /// <param name="tutorialFinished"></param>
        private void HandleTutorialIsFinished(TutorialFinished tutorialFinished)
        {
            SlowDownPlayer();
            _playerData.PersistentData.IsTutorialPlayed.Value = true;
            _playerData.IsPlayerDead = false;
        }

        /// <summary>
        /// Metoda koja usporava igraca nakon sto je uspesno zavrsio tutorijal
        /// WHY: Zato sto animacija daje najbolji feedback za igraca da se nesto desava i zato sto zelimo da mu dokazemo da je tutorijal gotov
        /// </summary>
        private void SlowDownPlayer()
        {
            StopAllCoroutines();
            LeanTween.value(gameObject, f => _playerPathFollower.followSpeed = f, _playerPathFollower.followSpeed, 0f, 3f);
        }

        /// <summary>
        /// Metoda koja ukljucuje opciju za ziroskop kada igrac po drugi put prodje pored prepreke sa tap
        /// WHY: Zato sto je takav redosled dogadjaja u tutorijalu
        /// </summary>
        /// <param name="obj"></param>
        private void HandlePlayerAvoidedObstacle(PlayerAvoidedObstacle obj)
        {
            if (obj.NumberOfAvoidedObstacles == 2 && (SystemInfo.supportsAccelerometer || SystemInfo.supportsGyroscope))
            {
                _playerData.PersistentData.IsGyroscopeEnabled.SetValueAndForceNotify(true);
            }
        }

        private void HandleTouchControls(TutorialMenuTouchControlsButtonClicked obj)
        {
            _playerData.PersistentData.IsGyroscopeEnabled.SetValueAndForceNotify(false);
        }

        #endregion
    }
}