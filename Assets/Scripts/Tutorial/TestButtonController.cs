﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Tunnel.Tutorial
{
    /// <summary>
    /// SCENE: Tutorial
    /// GAME_OBJECT: TestButton
    /// Testiranje raznih funkcionalnosti
    /// </summary>
    public class TestButtonController : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] private RectTransform _missionRectTransform;
        [SerializeField] private RectTransform _completedRectTransform;
        [SerializeField] private TMP_Text _ribbonText;

        private RectTransform _rectTransform;
        private Button _button;

        #endregion


        


        #region Monobehaviour Events

        private void OnEnable()
        {
            _rectTransform = GetComponent<RectTransform>();
            _button = GetComponent<Button>();
            
            int counter = 0;
            var missionRectTransformPositionX = _missionRectTransform.anchoredPosition.x;
            var completedRectTransformPositionX = _completedRectTransform.anchoredPosition.x;
            _button.OnClickAsObservable().Subscribe(unit =>
            {
                ++counter;
                switch (counter)
                {
                    case 1:
                        _ribbonText.text = LeanTweenType.clamp.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.clamp);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.clamp);
                        break;
                    case 2:
                        _ribbonText.text = LeanTweenType.linear.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.linear);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.linear);
                        break;
                    case 3:
                        _ribbonText.text = LeanTweenType.once.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.once);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.once);
                        break;
                    case 4:
                        _ribbonText.text = LeanTweenType.punch.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.punch);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.punch);
                        break;
                    case 5:
                        _ribbonText.text = LeanTweenType.animationCurve.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.animationCurve);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.animationCurve);
                        break;
                    case 6:
                        _ribbonText.text = LeanTweenType.easeShake.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeShake);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeShake);
                        break;
                    case 7:
                        _ribbonText.text = LeanTweenType.easeSpring.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeSpring);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeSpring);
                        break;
                    case 8:
                        _ribbonText.text = LeanTweenType.notUsed.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.notUsed);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.notUsed);
                        break;
                    case 9:
                        _ribbonText.text = LeanTweenType.pingPong.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.pingPong);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.pingPong);
                        break;
                    case 10:
                        _ribbonText.text = LeanTweenType.easeInBack.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInBack);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInBack);
                        break;
                    case 11:
                        _ribbonText.text = LeanTweenType.easeInBounce.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInBounce);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInBounce);
                        break;
                    case 12:
                        _ribbonText.text = LeanTweenType.easeInCirc.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInCirc);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInCirc);
                        break;
                    case 13:
                        _ribbonText.text = LeanTweenType.easeInCubic.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInCubic);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInCubic);
                        break;
                    case 14:
                        _ribbonText.text = LeanTweenType.easeInElastic.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInElastic);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInElastic);
                        break;
                    case 15:
                        _ribbonText.text = LeanTweenType.easeInExpo.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInExpo);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInExpo);
                        break;
                    case 16:
                        _ribbonText.text = LeanTweenType.easeInQuad.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInQuad);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInQuad);
                        break;
                    case 17:
                        _ribbonText.text = LeanTweenType.easeInQuart.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInQuart);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInQuart);
                        break;
                    case 18:
                        _ribbonText.text = LeanTweenType.easeInQuint.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInQuint);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInQuint);
                        break;
                    case 19:
                        _ribbonText.text = LeanTweenType.easeInSine.ToString();
                        LeanTween.moveX(_missionRectTransform, 0f, 0.5f).setFrom(missionRectTransformPositionX).setEase(LeanTweenType.easeInSine);
                        LeanTween.moveX(_completedRectTransform, 0f, 0.5f).setFrom(completedRectTransformPositionX).setEase(LeanTweenType.easeInSine);
                        break;
                    default:
                        counter = 1;
                        break;
                }
            }).AddTo(this);
        }

        #endregion
    }
}