﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using System.IO;
using Tunnel.Utilities;
using UnityEditor;

#endif

[Serializable]
public class Chapter
{
    [SerializeField] private List<LevelData> _levels;

    public List<LevelData> Levels
    {
        get => _levels;
        set => _levels = value;
    }
}

[CreateAssetMenu(fileName = "World Data", menuName = "ScriptableObjects/Levels/World Data", order = 0)]
public class WorldData : ScriptableObject
{
    [SerializeField] private Material _skybox;
    [SerializeField] private int _numberOfSidesInTube;
    [SerializeField] private int _distanceBetweenPoints;
    [SerializeField] private int _numberOfPointsInFirstLevel;
    [SerializeField] private int _numberOfNewPointsToAddPerLevel;
    [SerializeField] private int _minDistanceBetweenObstacles;
    [SerializeField] private int _maxDistanceBetweenObstacles;
    
    [SerializeField] private List<Chapter> _chapters;

    public Material Skybox
    {
        get => _skybox;
        set => _skybox = value;
    }

    public int NumberOfSidesInTube
    {
        get => _numberOfSidesInTube;
        set => _numberOfSidesInTube = value;
    }

    public int DistanceBetweenPoints
    {
        get => _distanceBetweenPoints;
        set => _distanceBetweenPoints = value;
    }

    public int NumberOfPointsInFirstLevel
    {
        get => _numberOfPointsInFirstLevel;
        set => _numberOfPointsInFirstLevel = value;
    }

    public int NumberOfNewPointsToAddPerLevel
    {
        get => _numberOfNewPointsToAddPerLevel;
        set => _numberOfNewPointsToAddPerLevel = value;
    }

    public int MinDistanceBetweenObstacles
    {
        get => _minDistanceBetweenObstacles;
        set => _minDistanceBetweenObstacles = value;
    }

    public int MaxDistanceBetweenObstacles
    {
        get => _maxDistanceBetweenObstacles;
        set => _maxDistanceBetweenObstacles = value;
    }

    public List<Chapter> Chapters
    {
        get => _chapters;
        set => _chapters = value;
    }

    public List<LevelData> Levels
    {
        get { return _chapters.SelectMany(x => x.Levels).ToList(); }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        var path = AssetDatabase.GetAssetPath(this);
        path = Path.GetDirectoryName(path);
        var allLevelsSOinWorld = AssetDatabase.FindAssets(Constants.Editor.Filters.LEVEL_DATA, new[] {path});
        var levelsToAdd = allLevelsSOinWorld
            .Select(s => AssetDatabase.LoadAssetAtPath<LevelData>(AssetDatabase.GUIDToAssetPath(s)))
            .ToArray();

        var numberOfLevels = levelsToAdd.Length;
        var numberOfChapters = _chapters.Count;
        // ispitujemo ukoliko neko namerno ukuca 0 chaptera u 1 svetu
        // to je nemoguce i onda je najmanji moguci broj chaptera 1
        if (numberOfChapters == 0)
        {
            _chapters = new List<Chapter>(Enumerable.Range(0, 1).Select(_ => new Chapter()));
        }
        // zatim proveravamo ukoliko neko namerano ukuca veci broj chaptera nego broj levela
        // to je nemoguce i onda je najveci broj chaptera onoliko koliko ima levela
        else if (numberOfChapters > numberOfLevels)
        {
            _chapters = new List<Chapter>(Enumerable.Range(0, numberOfLevels).Select(_ => new Chapter()));
        }
    }
#endif
}