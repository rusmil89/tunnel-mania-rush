﻿using System;
using System.Collections.Generic;
using Dreamteck.Splines;
using UnityEngine;

[Serializable]
public class ObstacleData
{
    public float positionOnPath;
    public Vector3 rotationOffset;
    public Vector2 positionOffset;
    public float speed;
    public float delay;
    public ObstacleTypeSO obstacleType;

    public ObstacleData(float positionOnPath, Vector3 rotationOffset, Vector2 positionOffset, float speed, float delay, ObstacleTypeSO obstacleType)
    {
        this.positionOnPath = positionOnPath;
        this.rotationOffset = rotationOffset;
        this.positionOffset = positionOffset;
        this.speed = speed;
        this.delay = delay;
        this.obstacleType = obstacleType;
    }
}

[CreateAssetMenu(fileName = "Level Data", menuName = "ScriptableObjects/Levels/Level Data", order = 0)]
public class LevelData : ScriptableObject
{
    [Header("Tube")]
    public SplinePoint[] tubePoints;
    public Texture2D tubeTexture;
    public Material tubeMaterial;
    
    [Header("Lights")]
    public Color tubeAndObstacleInsideLightColor;
    public Color tubeAndObstacleOutsideLightColor;

    [Header("Obstacles")] 
    public Material obstacleMaterial;
    public List<ObstacleData> insideObstaclesData = new List<ObstacleData>();
    public List<ObstacleData> outsideObstaclesData = new List<ObstacleData>();

    [Header("Environment VFX")]
    [SerializeField] private GameObject[] _insideVFXObjects;
    [SerializeField] private GameObject[] _outsideVFXObjects;




    #region Properties

    public GameObject[] InsideVfxObjects => _insideVFXObjects;
    public GameObject[] OutsideVfxObjects => _outsideVFXObjects;

    #endregion
}