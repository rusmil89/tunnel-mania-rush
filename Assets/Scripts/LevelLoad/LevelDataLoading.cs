﻿using System.Collections.Generic;
using System.Linq;
using Core.Events;
using Dreamteck.Splines;
using Tunnel.Controllers;
using Tunnel.DynamicObstacles;
using Tunnel.UI;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.LevelLoad
{
    
    public sealed class LevelStarted {}
    
    //TODO: prepraviti klasicno instanciranje prepreka u paljenje i gasenje
    //TODO: cuvati progress u google game services
    //TODO: dodati zvuk kada se ostvari nagrada na kraju levela u 1 chapteru
    /// <summary>
    /// SCENE: Level
    /// GAME OBJECT: LevelLoader
    /// DESCRIPTION: Klasa koja kontrolise ucitavanje levela
    /// </summary>
    public sealed class LevelDataLoading : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scene Objects")] 
        [SerializeField] private GameObject _tubeGameObject;

        [Header("Lights")] 
        [SerializeField] private Light _tubeLight;
        [SerializeField] private Light _obstacleLight;
        
        [Header("Portal Elements")]
        [SerializeField] private Transform _portalGate;
        [SerializeField] private ParticleSystem _particleSystemOne;
        [SerializeField] private ParticleSystem _particleSystemTwo;

        [Header("Obstacles")]
        [SerializeField] private Transform _insideParent;
        [SerializeField] private Transform _outsideParent;
        [SerializeField] private ObstaclesHolder _insideObstaclesHolder;
        [SerializeField] private ObstaclesHolder _outsideObstaclesHolder;
        [SerializeField] private Obstacle[] _insideObstaclesPool;
        [SerializeField] private Obstacle[] _outsideObstaclesPool;

        [Header("Scriptable Objects")] 
        [SerializeField] private global::PlayerData _playerDataController;
        [SerializeField] private ObstaclesHolderSO _insideObstacles;
        [SerializeField] private ObstaclesHolderSO _outsideObstacles;

        [Header("Levels Data")] 
        [SerializeField] private WorldData[] _allWorldData;
        [SerializeField] private LevelData[] _allLevelsData;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private bool _isPlayerReachedTheEndOfLevel = default;
        private SplineComputer _splineComputer;
        private MeshRenderer _tubeMeshRenderer;
        private TubeGenerator _tubeGenerator;
        private ObstaclesHolderSO _currentObstaclesHolder;
        private WorldData _currentWorldData;
        private LevelData _currentLevelData;
        private List<GameObject> _instantiatedVFXObjects = new List<GameObject>();
        
        /// <summary>
        /// Consts
        /// </summary>
        private readonly Vector3 PORTAL_OUTSIDE_SCALE = new Vector3(2.5f, 2.5f, 2.5f);
        private readonly ParticleSystem.MinMaxCurve PARTICLE_SYSTEM_1_INSIDE_START_SIZE = new ParticleSystem.MinMaxCurve(7f);
        private readonly ParticleSystem.MinMaxCurve PARTICLE_SYSTEM_2_INSIDE_START_SIZE = new ParticleSystem.MinMaxCurve(8f);
        private readonly ParticleSystem.MinMaxCurve PARTICLE_SYSTEM_1_OUTSIDE_START_SIZE = new ParticleSystem.MinMaxCurve(10f);
        private readonly ParticleSystem.MinMaxCurve PARTICLE_SYSTEM_2_OUTSIDE_START_SIZE = new ParticleSystem.MinMaxCurve(11f);

        #endregion





        #region Properties

        public WorldData CurrentWorldData => _currentWorldData;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
            bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
            bool prefabConnected = UnityEditor.PrefabUtility.GetPrefabInstanceStatus(gameObject) == UnityEditor.PrefabInstanceStatus.Connected;
            if (!isValidPrefabStage && prefabConnected)
            {
                ReferencesClient.CheckReferences(this, gameObject);
            }

            var insideObstaclesPoolGUIDs = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new[] {Constants.Editor.Paths.PREFAB_INSIDE_OBSTACLES});
            _insideObstaclesPool = insideObstaclesPoolGUIDs
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Obstacle>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToArray();
            
            var outsideObstaclePoolGUIDs = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PREFAB, new[] {Constants.Editor.Paths.PREFAB_OUTSIDE_OBSTACLES});
            _outsideObstaclesPool = outsideObstaclePoolGUIDs
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Obstacle>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToArray();
            
            var allWorldDataGUIDs = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.WORLD_DATA, new[] {Constants.Editor.Paths.SCRIPTABLE_OBJECTS});
            _allWorldData = allWorldDataGUIDs
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<WorldData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .Where(s => !s.name.Contains("Tutorial"))
                .ToArray();

            _allLevelsData = _allWorldData
                .SelectMany(s => s.Levels)
                .ToArray();
        }
#endif

        private void Awake()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            _playerDataController.PersistentData.CurrentLevel.Value = 1;
#endif
            GetRequiredComponents();
            LoadLevel();
        }

        private void GetRequiredComponents()
        {
            _splineComputer = _tubeGameObject.GetComponent<SplineComputer>();
            _tubeMeshRenderer = _tubeGameObject.GetComponent<MeshRenderer>();
            _tubeGenerator = _tubeGameObject.GetComponent<TubeGenerator>();
            
            UnityEngine.Assertions.Assert.IsNotNull(_splineComputer);
            UnityEngine.Assertions.Assert.IsNotNull(_tubeMeshRenderer);
            UnityEngine.Assertions.Assert.IsNotNull(_tubeGenerator);
        }

        private void Start()
        {
            RegisterObservableListeners();
            SetUpLights(_currentLevelData.tubeAndObstacleInsideLightColor);
            SetUpPortalEffect(Vector3.one, PARTICLE_SYSTEM_1_INSIDE_START_SIZE, PARTICLE_SYSTEM_2_INSIDE_START_SIZE);
            SetObstaclesForLevel(_insideObstacles, _outsideObstacles, true);
            SetUpEnvironmentVFX(_currentLevelData.InsideVfxObjects);
            MessageBroker.Default.Publish(new LevelStarted());
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<GamePaused>()
                .Subscribe(HandleGamePausedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfTube>()
                .TakeEvery(2)
                .Subscribe(HandlePlayerReachedTheEndOfTubeEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfLevel>()
                .Subscribe(HandlePlayerReachedEndOfLevelEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<FinishMenuContinueButtonClicked>()
                .Subscribe(HandlePlayerClickedContinueButtonInFinishMenuEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuClaimButtonClicked>()
                .Subscribe(HandleChapterClearedMenuClaimButtonClickedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuDoubleRewardButtonClicked>()
                .Subscribe(HandleChapterClearedMenuDoubleRewardButtonClicked)
                .AddTo(this);
        }

        #endregion





        #region Custom Methods

        /// <summary>
        /// Metoda koja se poziva kada se okine dogadjaj da je igrac stigao do kraja tunela
        /// WHY: Zato sto tada mora da se podesi novo okruzenje s obzirom da igrac prelazi spolja
        /// </summary>
        /// <param name="playerReachedEndOfTube"></param>
        private void HandlePlayerReachedTheEndOfTubeEvent(PlayerReachedEndOfTube playerReachedEndOfTube)
        {
            SetUpLights(_currentLevelData.tubeAndObstacleOutsideLightColor);
            SetUpPortalEffect(PORTAL_OUTSIDE_SCALE, PARTICLE_SYSTEM_1_OUTSIDE_START_SIZE, PARTICLE_SYSTEM_2_OUTSIDE_START_SIZE);
            SetObstaclesForLevel(_outsideObstacles, _insideObstacles, false);
            SetUpEnvironmentVFX(_currentLevelData.OutsideVfxObjects);
        }

        /// <summary>
        /// Metoda koja osluskuje kada je igrac stigao do kraja levela
        /// WHY: Zato sto tada mora da se podesi novo okruzenje
        /// </summary>
        /// <param name="playerReachedEndOfLevel"></param>
        private void HandlePlayerReachedEndOfLevelEvent(PlayerReachedEndOfLevel playerReachedEndOfLevel)
        {
            _isPlayerReachedTheEndOfLevel = true;
            _tubeGameObject.SetActive(false);
            ClearCurrentObstacles(_outsideObstaclesHolder, _outsideObstaclesPool);
        }

        /// <summary>
        /// Metoda koja osluskuje dogadjaj kada igrac klikne na continue dugme u finis meniju i tada se poziva
        /// WHY: Zato sto tek onda treba podesiti okruzenje za igraca
        /// </summary>
        /// <param name="finishMenuContinueButtonClicked"></param>
        private void HandlePlayerClickedContinueButtonInFinishMenuEvent(FinishMenuContinueButtonClicked finishMenuContinueButtonClicked)
        {
            SetParametersForNewLevel();
            LoadLevel();
            SetUpLights(_currentLevelData.tubeAndObstacleInsideLightColor);
            SetUpPortalEffect(Vector3.one, PARTICLE_SYSTEM_1_INSIDE_START_SIZE, PARTICLE_SYSTEM_2_INSIDE_START_SIZE);
            SetObstaclesForLevel(_insideObstacles, _outsideObstacles, true);
            SetUpEnvironmentVFX(_currentLevelData.InsideVfxObjects);
        }

        private void HandleChapterClearedMenuClaimButtonClickedEvent(ChapterClearedMenuClaimButtonClicked chapterClearedMenuClaimButtonClicked)
        {
            SetParametersForNewChapter();
        }

        private void HandleChapterClearedMenuDoubleRewardButtonClicked(ChapterClearedMenuDoubleRewardButtonClicked chapterClearedMenuDoubleRewardButtonClicked)
        {
            SetParametersForNewChapter();
        }

        /// <summary>
        /// Metoda koja podesava parametre kao sto su trenutni level i progress koji je napravio igrac kada je stigao do kraja tunela
        /// WHY: Zato sto nam je bitno da pratimo te parametre za svakog igraca posebno
        /// </summary>
        private void SetParametersForNewLevel()
        {
            //NOTE: broj levela povecavamo svaki put kada korisnik stigne do kraja levela i ako je broj trenutnog levela manji od broja levela u trenutnom chapteru, u suprotnom je trenutni level 1 jer se resetuje nakon prelaska u sledeci chapter  
            var currentWorld = _playerDataController.PersistentData.CurrentWorld.Value;
            var currentLevel = _playerDataController.PersistentData.CurrentLevel.Value;
            var chapterInWorld = _playerDataController.PersistentData.CurrentChapterInWorld;
            _playerDataController.PersistentData.CurrentLevel.Value =
                currentLevel <
                _allWorldData[currentWorld - 1].Chapters[chapterInWorld[currentWorld] - 1].Levels.Count
                    ? ++currentLevel
                    : 1;
        }

        /// <summary>
        /// Metoda koja podesava parametre za trenutni chapter i trenutni svet kada igrac stigne do kraja chaptera
        /// WHY: Zato sto je bitno za progres igraca
        /// </summary>
        private void SetParametersForNewChapter()
        {
            var currentWorld = _playerDataController.PersistentData.CurrentWorld.Value;
            var currentChapterInWorld = _playerDataController.PersistentData.CurrentChapterInWorld;
            var chaptersProgress = _playerDataController.PersistentData.ChaptersProgress;
            var previousWorldNumber = currentWorld;
            //NOTE: broj sveta se povecava svaki put kada korisnik predje sve levele u svim chapterima, u suprotnom ostaje nepromenjen
            _playerDataController.PersistentData.CurrentWorld.Value =
                currentWorld < _allWorldData.Length &&
                currentChapterInWorld[currentWorld] == _allWorldData[currentWorld - 1].Chapters.Count
                    ? ++currentWorld
                    : currentWorld;

            if (currentWorld != previousWorldNumber)
            {
                _playerDataController.PersistentData.WorldProgress[currentWorld - 1] = PlayerData.PersistentData.WorldState.Unlocked;
                currentChapterInWorld[currentWorld] = _playerDataController.PersistentData.CurrentLevel.Value = 1;
                chaptersProgress[currentWorld][currentChapterInWorld[currentWorld] - 1] = PlayerData.PersistentData.ChapterState.IsReadyForUnlock;
            }
            else
            {
                //NOTE: broj chaptera povecavamo svaki put kada korisnik stigne do kraja svih levela u jednom chapteru i ako postoji jos chaptera u celom svetu, u suprotnom chapter ostaje nepromenjen
                currentChapterInWorld[currentWorld] =
                    currentChapterInWorld[currentWorld] <
                    _allWorldData[currentWorld - 1].Chapters.Count
                        ? ++currentChapterInWorld[currentWorld]
                        : currentChapterInWorld[currentWorld];

                chaptersProgress[currentWorld][currentChapterInWorld[currentWorld] - 1] =
                    chaptersProgress[currentWorld][currentChapterInWorld[currentWorld] - 1] == PlayerData.PersistentData.ChapterState.Locked
                        ? PlayerData.PersistentData.ChapterState.IsReadyForUnlock
                        : PlayerData.PersistentData.ChapterState.Unlocked;
                _playerDataController.PersistentData.CurrentLevel.Value = 1;
            }
        }

        /// <summary>
        /// Metoda koja podesava boju svetla na tunelu i preprekama
        /// WHY: Zato sto se boja svetala za tunel i prepreke menja od levela do levela da bi korisnik video raznorsnost
        /// </summary>
        /// <param name="newColor"></param>
        private void SetUpLights(Color newColor)
        {
            _tubeLight.color = newColor;
            _obstacleLight.color = _tubeLight.color.GetComplementary();
        }

        /// <summary>
        /// Metoda koja podesava odgovarajuce partikle koji imitiraju portal
        /// WHY: Zato sto taj portal treba da se vidi i kada je igrac unutar levela i kada je spolja
        /// </summary>
        private void SetUpPortalEffect(Vector3 portalScale, ParticleSystem.MinMaxCurve particle1StartSize, ParticleSystem.MinMaxCurve particle2StartSize)
        {
            _portalGate.localScale = portalScale;
            var mainModule = _particleSystemOne.main;
            mainModule.startSize = particle1StartSize;
            mainModule = _particleSystemTwo.main;
            mainModule.startSize = particle2StartSize;
        }

        /// <summary>
        /// Metoda koja kontrolise prepreke za level
        /// WHY: Sta ako to treba da radi ona klasa koja podesava okruzenje???
        /// </summary>
        private void SetObstaclesForLevel(ObstaclesHolderSO obstaclesToShow, ObstaclesHolderSO obstaclesToHide, bool isFaceFlipped)
        {
            if (obstaclesToShow != null && obstaclesToHide != null)
            {
                _tubeGenerator.flipFaces = isFaceFlipped;
                _currentObstaclesHolder = obstaclesToShow;
                obstaclesToShow.Initialize();
                obstaclesToHide.Hide();
            }
        }

        /// <summary>
        /// Metoda koja postavlja neke dodatne elemente na sceni kao što su partikli ili razni prefabovi
        /// Povećati game feel kod igrača i izazvati neku emociju kod njega
        /// </summary>
        /// <param name="vfxObjects">Niz objekata koje treba postaviti na sceni</param>
        private void SetUpEnvironmentVFX(GameObject[] vfxObjects)
        {
            if (vfxObjects != null && vfxObjects.Length > 0)
            {
                foreach (var vfxObject in vfxObjects)
                {
                    GameObject instantiatedVFXObject = Instantiate(vfxObject, _currentLevelData.tubePoints[Random.Range(2, _currentLevelData.tubePoints.Length)].position, Quaternion.identity,
                        _tubeGenerator.transform);
                    _instantiatedVFXObjects.Add(instantiatedVFXObject);
                }
            }
        }

        /// <summary>
        /// Metoda kojia ucitava novi level
        /// WHY: Zato sto kada igrac zavrsi prethodni level koji se sadrzan celokupno u jednom skriptabilnom objektu, potrebno je ucitati novi SO
        /// </summary>
        private void LoadLevel()
        { 
            _isPlayerReachedTheEndOfLevel = false;
            _currentWorldData = _allWorldData[_playerDataController.PersistentData.CurrentWorld.Value - 1];
            _currentLevelData = _currentWorldData.Chapters[_playerDataController.PersistentData.CurrentChapterInWorld[_playerDataController.PersistentData.CurrentWorld.Value] - 1].Levels[_playerDataController.PersistentData.CurrentLevel.Value - 1];

            if (_insideObstaclesHolder.obstacles != null && _insideObstaclesHolder.obstacles.Length > 0)
            {
                ClearCurrentObstacles(_insideObstaclesHolder, _insideObstaclesPool);
            }

            if (_outsideObstaclesHolder.obstacles != null && _outsideObstaclesHolder.obstacles.Length > 0)
            {
                ClearCurrentObstacles(_outsideObstaclesHolder, _outsideObstaclesPool);
            }

            if (_playerDataController.PersistentData.CurrentLevel.Value == 1)
            {
                SetUpSkybox();
            }

            if (_instantiatedVFXObjects != null && _instantiatedVFXObjects.Count > 0)
            {
                ClearVFXObjects();
            }
            
            SetUpTube();
            InstantiateObstacles(_insideParent, _insideObstaclesHolder, _currentLevelData.insideObstaclesData, _insideObstaclesPool);
            InstantiateObstacles(_outsideParent, _outsideObstaclesHolder, _currentLevelData.outsideObstaclesData, _outsideObstaclesPool);
            MessageBroker.Default.Publish(new LevelStarted());

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            Analytics.WarmAppGamesAnalytics.LogLevelStartEvent(_playerDataController.PersistentData.CurrentWorld.Value, _playerDataController.PersistentData.CurrentChapterInWorld[_playerDataController.PersistentData.CurrentWorld.Value], _playerDataController.PersistentData.CurrentLevel.Value);
#endif
        }

        /// <summary>
        /// Metoda koja postavlja skybox materijal za svaki svet
        /// WHY: Zato sto zelimo da pokazemo igracu da se nesto menja i da napreduje nekako
        /// </summary>
        private void SetUpSkybox()
        {
            RenderSettings.skybox = CurrentWorldData.Skybox;
        }

        /// <summary>
        /// Metoda koja podesava izgled tunela na osnovu podataka iz skriptabilnog objekta
        /// WHY: Zato sto je to neophodno da bi igra funkcionisala
        /// </summary>
        private void SetUpTube()
        {
            _tubeGameObject.SetActive(true);
            _splineComputer.SetPoints(_currentLevelData.tubePoints);
            _tubeMeshRenderer.sharedMaterial = _currentLevelData.tubeMaterial;
            _tubeMeshRenderer.sharedMaterial.mainTexture = _currentLevelData.tubeTexture;
        }

        /// <summary>
        /// Metoda koja instancira sve prepreke (i unutrasnje i spoljasnje) za 1 level
        /// WHY: Zato sto je to neophpdno da bi se level pravilno ucitao i da bi ispunjavao zahteve igre 
        /// </summary>
        /// <param name="parent">Objekat na sceni pod kojim se nalaze sve prepreke</param>
        /// <param name="obstaclesHolder">Skripta koja cuva reference na sve prepreke</param>
        /// <param name="obstaclesData">Podaci o preprekama</param>
        /// <param name="obstaclesPool">Svi tipovi prepreka</param>
        private void InstantiateObstacles(Transform parent, ObstaclesHolder obstaclesHolder, List<ObstacleData> obstaclesData, Obstacle[] obstaclesPool)
        {
            obstaclesHolder.obstacles = new Obstacle[obstaclesData.Count];
            for (int i = 0; i < obstaclesData.Count; i++)
            {
                for (int j = 0; j < obstaclesPool.Length; j++)
                {
                    if (obstaclesData[i].obstacleType != obstaclesPool[j].obstacleType) continue;
                    Obstacle obstacle = Instantiate(obstaclesPool[j], parent, false);
                    obstacle.delay = obstaclesData[i].delay;
                    obstacle.speed = obstaclesData[i].speed;
                    obstacle.splineFollower.computer = null;
                    obstacle.splineFollower.computer = _splineComputer;
                    obstacle.splineFollower.SetPercent(obstaclesData[i].positionOnPath);
                    obstacle.splineFollower.motion.rotationOffset = obstaclesData[i].rotationOffset;
                    obstacle.splineFollower.motion.offset = obstaclesData[i].positionOffset;
                    foreach (var meshRenderer in obstacle.GetComponentsInChildren<MeshRenderer>())
                    {
                        meshRenderer.sharedMaterial = _currentLevelData.obstacleMaterial;
                    }

                    obstaclesHolder.obstacles[i] = obstacle;
                }
            }
        }

        /// <summary>
        /// Metoda koja brise sve prepreke
        /// WHY: Zato sto je pre ucitavanja novog levela sa preprekama potrebno ocistiti prethodni level
        /// </summary>
        /// <param name="obstaclesHolder">Skripta koja sadrzi sve prepreke</param>
        /// <param name="obstaclesPool">Svi tipovi prepreka</param>
        private void ClearCurrentObstacles(ObstaclesHolder obstaclesHolder, Obstacle[] obstaclesPool)
        {
            if (obstaclesHolder.obstacles != null && obstaclesHolder.obstacles.Length > 0)
            {
                for (int i = 0; i < obstaclesHolder.obstacles.Length; i++)
                {
                    for (int j = 0; j < obstaclesPool.Length; j++)
                    {
                        if (obstaclesHolder.obstacles[i].obstacleType == obstaclesPool[j].obstacleType)
                        {
                            Destroy(obstaclesHolder.obstacles[i].gameObject);
                        }
                    }
                }

                obstaclesHolder.obstacles = null;
            }
        }
        
        /// <summary>
        /// Metoda koja brise sve VFX objekte sa scene
        /// WHY: Zato sto scena mora da se ocisti pre nego se ucita naredna scena i odgovarajuci VFX objekti za nju
        /// </summary>
        private void ClearVFXObjects()
        {
            foreach (var vfxObject in _instantiatedVFXObjects)
            {
                Destroy(vfxObject);
            }
            _instantiatedVFXObjects.Clear();
        }

        #endregion





        #region Event Listeners

        private void HandleGamePausedEvent(GamePaused gameEvent)
        {
            if (!_isPlayerReachedTheEndOfLevel)
            {
                _currentObstaclesHolder.TooglePause(gameEvent.IsGamePaused);
            }
        }

        #endregion
    }
}