﻿using System;
using System.Collections;
using Tunnel.UI;
using UniRx;
using UnityEngine;

namespace Tunnel.Animations
{
    public sealed class ProgressAnimationFinished {}
    
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Klasa koja kontrolise animacije koje se odvijaju u Finish meniju. Poziva se iz finish menija
    /// </summary>
    public sealed class FinishMenuAnimations
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly FinishMenu _finishMenu;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private bool _isAnimationFinished;

        /// <summary>
        /// Consts
        /// </summary>
        private const float PROGRESS_ANIMATION_TIME = 3f;
        private const float PRESENT_BOX_INTERVAL = 1.5f;

        #endregion


        #region Custom Methods

        /// <summary>
        /// Kontruktor objekta
        /// WHY: Neophodan da bismo mogli da inicijalizujemo objekat i da pozivamo njegove metode i kontrolisemo njegovo ponasanje
        /// </summary>
        /// <param name="finishMenu"></param>
        public FinishMenuAnimations(FinishMenu finishMenu)
        {
            _finishMenu = finishMenu;
        }

        /// <summary>
        /// Metoda koja se koristi za animaciju progresa igraca kada se pojavi finish meni
        /// WHY: Zato sto zelimo da ulepsamo igracu dozivaljaj da je uspeo da uradi nesto jako bitno i da ga za to adekvatno nagradimo
        /// </summary>
        public IEnumerator AnimateTheProgress()
        {
            _isAnimationFinished = false;
            LeanTween.value(_finishMenu.gameObject, UpdateSliderValue, _finishMenu.ProgressSlider.value, _finishMenu.Progress, PROGRESS_ANIMATION_TIME)
                .setOnComplete(() => _isAnimationFinished = true);
            AnimateThePresentBox();
            
            yield return new WaitUntil(() => _isAnimationFinished);
        }

        private void UpdateSliderValue(float sliderValue)
        {
            _finishMenu.ProgressSlider.value = sliderValue;
            _finishMenu.PercentageText.text = sliderValue.ToString("P0");   
        }

        #endregion

        /// <summary>
        /// Metoda koja animira ikonicu poklona
        /// WHY: Da bi na taj nacin privukli vise paznju igracu da ga ocekuje neka nagrada i da treba da nastavi da igra
        /// </summary>
        private void AnimateThePresentBox()
        {
            var sequence = LeanTween.sequence();
            var dueTime = TimeSpan.FromSeconds(0f);
            var period = TimeSpan.FromSeconds(PRESENT_BOX_INTERVAL);
            Observable.Timer(dueTime, period)
                .Subscribe(_ => AnimatePresentBoxInIntervals(sequence))
                .AddTo(_compositeDisposable);
        }

        private void AnimatePresentBoxInIntervals(LTSeq sequence)
        {
            sequence.append(LeanTween.rotateZ(_finishMenu.RewardImage.gameObject, -15f, 0.05f).setLoopPingPong(3));
            sequence.append(LeanTween.rotateZ(_finishMenu.RewardImage.gameObject, 15f, 0.05f).setLoopPingPong(3));
        }

        /// <summary>
        /// Metoda koja prekida animiranje observable stream-a kada broj stitova postane > 0, ili kada se potpuno izadje iz Level scene
        /// WHY: Zato sto je najbolje da ta metoda bude zasebna u jednoj klasi koja radi samo 1 stvar (SRP)
        /// </summary>
        public void ClearCompositeDisposables()
        {
            _compositeDisposable.Clear();
        }
    }
}