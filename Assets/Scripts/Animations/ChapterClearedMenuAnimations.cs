﻿using System;
using System.Collections;
using Core.Events;
using Tunnel.UI;
using UniRx;
using UnityEngine;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Klasa koja kontrolise animacije u chapter cleared meniju. Poziva se iz tog menija
    /// </summary>
    public sealed class ChapterClearedMenuAnimations
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly ChapterClearedMenu _chapterClearedMenu;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly ChapterClearedMenuPresentRotated _chapterClearedMenuPresentRotated = new ChapterClearedMenuPresentRotated();
        private LTSeq _sequence;
        private bool _isAnimationFinished;

        /// <summary>
        /// Consts
        /// </summary>
        private const float CHAPTER_CLEARED_ANIMATION_DURATION = 1f;
        private const float SPIN_THE_WHEEL_ENABLING_ANIMATION_DURATION = 0.5f;
        private const float SPINNING_ANIMATION_DURATION = 5f;
        private const float FLASH_ANIMATION_DURATION = 0.2f;
        private const float PRESENT_BOX_Z_ANGLE = 15f;
        private const float PRESENT_BOX_ANIMATION_PERIOD = 1.5f;
        private const float PRESENT_BOX_ROTATION_DURATION = 0.15f;

        #endregion





        #region Custom Methods

        /// <summary>
        /// Kontruktor objekta
        /// WHY: Neophodan da bismo mogli da inicijalizujemo objekat i da pozivamo njegove metode i kontrolisemo njegovo ponasanje
        /// </summary>
        /// <param name="chapterClearedMenu"></param>
        public ChapterClearedMenuAnimations(ChapterClearedMenu chapterClearedMenu)
        {
            _chapterClearedMenu = chapterClearedMenu;
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju svih elemenata koji pokazuju korisniku da je presao odredjeni chapter
        /// WHY: Dobijamo efekat bljeska
        /// </summary>
        public void AnimateChapterClearedElements()
        {
            AnimateFlash();
            AnimateClearedText();
            AnimateChapterText();
            AnimateChapterNumberImage();
            AnimateChapterNumberText();
            AnimateThePresentBox();
        }

        private void AnimateFlash()
        {
            LeanTween.scale(_chapterClearedMenu.FlashImage.gameObject, _chapterClearedMenu.FlashImageScale, FLASH_ANIMATION_DURATION)
                .setFrom(Vector3.zero)
                .setOnComplete(() =>
                {
                    LeanTween.scale(_chapterClearedMenu.FlashImage.gameObject, Vector3.zero, CHAPTER_CLEARED_ANIMATION_DURATION)
                        .setFrom(_chapterClearedMenu.FlashImageScale);
                });
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju texta CLEARED
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        private void AnimateClearedText()
        {
            LeanTween.alpha(_chapterClearedMenu.ClearedText.gameObject, 1f, CHAPTER_CLEARED_ANIMATION_DURATION)
                .setFrom(0f);
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju texta CHAPTER
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        private void AnimateChapterText()
        {
            LeanTween.rotateAroundLocal(_chapterClearedMenu.ChapterText.gameObject, Vector3.forward, 0f, CHAPTER_CLEARED_ANIMATION_DURATION)
                .setFrom(Vector3.zero)
                .setRepeat(3);
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju pozadine koja se nalazi iza broja chaptera
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        private void AnimateChapterNumberImage()
        {
            LeanTween.value(_chapterClearedMenu.ChapterNumberBackgroundImage.gameObject, value => _chapterClearedMenu.ChapterNumberBackgroundImage.fillAmount = value, 0f, 1f, CHAPTER_CLEARED_ANIMATION_DURATION)
                .setFrom(0f);
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju broja chaptera
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        private void AnimateChapterNumberText()
        {
            LeanTween.alpha(_chapterClearedMenu.ChapterNumberText.gameObject, 1f, CHAPTER_CLEARED_ANIMATION_DURATION)
                .setFrom(0f);
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju slike poklona
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        private void AnimateThePresentBox()
        {
            _sequence = LeanTween.sequence();
            _sequence.append(LeanTween.scale(_chapterClearedMenu.RewardButton.gameObject, Vector3.one * PRESENT_BOX_ANIMATION_PERIOD, CHAPTER_CLEARED_ANIMATION_DURATION)
                .setFrom(Vector3.zero)
                .setEase(LeanTweenType.easeOutBounce));
            var dueTime = TimeSpan.FromSeconds(0f);
            var period = TimeSpan.FromSeconds(PRESENT_BOX_ANIMATION_PERIOD);
            Observable.Timer(dueTime, period)
                .Subscribe(RotatePresentBox)
                .AddTo(_compositeDisposable);
        }

        private void RotatePresentBox(long l)
        {
            _sequence.append(LeanTween.rotateZ(_chapterClearedMenu.RewardButton.gameObject, -PRESENT_BOX_Z_ANGLE, PRESENT_BOX_ROTATION_DURATION)
                .setLoopPingPong(3)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(() => MessageBroker.Default.Publish(_chapterClearedMenuPresentRotated)));
            _sequence.append(LeanTween.rotateZ(_chapterClearedMenu.RewardButton.gameObject, PRESENT_BOX_Z_ANGLE, PRESENT_BOX_ROTATION_DURATION)
                .setLoopPingPong(3)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(() => MessageBroker.Default.Publish(_chapterClearedMenuPresentRotated)));
        }

        public void AnimateDisablingThePresentBox()
        {
            LeanTween.scale(_chapterClearedMenu.RewardButton.gameObject, Vector3.zero, SPIN_THE_WHEEL_ENABLING_ANIMATION_DURATION)
                .setOnComplete(() =>
                {
                    _chapterClearedMenu.RewardButton.enabled = false;
                    _chapterClearedMenu.RewardImage.enabled = false;
                });
        }

        public void AnimateEnablingSpinTheWheel()
        {
            MessageBroker.Default.Publish(new AnimateEnablingSpinTheWheel());
            Vector3 bonusWheelScale = new Vector3(0.8f, 0.8f, 0.8f);
            LeanTween.scale(_chapterClearedMenu.WheelGameObject, bonusWheelScale, SPIN_THE_WHEEL_ENABLING_ANIMATION_DURATION)
                .setFrom(Vector3.zero);
            LeanTween.scale(_chapterClearedMenu.PointerGameObject, Vector3.one, SPIN_THE_WHEEL_ENABLING_ANIMATION_DURATION)
                .setFrom(Vector3.zero)
                .setOnComplete(() => _chapterClearedMenu.StartSpinningButton.gameObject.SetActive(true));
        }

        /// <summary>
        /// Metoda koja kontrolise animaciju okretanja tocka koji daje nagrade (spin the wheel)
        /// WHY: Zato sto zelimo da ima lepsi efekat i poboljsamo game feel
        /// </summary>
        public IEnumerator AnimateSpinningTheWheel()
        {
            _isAnimationFinished = false;
            _chapterClearedMenu.WheelGameObject.transform.localRotation = Quaternion.identity;
            const float startAngle = 1800f;
            LeanTween.rotateZ(_chapterClearedMenu.WheelGameObject, _chapterClearedMenu.RandomSpinAngle, SPINNING_ANIMATION_DURATION)
                .setFrom(startAngle)
                .setEase(LeanTweenType.easeOutCirc)
                .setOnComplete(() => _isAnimationFinished = true);
            
            yield return new WaitUntil(() => _isAnimationFinished);
        }

        /// <summary>
        /// Metoda koja prekida animiranje observable stream-a kada broj stitova postane > 0, ili kada se potpuno izadje iz Level scene
        /// WHY: Zato sto je najbolje da ta metoda bude zasebna u jednoj klasi koja radi samo 1 stvar (SRP)
        /// </summary>
        public void DisposeAnimations()
        {
            _compositeDisposable.Clear();
        }

        #endregion
    }
}
