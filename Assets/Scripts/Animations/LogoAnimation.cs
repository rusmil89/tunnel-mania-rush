﻿using Core.Events;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Tunnel.Utilities;
using UniRx;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: Splash
    /// GAME_OBJECT: Logo
    /// Klasa koja kontrolise animaciju WarmApp Games logoa na početku igre
    /// </summary>
    public sealed class LogoAnimation : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private global::PlayerData _playerData;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Image _logoImage;
        private RectTransform _rectTransform;
        private readonly Vector3 _initialLogoScale = new Vector3(0.9f, 0.9f, 0.9f);
        private readonly Color _initialLogoColor = new Color(1f, 1f, 1f, 0f);

        /// <summary>
        /// Consts
        /// </summary>
        private const float INTRO_ANIMATION_DURATION = 1.5f;
        private const float OUTRO_ANIMATION_DURATION = 0.5f;

        #endregion


        #region Monobehaviour Events

        // Start is called before the first frame update
        private void Start()
        {
            GetRequiredComponents();
            AssertReferences();
            SetInitialValues();
            RunAnimations();
        }

        /// <summary>
        /// Metoda koja prikuplja komponente neophodne za dalji proces rada
        /// WHY: Zato što je najefikasnije da se sve komponente neophodne za rad prikupe na samom početku i zatim sačuvaju njihove reference
        /// </summary>
        private void GetRequiredComponents()
        {
            _logoImage = GetComponent<Image>();
            _rectTransform = GetComponent<RectTransform>();
        }

        /// <summary>
        /// Metoda koja proverava da li su sve reference koje se prikupljaju ispravne
        /// WHY: Zato što su one neophodne za dalji proces rada
        /// </summary>
        private void AssertReferences()
        {
            Assert.IsNotNull(_logoImage);
            Assert.IsNotNull(_rectTransform);
            Assert.IsNotNull(_playerData);
        }

        /// <summary>
        /// Metoda koja postavlja neke početne vrednosti neophodne za uspešan rad
        /// WHY: Neophodno za normalno funkcionisanje
        /// </summary>
        private void SetInitialValues()
        {
            gameObject.transform.localScale = _initialLogoScale;
            _logoImage.color = _initialLogoColor;
        }

        /// <summary>
        /// Metoda koja pokreće animacije Logoa (review) i učitava sledeće scene u zavisnosti od uslova
        /// WHY: Neophodno
        /// </summary>
        private void RunAnimations()
        {
            LeanTween.scale(gameObject, Vector3.one, INTRO_ANIMATION_DURATION).setOnComplete(() =>
            {
                LeanTween.scale(gameObject, _initialLogoScale, OUTRO_ANIMATION_DURATION);
            });

            LeanTween.alpha(_rectTransform, 1f, INTRO_ANIMATION_DURATION).setOnComplete(() =>
            {
                LeanTween.alpha(_rectTransform, 0f, OUTRO_ANIMATION_DURATION).setOnComplete(() =>
                {
                    if (!_playerData.PersistentData.IsTutorialPlayed.Value)
                    {
                        MessageBroker.Default.Publish(new ShouldLoadTutorialScene());
                    }
                    else
                    {
                        MessageBroker.Default.Publish(new ShouldLoadMainMenuScene());
                    }
                });
            });
        }

        #endregion
    }
}