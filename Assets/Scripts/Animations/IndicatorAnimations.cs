﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: Menu/Level
    /// GAME_OBJECT: ../Indicator
    /// DESCRIPTION: Klasa koja kontrolise animaciju indikatora kada je upaljen
    /// </summary>
    public class IndicatorAnimations : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly Color _default = Color.red;
        private const float ANIMATION_LENGTH = 0.75f;
        private Image _indicatorImage;

        #endregion





        #region Monobehaviour Events

        private void Awake()
        {
            _indicatorImage = gameObject.GetComponent<Image>();
        }

        private void OnEnable()
        {
            LeanTween.value(gameObject, colorValue => _indicatorImage.color = colorValue, Color.clear, _default,  ANIMATION_LENGTH)
                .setLoopPingPong(-1)
                .setEase(LeanTweenType.easeOutBack);
        }

        #endregion
    }
}
