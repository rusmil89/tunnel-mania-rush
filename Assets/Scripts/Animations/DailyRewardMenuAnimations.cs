﻿using System;
using UniRx;
using UnityEngine;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Klasa koja kontrolise animacije u Daily Rewards meniju
    /// </summary>
    public class DailyRewardMenuAnimations
    {

        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly CompositeDisposable _compositeDisposable;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float BUTTON_PERIOD_DURATION = 0.5f;

        #endregion





        #region Custom Methods

        public DailyRewardMenuAnimations()
        {
            _compositeDisposable = new CompositeDisposable();
        }

        public void AnimateDayXButton(GameObject buttonObject)
        {
            AnimateDayXButtonInternal(buttonObject);
            TimeSpan period = TimeSpan.FromSeconds(10 * BUTTON_PERIOD_DURATION);
            Observable.Interval(period)
                .Subscribe(_ =>
                {
                    AnimateDayXButtonInternal(buttonObject);
                })
                .AddTo(_compositeDisposable);
        }

        private void AnimateDayXButtonInternal(GameObject buttonObject)
        {
            LeanTween.scale(buttonObject, Vector3.one * 1.05f, BUTTON_PERIOD_DURATION)
                .setFrom(Vector3.one)
                .setLoopPingPong(3)
                .setOnComplete(() => buttonObject.LeanScale(Vector3.one, 0f));
            LeanTween.rotateZ(buttonObject, 2.5f, BUTTON_PERIOD_DURATION)
                .setFrom(Vector3.zero)
                .setLoopPingPong(3)
                .setOnComplete(() => buttonObject.LeanRotateZ(0f, 0f));
        }

        public void AnimateClick(GameObject buttonGameObject)
        {
            LeanTween.scale(buttonGameObject, Vector3.one * 0.8f, 0.05f)
                .setFrom(Vector3.one);
        }

        public void CancelAllAnimations()
        {
            LeanTween.cancelAll(true);
            _compositeDisposable.Clear();
        }

        public void DisposeAllAnimations()
        {
            _compositeDisposable.Dispose();
        }

        #endregion
    }
}
