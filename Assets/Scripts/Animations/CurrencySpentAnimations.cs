﻿using System.Collections;
using System.Collections.Generic;
using Core.Events;
using TMPro;
using Tunnel.UI;
using UniRx;
using UnityEngine;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Bazna klasa koja kontrolise animacije trosenja valuta u igri (npr, trosenje valute za otkljucavanje novog chaptera, za nastavak sa mesta pogibije,...)
    /// </summary>
    public class CurrencySpentAnimations
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly ICurrencyAnimatingMenu _currencyAnimatingMenu;
        private IReadOnlyDictionary<int, object> _animationsEvents;
        private bool _isAnimationFinished;
        private List<RectTransform> _currencies;

        /// <summary>
        /// Consts
        /// </summary>
        private const float ANIMATION_DURATION = 0.4f;
        private const float DELAY_BETWEEN_EVERY_ANIMATION_ELEMENT = 0.2f;
        private const float ROTATION_STARTING_DELAY = 0.2f;
        private const float MOVING_START_DELAY = 0.6f;
        private const float DELAY_BETWEEN_EVERY_ELEMENT_IN_MOVING_ANIMATION = 0.1f;
        private const float BUTTON_SCALE_FACTOR = 0.8f;
        private const float RECEIVING_CURRENCIES_ANIMATION_DURATION = 0.05f;
        private const int RECEIVING_CURRENCIES_ANIMATION_LOOP_COUNT = 8;
        private const float CURRENCY_AMOUNT_START_POSITION_X = 50f;
        private const float CURRENCY_AMOUNT_START_POSITION_Y = 100f;
        private const float CURRENCY_AMOUNT_MOVING_ANIMATION_DURATION = 1.5f;
        private const float CURRENCY_AMOUNT_FINAL_POSITION_Y = 200f;
        private const float CURRENCY_AMOUNT_ALPHA_START_VALUE = 0f;
        private const float CURRENCY_AMOUNT_ALPHA_END_VALUE = 1f;
        private const float CURRENCY_AMOUNT_ALPHA_ANIMATION_DURATION = 1.55f;
        private const float MIN_RANDOM_VALUE = -50f;
        private const float MAX_RANDOM_VALUE = 50f;
        private const float SCALE_FACTOR = 0.3f;
        private readonly Vector3 _initialScale = new Vector3(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);

        #endregion





        #region Custom Methods

        public CurrencySpentAnimations(ICurrencyAnimatingMenu currencyAnimatingMenu)
        {
            _currencyAnimatingMenu = currencyAnimatingMenu;
        }

        public IEnumerator Animate()
        {
            _isAnimationFinished = false;
            _animationsEvents = GetAnimationsEvents();
            PublishNotification(1);
            AnimateCurrencyAmount(_currencyAnimatingMenu.CurrencyAmountPrefab);
            _currencies = _currencyAnimatingMenu.CurrenciesTransforms;
            for (int i = 0; i < _currencies.Count; i++)
            {
                PrepareForAnimation(_currencies[i]);
                Scale(_currencies[i]);
                RotateY(_currencies[i]);
                MoveToFinalDestination(_currencies[i]);
            }
            
            yield return new WaitUntil(() => _isAnimationFinished);
        }

        private IReadOnlyDictionary<int, object> GetAnimationsEvents()
        {
            return new Dictionary<int, object>
            {
                {1, new CurrenciesAnimationStarted()},
                {2, new CurrenciesStartedMovingFromTopMenu
                {
                    CurrenciesSpent = _currencyAnimatingMenu.CurrencyAmountSpent,
                    CurrencyType = _currencyAnimatingMenu.CurrencyType
                }},
                {3, new CurrenciesFinishedMovingFromTopMenu()},
                {4, new CurrenciesRemovingFromTopMenu
                {
                    CurrencyType = _currencyAnimatingMenu.CurrencyType
                }}
            };
        }

        private void PublishNotification(int notificationIndex)
        {
            if (_animationsEvents.TryGetValue(notificationIndex, out var notification))
            {
                MessageBroker.Default.Publish(notification);
            }
        }

        private void PrepareForAnimation(RectTransform currency)
        {
            Vector3 animationStartPosition;
            if (_currencyAnimatingMenu.CurrencyAnimationStartObject != null)
            {
                animationStartPosition = _currencyAnimatingMenu.CurrencyAnimationStartObject.localPosition;
                animationStartPosition = new Vector3(
                    Random.Range(animationStartPosition.x + MIN_RANDOM_VALUE,
                        animationStartPosition.x + MAX_RANDOM_VALUE),
                    Random.Range(animationStartPosition.y + MIN_RANDOM_VALUE,
                        animationStartPosition.y + MAX_RANDOM_VALUE),
                    0f);
            }
            else
            {
                animationStartPosition = new Vector3(Random.Range(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE), Random.Range(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE), 0f);
            }
            currency.gameObject.SetActive(true);
            currency.localPosition = animationStartPosition;
            currency.localScale = Vector3.zero;
        }

        private void Scale(RectTransform currency)
        {
            LeanTween.scale(currency, _initialScale, ANIMATION_DURATION)
                    .setFrom(Vector3.zero)
                    .setDelay(_currencies.IndexOf(currency) * DELAY_BETWEEN_EVERY_ANIMATION_ELEMENT)
                    .setEase(LeanTweenType.easeOutBack);
        }

        private void RotateY(RectTransform currency)
        {
            LeanTween.rotateY(currency.gameObject, 180f, ANIMATION_DURATION)
                .setFrom(0f)
                .setDelay(ROTATION_STARTING_DELAY + _currencies.IndexOf(currency) * DELAY_BETWEEN_EVERY_ANIMATION_ELEMENT)
                .setEase(LeanTweenType.easeSpring);
        }

        private void MoveToFinalDestination(RectTransform currency)
        {
            Vector3 finalDestinationPosition = RectTransformUtility.CalculateRelativeRectTransformBounds(currency.root, _currencyAnimatingMenu.CurrencyAnimationEndObject.transform).center;
            LeanTween.move(currency, finalDestinationPosition, ANIMATION_DURATION)
                .setFrom(currency.localPosition)
                .setDelay(MOVING_START_DELAY + _currencies.IndexOf(currency) * DELAY_BETWEEN_EVERY_ELEMENT_IN_MOVING_ANIMATION)
                .setEase(LeanTweenType.easeInQuart)
                .setOnComplete(() =>
                {
                    currency.gameObject.SetActive(false);
                    switch (_currencies.IndexOf(currency))
                    {
                        case 0:
                            AnimateReceivingCurrencies();
                            PublishNotification(2);
                            break;
                        case 9:
                            PublishNotification(3);
                            _isAnimationFinished = true;
                            break;
                    }
                });
        }

        private void AnimateReceivingCurrencies()
        {
            LeanTween.scale(_currencyAnimatingMenu.CurrencyAnimationEndObject, Vector3.one * BUTTON_SCALE_FACTOR, RECEIVING_CURRENCIES_ANIMATION_DURATION)
                .setFrom(Vector3.one)
                .setLoopPingPong(RECEIVING_CURRENCIES_ANIMATION_LOOP_COUNT)
                .setOnCompleteOnRepeat(true)
                .setOnComplete(() => PublishNotification(4));
        }

        private void AnimateCurrencyAmount(GameObject currencyAmount)
        {
            currencyAmount.GetComponent<TMP_Text>().text = $"-{_currencyAnimatingMenu.CurrencyAmountSpent}";
            currencyAmount.SetActive(true);
            currencyAmount.transform.localPosition = new Vector3(CURRENCY_AMOUNT_START_POSITION_X, CURRENCY_AMOUNT_START_POSITION_Y);
            var text = currencyAmount.GetComponent<TMP_Text>();
            LeanTween.moveLocalY(currencyAmount, CURRENCY_AMOUNT_FINAL_POSITION_Y, CURRENCY_AMOUNT_MOVING_ANIMATION_DURATION)
                .setFrom(currencyAmount.transform.localPosition);
            LeanTween.value(currencyAmount, value => text.alpha = value, CURRENCY_AMOUNT_ALPHA_START_VALUE, CURRENCY_AMOUNT_ALPHA_END_VALUE, CURRENCY_AMOUNT_ALPHA_ANIMATION_DURATION)
                .setOnComplete(() =>
                {
                    LeanTween.value(currencyAmount, value => text.alpha = value, CURRENCY_AMOUNT_ALPHA_END_VALUE, CURRENCY_AMOUNT_ALPHA_START_VALUE, CURRENCY_AMOUNT_ALPHA_ANIMATION_DURATION / 10f)
                        .setOnComplete(() => currencyAmount.SetActive(false));
                });
        }

        #endregion
    }
}
