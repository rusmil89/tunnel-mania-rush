using System.Collections;
using System.Collections.Generic;
using Core.Events;
using TMPro;
using Tunnel.Analytics;
using Tunnel.PlayerData;
using Tunnel.UI;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tunnel.Animations
{
    public class CurrencyBoughtAnimations
    {
        #region Private Fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly ICurrencyAnimatingMenu _currencyAnimatingMenu;
        private IReadOnlyDictionary<int, object> _animationsEvents;
        private bool _isAnimationFinished;

        /// <summary>
        /// Consts
        /// </summary>
        private const float SCALE_FACTOR = 0.3f;
        private const float MIN_RANDOM_VALUE = -50f;
        private const float MAX_RANDOM_VALUE = 50f;
        private readonly Vector3 _initialScale = new Vector3(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);

        #endregion





        #region Custom Methods

        public CurrencyBoughtAnimations(ICurrencyAnimatingMenu currencyAnimatingMenu)
        {
            _currencyAnimatingMenu = currencyAnimatingMenu;
        }
        
        public IEnumerator Animate()
        {
            _isAnimationFinished = false;
            _animationsEvents = GetAnimationsEvents();
            PublishNotification(1);
            AnimateCurrencyAmount(_currencyAnimatingMenu.CurrencyAmountPrefab);
            var currencies = _currencyAnimatingMenu.CurrenciesTransforms;
            for (int i = 0; i < currencies.Count; i++)
            {
                PrepareForAnimation(currencies[i]);
                Scale(currencies[i], i);
                RotateY(currencies[i], i);
                MoveToTopButtonsMenu(currencies[i], i);
            }

            yield return new WaitUntil(() => _isAnimationFinished);
        }
        
        private IReadOnlyDictionary<int, object> GetAnimationsEvents()
        {
            return new Dictionary<int, object>
            {
                {1, new CurrenciesAnimationStarted
                {
                    CurrenciesSpent = _currencyAnimatingMenu.CurrencyAmountSpent,
                    CurrencyType = _currencyAnimatingMenu.CurrencyType
                }},
                {2, new CurrenciesStartedMovingToTopMenu
                {
                    CurrenciesWon = _currencyAnimatingMenu.CurrencyAmountEarned,
                    CurrencyType = _currencyAnimatingMenu.CurrencyType
                }},
                {3, new CurrenciesFinishedMovingToTopMenu
                {
                    CurrencyType = _currencyAnimatingMenu.CurrencyType
                }}
            };
        }

        private void PublishNotification(int notificationIndex)
        {
            if (_animationsEvents.TryGetValue(notificationIndex, out var notification))
            {
                MessageBroker.Default.Publish(notification);
            }
        }

        private void PrepareForAnimation(RectTransform currency)
        {
            Vector3 animationStartPosition;
            if (_currencyAnimatingMenu.CurrencyAnimationStartObject != null)
            {
                animationStartPosition = _currencyAnimatingMenu.CurrencyAnimationStartObject.localPosition;
                animationStartPosition = new Vector3(
                    Random.Range(animationStartPosition.x + MIN_RANDOM_VALUE,
                        animationStartPosition.x + MAX_RANDOM_VALUE),
                    Random.Range(animationStartPosition.y + MIN_RANDOM_VALUE,
                        animationStartPosition.y + MAX_RANDOM_VALUE),
                    0f);
            }
            else
            {
                animationStartPosition = new Vector3(Random.Range(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE), Random.Range(MIN_RANDOM_VALUE, MAX_RANDOM_VALUE), 0f);
            }
            currency.gameObject.SetActive(true);
            currency.localPosition = animationStartPosition;
            currency.localScale = Vector3.zero;
        }

        private void Scale(RectTransform currency, int index)
        {
            LeanTween.scale(currency, _initialScale, 0.4f)
                    .setFrom(Vector3.zero)
                    .setDelay(index * 0.2f)
                    .setEase(LeanTweenType.easeOutBack);
        }

        private void RotateY(RectTransform currency, int index)
        {
            LeanTween.rotateY(currency.gameObject, 180f, 0.4f)
                .setFrom(0f)
                .setDelay(0.2f + index * 0.2f)
                .setEase(LeanTweenType.easeSpring);
        }

        private void MoveToTopButtonsMenu(RectTransform currency, int index)
        {
            LeanTween.move(currency, _currencyAnimatingMenu.CurrencyAnimationEndObject.localPosition, 0.4f)
                .setFrom(currency.localPosition)
                .setDelay(0.6f + index * 0.1f)
                .setEase(LeanTweenType.easeInBack)
                .setOnComplete(() =>
                {
                    currency.gameObject.SetActive(false);
                    switch (index)
                    {
                        case 0:
                            PublishNotification(2);
#if UNITY_ANDROID || UNITY_IOS
                            WarmAppGamesAnalytics.LogEarnVirtualCurrency(nameof(PersistentData.Coins), _currencyAnimatingMenu.CurrencyAmountEarned);
#endif
                            break;
                        case 9:
                            PublishNotification(3);
                            _isAnimationFinished = true;
                            break;
                    }
                });
        }
        
        private void AnimateCurrencyAmount(GameObject currencyAmount)
        {
            currencyAmount.GetComponent<TMP_Text>().text = $"+{_currencyAnimatingMenu.CurrencyAmountEarned}";
            currencyAmount.SetActive(true);
            currencyAmount.transform.localPosition = new Vector3(50f, 100f);
            var text = currencyAmount.GetComponent<TMP_Text>();
            LeanTween.moveLocalY(currencyAmount, 200f, 1.5f)
                .setFrom(currencyAmount.transform.localPosition);
            LeanTween.value(currencyAmount, value => text.alpha = value, 0.2f, 1f, 1.55f)
                .setOnComplete(() =>
                {
                    LeanTween.value(currencyAmount, value => text.alpha = value, 1f, 0f, 0.1f)
                        .setOnComplete(() => currencyAmount.SetActive(false));
                });
        }
        
        #endregion
    }
}