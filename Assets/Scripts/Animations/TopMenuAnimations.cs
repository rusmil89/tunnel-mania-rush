﻿using System;
using UniRx;
using UnityEngine;

namespace Tunnel.Animations
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// Klasa koja kontrolise animaciju stita kada se promeni njegova vrednost
    /// </summary>
    public sealed class TopMenuAnimations
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        /// <summary>
        /// Consts
        /// </summary>
        private const float SCALE_FACTOR = 1.2f;
        private const float SCALE_DURATION = 0.2f;
        private const float ADD_SHIELDS_DELAY = 5F;

        #endregion





        #region Custom Methods

        /// <summary>
        /// Metoda koja animira znak "+" koji se nalazi pored broj stitova u top meniju, samo onda kada je broj stitova = 0
        /// WHY: Zato sto je najbolje da ta metoda bude zasebna u jednoj klasi koja radi samo 1 stvar (SRP)
        /// </summary>
        /// <param name="addShieldsButton">objekat koji se animira</param>
        public void AnimateAddShieldsButton(GameObject addShieldsButton)
        {
            Observable.Interval(TimeSpan.FromSeconds(ADD_SHIELDS_DELAY))
                .Subscribe(_ => LeanTween.scale(addShieldsButton, Vector3.one * SCALE_FACTOR, SCALE_DURATION).setFrom(Vector3.one).setLoopPingPong(3))
                .AddTo(_compositeDisposable);
        }

        /// <summary>
        /// Metoda koja prekida animiranje observable stream-a kada broj stitova postane > 0, ili kada se potpuno izadje iz Level scene
        /// WHY: Zato sto je najbolje da ta metoda bude zasebna u jednoj klasi koja radi samo 1 stvar (SRP)
        /// </summary>
        public void ClearCompositeDisposals()
        {
            _compositeDisposable.Clear();
        }

        #endregion
    }
}
