﻿using System.Linq;
using Core.Events;
using Tunnel.Ads;
using Dreamteck.Splines;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.Controllers
{
    public sealed class PlayerBypassedObstacle {}
    
    /// <summary>
    /// SCENE: Level
    /// GAME OBJECT: Player
    /// DESCRIPTION: Klasa koja kontrolise smrt igraca
    /// </summary>
    public sealed class PlayerDeath : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float HIGH_SCORE_RAY_LENGTH = 1f;
        private const float OBSTACLE_DETECTION_RAY_LENGTH = 5f;
        private readonly Vector3 _rightUpDirection = new Vector3(1f, 1f);
        private readonly Vector3 _leftUpDirection = new Vector3(-1f, 1f);

        /// <summary>
        /// Serializable fields
        /// </summary>
        //NOTE: Objekat potreban za analytics evente
        [SerializeField] private SplineFollower _playerFollower;
        [SerializeField] private global::PlayerData _playerData;

        #endregion


        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
            bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
            bool prefabConnected = UnityEditor.PrefabUtility.GetPrefabInstanceStatus(gameObject) == UnityEditor.PrefabInstanceStatus.Connected;
            if (!isValidPrefabStage && prefabConnected)
            {
                ReferencesClient.CheckReferences(this, gameObject);
            }
        }
#endif

        private void Start()
        {
            RegisterObservableListeners();
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<RewardVideoAfterDeathCompleted>()
                .Subscribe(_ => HandleRewardVideoAfterDeathCompletedEvent())
                .AddTo(this);
            RaycastHit hitInfo = default;
            Observable.EveryFixedUpdate()
                .Where(_ => !_playerData.IsPlayerDead)
                .Where(_ => !_playerData.IsPlayerHitObstacle)
                .Subscribe(_ => DetectObstacleForSoundCollision(hitInfo))
                .AddTo(this);
        }

        /// <summary>
        /// Metoda koja registruje kada je igrac prosao pored prepreke kako bi okinula event za pustanje odgovarajuceg zvuka
        /// WHY: Povecanje UX-a i game feel-a
        /// </summary>
        /// <param name="hitInfo">Parametar koji cuva informacije o objektu koji je pogodio</param>
        private void DetectObstacleForSoundCollision(RaycastHit hitInfo)
        {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.DrawRay(transform.position, transform.right * OBSTACLE_DETECTION_RAY_LENGTH, Color.green);
            Debug.DrawRay(transform.position, -transform.right * OBSTACLE_DETECTION_RAY_LENGTH, Color.green);
            Debug.DrawRay(transform.position, transform.up * OBSTACLE_DETECTION_RAY_LENGTH, Color.green);
            Debug.DrawRay(transform.position, transform.rotation * _rightUpDirection * OBSTACLE_DETECTION_RAY_LENGTH, Color.green);
            Debug.DrawRay(transform.position, transform.rotation * _leftUpDirection * OBSTACLE_DETECTION_RAY_LENGTH, Color.green);
#endif
            
            if (Physics.Raycast(transform.position, transform.right, out hitInfo, OBSTACLE_DETECTION_RAY_LENGTH) ||
                Physics.Raycast(transform.position, -transform.right, out hitInfo, OBSTACLE_DETECTION_RAY_LENGTH) ||
                Physics.Raycast(transform.position, transform.rotation * _rightUpDirection, out hitInfo, OBSTACLE_DETECTION_RAY_LENGTH) ||
                Physics.Raycast(transform.position, transform.rotation * _leftUpDirection, out hitInfo, OBSTACLE_DETECTION_RAY_LENGTH) ||
                Physics.Raycast(transform.position, transform.up, out hitInfo, OBSTACLE_DETECTION_RAY_LENGTH))
            {
                if (hitInfo.collider.CompareTag(Constants.Tags.NEW_HIGH_SCORE)) return;
                
                MessageBroker.Default.Publish(new PlayerBypassedObstacle());
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Constants.Tags.NEW_HIGH_SCORE)) return;

            if (_playerData.PersistentData.Shields.Value > 0)
            {
                PlayerHitObstacle();
            }
            else
            {
                MessageBroker.Default.Publish(new GameOverMenuCalled());
                _playerData.IsPlayerDead = true;

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        var parentObstacle = other.gameObject.GetComponentInParent<DynamicObstacles.Obstacle>();

        Analytics.WarmAppGamesAnalytics.LogGameOverEvent(_playerData.PersistentData.CurrentWorld.Value, _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value], _playerData.PersistentData.CurrentLevel.Value, parentObstacle.name);
#endif
            }
        }

        #endregion





        #region Custom Methods

        private void PlayerHitObstacle()
        {
            _playerData.PersistentData.Shields.Value--;
            _playerData.IsPlayerHitObstacle = true;
        }

        public void HandleRewardVideoAfterDeathCompletedEvent()
        {
            _playerData.IsPlayerDead = false;
        }

        #endregion
    }
}