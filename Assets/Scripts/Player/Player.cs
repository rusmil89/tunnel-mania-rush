﻿using System;
using System.Collections;
using System.Linq;
using Core.Events;
using Dreamteck.Splines;
using Tunnel.LevelLoad;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using Utilities;

namespace Tunnel.Controllers
{
    public sealed class PlayerReachedEndOfTube {}
    public sealed class PlayerReachedEndOfLevel {}
    
    /// <summary>
    /// SCENE: Level
    /// GAME OBJECT: Player
    /// DESCRIPTION: Klasa koja kontrolise ponasanje igraca u igri
    /// </summary>
    public sealed class Player : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Scene Objects")]
        [SerializeField] private Transform _playerCamera;
        [SerializeField] private SplineFollower _playerFollower;
        [SerializeField] private LevelDataLoading _levelDataLoader;
        
        [Header("Scriptable Objects")]
        [SerializeField] private GameData _gameData;
        [SerializeField] private global::PlayerData _playerData;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Collider _playerCollider;

        /// <summary>
        /// Consts
        /// </summary>
        private const float ENABLE_PLAYER_COLLIDER_AFTER_DEATH_DELAY = 3f;
        private const string PAUSE_BUTTON_NAME = "PauseButton";
        private const float INITIAL_SPEED_FACTOR = 0.4f;
        private readonly Vector3 insidePosition = new Vector3(0, -1.5f, 0);
        private readonly Vector3 outsidePosition = new Vector3(0, 2.2f, 0);

        #endregion




        #region Properties

        public bool IsOutside { get; private set; }

        private IObservable<long> IsTheEndOfTheTubeObservable
        {
            get
            {
                return Observable.EveryFixedUpdate()
                    .Where(_ => EventSystem.current.currentSelectedGameObject == null || !EventSystem.current.currentSelectedGameObject.name.Contains(PAUSE_BUTTON_NAME))
                    .Where(_ => Math.Abs(_playerFollower.result.percent - 1) < 0.001f)
                    .DistinctUntilChanged();
            }
        }

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _gameData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.GAME_CONTROLLER, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<GameData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            _playerData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.PLAYER_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<global::PlayerData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            
            UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
            bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
            bool prefabConnected = UnityEditor.PrefabUtility.GetPrefabInstanceStatus(gameObject) == UnityEditor.PrefabInstanceStatus.Connected;
            if (!isValidPrefabStage && prefabConnected)
            {
                ReferencesClient.CheckReferences(this, gameObject);
            }
        }
#endif

        private void Awake()
        {
            GetRequiredComponents();
#if UNITY_EDITOR
            AssertReferences();      
#endif
            SetInitialValues();
        }

        private void GetRequiredComponents()
        {
            _playerCollider = GetComponent<CapsuleCollider>();
        }

        private void AssertReferences()
        {
            Assert.IsNotNull(_playerCamera);
            Assert.IsNotNull(_playerFollower);
            Assert.IsNotNull(_gameData);
            Assert.IsNotNull(_playerData);
            Assert.IsNotNull(_levelDataLoader);
            Assert.IsNotNull(_playerCollider);
        }

        private void SetInitialValues()
        {
            _playerData.IsPlayerDead = false;
        }

        private void Start()
        {
            StartCoroutine(IncreasePlayerSpeed());
            SetUpPlayerForLevel();
            RegisterObservableListeners();
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<GamePaused>()
                .Subscribe(HandleGamePausedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(HandlePlayerDiedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerRevived>()
                .Subscribe(HandlePlayerRevivedEvent)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(HandlePlayerHitObstacleEvent)
                .AddTo(this);
            IsTheEndOfTheTubeObservable
                .Subscribe(HandlePlayerReachedEndOfTube)
                .AddTo(this);
        }

        #endregion





        #region Custom Methods

        /// <summary>
        /// Metoda koja osluskuje kada igrac stigne do kraja tunela kada je unutar njega
        /// WHY: Zato sto treba obavestiti osluskivace kojima je bitno da znaju kada se ovaj event dogodio
        /// </summary>
        /// <param name="value">Time.deltaTime iz Update Observable stream-a</param>
        private void HandlePlayerReachedEndOfTube(long value)
        {
            MessageBroker.Default.Publish(new PlayerReachedEndOfTube());
            if (IsOutside)
            {
                MessageBroker.Default.Publish(new FinishMenuCalled());
                MessageBroker.Default.Publish(new PlayerReachedEndOfLevel());
            
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        Tunnel.Analytics.WarmAppGamesAnalytics.LogLevelEndEvent(_playerData.PersistentData.CurrentWorld.Value, _playerData.PersistentData.CurrentChapterInWorld[_playerData.PersistentData.CurrentWorld.Value], _playerData.PersistentData.CurrentLevel.Value);
#endif
            }
            IsOutside = !IsOutside;
            SetUpPlayerForLevel();
        }

        /// <summary>
        /// Metoda koja podesava parametre igraca za svaki novi level
        /// WHY: Neophodno da bi level bio spreman za igranje
        /// </summary>
        private void SetUpPlayerForLevel()
        {
            _gameData.PlayerCurrentSpeedUpPosition = 0;
            _playerFollower.Restart();
            _playerCamera.localPosition = IsOutside ? outsidePosition : insidePosition;
            _playerCamera.localEulerAngles = Vector3.zero;
        }

        #endregion





        #region Coroutines

        /// <summary>
        /// Metoda koja povecava brzinu igraca vremenom
        /// WHY: Da bi korisnik stekao osecaj ubrzavanja vremenom i da bi malo bolje simulirali osecaj iz realnog zivota
        /// </summary>
        /// <returns>null</returns>
        private IEnumerator IncreasePlayerSpeed()
        {
            _gameData.PlayerCurrentSpeed = _playerFollower.followSpeed = _gameData.PlayerMovementSpeed * INITIAL_SPEED_FACTOR;
            while (_playerFollower.followSpeed < _gameData.PlayerMovementSpeed)
            {
                if (!_gameData.IsGamePaused && !_playerData.IsPlayerDead)
                {
                    _playerFollower.followSpeed += Time.deltaTime;
                    _gameData.PlayerCurrentSpeed = _playerFollower.followSpeed;
                    _gameData.PlayerCurrentSpeedUpPosition = _playerFollower.result.percent;
                }

                yield return null;
            }
        }

        #endregion





        #region Event Listeners

        private void HandleGamePausedEvent(GamePaused gamePaused)
        {
            if (gamePaused.IsGamePaused)
            {
                _playerFollower.followSpeed = 0;
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine(IncreasePlayerSpeed());
            }
        }

        private void HandlePlayerDiedEvent(PlayerDied playerDied)
        {
            _playerCollider.enabled = false;
            _playerFollower.followSpeed = 0;
        }

        private void HandlePlayerRevivedEvent(PlayerRevived playerRevived)
        {
            StopAllCoroutines();
            StartCoroutine(EnableCollidersCoroutine());
            StartCoroutine(IncreasePlayerSpeed());
        }

        private void HandlePlayerHitObstacleEvent(PlayerHitObstacle playerHitObstacle)
        {
            StopAllCoroutines();
            _playerCollider.enabled = false;
            StartCoroutine(EnableCollidersCoroutine());
            StartCoroutine(IncreasePlayerSpeed());
        }

        #endregion





        #region Coroutines


        private IEnumerator EnableCollidersCoroutine()
        {
            yield return new WaitForSeconds(ENABLE_PLAYER_COLLIDER_AFTER_DEATH_DELAY);
            if (_playerCollider == null) yield break;

            _playerCollider.enabled = true;
            _playerData.IsPlayerHitObstacle = false;
        }

        #endregion
    }
}