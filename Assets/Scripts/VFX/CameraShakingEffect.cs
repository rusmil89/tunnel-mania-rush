﻿using System.Collections;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Tunnel.VFX
{
    /// <summary>
    /// SCENE: Level, Tutorial
    /// GAME_OBJECT: MainCamera
    /// Klasa koja kontrolise kameru
    /// Trenutno se tu nalazi samo metoda za efekat drmanja kamere kada igrac udari u prepreku
    /// </summary>
    public class CameraShakingEffect : MonoBehaviour
    {
        #region Private Fields
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Vector3 _originalPosition;
        private float _currentShakeDuration;
        private MobilePostProcessing _mobilePostProcessing;

        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private global::PlayerData _playerDataController;
        [SerializeField] private float _shakeDuration = 0.3f;
        [SerializeField] private float _shakeAmount = 0.3f;
        [SerializeField] private float _decreaseFactor = 1.0f;

        /// <summary>
        /// Consts
        /// </summary>
        private const float DEFAULT_BLUR_AMOUNT = 0.2f;
        private const float EFFECT_DURATION = 0.2f;
        private const int EFFECT_LOOPS = 3;
        private const float CHROMATIC_ABERRATION_OFFSET_VALUE = 10f;

        #endregion

        
        
        
        
        
        #region Public Fields

        public ReactiveProperty<bool> IsCameraShaking = new ReactiveProperty<bool>(false);

        #endregion





        #region Monobehaviour Events

        private void OnEnable()
        {
            SetInitialValues();
            RegisterObservableListeners();
        }

        private void SetInitialValues()
        {
            _mobilePostProcessing = GetComponent<MobilePostProcessing>();
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(PlayerDiedEventHandler)
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(HandlePlayerHitObstacle)
                .AddTo(this);
        }

        #endregion





        #region Event Listeners

        private void PlayerDiedEventHandler(PlayerDied playerDied)
        {
            VibrateTheDevice();
            MainThreadDispatcher.StartCoroutine(CameraShakeRoutine());
        }

        private void HandlePlayerHitObstacle(PlayerHitObstacle playerHitObstacle)
        {
            VibrateTheDevice();
            PlayEffects();
            MainThreadDispatcher.StartCoroutine(CameraShakeRoutine());
        }
        
        private void VibrateTheDevice()
        {
            if (_playerDataController.PersistentData.IsVibrationEnabled.Value)
            {
                Handheld.Vibrate();
            }
        }

        private void PlayEffects()
        {
            LeanTween.value(gameObject, value => _mobilePostProcessing.BlurAmount = value, DEFAULT_BLUR_AMOUNT, 1f, EFFECT_DURATION)
                .setLoopPingPong(EFFECT_LOOPS)
                .setOnComplete(() => _mobilePostProcessing.BlurAmount = DEFAULT_BLUR_AMOUNT);
            LeanTween.value(gameObject, value => _mobilePostProcessing.Offset = value, -CHROMATIC_ABERRATION_OFFSET_VALUE, CHROMATIC_ABERRATION_OFFSET_VALUE, EFFECT_DURATION)
                .setLoopPingPong(EFFECT_LOOPS)
                .setOnComplete(() => LeanTween.value(gameObject, value => _mobilePostProcessing.Offset = value, _mobilePostProcessing.Offset, 0f, EFFECT_DURATION)
                    .setEase(LeanTweenType.easeOutSine));
        }

        #endregion





        #region Coroutines

        private IEnumerator CameraShakeRoutine()
        {
            _originalPosition = transform.localPosition;
            _currentShakeDuration = _shakeDuration;
            IsCameraShaking.Value = true;
            while (_currentShakeDuration > 0)
            {
                transform.localPosition = _originalPosition + Random.insideUnitSphere * _shakeAmount;
                _currentShakeDuration -= Time.deltaTime * _decreaseFactor;

                yield return null;
            }
            
            IsCameraShaking.Value = false;
            _currentShakeDuration = 0f;
            transform.localPosition = _originalPosition;
        }

        #endregion
    }
}
