﻿using UniRx;
using UnityEngine;

/// <summary>
/// SCENE: /
/// GAME_OBJECT: /
/// DESCRIPTION: SO koji cuva podatke o svakom jeziku posebno, odnosno reci i kako se one pisu na tom jeziku
/// </summary>
[CreateAssetMenu(fileName = nameof(LanguageData))]
public class LanguageData : ScriptableObject
{
    #region Private Fields

    [SerializeField] private StringReactiveProperty _welcome;
    [SerializeField] private StringReactiveProperty _mission;
    [SerializeField] private StringReactiveProperty _completed;
    [SerializeField] private StringReactiveProperty _tapOnTheLeft;
    [SerializeField] private StringReactiveProperty _tapOnTheRight;
    [SerializeField] private StringReactiveProperty _tiltOnTheLeft;
    [SerializeField] private StringReactiveProperty _tiltOnTheRight;
    [SerializeField] private StringReactiveProperty _avoidObstacle;
    [SerializeField] private StringReactiveProperty _again;
    [SerializeField] private StringReactiveProperty _tutorial;
    [SerializeField] private StringReactiveProperty _reward;
    [SerializeField] private StringReactiveProperty _controlsPanelTitle;
    [SerializeField] private StringReactiveProperty _controlsPanelDescription;
    [SerializeField] private StringReactiveProperty _claim;
    [SerializeField] private StringReactiveProperty _unlock;
    [SerializeField] private StringReactiveProperty _play;
    [SerializeField] private StringReactiveProperty _easy;
    [SerializeField] private StringReactiveProperty _medium;
    [SerializeField] private StringReactiveProperty _hard;
    [SerializeField] private StringReactiveProperty _world;
    [SerializeField] private StringReactiveProperty _loading;
    [SerializeField] private StringReactiveProperty _dailyRewards;
    [SerializeField] private StringReactiveProperty _day;
    [SerializeField] private StringReactiveProperty _settings;
    [SerializeField] private StringReactiveProperty _general;
    [SerializeField] private StringReactiveProperty _support;
    [SerializeField] private StringReactiveProperty _privacy;
    [SerializeField] private StringReactiveProperty _audio;
    [SerializeField] private StringReactiveProperty _notification;
    [SerializeField] private StringReactiveProperty _vibration;
    [SerializeField] private StringReactiveProperty _gyroscopeControls;
    [SerializeField] private StringReactiveProperty _sensitivity;
    [SerializeField] private StringReactiveProperty _rotationAcceleration;
    [SerializeField] private StringReactiveProperty _rateUs;
    [SerializeField] private StringReactiveProperty _signIn;
    [SerializeField] private StringReactiveProperty _signOut;
    [SerializeField] private StringReactiveProperty _summary;
    [SerializeField] private StringReactiveProperty _enterText;
    [SerializeField] private StringReactiveProperty _email;
    [SerializeField] private StringReactiveProperty _message;
    [SerializeField] private StringReactiveProperty _submit;
    [SerializeField] private StringReactiveProperty _feedbackSubmitted;
    [SerializeField] private StringReactiveProperty _thankYou;
    [SerializeField] private StringReactiveProperty _privacyPolicy;
    [SerializeField] private StringReactiveProperty _termsOfService;
    [SerializeField] private StringReactiveProperty _gdpr;
    [SerializeField] private StringReactiveProperty _gdprDescription;
    [SerializeField] private StringReactiveProperty _shop;
    [SerializeField] private StringReactiveProperty _free;
    [SerializeField] private StringReactiveProperty _getTokens;
    [SerializeField] private StringReactiveProperty _getShields;
    [SerializeField] private StringReactiveProperty _refillYourEnergy;
    [SerializeField] private StringReactiveProperty _level;
    [SerializeField] private StringReactiveProperty _score;
    [SerializeField] private StringReactiveProperty _highScore;
    [SerializeField] private StringReactiveProperty _pause;
    [SerializeField] private StringReactiveProperty _mainMenu;
    [SerializeField] private StringReactiveProperty _continue;
    [SerializeField] private StringReactiveProperty _noThanks;
    [SerializeField] private StringReactiveProperty _chapter;
    [SerializeField] private StringReactiveProperty _perfect;
    [SerializeField] private StringReactiveProperty _cleared;
    [SerializeField] private StringReactiveProperty _tapToOpen;
    [SerializeField] private StringReactiveProperty _bonusWheel;
    [SerializeField] private StringReactiveProperty _spinTheWheel;
    [SerializeField] private StringReactiveProperty _start;
    [SerializeField] private StringReactiveProperty _unlockNewChapter;
    [SerializeField] private StringReactiveProperty _exitTitle;
    [SerializeField] private StringReactiveProperty _exitMenuDescription;
    [SerializeField] private StringReactiveProperty _yes;
    [SerializeField] private StringReactiveProperty _no;
    [SerializeField] private StringReactiveProperty _likeTheGameTitle;
    [SerializeField] private StringReactiveProperty _likeTheGameDescription;
    [SerializeField] private StringReactiveProperty _updateMenuTitle;
    [SerializeField] private StringReactiveProperty _updateMenuDescription;
    [SerializeField] private StringReactiveProperty _updateButton;

    #endregion





    #region Public Fields

    public StringReactiveProperty Welcome => _welcome;
    
    public StringReactiveProperty TapOnTheLeft => _tapOnTheLeft;
    
    public StringReactiveProperty TapOnTheRight => _tapOnTheRight;

    public StringReactiveProperty TiltOnTheLeft => _tiltOnTheLeft;

    public StringReactiveProperty TiltOnTheRight => _tiltOnTheRight;

    public StringReactiveProperty AvoidObstacle => _avoidObstacle;
    
    public StringReactiveProperty Again => _again;
    
    public StringReactiveProperty Mission => _mission;
    
    public StringReactiveProperty Completed => _completed;
    
    public StringReactiveProperty Tutorial => _tutorial;

    public StringReactiveProperty Reward => _reward;

    public StringReactiveProperty ControlsPanelTitle => _controlsPanelTitle;

    public StringReactiveProperty ControlsPanelDescription => _controlsPanelDescription;
    
    public StringReactiveProperty Claim => _claim;

    public StringReactiveProperty Unlock => _unlock;

    public StringReactiveProperty Play => _play;

    public StringReactiveProperty Easy => _easy;

    public StringReactiveProperty Medium => _medium;

    public StringReactiveProperty Hard => _hard;

    public StringReactiveProperty World => _world;

    public StringReactiveProperty Loading => _loading;

    public StringReactiveProperty DailyRewards => _dailyRewards;

    public StringReactiveProperty Day => _day;

    public StringReactiveProperty Settings => _settings;

    public StringReactiveProperty General => _general;

    public StringReactiveProperty Support => _support;

    public StringReactiveProperty Privacy => _privacy;

    public StringReactiveProperty Audio => _audio;

    public StringReactiveProperty Notification => _notification;

    public StringReactiveProperty Vibration => _vibration;

    public StringReactiveProperty GyroscopeControls => _gyroscopeControls;

    public StringReactiveProperty Sensitivity => _sensitivity;

    public StringReactiveProperty RotationAcceleration => _rotationAcceleration;

    public StringReactiveProperty RateUs => _rateUs;

    public StringReactiveProperty SignIn => _signIn;

    public StringReactiveProperty SignOut => _signOut;

    public StringReactiveProperty Summary => _summary;

    public StringReactiveProperty EnterText => _enterText;

    public StringReactiveProperty Email => _email;

    public StringReactiveProperty Message => _message;

    public StringReactiveProperty Submit => _submit;

    public StringReactiveProperty FeedbackSubmitted => _feedbackSubmitted;

    public StringReactiveProperty ThankYou => _thankYou;

    public StringReactiveProperty PrivacyPolicy => _privacyPolicy;

    public StringReactiveProperty TermsOfService => _termsOfService;

    public StringReactiveProperty Gdpr => _gdpr;

    public StringReactiveProperty GdprDescription => _gdprDescription;

    public StringReactiveProperty Shop => _shop;

    public StringReactiveProperty Free => _free;

    public StringReactiveProperty GetTokens => _getTokens;

    public StringReactiveProperty GetShields => _getShields;

    public StringReactiveProperty RefillYourEnergy => _refillYourEnergy;

    public StringReactiveProperty Level => _level;

    public StringReactiveProperty Score => _score;

    public StringReactiveProperty HighScore => _highScore;

    public StringReactiveProperty Pause => _pause;

    public StringReactiveProperty MainMenu => _mainMenu;

    public StringReactiveProperty Continue => _continue;

    public StringReactiveProperty NoThanks => _noThanks;

    public StringReactiveProperty Chapter => _chapter;

    public StringReactiveProperty Perfect => _perfect;

    public StringReactiveProperty Cleared => _cleared;

    public StringReactiveProperty TapToOpen => _tapToOpen;

    public StringReactiveProperty BonusWheel => _bonusWheel;

    public StringReactiveProperty SpinTheWheel => _spinTheWheel;

    public StringReactiveProperty Start => _start;

    public StringReactiveProperty UnlockNewChapter => _unlockNewChapter;

    public StringReactiveProperty ExitTitle => _exitTitle;

    public StringReactiveProperty ExitMenuDescription => _exitMenuDescription;

    public StringReactiveProperty Yes => _yes;

    public StringReactiveProperty No => _no;

    public StringReactiveProperty LikeTheGameTitle => _likeTheGameTitle;

    public StringReactiveProperty LikeTheGameDescription => _likeTheGameDescription;

    public StringReactiveProperty UpdateMenuTitle => _updateMenuTitle;

    public StringReactiveProperty UpdateMenuDescription => _updateMenuDescription;

    public StringReactiveProperty UpdateButton => _updateButton;

    #endregion
}