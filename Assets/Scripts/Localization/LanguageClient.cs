﻿using Core.Events;
using TMPro;
using UniRx;
using UnityEngine;

namespace Localization
{
    /// <summary>
    /// SCENE: Language
    /// GAME_OBJECT: LanguageClient
    /// DESCRIPTION: Klasa koja podesava trenutni jezik igrice
    /// </summary>
    public class LanguageClient : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] private LanguageData _currentLanguage;
        [SerializeField] private LanguageData[] _languages;
        [SerializeField] private TMP_FontAsset _regularFont;
        [SerializeField] private TMP_FontAsset _chineseFont;

        #endregion





        #region Public Fields

        public LanguageData CurrentLanguage => _currentLanguage;

        public TMP_FontAsset CurrentFont { get; private set; }

        #endregion





        #region Monobehaviour Events

        // Start is called before the first frame update
        private void Start()
        {
            SetLanguage();
            RegisterObservableListeners();
        }

        private void SetLanguage()
        {
            Utilities.Logging.Log($"Application.systemLanguage: {Application.systemLanguage}");
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    _currentLanguage = _languages[(int)ELanguage.Chinese];
                    CurrentFont = _chineseFont;
                    break;
                case SystemLanguage.English:
                case SystemLanguage.Unknown:
                default:
                    _currentLanguage = _languages[(int)ELanguage.English];
                    CurrentFont = _regularFont;
                    break;
            }
        }

        private void RegisterObservableListeners()
        {
            MessageBroker.Default.Receive<ChangeLanguage>()
                .Subscribe(HandleLanguageChanged)
                .AddTo(this);
        }

        private void HandleLanguageChanged(ChangeLanguage args)
        {
            _currentLanguage = _languages[(int)args.Language];
            switch (args.Language)
            {
                case ELanguage.Chinese:
                    CurrentFont = _chineseFont;
                    break;
                case ELanguage.English:
                default:
                    CurrentFont = _regularFont;
                    break;
            }
        }

        #endregion
    }
}