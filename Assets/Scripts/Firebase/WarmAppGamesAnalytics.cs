using System;
using System.Collections.Generic;
using System.Text;
using Firebase.Analytics;
using UnityEngine.Analytics;

namespace Tunnel.Analytics
{
    public enum SocialNetworks
    {
        Facebook,
        Instagram,
        Twitter,
        Discord,
        NativeShare
    }

    public class WarmAppGamesAnalytics
    {
        #region Private Fields

        private static Parameter _parameterLevelName;
        private static StringBuilder _worldNameStringBuilder;
        private static StringBuilder _chapterNameStringBuilder;
        private static StringBuilder _levelNameStringBuilder;

        private const string WORLD_STRING = "World_";
        private const string CHAPTER_STRING = "Chapter_";
        private const string LEVEL_STRING = "Level_";

        #endregion
        
        
        


        #region Custom Methods

        public static void LogGameOverEvent(int currentWorld, int currentChapter, int currentLevel, string obstacleName)
        {
            GenerateParameterValues(currentWorld, currentChapter, currentLevel, obstacleName);
            Parameter gameOverParameters = new Parameter(Parameters.GameOverParameters, _worldNameStringBuilder.ToString());
            FirebaseAnalytics.LogEvent(Events.EventGameOver, gameOverParameters);

            //unity
            AnalyticsEvent.GameOver(null, new Dictionary<string, object>
            {
                {Parameters.GameOverParameters, _worldNameStringBuilder.ToString()}
            });
        }

        public static void LogLevelStartEvent(int currentWorld, int currentChapter, int currentLevel)
        {
            GenerateParameterValues(currentWorld, currentChapter, currentLevel);
            Parameter parameters = new Parameter(Parameters.LevelStartParameters, _worldNameStringBuilder.ToString());

            //firebase event
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, parameters);

            //unity analytics event
            AnalyticsEvent.LevelStart(FirebaseAnalytics.EventLevelStart, new Dictionary<string, object>
            {
                {Parameters.LevelStartParameters, _worldNameStringBuilder.ToString()}
            });
        }

        public static void LogLevelEndEvent(int currentWorld, int currentChapter, int currentLevel)
        {
            GenerateParameterValues(currentWorld, currentChapter, currentLevel);
            Parameter parameters = new Parameter(Parameters.LevelEndParameters, _worldNameStringBuilder.ToString());

            // firebase event
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, parameters);

            //unity analytics event
            AnalyticsEvent.LevelComplete(FirebaseAnalytics.EventLevelEnd, new Dictionary<string, object>
            {
                {Parameters.LevelEndParameters, _worldNameStringBuilder.ToString()}
            });
        }

        private static void GenerateParameterValues(int currentWorld, int currentChapter, int currentLevel, string obstacleName = null)
        {
            _worldNameStringBuilder = new StringBuilder(WORLD_STRING);
            _worldNameStringBuilder.Append(currentWorld);
            _chapterNameStringBuilder = new StringBuilder(CHAPTER_STRING);
            _chapterNameStringBuilder.Append(currentChapter);
            _levelNameStringBuilder = new StringBuilder(LEVEL_STRING);
            _levelNameStringBuilder.Append(currentLevel);
            if (obstacleName == null)
            {
                _worldNameStringBuilder.AppendFormat($" - {_chapterNameStringBuilder} - {_levelNameStringBuilder}");
                return;
            }
            _worldNameStringBuilder.AppendFormat($" - {_chapterNameStringBuilder} - {_levelNameStringBuilder} - {obstacleName}");
        }

        public static void LogEarnVirtualCurrency(string currencyName, float currencyValue)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEarnVirtualCurrency, currencyName, currencyValue);
        }

        public static void LogSpentVirtualCurrency(string currencyName, float currencyValue)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSpendVirtualCurrency, currencyName, currencyValue);
        }

        public static void LogShopMenuOpened()
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventBeginCheckout);
        }

        public static void LogInAppClicked(string productName)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAddToCart, "product", productName);
        }

        public static void LogSuccessfulInAppPurchase(string productName, string productValue)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPurchase, productName, productValue);
        }

        public static void LogAudioCheckboxEvent(bool value)
        {
            const string audio = "audio";
            Parameter[] parameters =
            {
                new Parameter(Parameters.ParameterCheckboxID, audio),
                new Parameter(Parameters.ParameterCheckboxValue, value.ToString())
            };
            FirebaseAnalytics.LogEvent(Events.EventCheckboxSelected, parameters);

            //unity
            AnalyticsEvent.Custom(Events.EventButtonClicked, new Dictionary<string, object>
            {
                {Parameters.ParameterCheckboxID, audio},
                {Parameters.ParameterCheckboxValue, value.ToString()},
            });
        }

        public static void LogOpenLinkEvent(SocialNetworks socialNetworks)
        {
            string value = string.Empty;
            switch (socialNetworks)
            {
                case SocialNetworks.Facebook:
                    value = "fb_like";
                    break;
                case SocialNetworks.Instagram:
                    value = "instagram_like";
                    break;
                case SocialNetworks.Twitter:
                    value = "twitter_follow";
                    break;
                case SocialNetworks.NativeShare:
                    value = "native_share";
                    break;
                case SocialNetworks.Discord:
                    value = "discord";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventShare, new Parameter(Parameters.ParameterButtonID, value));

            //unity
            AnalyticsEvent.SocialShare(ShareType.Invite, value);
        }

        public static void LogResetProgressEvent()
        {
            //firebase
            const string resetProgress = "reset_progress";
            FirebaseAnalytics.LogEvent(Events.EventButtonClicked, new Parameter(Parameters.ParameterButtonID, resetProgress));

            //unity
            AnalyticsEvent.Custom(Events.EventButtonClicked, new Dictionary<string, object>
            {
                {
                    Parameters.ParameterButtonID, resetProgress
                }
            });
        }

        public static void LogLikeDislikeEvent(int option)
        {
            switch (option)
            {
                case 1:
                    //firebase
                    const string LIKE = "like";
                    FirebaseAnalytics.LogEvent(Events.EventLikeDislikeMenuButtons, new Parameter(Parameters.ParameterButtonID, LIKE));

                    //unity
                    AnalyticsEvent.Custom(Events.EventLikeDislikeMenuButtons, new Dictionary<string, object>
                    {
                        {Parameters.ParameterButtonID, LIKE}
                    });
                    break;
                case 2:
                    //firebase
                    const string DISLIKE = "dislike";
                    FirebaseAnalytics.LogEvent(Events.EventLikeDislikeMenuButtons, new Parameter(Parameters.ParameterButtonID, DISLIKE));

                    //unity
                    AnalyticsEvent.Custom(Events.EventLikeDislikeMenuButtons, new Dictionary<string, object>
                    {
                        {Parameters.ParameterButtonID, DISLIKE}
                    });
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static void LogMainMenuEvent(string fromMenu, int currentLevel)
        {
            _levelNameStringBuilder = new StringBuilder(LEVEL_STRING);
            _levelNameStringBuilder.Append(currentLevel);
            Parameter[] parameters =
            {
                new Parameter(FirebaseAnalytics.ParameterLevelName, _levelNameStringBuilder.ToString()),
                new Parameter(Parameters.ParameterMenuID, fromMenu)
            };

            //firebase
            FirebaseAnalytics.LogEvent(Events.EventLevelToMainMenu, parameters);

            //unity
            AnalyticsEvent.LevelQuit(Events.EventLevelToMainMenu, new Dictionary<string, object>
            {
                {FirebaseAnalytics.ParameterLevelName, _levelNameStringBuilder.ToString()},
                {Parameters.ParameterMenuID, fromMenu}
            });
        }

        public static void LogLevelDifficultyEvent(string levelDifficulty)
        {
            // firebase
            FirebaseAnalytics.LogEvent(Events.EventLevelsDifficulty, FirebaseAnalytics.ParameterItemId, levelDifficulty);

            // unity
            AnalyticsEvent.Custom(Events.EventLevelsDifficulty, new Dictionary<string, object>
            {
                {FirebaseAnalytics.ParameterItemId, levelDifficulty}
            });
        }

        public static void LogVideoNotReadyEvent()
        {
            // firebase
            FirebaseAnalytics.LogEvent(Events.EventVideoNotReady);

            // unity
            AnalyticsEvent.Custom(Events.EventVideoNotReady);
        }

        public static void LogSignInEvent()
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin, FirebaseAnalytics.ParameterMethod, Parameters.ParameterSignInMethodGoogleGameServices);

            // unity
            AnalyticsEvent.UserSignup(AuthorizationNetwork.Google);
        }

        public static void LogSignOutEvent()
        {
            // firebase
            FirebaseAnalytics.LogEvent(Events.EventSignOut);

            // unity
            AnalyticsEvent.Custom(Events.EventSignOut);
        }

        public static void LogUnlockAchievementEvent(string achievementId)
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventUnlockAchievement, FirebaseAnalytics.ParameterAchievementId, achievementId);

            // unity
            AnalyticsEvent.AchievementUnlocked(achievementId);
        }

        public static void LogNewHighScoreEvent(float highScore)
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPostScore, FirebaseAnalytics.ParameterScore, highScore);

            // unity
            AnalyticsEvent.Custom(FirebaseAnalytics.EventPostScore, new Dictionary<string, object>
            {
                {FirebaseAnalytics.ParameterScore, highScore}
            });
        }

        public static void LogShieldsRefillEvent(int currentLevel)
        {
            if (currentLevel == 0)
            {
                _levelNameStringBuilder = new StringBuilder("Menu");
            }
            else
            {
                _levelNameStringBuilder = new StringBuilder(LEVEL_STRING);
                _levelNameStringBuilder.Append(currentLevel);
            }

            _parameterLevelName = new Parameter(FirebaseAnalytics.ParameterLevelName, _levelNameStringBuilder.ToString());

            // firebase
            FirebaseAnalytics.LogEvent(Events.EventShieldsRefill, _parameterLevelName);

            // unity
            AnalyticsEvent.Custom(Events.EventShieldsRefill, new Dictionary<string, object>
            {
                {FirebaseAnalytics.ParameterLevelName, _levelNameStringBuilder.ToString()}
            });
        }

        public static void LogClaimButtonClicked()
        {
            //firebase
            const string CLAIM_BUTTON = "claim_button";
            FirebaseAnalytics.LogEvent(Events.EventButtonClicked, new Parameter(Parameters.ParameterButtonID, CLAIM_BUTTON));

            //unity
            AnalyticsEvent.Custom(Events.EventButtonClicked, new Dictionary<string, object>
            {
                {Parameters.ParameterButtonID, CLAIM_BUTTON}
            });
        }

        public static void LogDoubleRewardButtonClicked()
        {
            //firebase
            const string DOUBLE_REWARD_BUTTON = "double_reward_button";
            FirebaseAnalytics.LogEvent(Events.EventButtonClicked, new Parameter(Parameters.ParameterButtonID, DOUBLE_REWARD_BUTTON));

            //unity
            AnalyticsEvent.Custom(Events.EventButtonClicked, new Dictionary<string, object>
            {
                {Parameters.ParameterButtonID, DOUBLE_REWARD_BUTTON}
            });
        }

        public static void LogWorldUnlocked(int currentWorld)
        {
            _worldNameStringBuilder = new StringBuilder(WORLD_STRING);
            _worldNameStringBuilder.Append(currentWorld);
            Parameter parameters = new Parameter(Parameters.ParameterWorldName, _worldNameStringBuilder.ToString());

            // firebase event
            FirebaseAnalytics.LogEvent(Events.EventWorldUnlocked, parameters);

            //unity analytics event
            AnalyticsEvent.Custom(Events.EventWorldUnlocked, new Dictionary<string, object>
            {
                {Parameters.ParameterWorldName, _worldNameStringBuilder.ToString()}
            });
        }

        public static void LogChapterUnlocked(int currentWorld, int currentChapter)
        {
            _worldNameStringBuilder = new StringBuilder(WORLD_STRING);
            _worldNameStringBuilder.Append(currentWorld);
            _chapterNameStringBuilder = new StringBuilder(CHAPTER_STRING);
            _chapterNameStringBuilder.Append(currentChapter);
            _worldNameStringBuilder.AppendFormat($" - {_chapterNameStringBuilder}");
            Parameter parameters = new Parameter(Parameters.ChapterUnlockedParameters, _worldNameStringBuilder.ToString());

            // firebase event
            FirebaseAnalytics.LogEvent(Events.EventChapterUnlocked, parameters);

            //unity analytics event
            AnalyticsEvent.Custom(Events.EventChapterUnlocked, new Dictionary<string, object>
            {
                {Parameters.ChapterUnlockedParameters, _worldNameStringBuilder.ToString()}
            });
        }

        public static void LogTutorialBegin()
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialBegin);

            // unity
            AnalyticsEvent.TutorialStart(FirebaseAnalytics.EventTutorialBegin);
        }

        public static void LogTutorialCompleted()
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialComplete);

            // unity
            AnalyticsEvent.TutorialComplete(FirebaseAnalytics.EventTutorialComplete);
        }

        public static void LogAppOpen()
        {
            // firebase
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAppOpen);

            // unity
            AnalyticsEvent.Custom(FirebaseAnalytics.EventAppOpen);
        }

        #endregion


        private static class Events
        {
            #region Properties

            public static string EventGameOver
            {
                get
                {
                    const string EVENT_GAME_OVER = "game_over";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_GAME_OVER : null;
                }
            }

            public static string EventLevelRestart
            {
                get
                {
                    const string EVENT_LEVEL_RESTART = "level_restart";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_LEVEL_RESTART : null;
                }
            }

            public static string EventLevelToMainMenu
            {
                get
                {
                    const string EVENT_LEVEL_TO_MAIN_MENU = "level_to_main_menu";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_LEVEL_TO_MAIN_MENU : null;
                }
            }

            public static string EventLikeDislikeMenuButtons
            {
                get
                {
                    const string EVENT_RATE_US_MENU_BUTTONS = "like_dislike_menu_buttons";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_RATE_US_MENU_BUTTONS : null;
                }
            }

            public static string EventButtonClicked
            {
                get
                {
                    const string EVENT_BUTTON_CLICKED = "button_clicked";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_BUTTON_CLICKED : null;
                }
            }

            public static string EventCheckboxSelected
            {
                get
                {
                    const string EVENT_CHECKBOX_SELECTED = "checkbox_selected";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_CHECKBOX_SELECTED : null;
                }
            }

            public static string EventLevelsDifficulty
            {
                get
                {
                    const string EVENT_LEVELS_DIFFICULTY = "levels_difficulty";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_LEVELS_DIFFICULTY : null;
                }
            }

            public static string EventVideoNotReady
            {
                get
                {
                    const string EVENT_VIDEO_NOT_READY = "video_not_ready";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_VIDEO_NOT_READY : null;
                }
            }

            public static string EventSignOut
            {
                get
                {
                    const string EVENT_SIGN_OUT = "sign_out";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_SIGN_OUT : null;
                }
            }

            public static string EventShieldsRefill
            {
                get
                {
                    const string EVENT_SHIELDS_REFILL = "shields_refill";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? EVENT_SHIELDS_REFILL : null;
                }
            }

            public static string EventWorldUnlocked
            {
                get
                {
                    const string WORLD_UNLOCKED = "world_unlocked";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? WORLD_UNLOCKED : null;
                }
            }

            public static string EventChapterUnlocked
            {
                get
                {
                    const string CHAPTER_UNLOCKED = "chapter_unlocked";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? CHAPTER_UNLOCKED : null;
                }
            }

            #endregion
        }

        private static class Parameters
        {
            #region Properties

            public static string ParameterObstacleName
            {
                get
                {
                    const string PARAMETER_OBSTACLE_NAME = "obstacle_name";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_OBSTACLE_NAME : null;
                }
            }

            public static string ParameterCheckboxID
            {
                get
                {
                    const string PARAMETER_CHECKBOX_ID = "checkbox_id";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_CHECKBOX_ID : null;
                }
            }

            public static string ParameterCheckboxValue
            {
                get
                {
                    const string PARAMETER_CHECKBOX_VALUE = "checkbox_value";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_CHECKBOX_VALUE : null;
                }
            }

            public static string ParameterButtonID
            {
                get
                {
                    const string PARAMETER_BUTTON_ID = "button_id";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_BUTTON_ID : null;
                }
            }

            public static string ParameterMenuID
            {
                get
                {
                    const string PARAMETER_MENU_ID = "menu_id";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_MENU_ID : null;
                }
            }

            public static string ParameterSignInMethodGoogleGameServices
            {
                get
                {
                    const string PARAMETER_SIGN_IN_METHOD = "google_game_services";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? PARAMETER_SIGN_IN_METHOD : null;
                }
            }

            public static string ParameterWorldName
            {
                get
                {
                    const string WORLD_NAME = "world_name";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? WORLD_NAME : null;
                }
            }

            public static string ParameterChapterName
            {
                get
                {
                    const string CHAPTER_NAME = "chapter_name";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? CHAPTER_NAME : null;
                }
            }

            public static string GameOverParameters
            {
                get
                {
                    const string GAME_OVER_PARAMETERS = "game_over_parameters";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? GAME_OVER_PARAMETERS : null;
                }
            }

            public static string LevelStartParameters
            {
                get
                {
                    const string LEVEL_START_PARAMETERS = "level_start_parameters";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? LEVEL_START_PARAMETERS : null;
                }
            }

            public static string LevelEndParameters
            {
                get
                {
                    const string LEVEL_END_PARAMETERS = "level_end_parameters";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? LEVEL_END_PARAMETERS : null;
                }
            }

            public static string ChapterUnlockedParameters
            {
                get
                {
                    const string CHAPTER_UNLOCKED_PARAMETERS = "chapter_unlocked_parameters";
                    return FirebaseClient.Instance.IsFirebaseReady.Value ? CHAPTER_UNLOCKED_PARAMETERS : null;
                }
            }

            #endregion
        }
    }
}