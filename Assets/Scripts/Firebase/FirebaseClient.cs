﻿using System;
using System.Collections;
using Firebase;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.Analytics
{
    /// <summary>
    /// Singleton klasa koja kontroliše Firebase API
    /// </summary>
    public sealed class FirebaseClient
    {
        #region Private Fields

        private static readonly FirebaseClient _instance = new FirebaseClient();

        #endregion
        
        
        
        
        
        #region Public Fields

        public static FirebaseClient Instance => _instance;
        public ReactiveProperty<bool> IsFirebaseReady { get; private set; }
        public FirebaseApp FirebaseApp { get; private set; }

        #endregion





        #region Contructor

        private FirebaseClient()
        {
            IsFirebaseReady = new ReactiveProperty<bool>(false);
        }

        public IEnumerator Init()
        {
            // firebase google play services inspecting
            IsFirebaseReady.Value = false;
            try
            {
                FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
                {
                    var dependencyStatus = task.Result;
                    if (dependencyStatus == DependencyStatus.Available)
                    {
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
                        Logging.Log("Firebase dependencies are all resolved!");
#endif
                        FirebaseApp = FirebaseApp.DefaultInstance;
                        IsFirebaseReady.Value = true;
                    }
                    else
                    {
                        Logging.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                        // Firebase Unity SDK is not safe to use here.
                    }
                });
            }
            catch (Exception e)
            {
                Logging.LogError($"Greska prilikom inicijalizacije Firebase-a: {e.Message}");
                throw;
            }
            
            yield return new WaitUntil(() => IsFirebaseReady.Value);
        }

        #endregion
    }
}