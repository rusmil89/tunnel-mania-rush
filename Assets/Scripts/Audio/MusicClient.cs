﻿using System.Collections;
using System.Linq;
using Core.Events;
using Tunnel.Controllers;
using Tunnel.LevelLoad;
using Tunnel.Tutorial;
using Tunnel.Utilities;
using UniRx;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;
using Zenject;

namespace Tunnel.Audio
{
    /// <summary>
    /// SCENE: Menu, Level
    /// GAME_OBJECT: MusicAudioSource
    /// DESCRIPTION: Klasa koja kontroliše pustanje muzike u celoj igri
    /// </summary>
    public sealed class MusicClient : MonoBehaviour
    {
        #region private fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private float fadeInVolume;
        
        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private AudioData _audioData;
        private global::PlayerData _playerData;
        private int _randomMenuMusicTheme;
        private int _randomGameMusicTheme;
        private AudioSource _audioSource;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float AUDIO_FADE_EFFECT_DURATION = 0.5f;
        private const float FINISH_MENU_FADE_EFFECT_DURATION = 0.1f;
        private const float FINISH_MENU_VOLUME = 0.05f;
        private const float TUTORIAL_MENU_FADE_EFFECT_DURATION = 1f;
        private const float TUTORIAL_MENU_VOLUME = 0.05f;
        private const string MENU_SCENE_NAME = Constants.Scenes.MENU_SCENE_NAME;
        private const string LEVEL_SCENE_NAME = Constants.Scenes.LEVEL_SCENE_NAME;

        #endregion
        
        


        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _audioData = AssetDatabase.FindAssets(Constants.Editor.Filters.AUDIO_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => AssetDatabase.LoadAssetAtPath<AudioData>(AssetDatabase.GUIDToAssetPath(s)))
                .First();
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        [Inject]
        private void InjectDependencies(AudioData audioData, global::PlayerData playerData)
        {
            _audioData = audioData;
            _playerData = playerData;
        }

        private void OnEnable()
        {
            RegisterObservableListeners();
            SceneManager.sceneLoaded += HandleOnSceneLoaded;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= HandleOnSceneLoaded;
        }

        /// <summary>
        /// Prijavljivanje osluškivača na observable streamove
        /// WHY: Da bi klasa mogla adekvatno da odgovori nakon odigranog događaja
        /// </summary>
        private void RegisterObservableListeners()
        {
            _playerData.PersistentData.IsAudioEnabled
                .Where(audioEnabled => audioEnabled && SceneManager.GetActiveScene().name.Equals(MENU_SCENE_NAME))
                .Subscribe(_ => PlayMusicTheme(true))
                .AddTo(this);
            _playerData.PersistentData.IsAudioEnabled
                .Where(audioEnabled => audioEnabled && SceneManager.GetActiveScene().name.Equals(LEVEL_SCENE_NAME))
                .Subscribe(_ => PlayMusicTheme())
                .AddTo(this);
            _playerData.PersistentData.IsAudioEnabled
                .Where(audioEnabled => !audioEnabled)
                .Subscribe(_ => _audioSource.Stop())
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfLevel>()
                .Subscribe(_ => StartCoroutine(FadeInAndOut(FINISH_MENU_FADE_EFFECT_DURATION, FINISH_MENU_VOLUME)))
                .AddTo(this);
            MessageBroker.Default.Receive<FinishMenuContinueButtonClicked>()
                .Subscribe(_ =>
                    {
                        StopAllCoroutines();
                        StartCoroutine(FadeInAndOut(FINISH_MENU_FADE_EFFECT_DURATION, fadeInVolume));
                    })
                .AddTo(this);
            MessageBroker.Default.Receive<LevelStarted>()
                .Where(_ => _playerData.PersistentData.CurrentLevel.Value == 1)
                .Subscribe(_ => HandleLevelStartedEvent())
                .AddTo(this);

            if (_playerData.PersistentData.IsTutorialPlayed.Value)
            {
                MessageBroker.Default.Receive<PlayerDied>()
                    .Subscribe(OnPlayerDiedEventHandler)
                    .AddTo(this);
                MessageBroker.Default.Receive<PlayerRevived>()
                    .Subscribe(PlayerRevivedEventHandler)
                    .AddTo(this);
            }
            else
            {
                MessageBroker.Default.Receive<TutorialFinished>()
                    .Subscribe(HandleTutorialFinished)
                    .AddTo(this);
            }
        }

        #endregion


        #region Custom methods

        private void HandleOnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            _randomMenuMusicTheme = _randomGameMusicTheme = 0;
            if (scene.name.Equals(MENU_SCENE_NAME))
            {
                _randomMenuMusicTheme = Random.Range(0, _audioData.MenuMusicTheme.Length);
                PlayMusicTheme(true);
            }
            else if (scene.name.Equals(LEVEL_SCENE_NAME) || scene.name.Equals(Constants.Scenes.TUTORIAL_SCENE_NAME))
            {
                _randomGameMusicTheme = Random.Range(0, _audioData.GameMusicTheme.Length);
                PlayMusicTheme();
            }
            MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInAndOut(AUDIO_FADE_EFFECT_DURATION, fadeInVolume));
        }

        /// <summary>
        /// Metoda koja pusta odgovarajuću muziku u zavisnosti od toga u kojoj se sceni nalazi igrač
        /// WHY: Zato što na taj način dajemo raznovrsnost igraču
        /// </summary>
        /// <param name="isMenu">Parametar za određivanje muzičke teme koja će biti pokrenuta</param>
        public void PlayMusicTheme(bool isMenu = false)
        {
            if (!_playerData.PersistentData.IsAudioEnabled.Value) return;
            
            if (isMenu)
            {
                if (_randomMenuMusicTheme < _audioData.MenuMusicTheme.Length - 1)
                {
                    ++_randomMenuMusicTheme;
                    _audioSource.clip = _audioData.MenuMusicTheme[_randomMenuMusicTheme];
                }
                else
                {
                    _randomMenuMusicTheme = 0;
                    _audioSource.clip = _audioData.MenuMusicTheme[_randomMenuMusicTheme];
                }
            }
            else
            {
                if (_randomGameMusicTheme < _audioData.GameMusicTheme.Length - 1)
                {
                    ++_randomGameMusicTheme;
                    _audioSource.clip = _audioData.GameMusicTheme[_randomGameMusicTheme];
                }
                else
                {
                    _randomGameMusicTheme = 0;
                    _audioSource.clip = _audioData.GameMusicTheme[_randomGameMusicTheme];
                }
            }

            _audioSource.loop = true;
            _audioSource.Play();
        }

        /// <summary>
        /// Korutina koja simulira efekat fade in i fade out, odnosno postepenog pojačavanja i stišavanja muzike
        /// WHY: Bolji game feel
        /// </summary>
        /// <param name="duration">Parametar kojim definišemo koliko će taj efekat da traje</param>
        /// <param name="volume">Parametar kojim definišemo jačinu zvuka</param>
        /// <returns></returns>
        private IEnumerator FadeInAndOut(float duration, float volume)
        {
            float currentTime = 0;
            float start = _audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                _audioSource.volume = Mathf.Lerp(start, volume, currentTime / duration);
                yield return null;
            }
        }

        /// <summary>
        /// Metoda koja kontroliše pustanje muzike nakon što igrač ponovo krene da igra posle pogibije
        /// </summary>
        public void PlayMusicAfterDeath()
        {
            _audioSource.volume = 0f;
            MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInAndOut(0.5f, fadeInVolume));
            _audioSource.Play();
        }

        #endregion


        #region Event Listeners

        /// <summary>
        /// Metoda koja osluškuje događaj koji se okida kada počne neki novi level, nebitno da li je to prvi put ili u toku igranja nakon uspešnog završetka prethodnog levela
        /// WHY: Zato što na svakom 4. levelu menjamo muzičku temu zbog raznovrsnosti i game feela
        /// </summary>
        private void HandleLevelStartedEvent()
        {
            if (_playerData.PersistentData.CurrentLevel.Value % 2 != 0) return;
            _audioSource.volume = 0f;
            MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInAndOut(0.5f, fadeInVolume));
            PlayMusicTheme();
        }

        /// <summary>
        /// Metoda koja osluškuje događaj koji se okida kada korisnik pogine
        /// WHY: Zato što tada gasimo muziku
        /// </summary>
        /// <param name="obj">Parametri koji se prosleđuju zajedno sa događajem</param>
        private void OnPlayerDiedEventHandler(PlayerDied playerDied)
        {
            MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInAndOut(0.5f, 0f));
        }

        /// <summary>
        /// Metoda koja osluškuje događaj koji se okida svaki put kada igrač oživi
        /// WHY: Zato što tada treba pustiti muziku ponovo
        /// </summary>
        //TODO: proveriti da li je ovaj event uopšte i potreban ili se može iskoristiti game pause (resume)
        private void PlayerRevivedEventHandler(PlayerRevived playerRevived)
        {
            PlayMusicAfterDeath();
        }

        private void HandleTutorialFinished(TutorialFinished tutorialFinished)
        {
            StartCoroutine(FadeInAndOut(TUTORIAL_MENU_FADE_EFFECT_DURATION, TUTORIAL_MENU_VOLUME));
        }

        #endregion
    }
}