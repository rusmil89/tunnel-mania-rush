﻿using System;
using System.Linq;
using Core.Events;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.GooglePlayGameServices;
using Tunnel.Tutorial;
using Tunnel.UI;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using Utilities;
using Zenject;

namespace Tunnel.Audio
{
    /// <summary>
    /// SCENE: Menu, Level
    /// GAME_OBJECT: SoundAudioSource
    /// DESCRIPTION: Klasa koja (review)???
    /// </summary>
    public sealed class SoundClient : MonoBehaviour
    {
        #region private fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private AudioData _audioData;
        private global::PlayerData _playerData;
        private AudioSource _audioSource;
        
        /// <summary>
        /// Consts
        /// </summary>
        private const float FULL_VOLUME = 1F;
        private const float HALF_VOLUME = 0.5F;

        #endregion


        
        
        
        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _audioData = UnityEditor.AssetDatabase.FindAssets(Constants.Editor.Filters.AUDIO_DATA, new []{Constants.Editor.Paths.SCRIPTABLE_OBJECTS})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioData>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First();
            ReferencesClient.CheckReferences(this, gameObject);
        }
#endif

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        [Inject]
        private void InjectDependencies(AudioData audioData, global::PlayerData playerData)
        {
            _audioData = audioData;
            _playerData = playerData;
        }

        private void Start()
        {
            RegisterTutorialObservableListeners();
            RegisterObservableListeners();
        }

        private void RegisterTutorialObservableListeners()
        {
            if (_playerData.PersistentData.IsTutorialPlayed.Value) return;
            MessageBroker.Default.Receive<MissionCompletedAnimationsPlaying>()
                .Subscribe(_ => PlaySFX(_audioData.TutorialMissionCompleted))
                .AddTo(this);
            MessageBroker.Default.Receive<RibbonBlinkingAnimationPlaying>()
                .Subscribe(_ => PlaySFX(_audioData.TutorialRibbonBlinking))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTappedOnLeft>()
                .Subscribe(_ => PlaySFX(_audioData.Achievement))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTappedOnRight>()
                .Subscribe(_ => PlaySFX(_audioData.Achievement))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTiltedOnLeft>()
                .Subscribe(_ => PlaySFX(_audioData.Achievement))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerTiltedOnRight>()
                .Subscribe(_ => PlaySFX(_audioData.Achievement))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerAvoidedObstacle>()
                .Subscribe(list => PlaySFX(_audioData.Achievement))
                .AddTo(this);
            MessageBroker.Default.Receive<TutorialMenuClaimButtonClicked>()
                .Subscribe(_ => PlaySFX(_audioData.FinishMenuAchievement))
                .AddTo(this);
            MessageBroker.Default.Receive<RewardMenuInTutorialCalled>()
                .Subscribe(_ => PlaySFX(_audioData.FinishMenuProgress))
                .AddTo(this);
        }

        /// <summary>
        /// Prijavljivanje osluškivača na observable stream-ove
        /// WHY: Da bi klasa mogla adekvatno da odgovori nakon odigranog događaja
        /// </summary>
        private void RegisterObservableListeners()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            MessageBroker.Default.Receive<AchievementUnlocked>()
                .Subscribe(_ => PlaySFX(_audioData.Achievement))
                .AddTo(this);
#endif
            MessageBroker.Default.Receive<WorldScrollSnapSelectionPageChanged>()
                .Subscribe(_ => PlaySFX(_audioData.PlayerPassedTheObstacle))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterScrollSnapSelectionPageChanged>()
                .Subscribe(_ => PlaySFX(_audioData.TutorialMissionCompleted))
                .AddTo(this);
            MessageBroker.Default.Receive<MenuIsShown>()
                .Subscribe(_ => PlaySFX(_audioData.ShowMenu, HALF_VOLUME))
                .AddTo(this);
            MessageBroker.Default.Receive<MenuIsHidden>()
                .Subscribe(_ => PlaySFX(_audioData.HideMenu, HALF_VOLUME))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(_ => PlaySFX(_audioData.PlayerDeath))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(_ => PlaySFX(_audioData.PlayerDeath))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfLevel>()
                .Subscribe(_ => PlaySFX(_audioData.FinishMenuAchievement))
                .AddTo(this);
            MessageBroker.Default.Receive<PlayerReachedEndOfTube>()
                .TakeEvery(2)
                .Subscribe(_ => PlaySFX(_audioData.EndOfTubePass))
                .AddTo(this);
            var playerAvoidedObstacleStream = MessageBroker.Default.Receive<PlayerBypassedObstacle>();
            playerAvoidedObstacleStream.Buffer(playerAvoidedObstacleStream.Throttle(TimeSpan.FromMilliseconds(100f)))
                .Subscribe(_ => PlaySFX(_audioData.PlayerPassedTheObstacle))
                .AddTo(this);
            MessageBroker.Default.Receive<FinishMenuContinueButtonClicked>()
                .Subscribe(_ => PlayButtonClick())
                .AddTo(this);
            MessageBroker.Default.Receive<ProgressAnimationFinished>()
                .Subscribe(_ => PlaySFX(_audioData.FinishMenuProgress))
                .AddTo(this);
            MessageBroker.Default.Receive<GameOverOneSecondFrame>()
                .Subscribe(_ => PlaySFX(_audioData.Countdown))
                .AddTo(this);
            MessageBroker.Default.Receive<GameOverTimerFinished>()
                .Subscribe(_ => PlaySFX(_audioData.CountdownGo))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesAnimationStarted)
                .Select(eventType => eventType as CurrenciesAnimationStarted)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Coin)
                .Subscribe(_ => PlaySFX(_audioData.Coins))
                .AddTo(this);
            MessageBroker.Default.Receive<CurrenciesAddingToTopMenuAnimation>()
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Coin)
                .Subscribe(_ => PlaySFX(_audioData.CoinsIncreasing))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesAnimationStarted)
                .Select(eventType => eventType as CurrenciesAnimationStarted)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Token)
                .Subscribe(_ => PlaySFX(_audioData.Tokens))
                .AddTo(this);
            MessageBroker.Default.Receive<CurrenciesAddingToTopMenuAnimation>()
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Token)
                .Subscribe(_ => PlaySFX(_audioData.TokensIncreasing))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesAnimationStarted)
                .Select(eventType => eventType as CurrenciesAnimationStarted)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Shield)
                .Subscribe(_ => PlaySFX(_audioData.Shields))
                .AddTo(this);
            MessageBroker.Default.Receive<CurrenciesAddingToTopMenuAnimation>()
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Shield)
                .Subscribe(_ => PlaySFX(_audioData.ShieldsIncreasing))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesAnimationStarted)
                .Select(eventType => eventType as CurrenciesAnimationStarted)
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Energy)
                .Subscribe(_ => PlaySFX(_audioData.Energies))
                .AddTo(this);
            MessageBroker.Default.Receive<CurrenciesAddingToTopMenuAnimation>()
                .Where(eventType => eventType.CurrencyType == Constants.CurrencyType.Energy)
                .Subscribe(_ => PlaySFX(_audioData.EnergiesIncreasing))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuStartedSpinningWheel>()
                .Subscribe(_ => PlaySFX(_audioData.SpinTheWheel))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuFinishedSpinningWheel>()
                .Subscribe(_ => PlaySFX(_audioData.FinishMenuAchievement))
                .AddTo(this);
            MessageBroker.Default.Receive<ChapterClearedMenuPresentRotated>()
                .TakeUntil(MessageBroker.Default.Receive<AnimateEnablingSpinTheWheel>())
                .RepeatSafe()
                .Subscribe(_ => PlaySFX(_audioData.ChapterClearedPresentShaking))
                .AddTo(this);
            MessageBroker.Default.Receive<object>()
                .Where(eventType => eventType is CurrenciesRemovingFromTopMenu)
                .Subscribe(_ => PlaySFX(_audioData.CurrencyDecreasing))
                .AddTo(this);
            MessageBroker.Default.Receive<TutorialMenuClaimButtonClicked>()
                .Subscribe(_ => PlayButtonClick())
                .AddTo(this);
            MessageBroker.Default.Receive<CountdownFired>()
                .Subscribe(_ => PlaySFX(_audioData.Countdown))
                .AddTo(this);
            MessageBroker.Default.Receive<CountdownGoFired>()
                .Subscribe(_ => PlaySFX(_audioData.CountdownGo))
                .AddTo(this);
            MessageBroker.Default.Receive<ButtonClicked>()
                .Subscribe(_ => PlayButtonClick())
                .AddTo(this);
        }

        #endregion

        
        
        

        #region Custom methods

        /// <summary>
        /// Univerzalna metoda koja kontroliše puštanje zvuka koji se prosledi kao parametar
        /// </summary>
        /// <param name="sfx">Zvuk koji treba pustiti</param>
        /// <param name="volume">Jacina zvuka</param>
        private void PlaySFX(AudioClip sfx, float volume = FULL_VOLUME)
        {
            if (!_playerData.PersistentData.IsAudioEnabled.Value) return;
            if (_audioSource == null) return;

            _audioSource.volume = volume;
            _audioSource.clip = sfx;
            _audioSource.PlayOneShot(_audioSource.clip);
        }

        public void PlayButtonClick()
        {
            if (!_playerData.PersistentData.IsAudioEnabled.Value) return;
            if (_audioSource == null) return;

            _audioSource.clip = _audioData.ButtonClick;
            _audioSource.Play();
        }

        #endregion
    }
}