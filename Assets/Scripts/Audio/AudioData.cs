﻿using System;
using System.Linq;
using Core.Events;
using Tunnel.Animations;
using Tunnel.Controllers;
using Tunnel.GooglePlayGameServices;
using Tunnel.Tutorial;
using Tunnel.UI;
using Tunnel.Utilities;
using UniRx;
using UnityEngine;
using Utilities;
using Zenject;

namespace Tunnel.Audio
{
    /// <summary> 
    /// DESCRIPTION: Skriptabilni objekat koji čuva reference na zvukove i muziku (review) i kontroliše puštanje potrebnih zvukova  
    /// </summary>
    [CreateAssetMenu(fileName = "AudioData", menuName = "ScriptableObjects/Audio/AudioData", order = 0)]
    public class AudioData : ScriptableObjectInstaller
    {
        #region private fields

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private const string AUDIO_CLIP = Constants.Editor.Filters.AUDIO_CLIP;
        private const string AUDIO_PATH = Constants.Editor.Paths.AUDIO;
#endif

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Player Data")] 
        [SerializeField] private global::PlayerData _playerData;

        [Header("Music")] 
        [SerializeField] private AudioClip[] _menuMusicTheme;
        [SerializeField] private AudioClip[] _gameMusicTheme;

        [Header("SFX")] 
        [SerializeField] private AudioClip _buttonClick;
        [SerializeField] private AudioClip _playerDeath;
        [SerializeField] private AudioClip _highScore;
        [SerializeField] private AudioClip _countdown;
        [SerializeField] private AudioClip _countdownGo;
        [SerializeField] private AudioClip _achievement;
        [SerializeField] private AudioClip _tutorialMissionCompleted;
        [SerializeField] private AudioClip _tutorialRibbonBlinking;
        [SerializeField] private AudioClip _finishMenuProgress;
        [SerializeField] private AudioClip _finishMenuAchievement;
        [SerializeField] private AudioClip _endOfTubePass;
        [SerializeField] private AudioClip _playerPassedTheObstacle;
        [SerializeField] private AudioClip _showMenu;
        [SerializeField] private AudioClip _hideMenu;
        [SerializeField] private AudioClip _coins;
        [SerializeField] private AudioClip _coinsIncreasing;
        [SerializeField] private AudioClip _tokens;
        [SerializeField] private AudioClip _tokensIncreasing;
        [SerializeField] private AudioClip _currencyDecreasing;
        [SerializeField] private AudioClip _shields;
        [SerializeField] private AudioClip _shieldsIncreasing;
        [SerializeField] private AudioClip _energies;
        [SerializeField] private AudioClip _energiesIncreasing;
        [SerializeField] private AudioClip _spinTheWheel;
        [SerializeField] private AudioClip _chapterClearedPresentShaking;
        
        #endregion

        
        
        

        #region properties

        public AudioClip[] MenuMusicTheme => _menuMusicTheme;

        public AudioClip[] GameMusicTheme => _gameMusicTheme;

        public AudioClip ButtonClick => _buttonClick;

        public AudioClip PlayerDeath => _playerDeath;

        public AudioClip HighScore => _highScore;

        public AudioClip Countdown => _countdown;

        public AudioClip CountdownGo => _countdownGo;

        public AudioClip Achievement => _achievement;

        public AudioClip TutorialMissionCompleted => _tutorialMissionCompleted;

        public AudioClip TutorialRibbonBlinking => _tutorialRibbonBlinking;

        public AudioClip FinishMenuProgress => _finishMenuProgress;

        public AudioClip FinishMenuAchievement => _finishMenuAchievement;

        public AudioClip EndOfTubePass => _endOfTubePass;

        public AudioClip PlayerPassedTheObstacle => _playerPassedTheObstacle;

        public AudioClip ShowMenu => _showMenu;

        public AudioClip HideMenu => _hideMenu;

        public AudioClip Coins => _coins;

        public AudioClip CoinsIncreasing => _coinsIncreasing;

        public AudioClip Tokens => _tokens;

        public AudioClip TokensIncreasing => _tokensIncreasing;

        public AudioClip CurrencyDecreasing => _currencyDecreasing;

        public AudioClip Shields => _shields;

        public AudioClip ShieldsIncreasing => _shieldsIncreasing;

        public AudioClip Energies => _energies;

        public AudioClip EnergiesIncreasing => _energiesIncreasing;

        public AudioClip SpinTheWheel => _spinTheWheel;

        public AudioClip ChapterClearedPresentShaking => _chapterClearedPresentShaking;

        #endregion

        
        
        

        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        private void OnValidate()
        {
            _menuMusicTheme =  UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new[] {Constants.Editor.Paths.MENU_MUSIC})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToArray();
            _gameMusicTheme =  UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new[] {Constants.Editor.Paths.GAME_MUSIC})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .ToArray();
            _buttonClick = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("button_click"));
            _playerDeath = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("player_death"));
            _highScore = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("high_score"));
            _countdown = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("countdown"));
            _countdownGo = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("countdown_go"));
            _achievement = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("achievement"));
            _tutorialMissionCompleted = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("mission_completed"));
            _tutorialRibbonBlinking = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("ribbon_blinking"));
            _finishMenuProgress = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("finish_menu_progress"));
            _finishMenuAchievement = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("finish_menu_achievement"));
            _endOfTubePass = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("end_of_tube_pass"));
            _playerPassedTheObstacle = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("player_passed_the_obstacle"));
            _showMenu = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("show_menu"));
            _hideMenu = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("hide_menu"));
            _coins = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("coins"));
            _coinsIncreasing = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("increasing_coins"));
            _tokens = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("tokens"));
            _tokensIncreasing = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("increasing_tokens"));
            _currencyDecreasing = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("currency_decreasing"));
            _shields = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("shields"));
            _shieldsIncreasing = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("increasing_shields"));
            _energies = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("energy"));
            _energiesIncreasing = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("increasing_energy"));
            _spinTheWheel = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("spin_the_wheel"));
            _chapterClearedPresentShaking = UnityEditor.AssetDatabase.FindAssets(AUDIO_CLIP, new []{AUDIO_PATH})
                .Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<AudioClip>(UnityEditor.AssetDatabase.GUIDToAssetPath(s)))
                .First(x => x.name.Equals("chapter_cleared_present_shaking"));
            
            ReferencesClient.CheckReferences(this);
        }
#endif

        public override void InstallBindings()
        {
            Container.BindInstance(this);
        }

        #endregion
    }
}