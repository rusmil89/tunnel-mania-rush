﻿using System;
using Cysharp.Threading.Tasks;
using Tunnel.Controllers;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.PlayerData
{
    /// <summary>
    /// SCENE: /
    /// GAME_OBJECT: /
    /// DESCRIPTION: Klasa koja je zadužena za čuvanje podataka bitnih za igrača i njegov napredak kroz igru
    /// </summary>
    [Serializable]
    public sealed class PersistentData
    {
        #region Enums

        public enum Difficulty
        {
            Easy,
            Medium,
            Hard
        }

        public enum LikeDislikeOptions
        {
            NotChosen,
            Like,
            Dislike
        }

        public enum ChapterState
        {
            Locked,
            IsReadyForUnlock,
            Unlocked
        }

        public enum WorldState
        {
            Locked,
            Unlocked
        }

        public enum EDailyRewardState
        {
            NotCollected,
            Collected
        }

        #endregion
        
        
        
        
        
        #region Private Fields

        /// <summary>
        /// Serializable fields
        /// </summary>
        [Header("Enums")]
        [SerializeField] private Difficulty _difficultyOption = Difficulty.Medium;
        [SerializeField] private LikeDislikeOptions _likeDislikeOption = LikeDislikeOptions.NotChosen;

        [Header("Integers")] 
        [SerializeField] private int _numberOfAppStarts;
        public IntReactiveProperty CurrentWorld = new IntReactiveProperty(1);
        public IntReactiveProperty CurrentLevel = new IntReactiveProperty(1);
        public IntReactiveProperty BestScoreChapter = new IntReactiveProperty(1);
        public IntReactiveProperty BestScoreLevel = new IntReactiveProperty(1);
        public IntReactiveProperty NumberOfMenuStarts = new IntReactiveProperty(0);
        public IntReactiveProperty NumberOfDeaths = new IntReactiveProperty(0);

        [Header("Floats")]
        public FloatReactiveProperty PlayerMaxRecordedDistance = new FloatReactiveProperty(0f);
        
        [Header("Gyroscope")]
        public FloatReactiveProperty GyroscopeSensitivity = new FloatReactiveProperty(0.1f);
        public FloatReactiveProperty RotationAccelerationFactor = new FloatReactiveProperty(1000f);

        [Header("Energy")]
        public IntReactiveProperty CurrentEnergy = new IntReactiveProperty(15);
        public ReactiveProperty<DateTime> TimeSinceLastRefill = new ReactiveProperty<DateTime>(DateTime.Now);

        [Header("Currency")]
        public IntReactiveProperty Coins = new IntReactiveProperty(0);
        public IntReactiveProperty Tokens = new IntReactiveProperty(0);
        public IntReactiveProperty Shields = new IntReactiveProperty(0);
        public ReactiveProperty<DateTime> BuyCoinsForAdsLastAttemptDay = new ReactiveProperty<DateTime>(DateTime.Today.AddDays(-1));
        public ReactiveProperty<int> BuyCoinsForAdsDailyAttempts = new ReactiveProperty<int>(3);
        
        [Header("Daily Rewards")]
        public ReactiveProperty<bool> IsTodayRewardCollected = new ReactiveProperty<bool>(false);
        public ReactiveCollection<EDailyRewardState> DailyRewardStates = new ReactiveCollection<EDailyRewardState>
        {
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected,
            EDailyRewardState.NotCollected
        };

        [Header("Booleans")]
        public BoolReactiveProperty IsTutorialPlayed = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsAudioEnabled = new BoolReactiveProperty(true);
        public BoolReactiveProperty AreNotificationsEnabled = new BoolReactiveProperty(true);
        public BoolReactiveProperty IsVibrationEnabled = new BoolReactiveProperty(true);
        public BoolReactiveProperty IsGDPREnabled = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsPlayerSignedInGoogleGameServices = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsPlayerManuallySignedOut = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsGyroscopeEnabled = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsFacebookLiked = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsTwitterFollowed = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsInstagramFollowed = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsDiscordJoined = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsApplicationRated = new BoolReactiveProperty(false);
        public BoolReactiveProperty IsFeedbackSubmitted = new BoolReactiveProperty(false);

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private IDisposable _disposable;
        public ReactiveProperty<DateTime> DataSaveTime = new ReactiveProperty<DateTime>(DateTime.Today);
        public ReactiveCollection<WorldState> WorldProgress = new ReactiveCollection<WorldState>
        {
            WorldState.Unlocked, 
            WorldState.Locked, 
            WorldState.Locked, 
            WorldState.Locked
        };
    
        public ReactiveDictionary<int, ReactiveCollection<ChapterState>> ChaptersProgress = new ReactiveDictionary<int, ReactiveCollection<ChapterState>>
        {
            {1, new ReactiveCollection<ChapterState>{ChapterState.Unlocked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked}},
            {2, new ReactiveCollection<ChapterState>{ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked}},
            {3, new ReactiveCollection<ChapterState>{ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked}},
            {4, new ReactiveCollection<ChapterState>{ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked, ChapterState.Locked}}
        };
    
        public ReactiveDictionary<int, int> CurrentChapterInWorld = new ReactiveDictionary<int, int>
        {
            {1, 1},
            {2, 1},
            {3, 1},
            {4, 1}
        };

        #endregion





        #region Properties
    
        // enums
        public Difficulty DifficultyOption
        {
            get => _difficultyOption;
            set => _difficultyOption = value;
        }

        public LikeDislikeOptions LikeDislikeOption
        {
            get => _likeDislikeOption;
            set => _likeDislikeOption = value;
        }

        // integers

        public int NumberOfAppStarts
        {
            get => _numberOfAppStarts;
            set
            {
                if (value >= 0)
                {
                    var previousValue = _numberOfAppStarts;
                    _numberOfAppStarts = value;
              
                    if (previousValue == _numberOfAppStarts) return;
                    try
                    {
#if UNITY_ANDROID && !UNITY_EDITOR
                        _disposable = Observable.FromCoroutine(Analytics.FirebaseClient.Instance.Init)
                            .SelectMany(RemoteConfigClient.Instance.Init(this).ToCoroutine())
                            .SelectMany(GooglePlayGameServices.GooglePlayGameServicesClient.Instance.Init(this))
                            .SelectMany(EasyMobileProClient.Instance.Init)
                            .SelectMany(Ads.AdsClient.Instance.Init(this))
                            .SelectMany(NotificationsClient.Instance.Init(this))
                            .SelectMany(GoogleReviewClient.Instance.Init)
                            .Subscribe();
#elif UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
                        RemoteConfigClient.Instance.Init(this).ToCoroutine();
#endif
                    }
                    catch (Exception e)
                    {
                        Logging.LogError($"Greska u inicijalizaciji kontrolera: {e.Message}");
                        throw;
                    }
                }
                else
                {
                    throw new Exception("NumberOfAppStarts ne sme biti manji od 0");
                }
            }
        }

        // floats

        public float PlayTime { get; set; }
        
        #endregion

        public void UnregisterEventListeners()
        {
            Ads.AdsClient.Instance.UnregisterListeners();
            GooglePlayGameServices.GooglePlayGameServicesClient.Instance.UnregisterEventListeners();
            GoogleReviewClient.Instance.UnregisterEventListeners();
            RemoteConfigClient.Instance.UnregisterEventListeners();
            NotificationsClient.Instance.UnregisterEventListeners();
            _disposable.Dispose();
        }
    }
}
