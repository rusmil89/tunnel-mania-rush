﻿using System;
using Tunnel.Controllers;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;
using Utilities;

namespace Tunnel.PlayerData
{
    /// <summary>
    /// SCENE: Splash
    /// GAME_OBJECT: Controllers
    /// Klasa koja kontroliše serijalizaciju i deserijalizaciju podataka u JSON fajl koji se čuvaju
    /// </summary>
    public sealed class PlayerDataClient : MonoBehaviour
    {
        #region Private Fields

        private static PlayerDataClient _instance;
        
        /// <summary>
        /// serialized fileds
        /// </summary>
        [SerializeField] private global::PlayerData playerDataControllerSO;
        [SerializeField] private GameData gameController;
        [SerializeField] private bool save = true;
        
        /// <summary>
        /// consts
        /// </summary>
        private const string PLAYER_DATA = "PersistentData";

        #endregion





        #region Monobehaviour Events

        private void Awake()
        {
            // singleton
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                save = false;
                return;
            }

            _instance = this;
            DontDestroyOnLoad(gameObject);
            
            // ostala logika
            AssertReferences();
            LoadSavedPlayerData();
            IncreaseTheNumberOfAppStarts();
            RegisterObservableListeners();
        }

        private void AssertReferences()
        {
            Assert.IsNotNull(playerDataControllerSO);
            Assert.IsNotNull(gameController);
        }

        private void LoadSavedPlayerData()
        {
            if (PlayerPrefs.HasKey(PLAYER_DATA))
            {
                playerDataControllerSO.PersistentData = JsonHelper.Deserialize<global::Tunnel.PlayerData.PersistentData>(PlayerPrefs.GetString(PLAYER_DATA));
            }
        }

        private void IncreaseTheNumberOfAppStarts()
        {
            playerDataControllerSO.PersistentData.NumberOfAppStarts++;
        }

        private void RegisterObservableListeners()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            playerDataControllerSO.PersistentData.ChaptersProgress
                .ObserveReplace()
                .Subscribe(_ => Analytics.WarmAppGamesAnalytics.LogChapterUnlocked(playerDataControllerSO.PersistentData.CurrentWorld.Value, playerDataControllerSO.PersistentData.CurrentChapterInWorld[playerDataControllerSO.PersistentData.CurrentWorld.Value]))
                .AddTo(this);
#endif
        }

#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            IncreaseThePlayTime();
            SetDataSaveTime();
            Save();
        }
    }

    private void OnApplicationQuit()
    {
        IncreaseThePlayTime();
        SetDataSaveTime();
        Save();
    }
#endif

        private void OnDestroy()
        {
            if (save)
            {
                IncreaseThePlayTime();
                SetDataSaveTime();
                Save();
            }
        }

        #endregion





        #region Custom Methods

        private void Save()
        {
            PlayerPrefs.SetString(PLAYER_DATA, JsonHelper.Serialize<PersistentData>(playerDataControllerSO.PersistentData));

            //Potrebno zbog resetovanja nakod izlaska iz play moda
#if UNITY_EDITOR
            playerDataControllerSO.PersistentData = new PersistentData();
            playerDataControllerSO.IsPlayerDead = false;
            playerDataControllerSO.IsPlayerHitObstacle = false;
            gameController.IsGamePaused = false;
            gameController.PlayerCurrentSpeed = 0;
            gameController.PlayerCurrentSpeedUpPosition = 0;
#endif
        }

        private void IncreaseThePlayTime()
        {
            playerDataControllerSO.PersistentData.PlayTime += Time.time;
        }

        private void SetDataSaveTime()
        {
            if (playerDataControllerSO.PersistentData.IsTodayRewardCollected.Value)
            {
                playerDataControllerSO.PersistentData.IsTodayRewardCollected.Value = false;
                playerDataControllerSO.PersistentData.DataSaveTime.Value = DateTime.Today;
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Reset Player Prefs")]
        private void ResetPlayerPrefs()
        {
            PlayerPrefs.DeleteKey(PLAYER_DATA);
        }
#endif

        #endregion
    }
}