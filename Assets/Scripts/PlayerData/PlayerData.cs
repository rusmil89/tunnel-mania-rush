﻿using Tunnel.Controllers;
using Tunnel.PlayerData;
using UniRx;
using UnityEngine;
using Zenject;

public sealed class PlayerDied
{
    public int NumberOfDeaths { get; set; }
    public float HighScoreMeters { get; set; }
}
    
public sealed class PlayerRevived {}

public sealed class PlayerHitObstacle {}


[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/Player Data", order = 0)]
public class PlayerData : ScriptableObjectInstaller
{
    #region Private Fields

    /// <summary>
    /// Serializable fields
    /// </summary>
    [Header("Bools")]
    [SerializeField] private bool _canShowGDPR;
    [SerializeField] private bool _isPlayerHitObstacle;
    [SerializeField] private bool _isPlayerDead;
    
    [Header("Persistent Data")]
    [SerializeField] private PersistentData _persistentData;

    /// <summary>
    /// Non-Serializable fields
    /// </summary>
    private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
    
    #endregion




    #region Properties

    public bool CanShowGDPR
    {
        get => _canShowGDPR;
        set => _canShowGDPR = value;
    }

    public bool IsPlayerHitObstacle
    {
        get => _isPlayerHitObstacle;
        set
        {
            _isPlayerHitObstacle = value;
            if (_isPlayerHitObstacle)
            {
                MessageBroker.Default.Publish(new PlayerHitObstacle());
            }
        }
    }

    public bool IsPlayerDead
    {
        get => _isPlayerDead;
        set
        { 
            _isPlayerDead = value;
            if (_isPlayerDead)
            {
                MessageBroker.Default.Publish(new PlayerDied
                {
                    NumberOfDeaths = ++PersistentData.NumberOfDeaths.Value,
                    HighScoreMeters = PersistentData.PlayerMaxRecordedDistance.Value
                });
            }
            else
            {
                MessageBroker.Default.Publish(new PlayerRevived());
            }
        }
    }

    public PersistentData PersistentData
    {
        get => _persistentData;
        set => _persistentData = value;
    }

    #endregion





    #region Monobehaviour Events

    private void OnEnable()
    {
        MessageBroker.Default.Receive<EeaRegionChecked>()
            .Subscribe(EEARegionCheckedEventHandler)
            .AddTo(_compositeDisposable);
    }

    private void OnDisable()
    {
        _compositeDisposable.Dispose();
#if UNITY_ANDROID && !UNITY_EDITOR
        _persistentData.UnregisterEventListeners();
#endif
    }

    #endregion





    #region Custom Methods

    public override void InstallBindings()
    {
        Container.BindInstance(this);
    }

    public void SetLikeDislikeStatus(int option)
    {
        PersistentData.LikeDislikeOption = (PersistentData.LikeDislikeOptions) option;

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        Tunnel.Analytics.WarmAppGamesAnalytics.LogLikeDislikeEvent(option);
#endif
    }

    #endregion





    #region Event Listeners

    private void EEARegionCheckedEventHandler(EeaRegionChecked eeaRegionChecked)
    {
        CanShowGDPR = eeaRegionChecked.IsEEARegion;
    }

    #endregion
}