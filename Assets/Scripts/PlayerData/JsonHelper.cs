﻿using FullSerializer;

public static class JsonHelper {
    
    private static fsSerializer serializer = new fsSerializer();
    
    public static string Serialize<T>(object value) {
        // serialize the data
        fsData fsData;
        serializer.TrySerialize(typeof(T), value, out fsData).AssertSuccessWithoutWarnings();

        // emit the data via JSON
        return fsJsonPrinter.CompressedJson(fsData);
    }
	
    public static T Deserialize<T>(string serializedState) {
        // step 1: parse the JSON data
        fsData fsData = fsJsonParser.Parse(serializedState);

        // step 2: deserialize the data
        object deserialized = null;
        serializer.TryDeserialize(fsData, typeof(T), ref deserialized).AssertSuccessWithoutWarnings();

        return (T)deserialized;
    }    
    
}