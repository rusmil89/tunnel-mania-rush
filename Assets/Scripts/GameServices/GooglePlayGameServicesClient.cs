﻿using System;
using System.Collections;
using EasyFeedback;
using EasyMobile;
using Tunnel.Analytics;
using Tunnel.Controllers;
using UniRx;
using UnityEngine;
using Utilities;

namespace Tunnel.GooglePlayGameServices
{
    public sealed class AchievementUnlocked {}
    
    /// <summary>
    /// Singleton klasa koja kontroliše Play Game Services
    /// </summary>
    public sealed class GooglePlayGameServicesClient
    {
        #region Private Fields
        
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly CompositeDisposable _applicationRatedDisposable = new CompositeDisposable();
        private readonly CompositeDisposable _feedbackFormSubmittedDisposable = new CompositeDisposable();
        
        private static global::Tunnel.PlayerData.PersistentData _playerData;
        private static readonly GooglePlayGameServicesClient _instance = new GooglePlayGameServicesClient();

        #endregion





        #region Public Fields
        
        public static GooglePlayGameServicesClient Instance => _instance;

        #endregion
        
        
        
        
        
        #region Custom Methods

        private GooglePlayGameServicesClient()
        {
        }

        public IEnumerator Init(global::Tunnel.PlayerData.PersistentData playerData)
        {
            _playerData = playerData;
            GameServicesInitialization();
            RegisterEventListeners();
            RegisterObservableListeners();
            yield return null;
        }

        /// <summary>
        /// Metoda koja inicijalizuje GameServices dodataka
        /// WHY: Da bi mogli da ga koristimo i prikazemo igracu njegove achievemente
        /// </summary>
        private void GameServicesInitialization()
        {
            try
            {
                if (!_playerData.IsPlayerManuallySignedOut.Value)
                {
                    GameServices.Init();
                }
            }
            catch (Exception e)
            {
                Logging.LogError($"Greska prilikom inicijalizacije Google Game Services: {e.Message}");
                throw;
            }
        }

        private void RegisterEventListeners()
        {
            GameServices.UserLoginSucceeded += OnUserLoginSucceededEventHandler;
            GameServices.UserLoginFailed += OnUserLoginFailedEventHandler;
        }

        private void RegisterObservableListeners()
        {
            // osluskujemo kada se korisnik uloguje na GPS
            _playerData.IsPlayerSignedInGoogleGameServices
                .SkipWhile(_ => !FirebaseClient.Instance.IsFirebaseReady.Value)
                .Where(isPlayerSignedInGoogleGameServices => isPlayerSignedInGoogleGameServices)
                .Subscribe(
                    _ =>
                    {
                        bool isInitialized = GameServices.IsInitialized();
                        if (isInitialized) return;
                        GameServices.Init();
                        WarmAppGamesAnalytics.LogSignInEvent();
                    })
                .AddTo(_compositeDisposable);
            
            // osluskujemo kada se korisnik izloguje sa GPS
            _playerData.IsPlayerSignedInGoogleGameServices
                .SkipWhile(_ => !FirebaseClient.Instance.IsFirebaseReady.Value)
                .Where(isPlayerSignedInGoogleGameServices => !isPlayerSignedInGoogleGameServices && _playerData.IsPlayerManuallySignedOut.Value)
                .Subscribe(
                    _ =>
                    {
                        GameServices.SignOut();
                        WarmAppGamesAnalytics.LogSignOutEvent();
                    })
                .AddTo(_compositeDisposable);
            
            MessageBroker.Default.Receive<ApplicationRated>()
                .Subscribe(ApplicationRatedEventHandler)
                .AddTo(_applicationRatedDisposable);
            MessageBroker.Default.Receive<FormSuccessfullySubmitted>()
                .Subscribe(FeedbackFormSuccessfullySubmittedEventHandler)
                .AddTo(_feedbackFormSubmittedDisposable);
            MessageBroker.Default.Receive<PlayerDied>()
                .Subscribe(PlayerDiedEventHandler)
                .AddTo(_compositeDisposable);
        }

        public void UnregisterEventListeners()
        {
            GameServices.UserLoginSucceeded -= OnUserLoginSucceededEventHandler;
            GameServices.UserLoginFailed -= OnUserLoginFailedEventHandler;
            UnregisterObservableListeners();
        }

        private void UnregisterObservableListeners()
        {
            _compositeDisposable.Dispose();
            _applicationRatedDisposable.Dispose();
            _feedbackFormSubmittedDisposable.Dispose();
        }

        #endregion

        
        



        #region Event Listeners

        private void OnUserLoginSucceededEventHandler()
        {
            _playerData.IsPlayerSignedInGoogleGameServices.Value = true;
            MessageBroker.Default.Publish(new AchievementUnlocked());
        }

        private void OnUserLoginFailedEventHandler()
        {
            _playerData.IsPlayerSignedInGoogleGameServices.Value = false;
        }

        private void ApplicationRatedEventHandler(ApplicationRated applicationRated)
        {
            if (_playerData.IsPlayerSignedInGoogleGameServices.Value)
            {
                MessageBroker.Default.Publish(new AchievementUnlocked());
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_Positive_Participator);
                WarmAppGamesAnalytics.LogUnlockAchievementEvent(EM_GameServicesConstants.Achievement_Positive_Participator);
                _applicationRatedDisposable.Dispose();
            }
        }

        private void FeedbackFormSuccessfullySubmittedEventHandler(FormSuccessfullySubmitted formSuccessfullySubmitted)
        {
            if (_playerData.IsPlayerSignedInGoogleGameServices.Value)
            {
                MessageBroker.Default.Publish(new AchievementUnlocked());
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_Hyper_Helper);
                WarmAppGamesAnalytics.LogUnlockAchievementEvent(EM_GameServicesConstants.Achievement_Hyper_Helper);
                _feedbackFormSubmittedDisposable.Dispose();
                
            }
        }

        private void PlayerDiedEventHandler(PlayerDied eventArgs)
        {
            if (_playerData.IsPlayerSignedInGoogleGameServices.Value)
            {
                string achievementId;
                if (eventArgs.HighScoreMeters >= 1000f && eventArgs.HighScoreMeters < 2000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Shining_Star;
                }
                else if (eventArgs.HighScoreMeters >= 2000f && eventArgs.HighScoreMeters < 3000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Amazing_Dedication;
                }
                else if (eventArgs.HighScoreMeters >= 3000f && eventArgs.HighScoreMeters < 4000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Paramount_Achievement;
                }
                else if (eventArgs.HighScoreMeters >= 4000f && eventArgs.HighScoreMeters < 5000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Circle_of_Excellence;
                }
                else if (eventArgs.HighScoreMeters >= 5000f && eventArgs.HighScoreMeters < 6000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Cruising_and_Crushing_it;
                }
                else if (eventArgs.HighScoreMeters >= 6000f && eventArgs.HighScoreMeters < 7000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Exceeding_Expectations;
                }
                else if (eventArgs.HighScoreMeters >= 7000f && eventArgs.HighScoreMeters < 8000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Look_out_world_success_is_here;
                }
                else if (eventArgs.HighScoreMeters >= 8000f && eventArgs.HighScoreMeters < 9000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Bringing_it_Beyond;
                }
                else if (eventArgs.HighScoreMeters >= 9000f && eventArgs.HighScoreMeters < 10000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Respect;
                }
                else if (eventArgs.HighScoreMeters >= 10000f)
                {
                    achievementId = EM_GameServicesConstants.Achievement_Excellence;
                }
                else
                {
                    return;
                }
            
                MessageBroker.Default.Publish(new AchievementUnlocked());
                GameServices.UnlockAchievement(achievementId);
                WarmAppGamesAnalytics.LogUnlockAchievementEvent(achievementId);
            }
        }

        #endregion
    }
    
}





















