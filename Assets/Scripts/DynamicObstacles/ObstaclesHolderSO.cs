﻿using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    [CreateAssetMenu(fileName = "ObstacleHolder", menuName = "ScriptableObjects/Levels/ObstacleHolder", order = 2)]
    public class ObstaclesHolderSO : ScriptableObject
    {
        #region Public Fields

        public ObstaclesHolder obstaclesHolder;

        #endregion





        #region Custom Methods

        public void Initialize()
        {
            obstaclesHolder.obstaclesHolder.gameObject.SetActive(true);
            for (int i = 0; i < obstaclesHolder.obstacles.Length; i++)
            {
                obstaclesHolder.obstacles[i].Initialize();
            }
        }

        public void Hide()
        {
            obstaclesHolder.obstaclesHolder.gameObject.SetActive(false);
        }
    
        public void TooglePause(bool value)
        {
            if (value)
            {
                for (int i = 0; i < obstaclesHolder.obstacles.Length; i++)
                {
                    obstaclesHolder.obstacles[i].Pause();
                }
            }
            else
            {
                for (int i = 0; i < obstaclesHolder.obstacles.Length; i++)
                {
                    obstaclesHolder.obstacles[i].Resume();
                }
            }
        }

        #endregion
    }
}