﻿using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    public class ObstacleRevolvingCubes : Obstacle
    {
        #region Private Fields

        [SerializeField] private Obstacle[] cubes;

        #endregion



        
        
        #region Override Methods

        public override void Initialize()
        {
            for (int i = 0; i < cubes.Length; i++)
            {
                cubes[i].Initialize();
            }
        }

        public override void Pause()
        {
            for (int i = 0; i < cubes.Length; i++)
            {
                cubes[i].Pause();
            }
        }

        public override void Resume()
        {
            for (int i = 0; i < cubes.Length; i++)
            {
                cubes[i].Resume();
            }
        }

        #endregion
        
    }
}