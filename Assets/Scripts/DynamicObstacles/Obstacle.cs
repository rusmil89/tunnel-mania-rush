﻿using Dreamteck.Splines;
using Tunnel.Utilities;
using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    public class Obstacle : MonoBehaviour
    {
        #region Public Fields

        public ObstacleTypeSO obstacleType;
        public float speed;
        public float delay;
        public SplineFollower splineFollower;

        #endregion





        #region Monobehaviour Events

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        public void OnValidate()
        {
            // Testing is this component attached to scene game object or to prefab. If it is attached to a prefab we don't assert
            if (string.IsNullOrEmpty(gameObject.scene.name)) return;

            Debug.Assert(splineFollower != null, "Spline Follower je null na objektu: " + gameObject.name, gameObject);
            if (splineFollower.computer != null) return;
            splineFollower.computer = GameObject.FindGameObjectWithTag(Constants.Tags.TUBE).GetComponent<SplineComputer>();
        }
#endif

        #endregion





        #region Virtual Methods

        public virtual void Initialize()
        {
        }

        public virtual void Pause()
        {
        }

        public virtual void Resume()
        {
        }

        #endregion
    }
}