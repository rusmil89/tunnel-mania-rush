﻿using UnityEngine;

[CreateAssetMenu(fileName = "ObstacleType", menuName = "ScriptableObjects/Levels/ObstacleType", order = 1)]
public class ObstacleTypeSO : ScriptableObject{}
