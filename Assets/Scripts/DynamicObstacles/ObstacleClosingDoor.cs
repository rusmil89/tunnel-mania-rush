﻿using System.Collections;
using UniRx;
using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    public class ObstacleClosingDoor : Obstacle
    {
        #region Private Fields
        
        /// <summary>
        /// Serializable fields
        /// </summary>
        [SerializeField] private GameData gameController;

        /// <summary>
        /// Non-Serializable fields
        /// </summary>
        private Animator _animator;
        private float _playerArrivalTime;
        private float _closingDoorAnimationLength;
        private static readonly int startClosingAnimationHash = Animator.StringToHash("StartClosing");
        private static readonly int idleStateAnimationHash = Animator.StringToHash("Idle");
        private bool _isAnimatorNull;
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        #endregion





        #region Monobehaviour Events
        
#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        public new void OnValidate()
        {
            base.OnValidate();
            _animator = GetComponent<Animator>();
        }
#endif

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _isAnimatorNull = _animator == null;
            _closingDoorAnimationLength = _animator.runtimeAnimatorController.animationClips[0].length;
        }

        private void OnEnable()
        {
            MessageBroker.Default.Receive<PlayerHitObstacle>()
                .Subscribe(OnPlayerHitObstacleEventHandler)
                .AddTo(_disposable);
        }

        private void OnDisable()
        {
            _disposable.Dispose();
        }

        #endregion





        #region Coroutines

        private IEnumerator WaitForPlayer()
        {
            yield return new WaitUntil(() => gameController.PlayerCurrentSpeed >= gameController.PlayerMovementSpeed);
            _playerArrivalTime = splineFollower.CalculateLength(gameController.PlayerCurrentSpeedUpPosition, splineFollower.result.percent) / gameController.PlayerCurrentSpeed;
        
            _playerArrivalTime %= _closingDoorAnimationLength;
            while (_playerArrivalTime >= 0)
            {
                _playerArrivalTime -= Time.deltaTime;
                yield return null;
            }

            if (_isAnimatorNull) yield break;
            _animator.SetTrigger(startClosingAnimationHash);
        }

        #endregion





        #region Override Methods

        public override void Initialize()
        {
            StopAllCoroutines();
            _animator.Play(idleStateAnimationHash);
            _animator.speed = speed;
            StartCoroutine(WaitForPlayer());
        }

        public override void Pause()
        {
            _animator.enabled = false;
        }

        public override void Resume()
        {
            _animator.enabled = true;
            Initialize();
        }

        #endregion





        #region Event Listeners

        private void OnPlayerHitObstacleEventHandler(PlayerHitObstacle playerHitObstacle)
        {
            Initialize();
        }

        #endregion
    }
}