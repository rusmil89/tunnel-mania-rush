﻿using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    public class ObstaclesHolder : MonoBehaviour
    {
        #region Public Fields

        public ObstaclesHolderSO obstaclesHolderSO;
        public GameObject obstaclesHolder;
        public Obstacle[] obstacles;

        #endregion





        #region Monobehaviour Events

        private void Awake()
        {
            obstaclesHolderSO.obstaclesHolder = this;
        }

        #endregion
    }
}