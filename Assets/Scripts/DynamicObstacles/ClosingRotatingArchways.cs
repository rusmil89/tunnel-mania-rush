﻿using UnityEngine;

namespace Tunnel.DynamicObstacles
{
    public class ClosingRotatingArchways : ObstacleClosingDoor
    {
        #region Public Fields

        public Animator rotatingAnimator;

        #endregion





        #region Override Methods

        public override void Initialize()
        {
            rotatingAnimator.speed = speed;
            rotatingAnimator.Play("RotatingAnimation");
            base.Initialize();
        }

        public override void Pause()
        {
            rotatingAnimator.enabled = false;
            base.Pause();
        }
    
        public override void Resume()
        {
            rotatingAnimator.enabled = true;
            base.Resume();
        }

        #endregion
    }
}