﻿using System;
using System.Collections;
using Tunnel.Controllers;
using UniRx;
using UnityEngine;

public class GamePaused
{
    public bool IsGamePaused { get; set; }
}

[CreateAssetMenu(fileName = "GameData", menuName = "Game Data")]
public class GameData : ScriptableObject
{
    //TODO: zameniti native promenljivu za game paused sa reactive property
    //TODO: prebaciti _isOutside promenljivu u gamecontroller zato sto je vise objekata koristi
    #region Private Fields
    
    [SerializeField] private float _playerMovementSpeed;
    [SerializeField] private float _playerRotationSpeed;
    [SerializeField] private float _playerCurrentSpeed;
    [SerializeField] private double _playerCurrentSpeedUpPosition;
    [SerializeField] private bool _isGamePaused;
    
    private const float TIMEOUT = 0.125f;
    private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

    #endregion





    #region Properties
    
    public float PlayerMovementSpeed
    {
        get => _playerMovementSpeed;
        set
        {
            if (value >= 0)
            {
                _playerMovementSpeed = value;
            }
            else
            {
                throw new Exception("playerMovementSpeed ne sme biti manji od 0");
            }
        }
    }

    public float PlayerRotationSpeed
    {
        get => _playerRotationSpeed;
        set
        {
            if (value >= 0)
            {
                _playerRotationSpeed = value;
            }
            else
            {
                throw new Exception("playerRotationSpeed ne sme biti manji od 0");
            }
        }
    }

    public float PlayerCurrentSpeed
    {
        get => _playerCurrentSpeed;
        set
        {
            if (value >= 0)
            {
                _playerCurrentSpeed = value;
            }
            else
            {
                throw new Exception("playerCurrentSpeed ne sme biti manji od 0");
            }
        }
    }

    public double PlayerCurrentSpeedUpPosition
    {
        get => _playerCurrentSpeedUpPosition;
        set
        {
            if (value >= 0)
            {
                _playerCurrentSpeedUpPosition = value;
            }
            else
            {
                throw new Exception("playerCurrentSpeedUpPosition ne sme biti manji od 0");
            }
        }
    }

    public bool IsGamePaused
    {
        get => _isGamePaused;
        set
        {
            _isGamePaused = value;
            MessageBroker.Default.Publish(new GamePaused
            {
                IsGamePaused = value
            });
        }
    }

    #endregion





    #region Monobehaviour Events

    private void OnEnable()
    {
        MainThreadDispatcher.StartCoroutine(GetRemoteSettings());
    }

    private IEnumerator GetRemoteSettings()
    {
        while (RemoteConfigClient.Instance == null)
        {
            yield return null;
        }

        RemoteConfigClient.Instance.Ready
            .Where(isReady => isReady)
            .Subscribe(SetRemoteSettings)
            .AddTo(_compositeDisposable);
    }

    private void OnDisable()
    {
        _compositeDisposable.Dispose();
    }

    private void SetRemoteSettings(bool b)
    {
        _playerMovementSpeed = RemoteConfigClient.Instance.MediumMovementSpeed;
        _playerRotationSpeed = RemoteConfigClient.Instance.MediumRotationSpeed;
    }

    #endregion
}