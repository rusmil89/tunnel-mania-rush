using System.IO;
using UnityEditor;
using UnityEngine;

namespace Tunnel.Editor
{
    public class ChineseFontGeneration
    {
        [MenuItem("Tools/Generate Chinese Font Assets")]
        public static void GenerateChineseFontAssets()
        {
            var openFilePanelPath = EditorUtility.OpenFilePanel("Choose", "Assets/Textures/UI/Fonts", "txt");
            if (string.IsNullOrEmpty(openFilePanelPath)) return;
            var lines = File.ReadAllLines(openFilePanelPath);
            string newFilePath = Application.dataPath + "/Textures/UI/Fonts/NewUnicode.txt";
            FileStream stream = new FileStream(newFilePath, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);
            foreach (var line in lines)
            {
                var formattedLine = line.Split('+')[1]+ ",";
                Debug.Log(formattedLine);
                writer.Write(formattedLine);
            }
            writer.Close();
            stream.Close();
        }
    }
}