﻿using UnityEditor;
using UnityEngine;

namespace Tunnel.Editor
{
    public static class ClearPlayerPrefs
    {
        [MenuItem("Tools/Clear All Player Prefs")]
        public static void ClearAllPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}