﻿using UnityEditor;
using UnityEngine;

public class FindAssetByGUID
{
    [MenuItem("WarmApp Games/Find Asset by GUID", false, 1)]
    private static void FindAssetByGUIDMethod()
    {
        var materialguids = AssetDatabase.FindAssets("t:Material");
        foreach (var materialguid in materialguids)
        {
            if (materialguid.Equals("645955e5c3a3a1f4fb83978d2a63240f"))
            {
                Debug.Log("Materijal: " + AssetDatabase.GUIDToAssetPath(materialguid));
            }
        }
    }
}