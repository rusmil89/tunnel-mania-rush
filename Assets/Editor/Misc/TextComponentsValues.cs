using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using Unity.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tunnel.Editor
{
    public class TextComponentsValues
    {
        [MenuItem("Tools/Get all words from scene")]
        public static void GetAllTextsFromScene()
        {
            var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            List<string> textValues = (from gameObject in rootGameObjects from descendant in gameObject.Descendants().OfComponent<TMP_Text>() select descendant.text).ToList();
            string path = Path.Combine(Application.dataPath, "Words.txt");
            StreamWriter streamWriter = new StreamWriter(path);
            foreach (var textValue in textValues)
            {
                Debug.Log($"textValue: {textValue}");
                streamWriter.WriteLine(textValue);
            }
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}