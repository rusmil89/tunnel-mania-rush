﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class AnimatorCombiner : Editor {

    [MenuItem("Assets/Animation/Add animation clip to controller")]
    public static void Add()
    {
        var assets = Selection.objects.ToList();
        if (assets.Count > 1)
        {
            EditorUtility.DisplayDialog("Error", "Selektovati samo jedan Animation Controller", "OK");
        }
        else
        {
            RuntimeAnimatorController animator = assets[0] as RuntimeAnimatorController;
            long animatorFileid;
            string animatorGuid;
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(animator, out animatorGuid,
                out animatorFileid);
            if (animator != null)
            {
                for (int i = 0; i < animator.animationClips.Length; i++)
                {
                    long animationClipFileid;
                    string animationClipGuid;
                    AssetDatabase.TryGetGUIDAndLocalFileIdentifier(animator.animationClips[i], out animationClipGuid,
                        out animationClipFileid);
                    if (animatorGuid != animationClipGuid)
                    {
                        AnimationClip temp =
                            AssetDatabase.LoadAssetAtPath<AnimationClip>(
                                AssetDatabase.GetAssetPath(animator.animationClips[i]));
                        AnimationClip animationClip = Instantiate(temp);
                        animationClip.name = animator.animationClips[i].name;
                        AssetDatabase.AddObjectToAsset(animationClip, animator);
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                        long newAnimationClipFileid;
                        string newAnimationClipGuid;
                        if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(animationClip, out newAnimationClipGuid, out newAnimationClipFileid))
                        {
                            string guid =
                                AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(animator.animationClips[i]));
                            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(animator.animationClips[i]));
                            var text = File.ReadAllLines(AssetDatabase.GetAssetPath(animator));
                            for (int j = 0; j < text.Length; j++)
                            {
                                if (text[j].Contains("m_Motion:"))
                                {
                                    if (text[j].Contains(guid))
                                    {
                                        text[j] = "  m_Motion: {fileID: " + newAnimationClipFileid + "}";
                                    }
                                }
                            }

                            File.WriteAllLines(AssetDatabase.GetAssetPath(animator), text);
                            AssetDatabase.SaveAssets();
                            AssetDatabase.Refresh();
                        }
                    }
                }
            }
        }
    }


    [MenuItem("Assets/Animation/Add animation clip to controller", true)]
    private static bool AddValidation()
    {
        return Selection.activeObject is RuntimeAnimatorController;
    }

}