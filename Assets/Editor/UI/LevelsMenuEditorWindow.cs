﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TMPro;
using UnityEditor;
using UnityEditor.Events;

using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Tunnel.LevelsMenu
{
    public static class LevelsMenuEditorWindow
    {
        private const int numberOfLevelsInOnePanel = 10;
        private const string levelsMenuPath = "Assets/Prefabs/UI/Menus/LevelsMenu.prefab";
        private const string playerDataPath = "Assets/ScriptableObjects/PlayerData.asset";
        private const string sceneLoaderPath = "Assets/ScriptableObjects/Utilities/SceneLoading.asset";

        private static GridLayoutGroup[] arrayOfPanels;
        private static List<GameObject> listOfLevelsGameObjects;
        private static int numberOfLevels;
        private static Transform panelsParent;

        [MenuItem("WarmApp Games/UI/SetUp Levels Menu", false, 1)]
        public static void SetUpLevelsMenu()
        {
            // fields initialization
            panelsParent = GetPanelsParent();
            arrayOfPanels = GetPanels();
            listOfLevelsGameObjects = new List<GameObject>();
            numberOfLevels = GetNumberOfLevels();

            DeletePanels();
            InstantiatePanels();
            InstantiateLevelGameObjects();

            arrayOfPanels = GetPanels();
            for (int i = 0; i < arrayOfPanels.Length; i++)
            {
                SetUpParentsForTheLevelGameObjects(arrayOfPanels[i].transform, i);
            }

            listOfLevelsGameObjects.Clear();
        }

        public static Transform GetPanelsParent()
        {
            return GetPanelsInLevelsMenuPrefab().First() != null
                ? GetPanelsInLevelsMenuPrefab().First().transform.parent
                : (from rectTransform in Object.FindObjectsOfType<RectTransform>() where rectTransform.name.Contains("Content") select rectTransform.gameObject.transform).FirstOrDefault();
        }

        private static GridLayoutGroup[] GetPanels()
        {
            return panelsParent.GetComponentsInChildren<GridLayoutGroup>().Select(gridLayoutGroup => gridLayoutGroup).ToArray();
        }

        public static IEnumerable<GridLayoutGroup> GetPanelsInLevelsMenuPrefab()
        {
            if (AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath<GameObject>(levelsMenuPath)))
            {
                UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
                return prefabStage.prefabContentsRoot.GetComponentsInChildren<GridLayoutGroup>();
            }

            EditorUtility.DisplayDialog("Wrong Path", "Putanja: " + levelsMenuPath + ", nije validna!", "OK");
            return null;
        }

        public static int GetNumberOfLevels()
        {
            string path = EditorUtility.OpenFolderPanel("Izaberi putanju sa levelima", "Assets/ScriptableObjects", string.Empty);
            if (string.IsNullOrEmpty(path))
            {
                path = "Assets/ScriptableObjects/Worlds";
            }
            else
            {
                if (path.StartsWith(Application.dataPath))
                {
                    path = "Assets/" + path.Substring(Application.dataPath.Length);
                }
            }

            if (AssetDatabase.IsValidFolder(path)) return AssetDatabase.FindAssets("t:LevelData", new[] {path}).Length * 2;
            EditorUtility.DisplayDialog("Wrong Path", "Putanja: " + path + ", nije validna!", "OK");
            return 0;
        }

        private static void DeletePanels()
        {
            for (int i = 1; i < panelsParent.childCount; i++)
            {
                Object.DestroyImmediate(panelsParent.GetChild(i).gameObject);
            }
        }

        private static void InstantiatePanels()
        {
            const string panelPath = "Assets/Prefabs/UI/MenuElements/Page_1.prefab";
            var panelPrefab = AssetDatabase.LoadMainAssetAtPath(panelPath) as GameObject;
            if (panelPrefab == null)
            {
                EditorUtility.DisplayDialog("Wrong Path", "Putanja: " + panelPath + ", nije validna!", "OK");
                return;
            }

            var numberOfNeededPanels = numberOfLevels / numberOfLevelsInOnePanel;
            var numberOfLevelsModulo = numberOfLevels % numberOfLevelsInOnePanel;
            if (numberOfLevelsModulo > 0)
            {
                numberOfNeededPanels += 1;
            }

            for (int i = 1; i < numberOfNeededPanels; i++)
            {
                var panelGameObject = (GameObject) PrefabUtility.InstantiatePrefab(panelPrefab, panelsParent);
                panelGameObject.name = "Page_" + (i + 1);
            }
        }

        private static void InstantiateLevelGameObjects()
        {
            var playerDataAsset = AssetDatabase.LoadMainAssetAtPath(playerDataPath) as global::PlayerData;
            if (playerDataAsset == null)
            {
                EditorUtility.DisplayDialog("Wrong Path", "Putanja: " + playerDataPath + ", nije validna!", "OK");
                return;
            }

            for (int i = 0; i < numberOfLevels; i++)
            {
                var levelPrefab = InstantiateLevelPrefab();
                listOfLevelsGameObjects.Add(levelPrefab);
                StringBuilder sb = new StringBuilder();
                sb.Append(i + 1);
                levelPrefab.GetComponentInChildren<TextMeshProUGUI>().SetText(sb);
                var levelNumberButton = levelPrefab.GetComponent<Button>();
            }
        }

        private static GameObject InstantiateLevelPrefab()
        {
            const string levelPath = "Assets/Prefabs/UI/MenuElements/Buttons/DiamondButton_Variant_LevelNumber.prefab";
            var levelPrefab = AssetDatabase.LoadMainAssetAtPath(levelPath) as GameObject;
            if (levelPrefab == null)
            {
                EditorUtility.DisplayDialog("Wrong Path", "Putanja: " + levelPath + ", nije validna!", "OK");
                return null;
            }

            var levelNumberPrefab = (GameObject) PrefabUtility.InstantiatePrefab(levelPrefab, panelsParent);
            levelNumberPrefab.name = "Level";
            return levelNumberPrefab;
        }

        private static void SetUpParentsForTheLevelGameObjects(Transform parent, int indexOfPanel)
        {
            for (int i = indexOfPanel * 10; i < (indexOfPanel + 1) * numberOfLevelsInOnePanel; i++)
            {
                listOfLevelsGameObjects[i].transform.parent = parent;
            }
        }
    }

    public class LevelsMenuEditorWindowTests
    {
        //NOTE: iz čitanja testova koji se nalaze u UnityAssetAuditor/TreeElementUtilityTests klasi zaključio sam sledeće: 1. ukoliko treba da se testira neki objekat, onda možemo da napravimo test klasu koja nasleđuje tu klasu koja predstavlja objekat koji treba da se testira (npr, TestElement : TreeElement) pod uslovom da je to obična klasa (nije MonoBehaviour)
        //NOTE: predlažu da se napravi [SetUp] i [TearDown] pre početka i posle završetka svakog testa jer se na taj način priprema okruženje za test i vraća se u njegovo podrazumevano stanje
        //NOTE: svai test počinje sa ciljem tog testa, odnosno onoga što testiramo (npr, Assert.IsFalse(condition);)

        #region Tests

        [Test]
        public static void TestNumberOfLevels()
        {
            int numberOfLevels = LevelsMenuEditorWindow.GetNumberOfLevels();
            Debug.Log(numberOfLevels);
            Assert.NotZero(numberOfLevels);
        }

        [Test]
        public static void PanelsParentIsNotNull()
        {
            var panelsParent = LevelsMenuEditorWindow.GetPanelsParent();
            Debug.Log(panelsParent.name, panelsParent);
            Assert.IsNotNull(panelsParent);
        }

        [Test]
        public static void TestNumberOfPanels()
        {
            int numberOfPanels = LevelsMenuEditorWindow.GetPanelsInLevelsMenuPrefab().Count();
            Debug.Log(numberOfPanels);
            Assert.NotZero(numberOfPanels);
        }

        #endregion
    }
}