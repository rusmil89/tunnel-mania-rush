using System;
using UnityEngine;
using Object = UnityEngine.Object;

[Serializable]
public class ButtonListeners
{
    [SerializeField] private Object persistentTarget;

    [SerializeField] private string persistentTargetName;

    [SerializeField] private string persistentMethodName;

    public ButtonListeners(Object persistentTarget, string persistentTargetName, string persistentMethodName)
    {
        this.persistentTarget = persistentTarget;
        this.persistentTargetName = persistentTargetName;
        this.persistentMethodName = persistentMethodName;
    }
}

[Serializable]
public class UIButtons
{
    [SerializeField] private string buttonName;

    [SerializeField] private ButtonListeners[] buttonListeners;

    public UIButtons(string buttonName, ButtonListeners[] buttonListeners)
    {
        this.buttonName = buttonName;
        this.buttonListeners = buttonListeners;
    }
}

//[CreateAssetMenu(fileName = "FILENAME", menuName = "MENUNAME", order = 0)]
public class ButtonListenersSO : ScriptableObject
{
    public UIButtons[] uiButtons;
}