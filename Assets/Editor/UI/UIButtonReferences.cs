﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonReferences
{
    private static List<UIButtons> allUIButtons;

    [MenuItem("WarmApp Games/UI/Get All UI Buttons References", false, 2)]
    public static void GetAllUIButtonReferences()
    {
        GetAllListeners();
        CreateDataSO();
    }

    private static void GetAllListeners()
    {
        var rootGameObjects = GetAllRootGameObjects();
        foreach (var rootGameObject in rootGameObjects)
        {
            if (!rootGameObject.GetComponent<Canvas>()) continue;

            var allButtons = rootGameObject.GetComponentsInChildren<Button>(true);
            allUIButtons = new List<UIButtons>(allButtons.Length);
            foreach (var button in allButtons)
            {
                int persistentEventCount = button.onClick.GetPersistentEventCount();
                ButtonListeners[] buttonListeners = new ButtonListeners[persistentEventCount];
                for (int i = 0; i < persistentEventCount; i++)
                {
                    var persistentTarget = button.onClick.GetPersistentTarget(i);
                    var persistentMethodName = button.onClick.GetPersistentMethodName(i);
                    buttonListeners[i] = new ButtonListeners(persistentTarget, persistentTarget.name, persistentMethodName);
                }

                UIButtons uiButtons = new UIButtons(button.gameObject.name, buttonListeners);
                allUIButtons.Add(uiButtons);
            }
        }
    }

    private static void CreateDataSO()
    {
        ButtonListenersSO buttonListenersSo = ScriptableObject.CreateInstance<ButtonListenersSO>();
        buttonListenersSo.uiButtons = allUIButtons.ToArray();
        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/Editor/UI/ButtonsListeners_" + SceneManager.GetActiveScene().name + ".asset");
        AssetDatabase.CreateAsset(buttonListenersSo, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = buttonListenersSo;
    }

    private static IEnumerable<GameObject> GetAllRootGameObjects()
    {
        return SceneManager.GetActiveScene().GetRootGameObjects();
    }
}