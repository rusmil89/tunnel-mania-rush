﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dreamteck.Splines;
using NUnit.Framework;
using Tunnel.DynamicObstacles;
using Unity.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class AutomaticWorldBuilder : EditorWindow
{
    /// <summary>
    /// consts
    /// </summary>
    //TODO: napraviti da broj stranica stvarno utice na broj stranica tunela u sceni (WHY: zato sto cemo u buducnosti praviti tunele sa razlicitim brojem tranica)
    //TODO: sve metode koje nisu vezane za UI prebaciti u posebnu klasu kao sto je asset auditor sa static metodama (WHY: zato sto tako vodim racuna o kvalitetu i cistoci koda na duge staze)
    //TODO: sve brojke i stringove pretvoriti u const (WHY: zato sto tako vodim racuna o kvalitetu i cistoci koda na duge staze)

    // paths
    private const string ASSETS_PATH = "Assets";

    private const string WORLDS_FOLDER_PATH = "Assets/ScriptableObjects/Worlds/";
    private const string LEVEL_BUILDER_SCENE_PATH = "Assets/_Scenes/Editor/LevelBuilder.unity";
    private const string ALL_TUBES_MATERIALS_PATH = "Assets/Materials/Levels/Tube";
    private const string All_OBSTACLES_MATERIALS_PATH = "Assets/Materials/Levels/Obstacles";
    private const string INSIDE_OBSTACLES_PATH = "Assets/Prefabs/Level/Obstacles/InsideObstacles";
    private const string OUTSIDE_OBSTACLES_PATH = "Assets/Prefabs/Level/Obstacles/OutsideObstacles";

    // filters
    private const string MATERIAL_FILTER = "t:material";
    private const string PREFAB_FILTER = "t:prefab";
    private const string TEXTURE_FILTER = "t:texture";

    // floats
    private const float FIELDS_SPACE = 3f;

    // strings
    private const string SLASH_STRING = "/";
    private const string LEVEL_STRING = "Level_";
    private const string ASSET_EXTENSION = ".asset";

    // styles
    private const string BOX_STYLE_STRING = "Box";

    // Vectors
    private readonly Vector3 _insideCameraPosition = new Vector3(0f, -1.5f, 0f);
    private readonly Vector3 _outsideCameraPosition = new Vector3(0f, 2.2f, 0f);

    // colors
    private readonly Color _insideTubeAndObstacleDefaultColor = new Color(0f, 0.8509f, 1f, 1f);
    private readonly Color _outsideTubeAndObstacleDefaultColor = new Color(1f, 0f, 0f, 1f);

    /// <summary>
    /// fields
    /// </summary>
    // instances
    private static AutomaticWorldBuilder Instance;

    // ints
    private int _numberOfPointsInFirstLevel;
    private int _numberOfNewPointsToAddPerLevel;
    private int _numberOfChaptersInWorld;
    private int _numberOfLevelsInChapter;
    private int _numberOfSidesInTube;
    private int _currentNumberOfSidesInTube;
    private int _currentLevelLength;
    private int _distanceBetweenPoints;
    private int[] _numberOfObstaclesInLevel;
    private int[] _rotationOffsetsZAxis;
    private int _minDistanceBetweenObstacles;
    private int _maxDistanceBetweenObstacles;

    // bools
    private bool _isRandomObstacle;
    private bool[] _isLevelOutside;
    private List<bool> _isObstacleSelected;
    private bool _isAddingNewObstacleForbidden = false;

    // strings
    private string _worldName;
    private string[] _rotationOffsetsZAxisString;

    // types
    private WorldData _currentWorld;
    private LevelData _currentLevel;
    private SplineComputer _splineComputer;
    private TubeGenerator _tubeGenerator;
    private Camera _mainCamera;
    private GameObject _tubeGameObject;
    private Obstacle[] _insideObstacles;
    private Obstacle[] _outsideObstacles;
    private Obstacle[] _currentObstaclesArray;
    private List<ObstacleData> _currentObstacleData = new List<ObstacleData>();
    private List<Obstacle> _obstaclesInCurrentLevel = new List<Obstacle>();
    private Vector2 _scrollPosition;
    private MeshRenderer _tubeMeshRenderer;
    private Light _tubeLight;
    private Light _obstacleLight;
    private Color _defaultGUIColor;
    private LengthCalculator _lengthCalculator;
    private ObstacleTypeSO _obstacleTypeToReplaceWith;
    private Material _skybox;

    // enums
    private enum States
    {
        Uninitialized,
        Initialized,
        CreateWorld,
        LoadWorld
    }

    [NonSerialized] private States _states = States.Uninitialized;

    private Rect _bottomToolbarRect => new Rect(0f, 0f, position.width / 2, 50f);

    [MenuItem("WarmApp Games/Automatic World Builder", false, 1)]
    public static void ShowEditorWindow()
    {
        if (SceneManager.GetActiveScene().name.Equals("LevelBuilder"))
        {
            Instance = GetWindow<AutomaticWorldBuilder>(false, "Generator", true);
        }
        else
        {
            if (!EditorUtility.DisplayDialog("Automatic World Builder", "Ovu opciju je moguće pokrenuti samo kada je aktivna scena \"LevelBuilder\"!", "OK")) return;
            if (EditorUtility.DisplayDialog("Automatic World Builder", "Da li želite da učitate \"LevelBuilder\" scenu?", "DA", "NE"))
            {
                EditorSceneManager.OpenScene(LEVEL_BUILDER_SCENE_PATH);
            }
        }
    }

    private void OnEnable()
    {
        SetInitialValues();
        GetRequiredReferences();
        SetUpScene();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SetInitialValues()
    {
        _defaultGUIColor = GUI.color;
        _worldName = string.Empty;
        _currentLevel = null;
        _currentWorld = null;
        _numberOfPointsInFirstLevel = 0;
        _numberOfNewPointsToAddPerLevel = 0;
        _distanceBetweenPoints = 0;
        _numberOfChaptersInWorld = 0;
        _numberOfLevelsInChapter = 0;
        _numberOfSidesInTube = 0;
        _currentNumberOfSidesInTube = 0;
        _currentObstacleData = null;
        _currentObstaclesArray = null;
        _isRandomObstacle = true;
        _minDistanceBetweenObstacles = Mathf.Clamp(_minDistanceBetweenObstacles, 20, 100);
        _maxDistanceBetweenObstacles = Mathf.Clamp(_maxDistanceBetweenObstacles, 20, 100);
        _skybox = default;
    }

    /// <summary>
    /// Metoda koja pronalazi sve objekte na sceni i uzima njihove komponente
    /// WHY: Neophodno da postoje te reference kako bi mogli dalje da radimo
    /// </summary>
    private void GetRequiredReferences()
    {
        _mainCamera = Camera.main;
        _tubeGameObject = GameObject.Find("Tube");
        _splineComputer = _tubeGameObject.GetComponent<SplineComputer>();
        _tubeGenerator = _tubeGameObject.GetComponent<TubeGenerator>();
        _lengthCalculator = _tubeGameObject.GetComponent<LengthCalculator>();
        _tubeMeshRenderer = _splineComputer.GetComponent<MeshRenderer>();
        _tubeLight = GameObject.Find("TubeLight").GetComponent<Light>();
        _obstacleLight = GameObject.Find("ObstacleLight").GetComponent<Light>();

        _insideObstacles = AssetDatabase.FindAssets(PREFAB_FILTER, new[] {INSIDE_OBSTACLES_PATH})
            .Select(s => AssetDatabase.LoadAssetAtPath<Obstacle>(AssetDatabase.GUIDToAssetPath(s)))
            .ToArray();
        _outsideObstacles = AssetDatabase.FindAssets(PREFAB_FILTER, new[] {OUTSIDE_OBSTACLES_PATH})
            .Select(s => AssetDatabase.LoadAssetAtPath<Obstacle>(AssetDatabase.GUIDToAssetPath(s)))
            .ToArray();
    }

    /// <summary>
    /// Metoda koja svaki put podesava scenu tako sto instancira sve objekte za prepreke u odgovarajuce objekte roditelje
    /// WHY: Zato sto uvek moze da se dogodi da kreator levela doda ili obrise neku prepreku iz prefab-ova i onda je potrebno sinhronizovati prepreke na sceni sa preprekama u prefab-ovima
    /// </summary>
    private void SetUpScene()
    {
        var insideObstacleHolder = _tubeGameObject.AfterSelf().OfComponent<ObstaclesHolder>().First();
        var insideObstacleHolderTransform = insideObstacleHolder.transform.GetChild(0);
        var allInsideObstaclesOnScene = insideObstacleHolderTransform.gameObject.Children().ToArray();

        if (_insideObstacles.Length != allInsideObstaclesOnScene.Length)
        {
            foreach (var gameObject in allInsideObstaclesOnScene)
            {
                DestroyImmediate(gameObject);
            }

            foreach (var insideObstacle in _insideObstacles)
            {
                Instantiate(insideObstacle.gameObject, insideObstacleHolderTransform, false);
            }
        }

        var outsideObstaclesHolder = _tubeGameObject.AfterSelf().OfComponent<ObstaclesHolder>().Last();
        var outsideObstacleHolderTransform = outsideObstaclesHolder.transform.GetChild(0);
        var allOutsideObstaclesOnScene = outsideObstacleHolderTransform.gameObject.Children().ToArray();

        if (_outsideObstacles.Length != allOutsideObstaclesOnScene.Length)
        {
            foreach (var gameObject in allOutsideObstaclesOnScene)
            {
                DestroyImmediate(gameObject);
            }

            foreach (var insideObstacle in _insideObstacles)
            {
                Instantiate(insideObstacle.gameObject, outsideObstacleHolderTransform, false);
            }
        }
    }

    /// <summary>
    /// Metoda koja iscrtava ceo UI nekoliko puta u toku 1 frame-a
    /// WHY: Zato sto je to predefinisan unity metoda 
    /// </summary>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    private void OnGUI()
    {
        switch (_states)
        {
            case States.Uninitialized:
                UninitializedChecks();
                break;
            case States.Initialized:
                SetInitialValues();
                DrawInitialUI(_bottomToolbarRect);
                break;
            case States.CreateWorld:
                DrawBackButton();
                DrawNewWorldUI();
                SetObstaclesRotation();
                break;
            case States.LoadWorld:
                DrawBackButton();
                DrawLoadWorldUI();
                SetObstaclesRotation();

                //NOTE: EditorGUI.BeginChangeCheck() se nalazi u metodi DrawLoadWorldUI() pre polja koje ucitava SO za svet
                if (EditorGUI.EndChangeCheck())
                {
                    if (_currentWorld != null)
                    {
                        SetLocalDataFromWorldData(_currentWorld);
                    }
                }

                if (_currentWorld != null)
                {
                    DrawScrollViewForLevels();
                }

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (_currentWorld == null || _currentLevel == null) return;

        EditorUtility.SetDirty(_currentWorld);
        EditorUtility.SetDirty(_currentLevel);
    }

    /// <summary>
    /// Metoda koja proverava da li su ispunjeni svi preduslovi da bi ova skripta radila
    /// Neophpodno za dalji rad
    /// </summary>
    private void UninitializedChecks()
    {
        string message = string.Empty;
        if (_splineComputer == null)
        {
            message = "Ne postoji objekat sa _splineComputer komponentom na sceni";
        }
        else if (_lengthCalculator == null)
        {
            message = "Ne postoji objekat sa _lengthCalculator komponentom na sceni";
        }
        else if (_tubeMeshRenderer == null)
        {
            message = "Ne postoji objekat sa _tubeMeshRenderer komponentom na sceni";
        }
        else if (_tubeLight == null)
        {
            message = "Ne postoji objekat sa _tubeLight komponentom na sceni";
        }
        else if (_obstacleLight == null)
        {
            message = "Ne postoji objekat sa _obstacleLight komponentom na sceni";
        }
        else
        {
            _states = States.Initialized;
        }

        EditorGUILayout.HelpBox(message, MessageType.Warning);
    }

    /// <summary>
    /// Metoda koja iscrtava UI nakon sto su sve potrebne komponente inicijalizovane
    /// Zato sto u ovom stanju kreator levela moze da izabere da li zeli da kreira potpuno novi svet ili da ucita neki postojeci i da njega menja
    /// </summary>
    /// <param name="rect"></param>
    private void DrawInitialUI(Rect rect)
    {
        if (GUI.Button(new Rect(rect.width / 2, position.height / 2 - 0.75f * rect.height, rect.width, rect.height), "Create World"))
        {
            _states = States.CreateWorld;
        }
        else if (GUI.Button(new Rect(rect.width / 2, position.height / 2 + 0.75f * rect.height, rect.width, rect.height), "Load world"))
        {
            _states = States.LoadWorld;
        }
    }

    /// <summary>
    /// Metoda koja iscrtava back dugme kada kreator levela izabere opciju kreiranja ili ucitavanja postojeceg levela
    /// WHY: Zato sto olaksava vracanje na inicijalnu postavku gde moze da se izabere jedna od ponudjenih opcija
    /// </summary>
    /// <param name="buttonRect">Parametar koji definise postavku dugmeta</param>
    private void DrawBackButton()
    {
        EditorGUILayout.Space(FIELDS_SPACE);
        GUI.color = Color.cyan;

        if (GUILayout.Button("Back"))
        {
            _states = States.Initialized;
        }

        GUI.color = _defaultGUIColor;
    }

    /// <summary>
    /// Metoda koja iscrtava pocetni UI u kome definisemo parametre za kreiranje levela
    /// WHY: Zato sto su ti parametri neophodni za kreiranje levela, i njih kontrolise kreator levela
    /// </summary>
    private void DrawNewWorldUI()
    {
        EditorGUILayout.Space(5 * FIELDS_SPACE);
        EditorGUILayout.BeginVertical();
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("World name: ");
                _worldName = EditorGUILayout.TextField(_worldName);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Skybox: ");
                _skybox = EditorGUILayout.ObjectField(_skybox, typeof(Material)) as Material;
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of chapters in world: ");
                _numberOfChaptersInWorld = EditorGUILayout.IntField(_numberOfChaptersInWorld);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of levels in chapter: ");
                _numberOfLevelsInChapter = EditorGUILayout.IntField(_numberOfLevelsInChapter);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of sides in tube: ");
                //TODO: pretvoriti broj stranica tunela u padajuci meni a ne slider
                _numberOfSidesInTube = EditorGUILayout.IntSlider(_numberOfSidesInTube, 6, 10);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of points in the first level: ");
                _numberOfPointsInFirstLevel = EditorGUILayout.IntSlider(_numberOfPointsInFirstLevel, 5, 50);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Distance between points: ");
                _distanceBetweenPoints = EditorGUILayout.IntSlider(_distanceBetweenPoints, 50, 200);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of new points to add per level: ");
                _numberOfNewPointsToAddPerLevel = EditorGUILayout.IntSlider(_numberOfNewPointsToAddPerLevel, 1, 10);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Minimal distance between obstacles in the level: ");
                _minDistanceBetweenObstacles = EditorGUILayout.IntField(Mathf.Clamp(_minDistanceBetweenObstacles, 20, 100));
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Maximal distance between obstacles in the level: ");
                _maxDistanceBetweenObstacles = EditorGUILayout.IntField(Mathf.Clamp(_maxDistanceBetweenObstacles, 20, 100));
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();
        GUILayout.FlexibleSpace();

        // definišemo dugme za kreiranje levela unutar jednog sveta
        GUI.color = Color.green;
        GUI.enabled = _numberOfSidesInTube != 0
                      && !_worldName.Equals(string.Empty)
                      && _distanceBetweenPoints != 0
                      && _numberOfPointsInFirstLevel != 0
                      && _numberOfNewPointsToAddPerLevel != 0
                      && _numberOfChaptersInWorld != 0
                      && _numberOfLevelsInChapter != 0;
        if (GUILayout.Button("Create Levels"))
        {
            CreateWorldButtonHandler();
            CreateLevelsButtonHandler();
            if (EditorUtility.DisplayDialog("Success", "Svet sa levelima je uspesno kreiran!", "OK"))
            {
                _states = States.Uninitialized;
            }
        }

        GUI.color = _defaultGUIColor;
        GUILayout.Space(20f);
    }

    /// <summary>
    /// Metoda koja iscrtava UI kada kreator levela zeli da ucita neki level i edituje njegove parametre
    /// Zato sto je to drugacije od kreiranja novog levela
    /// </summary>
    private void DrawLoadWorldUI()
    {
        EditorGUILayout.Space(5 * FIELDS_SPACE);
        EditorGUILayout.BeginVertical();
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Current World: ");
                EditorGUI.BeginChangeCheck();
                _currentWorld = EditorGUILayout.ObjectField(_currentWorld, typeof(WorldData)) as WorldData;
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(5 * FIELDS_SPACE);

            if (_currentWorld != null)
            {
                _numberOfSidesInTube = _currentWorld.NumberOfSidesInTube;
                _worldName = _currentWorld.name;
                _distanceBetweenPoints = _currentWorld.DistanceBetweenPoints;
                _numberOfPointsInFirstLevel = _currentWorld.NumberOfPointsInFirstLevel;
                _numberOfNewPointsToAddPerLevel = _currentWorld.NumberOfNewPointsToAddPerLevel;
                _numberOfChaptersInWorld = _currentWorld.Chapters.Count;
                _numberOfLevelsInChapter = _currentWorld.Chapters[0].Levels.Count;
                _skybox = _currentWorld.Skybox;
                _minDistanceBetweenObstacles = _currentWorld.MinDistanceBetweenObstacles;
                _maxDistanceBetweenObstacles = _currentWorld.MaxDistanceBetweenObstacles;
            }

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Skybox: ");
                _skybox = EditorGUILayout.ObjectField(_skybox, typeof(Material)) as Material;
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of sides in tube: ");
                EditorGUILayout.LabelField(_numberOfSidesInTube.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("World name: ");
                EditorGUILayout.LabelField(_worldName);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of chapters in world: ");
                EditorGUILayout.LabelField(_numberOfChaptersInWorld.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of levels in chapter: ");
                EditorGUILayout.LabelField(_numberOfLevelsInChapter.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of points in the first level: ");
                EditorGUILayout.LabelField(_numberOfPointsInFirstLevel.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Distance between points: ");
                EditorGUILayout.LabelField(_distanceBetweenPoints.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Number of new points to add per level: ");
                EditorGUILayout.LabelField(_numberOfNewPointsToAddPerLevel.ToString());
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(FIELDS_SPACE);
        }
        EditorGUILayout.EndVertical();
    }

    /// <summary>
    /// Metoda koja iscrtava listu koja prikazuje sve levele i njihove elemente
    /// WHY: Zato sto je potrebno kreatoru levela da vidi kakave je levele automator napravio i da modifikuje to ukoliko je potrebno
    /// </summary>
    private void DrawScrollViewForLevels()
    {
        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
        {
            if (_currentLevel != null)
            {
                EditorGUILayout.BeginHorizontal(BOX_STYLE_STRING);
                {
                    EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
                    {
                        DrawTextureAndLightsColumn();
                        DrawLevelsColumn();
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
                    {
                        DrawPointsColumn();
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
                    {
                        DrawObstacleTypes();
                        DrawReplaceButton();
                        DrawDistanceBetweenObstacles();
                        DrawObstaclesColumn();
                    }
                    EditorGUILayout.EndVertical();
                    GUILayout.Space(20f);
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        EditorGUILayout.EndScrollView();
    }

    /// <summary>
    /// Metoda koja iscrtava polje za promenu teksture i boja svetala u svim levelima
    /// WHY: Zato sto ako zelimo da automatizujemo taj proces 
    /// </summary>
    private Texture2D _currentTexture;
    private Material _currentMaterial;
    private Color _currentInsideColor = Color.clear;
    private Color _currentOutsideColor = Color.clear;

    private void DrawTextureAndLightsColumn()
    {
        GUILayout.Space(20f);
        EditorGUILayout.HelpBox(
            "Izaberi teksturu, materijal, i boje svetala za unutrasnji i spoljasnji deo tunela kako bi zamenio pomenute elemente na svim levelima! \nMoraju biti izabrani svi elementi kako bi se ukljucilo dugme za zamenu",
            MessageType.Info);
        EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
        {
            _currentTexture = EditorGUILayout.ObjectField("All Tubes Texture", _currentTexture, typeof(Texture2D), false) as Texture2D;
            _currentMaterial = EditorGUILayout.ObjectField("All Tubes Material", _currentMaterial, typeof(Material), false) as Material;
            _currentInsideColor = EditorGUILayout.ColorField("Inside Light Color", _currentInsideColor);
            _currentOutsideColor = EditorGUILayout.ColorField("Outside Light Color", _currentOutsideColor);

            GUI.enabled = _currentTexture != null &&
                          _currentMaterial != null &&
                          _currentInsideColor != Color.clear &&
                          _currentOutsideColor != Color.clear;
            if (GUILayout.Button("Change All"))
            {
                foreach (var level in _currentWorld.Levels)
                {
                    level.tubeTexture = _currentTexture;
                    level.tubeMaterial = _currentMaterial;
                    level.tubeAndObstacleInsideLightColor = _currentInsideColor;
                    level.tubeAndObstacleOutsideLightColor = _currentOutsideColor;
                }
            }

            GUI.enabled = true;
        }
        EditorGUILayout.EndVertical();
        GUILayout.Space(50f);
    }

    /// <summary>
    /// Metoda koja iscrtava prvu kolonu sa levelima u tabeli koja pokazuje levele i prepreke kada ucitamo neki svet
    /// WHY: Bitna metoda za prikaz svih levela i njihovih najbitnijih podataka
    /// </summary>
    private void DrawLevelsColumn()
    {
        for (int i = 0; i < _currentWorld.Levels.Count; i++)
        {
            GUI.color = _currentLevel == _currentWorld.Levels[i] ? Color.cyan : _defaultGUIColor;

            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                if (GUILayout.Button(LEVEL_STRING + (i + 1)))
                {
                    LoadLevelButtonHandler(i);
                }

                if (_currentLevel == _currentWorld.Levels[i])
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Level Length");
                        GUILayout.Label(_lengthCalculator.length.ToString("F1"));
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Number of obstacles");
                        GUILayout.Label(_numberOfObstaclesInLevel[i].ToString());
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUI.BeginChangeCheck();
                _isLevelOutside[i] = EditorGUILayout.Toggle("Outside Obstacles", _isLevelOutside[i]);
                _currentWorld.Levels[i].tubeTexture = EditorGUILayout.ObjectField("Tube Texture", _currentWorld.Levels[i].tubeTexture, typeof(Texture2D), false) as Texture2D;
                _currentWorld.Levels[i].tubeMaterial = EditorGUILayout.ObjectField("Tube Material", _currentWorld.Levels[i].tubeMaterial, typeof(Material), false) as Material;
                _currentWorld.Levels[i].tubeAndObstacleInsideLightColor = EditorGUILayout.ColorField("Tube And Obstacle Inside Light Color", _currentWorld.Levels[i].tubeAndObstacleInsideLightColor);
                _currentWorld.Levels[i].tubeAndObstacleOutsideLightColor =
                    EditorGUILayout.ColorField("Tube And Obstacle Outside Light Color", _currentWorld.Levels[i].tubeAndObstacleOutsideLightColor);
                _currentWorld.Levels[i].obstacleMaterial = EditorGUILayout.ObjectField("Obstacles Material", _currentWorld.Levels[i].obstacleMaterial, typeof(Material), false) as Material;

                if (EditorGUI.EndChangeCheck())
                {
                    LoadLevelButtonHandler(i);
                }

                EditorGUILayout.Space(10f);
                GUI.color = Color.yellow;
                if (GUILayout.Button("Generate Random Obstacles"))
                {
                    _isAddingNewObstacleForbidden = false;
                    while (!_isAddingNewObstacleForbidden)
                    {
                        AddObstacleButtonListener();
                    }
                }

                GUI.color = _defaultGUIColor;
            }
            EditorGUILayout.EndVertical();

            if (i == _currentWorld.Levels.Count - 1)
            {
                GUI.color = _defaultGUIColor;
            }

            GUILayout.Space(20f);
        }
    }

    /// <summary>
    /// Metoda koja iscrtava kolonu sa tackama koje definisu jedan level
    /// WHY: Zato sto je jako bitno da vidimo koje su to tacke i da preko odgovarajucih dugmica menjamo njihove vrednosti ako je potrebno
    /// </summary>
    private void DrawPointsColumn()
    {
        if (GUILayout.Button("Generate Again"))
        {
            for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
            {
                _currentLevel.tubePoints[i] = GenerateRandomPoint(i != 0 ? _currentLevel.tubePoints[i - 1] : new SplinePoint());
            }

            _splineComputer.SetPoints(_currentLevel.tubePoints);
        }

        // positions
        EditorGUILayout.BeginHorizontal(BOX_STYLE_STRING);
        {
            if (GUILayout.Button("Set All Positions X to 0"))
            {
                for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                {
                    _currentLevel.tubePoints[i].position.x = 0f;
                }

                _splineComputer.SetPoints(_currentLevel.tubePoints);
            }

            if (GUILayout.Button("Set All Positions Y to 0"))
            {
                for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                {
                    _currentLevel.tubePoints[i].position.y = 0f;
                }

                _splineComputer.SetPoints(_currentLevel.tubePoints);
            }

            if (GUILayout.Button("Set All Positions Z to 0"))
            {
                for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                {
                    _currentLevel.tubePoints[i].position.z = 0f;
                }

                _splineComputer.SetPoints(_currentLevel.tubePoints);
            }
        }
        EditorGUILayout.EndHorizontal();

        // normals
        EditorGUILayout.BeginHorizontal(BOX_STYLE_STRING);
        {
            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                if (GUILayout.Button("Set All Normals X to 0"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.x = 0f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }

                if (GUILayout.Button("Set All Normals X to 1"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.x = 1f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                if (GUILayout.Button("Set All Normals Y to 0"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.y = 0f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }

                if (GUILayout.Button("Set All Normals Y to 1"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.y = 1f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                if (GUILayout.Button("Set All Normals Z to 0"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.z = 0f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }

                if (GUILayout.Button("Set All Normals Z to 1"))
                {
                    for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
                    {
                        _currentLevel.tubePoints[i].normal.z = 1f;
                    }

                    _splineComputer.SetPoints(_currentLevel.tubePoints);
                }
            }
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < _currentLevel.tubePoints.Length; i++)
        {
            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                var point = _currentLevel.tubePoints[i];
                point.position = EditorGUILayout.Vector3Field("Point " + (i + 1) + " Position", point.position);
                point.normal = EditorGUILayout.Vector3Field("Point Normal", point.normal).normalized;
                _currentLevel.tubePoints[i] = point;
            }
            EditorGUILayout.EndVertical();
            GUILayout.Space(5f);
        }
    }

    /// <summary>
    /// Metoda koja iscrtava spisak sa svim preprekama koje se nalaze u editor SO-u u obliku checkboxova
    /// WHY: Da bi mogao da izaberem koju tip prepreka zelim da udje u sastav rog levela koji pravim
    /// </summary>
    private void DrawObstacleTypes()
    {
        //TODO: dugme "generate random obstacles" prebaciti iz 1. u 3. kolonu
        EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
        {
            EditorGUILayout.HelpBox("Izaberi prepreke koje zelis da dodas u level! \nUkoliko ne izaberes nista, sistem ce sam da dodaje prepreke po svom izboru!", MessageType.Info);
            EditorGUILayout.BeginHorizontal();
            {
                GUI.enabled = _isObstacleSelected.Where(s => s).ToArray().Length > 0 && !_isRandomObstacle;
                if (GUILayout.Button("Disable All"))
                {
                    for (int i = 0; i < _isObstacleSelected.Count; i++)
                    {
                        _isObstacleSelected[i] = false;
                    }
                }

                GUI.enabled = true;
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            {
                GUI.color = _isRandomObstacle ? Color.green : _defaultGUIColor;
                EditorGUILayout.LabelField("Random: ");
                _isRandomObstacle = EditorGUILayout.Toggle(_isRandomObstacle);
                GUI.color = _defaultGUIColor;
            }
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < _currentObstaclesArray.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUI.color = _isObstacleSelected[i] ? Color.green : _defaultGUIColor;
                    EditorGUILayout.LabelField(_currentObstaclesArray[i].name);
                    _isObstacleSelected[i] = _isRandomObstacle || EditorGUILayout.Toggle(_isObstacleSelected[i]);
                    GUI.color = _defaultGUIColor;
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        EditorGUILayout.EndVertical();
    }

    /// <summary>
    /// Metoda koja trazi selektovanu prepreku u levelu i menja je sa tipom prepreke koji ucitamo
    /// WHY: Zato sto postoje neke prepreke na koje se korisnici zale i koje zelimo potpuno da izbacimo iz svih levela
    /// </summary>
    private void DrawReplaceButton()
    {
        if (_isRandomObstacle || _isObstacleSelected.Where(s => s).ToArray().Length != 1) return;

        _obstacleTypeToReplaceWith = EditorGUILayout.ObjectField("Type of obstacle to replace with: ", _obstacleTypeToReplaceWith, typeof(ObstacleTypeSO), false) as ObstacleTypeSO;
        GUILayout.Space(5 * FIELDS_SPACE);
        bool isSelectedObstacleInScene = false;
        if (_obstaclesInCurrentLevel.Any(obstacle => obstacle.obstacleType.Equals(_currentObstaclesArray[_isObstacleSelected.IndexOf(true)].obstacleType)))
        {
            isSelectedObstacleInScene = true;
            EditorGUILayout.HelpBox(
                $"Na sceni postoji: {_obstaclesInCurrentLevel.Count(obstacle => obstacle.obstacleType.Equals(_currentObstaclesArray[_isObstacleSelected.IndexOf(true)].obstacleType))} prepreka",
                MessageType.Info);
        }
        else
        {
            EditorGUILayout.HelpBox("Selektovana prepreka ne postoji u izabranom levelu!", MessageType.Info);
        }

        GUILayout.Space(5 * FIELDS_SPACE);

        GUI.enabled = _obstacleTypeToReplaceWith != null && isSelectedObstacleInScene;
        GUI.color = GUI.enabled ? Color.cyan : _defaultGUIColor;
        if (GUILayout.Button("Replace"))
        {
            for (int i = 0; i < _obstaclesInCurrentLevel.Count; i++)
            {
                if (_obstaclesInCurrentLevel[i].obstacleType.Equals(_currentObstaclesArray[_isObstacleSelected.IndexOf(true)].obstacleType))
                {
                    _obstaclesInCurrentLevel[i].obstacleType = _currentObstacleData[i].obstacleType = _obstacleTypeToReplaceWith;
                    ReplaceObstacle(i, _obstaclesInCurrentLevel[i]);
                }
            }

            for (int i = 0; i < _currentWorld.Levels.Count; i++)
            {
                if (_currentLevel == _currentWorld.Levels[i])
                {
                    LoadLevelButtonHandler(i);
                }
            }

            EditorUtility.DisplayDialog("Success", "Prepreke su uspešno zamenjene!", "Ok");
        }

        GUI.color = _defaultGUIColor;
        GUILayout.Space(7 * FIELDS_SPACE);
        GUI.enabled = true;
    }

    /// <summary>
    /// Metoda koja iscrtava polja u kojima definisemo koliko je najmanji i najveci razmak izmedju prepreka
    /// WHY: Zato sto daje mogucnost kreatoru levela da izabere duzinu po svojoj zelji i tako donese dinamiku levela
    /// </summary>
    private void DrawDistanceBetweenObstacles()
    {
        GUILayout.Space(20f);
        EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
        {
            EditorGUILayout.HelpBox("Izaberi najmanju i najvecu duzinu razmaka izmedju prepreka! \nUkoliko ne izaberes, sistem ce staviti default vrednosti (20 - 100)!", MessageType.Info);
            EditorGUILayout.LabelField("Minimalna distanca izmedju prepreka: ");
            _minDistanceBetweenObstacles = EditorGUILayout.IntField(Mathf.Clamp(_minDistanceBetweenObstacles, 20, 100));
            EditorGUILayout.LabelField("Maximalna distanca izmedju prepreka: ");
            _maxDistanceBetweenObstacles = EditorGUILayout.IntField(Mathf.Clamp(_maxDistanceBetweenObstacles, 20, 100));
        }
        EditorGUILayout.EndVertical();
    }

    /// <summary>
    /// Metoda koja iscrtava kolonu sa preprekama
    /// WHY: Zato sto je neophodno u svakom trenutku da vidimo koje su to prepreke koje se nalaze u jednom levelu
    /// </summary>
    private void DrawObstaclesColumn()
    {
        DrawAddObstacleButton();
        DrawDeleteAllObstaclesButton();
        GUILayout.Space(20f);

        for (int i = 0; i < _currentObstacleData.Count; i++)
        {
            if (_currentObstacleData[i] == null) continue;
            GUI.color = Selection.activeGameObject == _obstaclesInCurrentLevel[i].gameObject ? Color.cyan : _defaultGUIColor;
            EditorGUILayout.BeginVertical(BOX_STYLE_STRING);
            {
                DrawObstacleInfo(i);
                GUI.color = _defaultGUIColor;
                GUILayout.Space(5f);
            }
            EditorGUILayout.EndVertical();
            GUILayout.Space(20f);
        }
    }

    /// <summary>
    /// Metoda koja iscrtava dugme "Add Obstacle" i poziva sve dogadjaje koji treba da se okinu pritiskom na to dugme
    /// WHY: Zato sto je to jedno od dugmica neophodno da bi kreator levela dodao prepreku 
    /// </summary>
    private void DrawAddObstacleButton()
    {
        GUILayout.Space(20f);
        GUI.color = Color.green;
        if (GUILayout.Button("Add Obstacle"))
        {
            GUI.color = _defaultGUIColor;
            AddObstacleButtonListener();
        }
    }

    private void AddObstacleButtonListener()
    {
        if (_isRandomObstacle)
        {
            int randomObstacle = Random.Range(0, _currentObstaclesArray.Length);
            AddObstacle(randomObstacle);
        }
        else
        {
            System.Random random = new System.Random();
            var arrayOfIndexesOfSelectedObstacle = _isObstacleSelected
                .Select((element, index) => element ? index : -1)
                .Where(index => index >= 0)
                .OrderBy(_ => random.Next())
                .ToList();
            for (int i = 0; i < arrayOfIndexesOfSelectedObstacle.Count; i++)
            {
                if (!AddObstacle(arrayOfIndexesOfSelectedObstacle[i]))
                    break;
            }
        }
    }

    /// <summary>
    /// Metoda koja dodaje prepreke u level
    /// </summary>
    /// <param name="randomObstacle">Parametar koji definise da li je random ili jasno definisana prepreka</param>
    /// <returns></returns>
    private bool AddObstacle(int randomObstacle)
    {
        float currentObstaclePosition;
        if (_currentObstacleData.Count > 0)
        {
            currentObstaclePosition = _obstaclesInCurrentLevel[_currentObstacleData.Count - 1].splineFollower.CalculateLength(0, _currentObstacleData[_currentObstacleData.Count - 1].positionOnPath) +
                                      Random.Range(_minDistanceBetweenObstacles, _maxDistanceBetweenObstacles);
            if (currentObstaclePosition > _obstaclesInCurrentLevel[_currentObstacleData.Count - 1].splineFollower.CalculateLength())
            {
                _isAddingNewObstacleForbidden = EditorUtility.DisplayDialog("Warning", "Ne moze se dodati nova prepreka jer nema vise mesta na putanji", "Ok");
                return false;
            }
        }
        else
        {
            currentObstaclePosition = Random.Range(_minDistanceBetweenObstacles, _maxDistanceBetweenObstacles);
        }

        Obstacle obstacle = GenerateObstacle(_currentObstaclesArray, currentObstaclePosition, randomObstacle);
        _obstaclesInCurrentLevel.Add(obstacle);
        _currentObstacleData.Add(new ObstacleData(obstacle.splineFollower.GetPercentFromDistance(currentObstaclePosition),
            obstacle.splineFollower.motion.rotationOffset,
            obstacle.splineFollower.motion.offset, obstacle.speed, obstacle.delay,
            obstacle.obstacleType));
        _numberOfObstaclesInLevel[_currentWorld.Levels.IndexOf(_currentLevel)]++;
        return true;
    }

    /// <summary>
    /// Metoda koja iscrtava dugme koje brise sve prepreke u jednom levelu
    /// WHY: Zato sto je lakse obrisati sve prepreke odjednom nego svaku posebno
    /// </summary>
    private void DrawDeleteAllObstaclesButton()
    {
        GUILayout.Space(20f);
        GUI.color = Color.red;
        if (GUILayout.Button("Delete All Obstacles"))
        {
            GUI.color = _defaultGUIColor;
            foreach (var obstacle in _obstaclesInCurrentLevel)
            {
                DestroyImmediate(obstacle.gameObject);
            }

            _currentObstacleData.Clear();
            _obstaclesInCurrentLevel.Clear();
            _numberOfObstaclesInLevel[_currentWorld.Levels.IndexOf(_currentLevel)] = 0;
        }
    }

    /// <summary>
    /// Metoda koja iscrtava informacije o prepreci koja se nalazi u nekom levelu
    /// WHY: Zato sto je neophodno prikazati tu informaciju kreatoru levela kako bi mogao da kontrolise prepreke u nekom levelu naknadno, nakon kreiranja levela i prepeka
    /// </summary>
    /// <param name="obstacleNumber">Parametar koji odredjuje o kojoj prepreci u izabranom levelu je rec</param>
    private void DrawObstacleInfo(int obstacleNumber)
    {
        EditorGUILayout.LabelField((obstacleNumber + 1).ToString("000"), new GUIStyle
        {
            fontSize = 45,
            alignment = TextAnchor.MiddleLeft,
            clipping = TextClipping.Overflow,
            stretchHeight = true,
            stretchWidth = true
        });
        
        EditorGUILayout.Space(20f);
        if (GUILayout.Button("Select"))
        {
            Selection.activeGameObject = _obstaclesInCurrentLevel[obstacleNumber].gameObject;
        }

        EditorGUI.BeginChangeCheck();
        _obstaclesInCurrentLevel[obstacleNumber].obstacleType = _currentObstacleData[obstacleNumber].obstacleType =
            (ObstacleTypeSO) EditorGUILayout.ObjectField("Obstacle Type", _currentObstacleData[obstacleNumber].obstacleType, typeof(ObstacleTypeSO), false);
//        _obstaclesInCurrentLevel[obstacleNumber].obstacleType = _currentObstacleData[obstacleNumber].obstacleType;
        if (EditorGUI.EndChangeCheck())
        {
            if (_obstaclesInCurrentLevel[obstacleNumber].obstacleType != null)
            {
                ReplaceObstacle(obstacleNumber, _obstaclesInCurrentLevel[obstacleNumber]);
            }
        }

        _currentObstacleData[obstacleNumber].positionOnPath = EditorGUILayout.FloatField("Position on Path", _currentObstacleData[obstacleNumber].positionOnPath);
        _obstaclesInCurrentLevel[obstacleNumber].splineFollower.SetPercent(_currentObstacleData[obstacleNumber].positionOnPath);
        _currentObstacleData[obstacleNumber].delay = EditorGUILayout.FloatField("Delay", _currentObstacleData[obstacleNumber].delay);
        _obstaclesInCurrentLevel[obstacleNumber].delay = _currentObstacleData[obstacleNumber].delay;
        _currentObstacleData[obstacleNumber].speed = EditorGUILayout.FloatField("Speed", _currentObstacleData[obstacleNumber].speed);
        _obstaclesInCurrentLevel[obstacleNumber].speed = _currentObstacleData[obstacleNumber].speed;
        _currentObstacleData[obstacleNumber].positionOffset = EditorGUILayout.Vector2Field("Position Offset", _currentObstacleData[obstacleNumber].positionOffset);
        _obstaclesInCurrentLevel[obstacleNumber].splineFollower.motion.offset = _currentObstacleData[obstacleNumber].positionOffset;

        int selectedIndex = _rotationOffsetsZAxis
            .ToList()
            .FindIndex(x => x == Mathf.Abs(_currentObstacleData[obstacleNumber].rotationOffset.z));
        selectedIndex = selectedIndex < 0 ? 0 : selectedIndex;
        _currentObstacleData[obstacleNumber].rotationOffset = new Vector3(_currentObstacleData[obstacleNumber].rotationOffset.x, _currentObstacleData[obstacleNumber].rotationOffset.y,
            _rotationOffsetsZAxis[EditorGUILayout.Popup("Rotation Offset", selectedIndex, _rotationOffsetsZAxisString)]);
        _obstaclesInCurrentLevel[obstacleNumber].splineFollower.motion.rotationOffset = _currentObstacleData[obstacleNumber].rotationOffset;

        GUI.color = Color.red;
        if (GUILayout.Button("Delete"))
        {
            _currentObstacleData.RemoveAt(obstacleNumber);
            DestroyImmediate(_obstaclesInCurrentLevel[obstacleNumber].gameObject);
            _obstaclesInCurrentLevel.RemoveAt(obstacleNumber);
            _numberOfObstaclesInLevel[_currentWorld.Levels.IndexOf(_currentLevel)]--;
        }
    }

    /// <summary>
    /// Metoda koja menja trenutnu prepreku za novu na osnovu skriptabilnog objekta
    /// WHY: Zato sto omogucava kreatoru levela da naknadno izmeni prepreke koje mu se ne uklapaju u level
    /// </summary>
    /// <param name="obstacleNumber">broj prepreke koja se menja</param>
    /// <param name="obstacleToReplaceWith">prepreka koja treba da se zameni novom preprekom</param>
    private void ReplaceObstacle(int obstacleNumber, Obstacle obstacleToReplaceWith)
    {
        _obstaclesInCurrentLevel[obstacleNumber] = GenerateObstacle(_currentObstaclesArray, obstacleToReplaceWith);
        if (_obstaclesInCurrentLevel[obstacleNumber] != obstacleToReplaceWith)
        {
            DestroyImmediate(obstacleToReplaceWith.gameObject);
        }
    }

    /// <summary>
    /// Metoda koja kreira skriptabilni objekat za svet
    /// WHY: Zato sto taj skriptabilni objekat drzi reference na sve levele koji se nalaze u tom svetu
    /// </summary>
    private void CreateWorldButtonHandler()
    {
        if (_currentWorld == null)
        {
            _currentWorld = CreateOrGetWorld();
        }
        else if (_currentWorld.Levels.Count > _numberOfLevelsInChapter * _numberOfChaptersInWorld)
        {
            for (int i = _currentWorld.Levels.Count; i > _numberOfLevelsInChapter * _numberOfChaptersInWorld; i--)
            {
                _currentWorld.Levels.Remove(_currentWorld.Levels.Last());
            }
        }
        else if (_currentWorld.Levels.Count < _numberOfLevelsInChapter * _numberOfChaptersInWorld)
        {
            for (int i = _currentWorld.Levels.Count; i < _numberOfLevelsInChapter * _numberOfChaptersInWorld; i++)
            {
                _currentWorld.Levels.Add(null);
            }
        }
    }

    /// <summary>
    /// Metoda koja se poziva kada kreator levela klikne na dugme "Create Levels" u editor prozoru
    /// WHY: Zato sto ona kreira skriptabilne objekte koji ce cuvati sve podatke o jednom levelu
    /// </summary>
    private void CreateLevelsButtonHandler()
    {
        _currentLevelLength = _numberOfPointsInFirstLevel;

        var allLevelsNumbers = Enumerable.Range(1, _numberOfChaptersInWorld * _numberOfLevelsInChapter)
            .Reverse()
            .ToList();
        for (int i = 0; i < _numberOfChaptersInWorld; i++)
        {
            for (int j = 0; j < _numberOfLevelsInChapter; j++)
            {
                if (_currentWorld.Levels.Count <= j)
                {
                    _currentWorld.Levels.Add(null);
                }

                if (j != 0)
                {
                    _currentLevelLength += _numberOfNewPointsToAddPerLevel;
                }

                _currentWorld.Chapters[i].Levels[j] = CreateOrGetLevel(_currentLevelLength, allLevelsNumbers.Last());
                allLevelsNumbers.RemoveAt(allLevelsNumbers.IndexOf(allLevelsNumbers.Last()));
            }
        }

        _numberOfObstaclesInLevel = new int[_numberOfLevelsInChapter];
        _isLevelOutside = new bool[_numberOfLevelsInChapter];
        _currentLevel = _currentWorld.Levels[0];
        _splineComputer.SetPoints(_currentLevel.tubePoints);
    }

    /// <summary>
    /// Metoda koja postavlja rotaciju prepreka u tunelu na osnovu toga koliko tunel ima strana (uglova)
    /// WHY: Zato sto se tada rotacija prepreka razlikuje
    /// </summary>
    private void SetObstaclesRotation()
    {
        if (_currentNumberOfSidesInTube == _numberOfSidesInTube) return;
        _currentNumberOfSidesInTube = _numberOfSidesInTube;
        var angle = 360 / _numberOfSidesInTube;
        var rotationOffsetsZAxisTemporaryList = new List<int>();
        for (int i = 0; i < _numberOfSidesInTube; i++)
        {
            rotationOffsetsZAxisTemporaryList.Add(i * angle);
        }

        _rotationOffsetsZAxis = rotationOffsetsZAxisTemporaryList.ToArray();
        _rotationOffsetsZAxisString = _rotationOffsetsZAxis.Select(x => x.ToString()).ToArray();
    }

    private void LoadLevelButtonHandler(int i)
    {
        ClearLoadedLevel();
        _currentLevel = _currentWorld.Levels[i];
        _splineComputer.SetPoints(_currentLevel.tubePoints);
        _currentObstacleData = _isLevelOutside[i] ? _currentWorld.Levels[i].outsideObstaclesData : _currentWorld.Levels[i].insideObstaclesData;
        _currentObstaclesArray = _isLevelOutside[i] ? _outsideObstacles : _insideObstacles;
        _mainCamera.transform.localPosition = _isLevelOutside[i] ? _outsideCameraPosition : _insideCameraPosition;
        _tubeGenerator.flipFaces = !_isLevelOutside[i];
        _numberOfObstaclesInLevel[i] = _currentObstacleData.Count;
        //NOTE: dodato za prepreke
        _isObstacleSelected = new List<bool>(Enumerable.Range(0, _currentObstaclesArray.Length).Select(s => false));
        _obstaclesInCurrentLevel.Clear();
        for (int j = 0; j < _currentObstacleData.Count; j++)
        {
            _obstaclesInCurrentLevel.Add(null);
        }

        LoadLevel(_currentObstaclesArray, _currentObstacleData);
        ChangeTubeAndObstacleTextureAndColor(i);
    }

    private void LoadLevel(Obstacle[] editorObstacleData, List<ObstacleData> obstacleData)
    {
        for (int i = 0; i < obstacleData.Count; i++)
        {
            Obstacle currentEditorObstacleData = null;
            for (int j = 0; j < editorObstacleData.Length; j++)
            {
                if (editorObstacleData[j].obstacleType.Equals(obstacleData[i].obstacleType))
                {
                    currentEditorObstacleData = editorObstacleData[j];
                }
            }

            Obstacle obstacle = Instantiate(currentEditorObstacleData);
            obstacle.transform.parent = _splineComputer.transform;
            obstacle.delay = obstacleData[i].delay;
            obstacle.speed = obstacleData[i].speed;
            obstacle.splineFollower.computer = _splineComputer;
            obstacle.splineFollower.SetPercent(obstacleData[i].positionOnPath);
            obstacle.splineFollower.motion.rotationOffset = obstacleData[i].rotationOffset;
            obstacle.splineFollower.motion.offset = obstacleData[i].positionOffset;
            _obstaclesInCurrentLevel[i] = obstacle;
        }
    }

    private void ChangeTubeAndObstacleTextureAndColor(int i)
    {
        _tubeMeshRenderer.sharedMaterial = _currentWorld.Levels[i].tubeMaterial;
        _tubeMeshRenderer.sharedMaterial.mainTexture = _currentWorld.Levels[i].tubeTexture;
        _tubeLight.color = _obstacleLight.color = _isLevelOutside[i] ? _currentWorld.Levels[i].tubeAndObstacleOutsideLightColor : _currentWorld.Levels[i].tubeAndObstacleInsideLightColor;
        for (int j = 0; j < _obstaclesInCurrentLevel.Count; j++)
        {
            foreach (var meshRenderer in _obstaclesInCurrentLevel[j].GetComponentsInChildren<MeshRenderer>())
            {
                meshRenderer.sharedMaterial = _currentWorld.Levels[i].obstacleMaterial;
            }
        }
    }

    private WorldData CreateOrGetWorld()
    {
        WorldData worldData = AssetDatabase.LoadAssetAtPath<WorldData>(WORLDS_FOLDER_PATH + _worldName + "/_" + _worldName + ASSET_EXTENSION);
        if (worldData == null)
        {
            worldData = CreateInstance<WorldData>();
            worldData.Chapters = new List<Chapter>(Enumerable.Range(0, _numberOfChaptersInWorld).Select(x => new Chapter()));
            foreach (var chapter in worldData.Chapters)
            {
                chapter.Levels = new List<LevelData>(Enumerable.Range(0, _numberOfLevelsInChapter).Select(x => CreateInstance<LevelData>()));
            }

            worldData.Skybox = _skybox;
            worldData.NumberOfSidesInTube = _numberOfSidesInTube;
            worldData.NumberOfPointsInFirstLevel = _numberOfPointsInFirstLevel;
            worldData.NumberOfNewPointsToAddPerLevel = _numberOfNewPointsToAddPerLevel;
            worldData.DistanceBetweenPoints = _distanceBetweenPoints;
            worldData.MinDistanceBetweenObstacles = _minDistanceBetweenObstacles;
            worldData.MaxDistanceBetweenObstacles = _maxDistanceBetweenObstacles;
            AssetDatabase.CreateFolder(WORLDS_FOLDER_PATH.Remove(WORLDS_FOLDER_PATH.Length - 1), _worldName);
            AssetDatabase.CreateAsset(worldData, WORLDS_FOLDER_PATH + _worldName + "/_" + _worldName + ASSET_EXTENSION);
        }
        else
        {
            SetLocalDataFromWorldData(worldData);
        }

        return worldData;
    }

    private void SetLocalDataFromWorldData(WorldData worldData)
    {
        _worldName = worldData.name.Replace("_", "");
        _numberOfPointsInFirstLevel = worldData.NumberOfPointsInFirstLevel;
        _numberOfNewPointsToAddPerLevel = worldData.NumberOfNewPointsToAddPerLevel;
        _distanceBetweenPoints = worldData.DistanceBetweenPoints;
        _numberOfChaptersInWorld = worldData.Chapters.Count;
        _numberOfLevelsInChapter = worldData.Chapters[0].Levels.Count;
        RenderSettings.skybox = worldData.Skybox;
        if (worldData.Levels.Count > 0)
        {
            _currentLevel = worldData.Levels[0];
            _currentObstacleData = worldData.Levels[0].insideObstaclesData;
            _currentObstaclesArray = _insideObstacles;
        }

        _numberOfObstaclesInLevel = new int[worldData.Levels.Count];
        _isLevelOutside = new bool[worldData.Levels.Count];
        for (int i = 0; i < _numberOfObstaclesInLevel.Length; i++)
        {
            _numberOfObstaclesInLevel[i] = worldData.Levels[i].insideObstaclesData.Count;
        }

        //NOTE: dodato za prepreke
        _isObstacleSelected = new List<bool>(Enumerable.Range(0, _currentObstaclesArray.Length).Select(s => false));

        _obstaclesInCurrentLevel.Clear();
        for (int i = 0; i < _currentObstacleData.Count; i++)
        {
            _obstaclesInCurrentLevel.Add(null);
        }

        ClearLoadedLevel();
        LoadLevel(_insideObstacles, _currentObstacleData);
        ChangeTubeAndObstacleTextureAndColor(0);
    }

    private LevelData CreateOrGetLevel(int numberOfPoints, int levelNumber)
    {
        LevelData levelData = AssetDatabase.LoadAssetAtPath<LevelData>(WORLDS_FOLDER_PATH + _worldName + SLASH_STRING + LEVEL_STRING + levelNumber + ASSET_EXTENSION);
        if (levelData == null)
        {
            levelData = CreateInstance<LevelData>();
            levelData.tubePoints = new SplinePoint[numberOfPoints];
            levelData.tubePoints[0] = new SplinePoint
            {
                size = 1,
                normal = Vector3.up
            };
            for (int i = 1; i < levelData.tubePoints.Length; i++)
            {
                levelData.tubePoints[i] = GenerateRandomPoint(i != 0 ? levelData.tubePoints[i - 1] : new SplinePoint());
            }

            var defaultTubeTexture = AssetDatabase.FindAssets(TEXTURE_FILTER, new[] {ASSETS_PATH});
            levelData.tubeTexture = defaultTubeTexture
                .Select(s => AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(s)))
                .First(s => s.name.Contains("Tube_15"));

            var allTubesMaterialsGUID = AssetDatabase.FindAssets(MATERIAL_FILTER, new[] {ALL_TUBES_MATERIALS_PATH});
            levelData.tubeMaterial = allTubesMaterialsGUID
                .Select(s => AssetDatabase.LoadAssetAtPath<Material>(AssetDatabase.GUIDToAssetPath(s)))
                .First(s => s.name.Contains("Tilling"));

            var allObstaclesMaterialsGUID = AssetDatabase.FindAssets(MATERIAL_FILTER, new[] {All_OBSTACLES_MATERIALS_PATH});
            levelData.obstacleMaterial = allObstaclesMaterialsGUID
                .Select(s => AssetDatabase.LoadAssetAtPath<Material>(AssetDatabase.GUIDToAssetPath(s)))
                .First(s => s.name.Contains("Metal"));

            levelData.tubeAndObstacleInsideLightColor = _insideTubeAndObstacleDefaultColor;
            levelData.tubeAndObstacleOutsideLightColor = _outsideTubeAndObstacleDefaultColor;
            levelData.insideObstaclesData.Clear();
            levelData.outsideObstaclesData.Clear();
            AssetDatabase.CreateAsset(levelData, WORLDS_FOLDER_PATH + _worldName + SLASH_STRING + LEVEL_STRING + levelNumber + ASSET_EXTENSION);
            AssetDatabase.Refresh();
        }

        return levelData;
    }

    private SplinePoint GenerateRandomPoint(SplinePoint previousSplinePoint)
    {
        var randomMin = Random.Range(-15, -25);
        var randomMax = Random.Range(15, 25);
        var random = Random.Range(randomMin, randomMax);
        SplinePoint splinePoint = new SplinePoint
        {
            position = new Vector3(previousSplinePoint.position.x + _distanceBetweenPoints, previousSplinePoint.position.y + random, previousSplinePoint.position.z + random),
            size = 1,
            normal = Vector3.up
        };
        return splinePoint;
    }

    private void ClearLoadedLevel()
    {
        int childCount = _splineComputer.transform.childCount;
        for (int i = 0; i < childCount; i++)
        {
            DestroyImmediate(_splineComputer.transform.GetChild(0).gameObject);
        }
    }

    private Obstacle GenerateObstacle(Obstacle[] editorObstacleData, float currentObstaclePosition, int randomObstacle)
    {
        Obstacle obstacle = Instantiate(editorObstacleData[randomObstacle]);
        obstacle.splineFollower.computer = _splineComputer;
        obstacle.splineFollower.SetPercent(obstacle.splineFollower.GetPercentFromDistance(currentObstaclePosition));
        obstacle.transform.parent = _splineComputer.transform;
        obstacle.splineFollower.motion.rotationOffset = new Vector3(obstacle.splineFollower.motion.rotationOffset.x, obstacle.splineFollower.motion.rotationOffset.y,
            _rotationOffsetsZAxis[Random.Range(0, _rotationOffsetsZAxis.Length)]);
        return obstacle;
    }

    private Obstacle GenerateObstacle(Obstacle[] editorObstacleData, Obstacle previousObstacle)
    {
        Obstacle obstacle = null;
        for (int i = 0; i < editorObstacleData.Length; i++)
        {
            if (editorObstacleData[i].obstacleType.Equals(previousObstacle.obstacleType))
            {
                obstacle = Instantiate(editorObstacleData[i]);
            }
        }

        if (obstacle == null)
        {
            EditorUtility.DisplayDialog("Warning", "Izabran tip prepreke nije za trenutnu stranu puta", "Ok");
            return previousObstacle;
        }

        obstacle.splineFollower.computer = _splineComputer;
        obstacle.splineFollower.SetPercent(previousObstacle.splineFollower.result.percent);
        obstacle.transform.parent = _splineComputer.transform;
        obstacle.splineFollower.motion.rotationOffset = previousObstacle.splineFollower.motion.rotationOffset;

        return obstacle;
    }

    /// <summary>
    /// Klasa koja sluzi za pisanje automatizovanih Unit testova
    /// WHY: Zato sto automatizovani Unit testovi ubrzavaju proces rada, povecavaju kvalitet i cistocu koda, povecavaju sigurnost
    /// </summary>
    public class AutomaticWorldBuilderTest
    {
        [Test]
        public void DoesShowEditorWindow()
        {
            ShowEditorWindow();
            Assert.IsNotNull(Instance);
        }

        [Test]
        public void DoesGetAllRequiredComponents()
        {
        }

        [Test]
        public void GetAllLevelsInTheWorld()
        {
        }
    }
}