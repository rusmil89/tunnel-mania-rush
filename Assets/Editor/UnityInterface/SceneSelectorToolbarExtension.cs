using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityToolbarExtender;

namespace Tunnel.UnityInterface
{
    [InitializeOnLoad]
    public class SceneSelectorToolbarExtension
    {
        private static string[] scenePaths;
        private static string[] sceneNames;
        private static int selectedSceneIndex = 0;

        static SceneSelectorToolbarExtension()
        {
            // Populate the scene list when the project is loaded
            PopulateSceneList();

            // Subscribe to the event that is raised when scripts are recompiled
            EditorApplication.update += OnUpdate;
        }

        private static void PopulateSceneList()
        {
            scenePaths = EditorBuildSettings.scenes
                                            .Where(s => s.enabled)
                                            .Select(s => s.path)
                                            .ToArray();

            sceneNames = scenePaths
                         .Select(s => System.IO.Path.GetFileNameWithoutExtension(s))
                         .ToArray();
        }

        private static void OnUpdate()
        {
            // Only add the toolbar items once, and remove the callback afterwards
            EditorApplication.update -= OnUpdate;
            ToolbarExtender.LeftToolbarGUI.Add(OnToolbarGUI);
        }

        private static void OnToolbarGUI()
        {
            GUILayout.FlexibleSpace();

            // Create the dropdown list
            selectedSceneIndex = EditorGUILayout.Popup(selectedSceneIndex, sceneNames, GUILayout.Width(150));

            // Create the button next to the dropdown
            if (GUILayout.Button("Open Scene", GUILayout.Width(100)))
            {
                if (selectedSceneIndex >= 0 && selectedSceneIndex < scenePaths.Length)
                {
                    if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                    {
                        EditorSceneManager.OpenScene(scenePaths[selectedSceneIndex]);
                    }
                }
            }
        }
    }
}