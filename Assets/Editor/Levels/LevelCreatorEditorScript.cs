﻿using System;
using System.Collections.Generic;
using Dreamteck.Splines;
using Tunnel.DynamicObstacles;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace WarmAppGamesLevelCreator
{
    public class LevelCreatorEditorScript
    {
        public const string TUBE_TAG = "Tube";
        private static GameObject tubeGameObject;
        private static SplineComputer tubeComputer;
        public static SplinePoint[] tubePoints;
        private static Texture2D tubeTexture;
        private static Color tubeColor;
        private static Material tubeMaterial;

        private static Material obstacleMaterial;
        public const string OBSTACLE_INSIDE_GAMEOBJECT_TAG = "ObstaclesInside";
        private static GameObject obstacleInsideGameObject;
        private static List<ObstacleData> insideObstaclesData = new List<ObstacleData>();

        public const string OBSTACLE_OUTSIDE_GAMEOBJECT_TAG = "ObstaclesOutside";
        private static GameObject obstacleOutsideGameObject;
        private static List<ObstacleData> outsideObstacleData = new List<ObstacleData>();

        public static void CreateLevelDataSO(string levelDataSoName)
        {
            GetTubeData();

            GetObstacleData(obstacleInsideGameObject, ref insideObstaclesData);

            GetObstacleData(obstacleOutsideGameObject, ref outsideObstacleData);

            CreateScriptableObject(levelDataSoName);
        }

        public static bool FindInScene(Func<Transform, bool> searchPattern)
        {
            var allRootGameObjects = EditorSceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in allRootGameObjects)
            {
                var allChildrenGameObjects = rootGameObject.GetComponentsInChildren<Transform>(true);
                foreach (var childGameObject in allChildrenGameObjects)
                {
                    if (searchPattern.Invoke(childGameObject)) continue;
                    return true;
                }
            }

            return false;
        }

        public static bool IsThereSplineTubeInTheScene(Transform childGameObject)
        {
            if (childGameObject.tag.Equals(TUBE_TAG))
            {
                tubeGameObject = childGameObject.gameObject;
                tubeComputer = childGameObject.GetComponent<SplineComputer>();
                return false;
            }

            return true;
        }

        public static bool IsThereObstacleInsideGameObject(Transform childGameObject)
        {
            if (childGameObject.name.Equals(OBSTACLE_INSIDE_GAMEOBJECT_TAG))
            {
                obstacleInsideGameObject = childGameObject.gameObject;
                return false;
            }

            return true;
        }

        public static bool IsThereObstacleOutsideGameObject(Transform childGameObject)
        {
            if (childGameObject.name.Equals(OBSTACLE_OUTSIDE_GAMEOBJECT_TAG))
            {
                obstacleOutsideGameObject = childGameObject.gameObject;
                return false;
            }

            return true;
        }

        public static void GetTubeData()
        {
            GetTubePoints();
            GetTubeTextureAndColor();
        }

        public static void GetTubePoints()
        {
            tubePoints = tubeComputer.GetPoints();
        }

        public static void GetTubeTextureAndColor()
        {
            tubeMaterial = tubeGameObject.GetComponent<MeshRenderer>().sharedMaterial;
            if (tubeMaterial == null)
            {
                EditorUtility.DisplayDialog("Tube Material", "Objekat: " + tubeGameObject.name + ", nema na sebi materijal!", "OK");
                Selection.activeGameObject = tubeGameObject;
                return;
            }

            tubeTexture = tubeMaterial.mainTexture as Texture2D;
            tubeColor = tubeMaterial.color;
        }

        public static void GetObstacleData(GameObject obstaclesHolder, ref List<ObstacleData> obstaclesData)
        {
            var allObstacles = obstaclesHolder.GetComponent<ObstaclesHolder>().obstacles;
            List<ObstacleData> obstaclesDataList = new List<ObstacleData>();
            foreach (var obstacle in allObstacles)
            {
                var positionOnPath = (float) obstacle.GetComponent<SplineFollower>().result.percent;
                var rotationOffset = obstacle.GetComponent<SplineFollower>().motion.rotationOffset;
                var positionOffset = obstacle.GetComponent<SplineFollower>().motion.offset;
                var speed = obstacle.speed;
                var delay = obstacle.delay;
                var obstacleType = obstacle.obstacleType;
                ObstacleData obstacleData = new ObstacleData(positionOnPath, rotationOffset, positionOffset, speed, delay,
                    obstacleType);
                obstaclesDataList.Add(obstacleData);
            }

            var meshRendererComponent = allObstacles[0].GetComponentInChildren<MeshRenderer>();
            var sharedMaterial = meshRendererComponent.sharedMaterial;
            obstacleMaterial = sharedMaterial;

            obstaclesData = obstaclesDataList;
        }

        private static void CreateScriptableObject(string levelDataSoName)
        {
            LevelData levelData = ScriptableObject.CreateInstance<LevelData>();
            levelData.tubePoints = tubePoints;
            levelData.tubeTexture = tubeTexture;
//        levelData.tubeColor = tubeColor;
            levelData.tubeMaterial = tubeMaterial;
            levelData.obstacleMaterial = obstacleMaterial;
            levelData.insideObstaclesData = insideObstaclesData;
            levelData.outsideObstaclesData = outsideObstacleData;
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/ScriptableObjects/LevelsData/" + levelDataSoName + ".asset");
            AssetDatabase.CreateAsset(levelData, assetPathAndName);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = levelData;
        }
    }
}