﻿using System;
using System.Linq;
using Dreamteck.Splines;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public static class LevelsMaintainerUtils
{
    private static GameObject[] allRootGameObjects;

    public const string tubeTag = "Tube";
    public static GameObject tubeGameObject;
    public static SplineComputer tubeComputer;
    public static SplinePoint[] tubePoints;
    public static Texture2D tubeTexture;
    public static Color tubeColor;

    public const string obstacleInsideGameObjectTag = "ObstaclesInside";
    public static GameObject obstacleInsideGameObject;
    public static ObstacleData[] insideObstaclesData;

    public const string obstacleOutsideGameObjectTag = "ObstaclesOutside";
    public static GameObject obstacleOutsideGameObject;
    public static ObstacleData[] outsideObstacleData;

    public static bool FindInScene(Func<Transform, bool> searchPattern)
    {
        allRootGameObjects = EditorSceneManager.GetActiveScene().GetRootGameObjects();
        return allRootGameObjects.SelectMany(rootGameObject => rootGameObject.GetComponentsInChildren<Transform>(true)).Any(childGameObject => !searchPattern.Invoke(childGameObject));
    }

    public static bool IsThereSplineTubeInTheScene(Transform childGameObject)
    {
        if (!childGameObject.CompareTag(tubeTag)) return true;
        tubeGameObject = childGameObject.gameObject;
        tubeComputer = childGameObject.GetComponent<SplineComputer>();
        return false;
    }

    public static void GetTubeData()
    {
        GetTubePoints();
        GetTubeTextureAndColor();
    }

    private static void GetTubePoints()
    {
        tubePoints = tubeComputer.GetPoints();
    }

    private static void GetTubeTextureAndColor()
    {
        var tubeMaterial = tubeGameObject.GetComponent<MeshRenderer>().sharedMaterial;
        if (tubeMaterial == null)
        {
            EditorUtility.DisplayDialog("Tube Material", "Objekat: " + tubeGameObject.name + ", nema na sebi materijal!", "OK");
            Selection.activeGameObject = tubeGameObject;
            return;
        }

        tubeTexture = tubeMaterial.mainTexture as Texture2D;
        tubeColor = tubeMaterial.color;
    }
}