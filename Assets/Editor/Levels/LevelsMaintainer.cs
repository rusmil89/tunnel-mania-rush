﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dreamteck.Splines;
using Tunnel.DynamicObstacles;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace WarmAppGamesLevelCreator
{
    public class LevelsMaintainer : EditorWindow
    {
        private static LevelsMaintainer window;

        private List<Category> categories;
        private List<string> _categoryLabels;
        private Category categorySelected;
        private GUIStyle labelHeading;
        private string levelDataSOName = string.Empty;

        private GameObject trackGameObject, tubeGameObject, obstaclesInsideGameObject, obstaclesOutsideGameObject, obstaclesInsideHolderGameObject, obstaclesOutsideHolderGameObject;
        private SplineComputer splineComputer;
        private MeshRenderer tubeMeshRenderer;
        private TubeGenerator tubeGenerator;
        private ObstaclesHolder obstaclesInsideHolder, obstaclesOutsideHolder;

        private LevelData levelData;

        public enum Category
        {
            Create,
            Import
        }

        public Category category = Category.Create;

        [MenuItem("WarmApp Games/Setting Up Scene", false, 1)]
        public static void ShowEditorWindow()
        {
            window = GetWindow<LevelsMaintainer>(true, "Modifikovanje scene i skriptabilnih objekata", true);
            window.minSize = new Vector2(450f, 350f);
            window.maxSize = new Vector2(450f, 350f);
        }

        private void OnEnable()
        {
            window = this;
            window.Repaint();
            if (categories == null)
            {
                InitCategories();
            }

            labelHeading = new GUIStyle
            {
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.BoldAndItalic,
                fontSize = 14,
                normal = new GUIStyleState
                {
                    textColor = Color.white
                }
            };
        }

        private void OnGUI()
        {
            DrawTabs();
        }

        private void DrawTabs()
        {
            int index = (int) categorySelected;
            EditorGUILayout.Space();
            index = GUILayout.Toolbar(index, _categoryLabels.ToArray());
            EditorGUILayout.Space();
            categorySelected = categories[index];
            if (categorySelected == Category.Create)
            {
                DrawCreateTab();
            }
            else
            {
                DrawImportTab();
            }
        }

        private void DrawCreateTab()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.Space();
                if (GUILayout.Button("Create Empty Track"))
                {
                    CreateEmptyTrack();
                }

                if (!LevelCreatorEditorScript.FindInScene(LevelCreatorEditorScript.IsThereSplineTubeInTheScene))
                {
                    const string message = "Ne postoji nijedan Spline Tube objekat u sceni!\nMora da postoji bar jedan!";
                    EditorGUILayout.HelpBox(message, MessageType.Warning);
                    return;
                }

                if (!LevelCreatorEditorScript.FindInScene(LevelCreatorEditorScript.IsThereObstacleInsideGameObject))
                {
                    const string message = "Ne postoji objekat koji sadrži sve prepreke sa unutrašnje strane levela, ili nije dobro imenovan, ili mu nije dodata odgovarajuća skripta!";
                    EditorGUILayout.HelpBox(message, MessageType.Warning);
                    return;
                }

                if (!LevelCreatorEditorScript.FindInScene(LevelCreatorEditorScript.IsThereObstacleOutsideGameObject))
                {
                    const string message = "Ne postoji objekat koji sadrži sve prepreke sa spoljašnje strane levela, ili nije dobro imenovan, ili mu nije dodata odgovarajuća skripta!";
                    EditorGUILayout.HelpBox(message, MessageType.Warning);
                    return;
                }

                const string messageInfo = "Naziv objekta mora da bude u obliku Level_BrojLevela_Data (npr: Level_13_Data)!";
                EditorGUILayout.HelpBox(messageInfo, MessageType.Error);
                levelDataSOName = EditorGUILayout.TextField("Naziv skriptabilnog objekta: ", levelDataSOName);
                GUI.enabled = levelDataSOName != string.Empty;
                if (GUILayout.Button("Create Level Data SO"))
                {
                    LevelCreatorEditorScript.CreateLevelDataSO(levelDataSOName);
                }
            }
            EditorGUILayout.EndVertical();
        }

        private void CreateEmptyTrack()
        {
            trackGameObject = new GameObject
            {
                name = "Track",
                transform =
                {
                    position = Vector3.zero
                }
            };
            tubeGameObject = new GameObject("Tube", typeof(SplineComputer), typeof(TubeGenerator), typeof(LengthCalculator))
            {
                tag = LevelCreatorEditorScript.TUBE_TAG,
                layer = 8,
                transform =
                {
                    position = Vector3.zero
                }
            };
            obstaclesInsideGameObject = new GameObject
            {
                name = LevelCreatorEditorScript.OBSTACLE_INSIDE_GAMEOBJECT_TAG,
                tag = LevelCreatorEditorScript.OBSTACLE_INSIDE_GAMEOBJECT_TAG,
                transform =
                {
                    position = Vector3.zero
                }
            };
            obstaclesOutsideGameObject = new GameObject
            {
                name = LevelCreatorEditorScript.OBSTACLE_OUTSIDE_GAMEOBJECT_TAG,
                tag = LevelCreatorEditorScript.OBSTACLE_OUTSIDE_GAMEOBJECT_TAG,
                transform =
                {
                    position = Vector3.zero
                }
            };
            obstaclesInsideHolderGameObject = new GameObject
            {
                name = "ObstaclesHolder",
                transform =
                {
                    position = Vector3.zero
                }
            };
            obstaclesOutsideHolderGameObject = new GameObject
            {
                name = "ObstaclesHolder",
                transform =
                {
                    position = Vector3.zero
                }
            };

            //setting hierarchy
            tubeGameObject.transform.parent = trackGameObject.transform;
            obstaclesInsideGameObject.transform.parent = trackGameObject.transform;
            obstaclesOutsideGameObject.transform.parent = trackGameObject.transform;
            obstaclesInsideHolderGameObject.transform.parent = obstaclesInsideGameObject.transform;
            obstaclesOutsideHolderGameObject.transform.parent = obstaclesOutsideGameObject.transform;

            //tube spline computer
            splineComputer = tubeGameObject.GetComponent<SplineComputer>();
            splineComputer.precision = 0.98f;

            //main camera
            if (Camera.main != null)
            {
//            Transform mainCameraTransform;
//            Camera mainCamera;
//            (mainCameraTransform = (mainCamera = Camera.main).transform).position = splineComputer.GetPoints()[0].position;
//            mainCameraTransform.rotation = new Quaternion(180f, 0f, 0f, 0f);
                Camera.main.orthographic = false;
            }

            //tube mesh renderer
            tubeMeshRenderer = tubeGameObject.GetComponent<MeshRenderer>();
            tubeMeshRenderer.receiveShadows = false;
            tubeMeshRenderer.shadowCastingMode = ShadowCastingMode.Off;
            tubeMeshRenderer.lightProbeUsage = LightProbeUsage.Off;
            tubeMeshRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;

            //tube generator
            tubeGenerator = tubeGameObject.GetComponent<TubeGenerator>();
            tubeGenerator.computer = splineComputer;
            tubeGenerator.size = 4f;
            tubeGenerator.rotation = 22f;
            tubeGenerator.flipFaces = true;
            tubeGenerator.sides = 8;
            tubeGenerator.uvMode = MeshGenerator.UVMode.UniformClip;

            //length calculator
            var lengthCalculator = tubeGameObject.GetComponent<LengthCalculator>();

            obstaclesInsideHolder = obstaclesInsideGameObject.AddComponent<ObstaclesHolder>();
            obstaclesInsideHolder.obstaclesHolderSO = AssetDatabase.LoadAssetAtPath<ObstaclesHolderSO>("Assets/ScriptableObjects/ObstacleHolderInside.asset");
            obstaclesInsideHolder.obstaclesHolder = obstaclesInsideHolderGameObject;

            obstaclesOutsideHolder = obstaclesOutsideGameObject.AddComponent<ObstaclesHolder>();
            obstaclesOutsideHolder.obstaclesHolderSO = AssetDatabase.LoadAssetAtPath<ObstaclesHolderSO>("Assets/ScriptableObjects/ObstacleHolderOutside.asset");
            obstaclesOutsideHolder.obstaclesHolder = obstaclesOutsideHolderGameObject;
        }

        private void DrawImportTab()
        {
//        EditorGUILayout.HelpBox(
//            "Ukoliko na sceni već postoji Tube, ObstacleInside, i ObstacleOutside objekat, obrisati ih kako bi ova funkcija radila dobro za sada.\nNajbolje je ovu funkcionalnost izvršavati u potpuno novoj sceni!",
//            MessageType.Warning);
            EditorGUILayout.Space();

            levelData = (LevelData) EditorGUILayout.ObjectField("Level Data", levelData, typeof(LevelData), false);
            EditorGUILayout.Space();
            if (levelData != null)
            {
//            DrawLevelDataInfo();
            }

            GUI.enabled = levelData != null;
            if (GUILayout.Button("Import Level"))
            {
                ImportLevel();
            }
        }

        private void DrawInformation(string tag)
        {
            if (tag.Equals(LevelCreatorEditorScript.TUBE_TAG))
            {
                LevelCreatorEditorScript.GetTubeData();
                EditorGUILayout.Space();
                EditorGUILayout.BeginVertical("box");
                {
                    EditorGUILayout.LabelField("Tube Points".ToUpper(), labelHeading);
                    for (int i = 0; i < LevelCreatorEditorScript.tubePoints.Length; i++)
                    {
                        EditorGUILayout.LabelField("Tube point " + (i + 1) + ": " + LevelCreatorEditorScript.tubePoints[i].position);
                    }
                }
                EditorGUILayout.EndVertical();
            }
        }

        private void ImportLevel()
        {
            CreateEmptyTrack();

            //setting inside components
            var insideObstaclesPrefabs = AssetDatabase.FindAssets("t:Prefab", new[] {"Assets/Prefabs/Level/Obstacles/InsideObstacles"});
            var outsideObstaclesPrefabs = AssetDatabase.FindAssets("t:Prefab", new[] {"Assets/Prefabs/Level/Obstacles/OutsideObstacles"});
            var obstaclesPrefabs = insideObstaclesPrefabs.Concat(outsideObstaclesPrefabs).ToArray();

            if (insideObstaclesPrefabs.Length == 0 || outsideObstaclesPrefabs.Length == 0 || obstaclesPrefabs.Length == 0)
            {
                EditorUtility.DisplayDialog("Putanja", "Nije učitan nijedan prefab za prepreke. Proveriti njihovu putanju!", "OK");
                return;
            }

            foreach (var obstacleData in levelData.insideObstaclesData)
            {
                foreach (var guid in obstaclesPrefabs)
                {
                    var insideObstaclePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(guid));
                    if (obstacleData.obstacleType == insideObstaclePrefab.GetComponent<Obstacle>().obstacleType)
                    {
                        var instantiatePrefab = (GameObject) PrefabUtility.InstantiatePrefab(insideObstaclePrefab, obstaclesInsideHolderGameObject.transform);
                        instantiatePrefab.GetComponent<SplineFollower>().computer = tubeGameObject.GetComponent<SplineComputer>();
                    }
                }
            }


            obstaclesInsideHolder.obstacles = new Obstacle[obstaclesInsideHolderGameObject.transform.childCount];
            for (int i = 0; i < obstaclesInsideHolderGameObject.transform.childCount; i++)
            {
                obstaclesInsideHolder.obstacles[i] = obstaclesInsideHolderGameObject.transform.GetChild(i).GetComponent<Obstacle>();
            }

            for (int i = 0; i < obstaclesInsideHolder.obstacles.Length; i++)
            {
                obstaclesInsideHolder.obstacles[i].delay = levelData.insideObstaclesData[i].delay;
                obstaclesInsideHolder.obstacles[i].speed = levelData.insideObstaclesData[i].speed;
                obstaclesInsideHolder.obstacles[i].obstacleType = levelData.insideObstaclesData[i].obstacleType;
                obstaclesInsideHolder.obstacles[i].GetComponent<SplineFollower>().result.percent = levelData.insideObstaclesData[i].positionOnPath;
                obstaclesInsideHolder.obstacles[i].GetComponent<SplineFollower>().motion.offset = levelData.insideObstaclesData[i].positionOffset;
                obstaclesInsideHolder.obstacles[i].GetComponent<SplineFollower>().motion.rotationOffset = levelData.insideObstaclesData[i].rotationOffset;
            }

            //setting outside components
            foreach (var obstacleData in levelData.outsideObstaclesData)
            {
                foreach (var guid in obstaclesPrefabs)
                {
                    var outsideObstaclePrefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(guid));
                    if (obstacleData.obstacleType == outsideObstaclePrefab.GetComponent<Obstacle>().obstacleType)
                    {
                        var instantiatePrefab = (GameObject) PrefabUtility.InstantiatePrefab(outsideObstaclePrefab, obstaclesOutsideHolderGameObject.transform);
                        instantiatePrefab.GetComponent<SplineFollower>().computer = tubeGameObject.GetComponent<SplineComputer>();
                    }
                }
            }

            obstaclesOutsideHolder.obstacles = new Obstacle[obstaclesOutsideHolderGameObject.transform.childCount];
            for (int i = 0; i < obstaclesOutsideHolderGameObject.transform.childCount; i++)
            {
                obstaclesOutsideHolder.obstacles[i] = obstaclesOutsideHolderGameObject.transform.GetChild(i).GetComponent<Obstacle>();
            }

            for (int i = 0; i < obstaclesOutsideHolder.obstacles.Length; i++)
            {
                obstaclesOutsideHolder.obstacles[i].delay = levelData.outsideObstaclesData[i].delay;
                obstaclesOutsideHolder.obstacles[i].speed = levelData.outsideObstaclesData[i].speed;
                obstaclesOutsideHolder.obstacles[i].obstacleType = levelData.outsideObstaclesData[i].obstacleType;
                obstaclesOutsideHolder.obstacles[i].GetComponent<SplineFollower>().result.percent = levelData.outsideObstaclesData[i].positionOnPath;
                obstaclesOutsideHolder.obstacles[i].GetComponent<SplineFollower>().motion.offset = levelData.outsideObstaclesData[i].positionOffset;
                obstaclesOutsideHolder.obstacles[i].GetComponent<SplineFollower>().motion.rotationOffset = levelData.outsideObstaclesData[i].rotationOffset;
            }

            splineComputer.SetPoints(levelData.tubePoints);
            tubeMeshRenderer.sharedMaterial = levelData.tubeMaterial;

            // set material on all obstacles
            var obstacleMaterials = trackGameObject.GetComponentsInChildren<MeshRenderer>();
            foreach (var obstacleMaterial in obstacleMaterials)
            {
                if (obstacleMaterial.gameObject.CompareTag("Tube")) continue;

                obstacleMaterial.sharedMaterial = levelData.obstacleMaterial;
            }
        }

        private void DrawLevelDataInfo()
        {
            EditorGUILayout.BeginVertical("box");
            {
                EditorGUILayout.LabelField("Tube data".ToUpper(), labelHeading);
                for (int i = 0; i < levelData.tubePoints.Length; i++)
                {
                    EditorGUILayout.LabelField("Tube Point " + (i + 1) + " position: " + levelData.tubePoints[i].position);
                }

//            EditorGUILayout.ColorField(new GUIContent("Tube Color: "), levelData.tubeColor, false, true, false, GUILayout.MaxWidth(100f));
                EditorGUILayout.LabelField("Tube texture: " + levelData.tubeTexture);
                EditorGUI.DrawPreviewTexture(new Rect(position.x, position.y, 100, 100), levelData.tubeTexture);
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// inicijalizacija kategorija tabova u prozoru
        /// </summary>
        private void InitCategories()
        {
            categories = GetListFromEnum<Category>();
            _categoryLabels = new List<string>();
            foreach (Category item in categories)
            {
                _categoryLabels.Add(item.ToString());
            }
        }

        /// <summary>
        /// vraca listu od vrednosti enumeratora
        /// </summary>
        /// <returns>The list from enum.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        private static List<T> GetListFromEnum<T>()
        {
            Array enums = Enum.GetValues(typeof(T));
            return enums.Cast<T>().ToList();
        }
    }
}