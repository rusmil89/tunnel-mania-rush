using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Tunnel.Build
{
    public class PreBuildChecking : IPreprocessBuildWithReport
    {
        public int callbackOrder => 0;

        public void OnPreprocessBuild(BuildReport report)
        {
            if (report.summary.platform == BuildTarget.Android)
            {
                CheckForPlayCoreLibraryDuplication();
                CheckForMinimumAPILevel();
                CheckForEnableLogScriptingDefineSymbol();
                CheckForLZ4HCCompression(report);
            }
        }

        private void CheckForLZ4HCCompression(BuildReport buildReport)
        {
            // check if LZ4HC compression is enabled
            if (!buildReport.summary.options.HasFlag(BuildOptions.CompressWithLz4HC))
            {
                // ask user to change the compression method
                if (EditorUtility.DisplayDialog("Compression Method",
                        "Please change the compression method to LZ4HC before building for Android.", "OK", "Cancel"))
                {
                    // user clicked OK
                    // buildReport.summary.options &= ~BuildOptions.CompressWithLz4;
                    Debug.Log("Changed compression method to LZ4HC");
                }
                else
                {
                    // user clicked Cancel
                    Debug.Log("Build cancelled");
                    throw new BuildFailedException("Build cancelled by user because of compression method");
                }
            }
        }

        private static void CheckForPlayCoreLibraryDuplication()
        {
            const string filter = "com.google.android.play.core-1";
            string[] guids = AssetDatabase.FindAssets(filter);
            if (guids.Length > 0)
            {
                foreach (string guid in guids)
                {
                    string guidToAssetPath = AssetDatabase.GUIDToAssetPath(guid);
                    if (guidToAssetPath.Contains("/Plugins/Android/"))
                    {
                        // ask user to remove the file
                        if (EditorUtility.DisplayDialog("Google Play Core Library",
                                "Please remove the file " + guidToAssetPath + " before building for Android.", "OK",
                                "Cancel"))
                        {
                            // user clicked OK
                            AssetDatabase.DeleteAsset(guidToAssetPath);
                            Debug.Log($"Deleted {guidToAssetPath}");
                        }
                        else
                        {
                            // user clicked Cancel
                            Debug.Log("Build cancelled");
                            throw new BuildFailedException(
                                "Build cancelled by user because of Google Play Core Library file in Assets/Plugins/Android/");
                        }
                    }
                }
            }
        }

        private static void CheckForMinimumAPILevel()
        {
            // check if minimum API level is 26
            if (PlayerSettings.Android.minSdkVersion < AndroidSdkVersions.AndroidApiLevel26)
            {
                // ask user to change the minimum API level
                if (EditorUtility.DisplayDialog("Minimum API Level",
                        "Please change the minimum API level to 26 before building for Android.", "OK", "Cancel"))
                {
                    // user clicked OK
                    PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel26;
                    Debug.Log("Changed minimum API level to 26");
                }
                else
                {
                    // user clicked Cancel
                    Debug.Log("Build cancelled");
                    throw new BuildFailedException("Build cancelled by user because of minimum API level");
                }
            }
        }

        private static void CheckForEnableLogScriptingDefineSymbol()
        {
            // Check for ENABLE_LOG directive
            bool isLogEnabled = IsLoggingEnabled();
            if (EditorUserBuildSettings.buildAppBundle)
            {
                // We're building an app bundle
                if (isLogEnabled)
                {
                    // If ENABLE_LOG is defined, cancel the build
                    throw new BuildFailedException(
                        "ENABLE_LOG is defined. App bundles should not include debug logging.");
                }
            }
            else
            {
                // We're building a regular APK
                if (isLogEnabled)
                {
                    // Show a warning but continue the build
                    Debug.LogWarning("ENABLE_LOG is defined. Remember to disable debug logging for production builds.");
                }
            }
        }

        private static bool IsLoggingEnabled()
        {
            string[] defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android).Split(';');
            return defines.Any(define => define == "ENABLE_LOG");
        }
    }
}