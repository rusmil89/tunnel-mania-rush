using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Tunnel.Build
{
    public class PostBuildChecking : IPostprocessBuildWithReport
    {
        public int callbackOrder { get; }

        public void OnPostprocessBuild(BuildReport report)
        {
            if (report.summary.platform == BuildTarget.Android)
            {
                const string filter = "com.google.android.play.core-1";
                string[] guids = AssetDatabase.FindAssets(filter);

                if (guids.Length > 0)
                {
                    foreach (string guid in guids)
                    {
                        string guidToAssetPath = AssetDatabase.GUIDToAssetPath(guid);
                        if (guidToAssetPath.Contains("/Plugins/Android/"))
                        {
                            // ask user to remove the file
                            if (EditorUtility.DisplayDialog("Google Play Core Library",
                                    "Build probably failed because there is duplicate Google Play Core Library at " + guidToAssetPath + ".", "OK", "Cancel"))
                            {
                                // user clicked OK
                                AssetDatabase.DeleteAsset(guidToAssetPath);
                                Debug.Log($"Deleted {guidToAssetPath}");
                            }
                            else
                            {
                                // user clicked Cancel
                                Debug.Log($"Build cancelled");
                                throw new BuildFailedException(
                                    "Build cancelled by user because of Google Play Core Library file in Assets/Plugins/Android/");
                            }
                        }
                    }
                }
            }
        }
    }
}