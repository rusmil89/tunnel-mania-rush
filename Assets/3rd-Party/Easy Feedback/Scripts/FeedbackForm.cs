﻿using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace EasyFeedback
{
    public sealed class CheatingEnabled {}
    public sealed class FormSuccessfullySubmitted {}
    
    public class FeedbackForm : MonoBehaviour
    {
        [Tooltip("Easy Feedback configuration file")]
        public EFConfig Config;

        public Transform Form, Alert;
        [SerializeField] private TextMeshProUGUI alertText;

        /// <summary name="OnFormOpened">
        /// Called when the form is first opened, right before it is shown on screen
        /// </summary>
        [Tooltip("Functions to be called when the form is first opened")]
        public UnityEvent OnFormOpened;

        /// <summary name="OnFormSubmitting">
        /// Called right before the report is sent to Trello
        /// </summary>
        [Tooltip("Functions to be called when the form is submitting")]
        public UnityEvent OnFormSubmitting; // called before report is sent to Trello, so that any information in the form can be added

        /// <summary name="OnFormClosed">
        /// Called when the form is closed, whether or not it was submitted
        /// </summary>
        [Tooltip("Functions to be called when the form is closed")]
        public UnityEvent OnFormClosed;

        /// <summary name="OnFormSuccessfullySubmitted">
        /// Called when the form is closed, whether or not it was submitted
        /// </summary>
        [Tooltip("Functions to be called when the form is successfully submitted")]
        public UnityEvent OnFormSuccessfullySubmitted;

        /// <summary name="OnFormUnsuccessfullySubmitted">
        /// Called when the form is closed, whether or not it was submitted
        /// </summary>
        [Tooltip("Functions to be called when the form is unsuccessfully submitted")]
        public UnityEvent OnFormUnsuccessfullySubmitted;


        /// <summary>
        /// The current report being built.
        /// Will be sent as next report
        /// </summary>
        public Report CurrentReport;

        /// <summary>
        /// Whether or not the form is currently being displayed
        /// </summary>
        public bool IsOpen
        {
            get { return Form.gameObject.activeSelf; }
        }

        // api handler
        private APIs.Trello trello;

        public void Awake()
        {
            if (!Config.StoreLocal)
                InitTrelloAPI();

            // initialize current report
            initCurrentReport();
        }

        private void InitTrelloAPI()
        {
            // initialize api handler
            trello = new APIs.Trello(Config.Token);
        }

        /// <summary>
        /// Replaces currentReport with a new instance of Report
        /// </summary>
        private void initCurrentReport()
        {
            CurrentReport = new Report();
        }

        /// <summary>
        /// Called by the submit button, submits the form.
        /// </summary>
        public void Submit()
        {
            if (IsCheating())
            {
                return;
            }

            //are fields filled
            if (!AreAllFieldsFilled())
            {
                return;
            }

            StartCoroutine(submitAsync());
        }

        private bool IsCheating()
        {
            Transform child = Form.GetChild(1);
            if (!child.GetComponent<TMP_InputField>()) return false;
            TMP_InputField childInputField = child.GetComponent<TMP_InputField>();
            if (!childInputField.text.Equals("trm_unlock_all?")) return false;
            MessageBroker.Default.Publish(new CheatingEnabled());
            return true;
        }

        private bool AreAllFieldsFilled()
        {
            for (int i = 0; i < Form.childCount; ++i)
            {
                Transform child = Form.GetChild(i);
                if (!child.GetComponent<TMP_InputField>()) continue;
                TMP_InputField childInputField = child.GetComponent<TMP_InputField>();

                if (!string.IsNullOrEmpty(childInputField.text))
                {
                    ColorBlock colorBlockNormal = childInputField.colors;
                    colorBlockNormal.normalColor = Color.white;
                    childInputField.colors = colorBlockNormal;

                    if (!childInputField.targetGraphic.name.Equals("EmailInputField")) continue;
                    if (IsValidEmail(childInputField.text)) continue;
                    ColorBlock colorBlock = childInputField.colors;
                    colorBlock.normalColor = Color.red;
                    childInputField.colors = colorBlock;
                    return false;
                }
                else
                {
                    ColorBlock colorBlock = childInputField.colors;
                    colorBlock.normalColor = Color.red;
                    childInputField.colors = colorBlock;
                    return false;
                }
            }

            return true;
        }

        private bool IsValidEmail(string email)
        {
            const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                             + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                             + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            return email != null && Regex.IsMatch(email, MatchEmailPattern);
        }

        private void showAlert(string message)
        {
            alertText.text = message;
            Alert.gameObject.SetActive(true);
        }

        public void HideAlert()
        {
            Alert.gameObject.SetActive(false);
        }

        private IEnumerator submitAsync()
        {
            // show submitting alert
            showAlert("Submitting...");

            // call OnFormSubmitting
            OnFormSubmitting.Invoke();

            // brzi hack, biramo listu na osnovu izabrane liste u editoru
            CurrentReport.List.id = Config.selectedListId;
            CurrentReport.Label.id = Config.Board.Labels[1].id;

            if (!Config.StoreLocal)
            {
                // add card to board
                yield return trello.AddCard(
                    CurrentReport.Title ?? "[no summary]",
                    CurrentReport.ToString() ?? "[no detail]",
                    CurrentReport.Label.id ?? "",
                    CurrentReport.List.id,
                    CurrentReport.Screenshot);

                // send up attachments 
                if (trello.LastAddCardResponse != null && !trello.UploadError)
                    yield return attachFilesAsync(trello.LastAddCardResponse.id);
            }
            else
            {
                // store entire report locally, then return
                string localPath = writeLocal(CurrentReport);
                Debug.Log(localPath);
            }

            // close form
            Hide();

            if (!Config.StoreLocal && trello.UploadError)
            {
                // preserve report locally if there's an issue during upload
                Debug.Log(writeLocal(CurrentReport));

                showAlert("Error: Failed to upload report!\n " + trello.ErrorMessage);
                if (trello.UploadException != null)
                    Debug.LogException(trello.UploadException);
                else
                    Debug.LogError(trello.ErrorMessage);
                OnFormUnsuccessfullySubmitted.Invoke();
            }
            else
            {
                OnFormSuccessfullySubmitted.Invoke();
                showAlert("Feedback submitted successfully!");
            }

            initCurrentReport();
        }

        /// <summary>
        /// Attaches files on current report to card
        /// </summary>
        /// <param name="cardID"></param>
        /// <returns></returns>
        IEnumerator attachFilesAsync(string cardID)
        {
            for (int i = 0; i < CurrentReport.Attachments.Count; i++)
            {
                FileAttachment attachment = CurrentReport.Attachments[i];
                yield return trello.AddAttachmentAsync(cardID, attachment.Data, null, attachment.Name, null);

                if (trello.UploadError) // failed to add attachment
                {
                    showAlert("Error: Failed to attach file to report!\n" + trello.ErrorMessage);
                }
            }
        }

        /// <summary>
        /// Saves the report in a local directory
        /// </summary>
        private string writeLocal(Report report)
        {
            // create the report directory
            string feedbackDirectory = Application.persistentDataPath + "/feedback-" + DateTime.Now.ToString("MMddyyyy-HHmmss");
            Directory.CreateDirectory(feedbackDirectory);

            // save the report
            File.WriteAllText(feedbackDirectory + "/report.txt", report.GetLocalFileText());

            // save screenshot
            File.WriteAllBytes(feedbackDirectory + "/screenshot.png", report.Screenshot);

            // save attachments
            for (int i = 0; i < report.Attachments.Count; i++)
            {
                FileAttachment attachment = report.Attachments[i];
                File.WriteAllBytes(feedbackDirectory + "/" + attachment.Name, attachment.Data);
            }

            return feedbackDirectory;
        }

        /// <summary>
        /// Hides the form, called by the Close button.
        /// </summary>
        public void Hide()
        {
            // don't do anything if the form is already hidden
            if (!Form.gameObject.activeInHierarchy)
                return;

            // call OnFormClosed
            StartCoroutine(OnFormClosedDelayedCoroutine());
        }

        // delay
        private IEnumerator OnFormClosedDelayedCoroutine()
        {
            yield return new WaitForSeconds(1.5f);
            HideAlert();
            MessageBroker.Default.Publish(new FormSuccessfullySubmitted());
            OnFormClosed.Invoke();
        }
    }
}