﻿namespace EasyFeedback.Editor
{
    using APIs;
    using UnityEditor;
    using UnityEngine;

    public class NewListWindow : EditorWindow
    {
        private const string WINDOW_TITLE = "New Feedback List";
        private const int WIDTH = 312;
        private const int MAX_WIDTH = 500;
        private const int HEIGHT = 46;
        private EFConfig config;
        private ConfigWindow configWindow;
        private string listName = "My Feedback List";

        private Trello trello;

        public void SetConfigWindow(ConfigWindow configWindow)
        {
            this.configWindow = configWindow;
        }

        public static NewListWindow GetWindow(ConfigWindow configWindow)
        {
            NewListWindow window = GetWindow<NewListWindow>(true, WINDOW_TITLE);
            window.SetConfigWindow(configWindow);

            window.minSize = new Vector2(WIDTH, HEIGHT);
            window.maxSize = window.minSize;

            return window;
        }

        /// <summary>
        /// Raises the enable event.
        /// </summary>
        private void OnEnable()
        {
            if (trello == null)
            {
                config = AssetDatabase.LoadAssetAtPath<EFConfig>(ConfigWindow.CONFIG_ASSET_LOCATION);
                trello = new Trello(config.Token);
            }
        }

        /// <summary>
        /// Raises the OnGUI event.
        /// </summary>
        private void OnGUI()
        {
            if (trello == null)
                return;

            listName = EditorGUILayout.TextField("List Name", listName);

            if (GUILayout.Button("Create List"))
            {
                listName += " " + Trello.CATEGORY_TAG;
                SetUpList(listName);

                if (EditorUtility.DisplayDialog("List Created!", "The list " + listName + " has been successfully created!", "OK"))
                {
                    if (configWindow)
                        configWindow.RefreshBoardsList();

                    Close();
                }
            }
        }

        private void SetUpList(string listName)
        {
            trello.AddList(listName, config.Board.Id, "bottom");
        }
    }
}