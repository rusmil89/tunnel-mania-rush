﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EasyFeedback
{
    public class TabNext : MonoBehaviour
    {
        public TMP_InputField Next;
        public TMP_InputField Previous;

        // Update is called once per frame
        void Update()
        {
            if (Next != null && this.GetComponent<TMP_InputField>().isFocused
                && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                && Input.GetKeyDown(KeyCode.Tab))
            {
                GetComponent<TMP_InputField>().DeactivateInputField();
                Next.Select();
                Next.ActivateInputField();
            }
            else if (Previous != null && this.GetComponent<TMP_InputField>().isFocused
                && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                && Input.GetKeyDown(KeyCode.Tab))
            {
                GetComponent<TMP_InputField>().DeactivateInputField();
                Previous.Select();
                Previous.ActivateInputField();
            }
        }
    }
}