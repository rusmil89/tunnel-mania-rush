﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EasyFeedback
{
    [RequireComponent(typeof(TMP_InputField))]
    public class ReportTitle : FormElement
    {
        public override void FormClosed()
        {
        }

        public override void FormOpened()
        {
        }

        public override void FormSubmitted()
        {
            Form.CurrentReport.Title = GetComponent<TMP_InputField>().text;
        }
    }

}