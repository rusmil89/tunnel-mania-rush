﻿namespace EasyFeedback
{
    using UnityEngine;

    public class BatteryInfoCollector : FormField
    {
        public override void FormOpened()
        {
            
        }

        public override void FormClosed()
        {
            
        }

        public override void FormSubmitted()
        {
            // add section to report if it doesn't already exist
            if (!Form.CurrentReport.HasSection(SectionTitle))
                Form.CurrentReport.AddSection(SectionTitle, SortOrder);

            // battery info
            Form.CurrentReport ["Battery Info"].AppendLine("Battery Level: " + SystemInfo.batteryLevel * 100 + "%");
            Form.CurrentReport ["Battery Info"].AppendLine("Battery Status: " + SystemInfo.batteryStatus);
        }
    }
}
