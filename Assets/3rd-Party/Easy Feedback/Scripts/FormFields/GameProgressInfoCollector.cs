﻿namespace EasyFeedback
{
    public class GameProgressInfoCollector : FormField
    {
        private readonly string gameInfoString = "Game Info";

        public override void FormOpened()
        {
            
        }

        public override void FormClosed()
        {
            
        }

        public override void FormSubmitted()
        {

            // add section to report if it doesn't already exist
            if (!Form.CurrentReport.HasSection(SectionTitle))
                Form.CurrentReport.AddSection(SectionTitle, SortOrder);

            //TODO: dodati podatke vezane za samu igru (progress, broj otkljucanih levela,...)
            // game info
//            Form.CurrentReport [gameInfoString].AppendLine("### General");
//            Form.CurrentReport [gameInfoString].AppendLine("Privacy Terms: " + GeneralSettings.privacy);
//            Form.CurrentReport [gameInfoString].AppendLine("Coins: " + GeneralSettings.coins);
//            Form.CurrentReport [gameInfoString].AppendLine("Is InApp Buyed: " + GeneralSettings.isInAppBought);
//            Form.CurrentReport [gameInfoString].AppendLine("Notifications: " + GeneralSettings.showNotifications);
//            Form.CurrentReport [gameInfoString].AppendLine("Game Language: " + LanguageManager.choosenLanguage);
//            Form.CurrentReport [gameInfoString].AppendLine("\n");
//
//            // worlds and levels
//            Form.CurrentReport [gameInfoString].AppendLine("### Worlds And Levels");
//            Form.CurrentReport [gameInfoString].AppendLine("Current World: " + (GeneralSettings.currentWorld + 1));
//            Form.CurrentReport [gameInfoString].AppendLine("Current Level : " + GeneralSettings.currentLevel);
//            for (int i = 0; i < GeneralSettings.numberOfWorlds; i++)
//            {
//                if (GeneralSettings.numberOfUnlockedLevelsInWorld [i] > 1 || i == 0)
//                {
//                    Form.CurrentReport [gameInfoString].AppendLine("Number of Unlocked Levels in World " + (i + 1) + ": " + GeneralSettings.numberOfUnlockedLevelsInWorld [i]);
//                }
//            }
//            Form.CurrentReport [gameInfoString].AppendLine("\n");
//
//            // game number
//            Form.CurrentReport [gameInfoString].AppendLine("### Game Number");
//            for (int i = 0; i < GeneralSettings.numberOfWorlds; i++) 
//            {
//                if (GeneralSettings.numberOfUnlockedLevelsInWorld [i]> 1 || i == 0)
//                {
//                    for (int n = 0; n < GeneralSettings.numberOfUnlockedLevelsInWorld [i]; ++n)
//                    {
//                        Form.CurrentReport [gameInfoString].AppendLine("Number in World " + (i + 1) + " Level " + (n + 1) + ": " + GeneralSettings.gameNumberInLevel [(i * 10) + n]);
//                    }
//                }
//            }
//            Form.CurrentReport [gameInfoString].AppendLine("\n");
//
//            // stars in worlds and levels
//            Form.CurrentReport [gameInfoString].AppendLine("### Stars");
//            for (int i = 0; i < GeneralSettings.numberOfWorlds; i++) 
//            {
//                if (GeneralSettings.numberOfUnlockedLevelsInWorld [i]> 1 || i == 0)
//                {
//                    for (int n = 0; n < GeneralSettings.numberOfUnlockedLevelsInWorld [i]; ++n)
//                    {
//                        Form.CurrentReport [gameInfoString].AppendLine("Number in World " + (i + 1) + " Level " + (n + 1) + ": " + GeneralSettings.starsWonInLevel [(i * 10) + n]);
//                    }
//                }
//            }
        }
    }
}