﻿namespace EasyFeedback
{
    using UnityEngine;

    public class DeviceInfoCollector : FormField
    {
        public override void FormOpened()
        {
            
        }

        public override void FormClosed()
        {
            
        }

        public override void FormSubmitted()
        {
            // add section to report if it doesn't already exist
            if (!Form.CurrentReport.HasSection(SectionTitle))
                Form.CurrentReport.AddSection(SectionTitle, SortOrder);

            // device info
            Form.CurrentReport ["Device Info"].AppendLine("Device Model: " + SystemInfo.deviceModel);
            Form.CurrentReport ["Device Info"].AppendLine("Device Name: " + SystemInfo.deviceName);
            Form.CurrentReport ["Device Info"].AppendLine("Device Type: " + SystemInfo.deviceType);
        }
    }
}