﻿namespace EasyFeedback
{
    using UnityEngine;

    public class GraphicsCardInfoCollector : FormField
    {
        public override void FormOpened()
        {
            
        }

        public override void FormClosed()
        {
            
        }

        public override void FormSubmitted()
        {
            // add section to report if it doesn't already exist
            if (!Form.CurrentReport.HasSection(SectionTitle))
                Form.CurrentReport.AddSection(SectionTitle, SortOrder);

            // graphics card info
            Form.CurrentReport ["Graphics Card Info"].AppendLine("Graphics API: " + SystemInfo.graphicsDeviceType);
            Form.CurrentReport ["Graphics Card Info"].AppendLine("Graphics Processor: " + SystemInfo.graphicsDeviceName);
            Form.CurrentReport ["Graphics Card Info"].AppendLine("Graphics Memory: " + SystemInfo.graphicsMemorySize);
            Form.CurrentReport ["Graphics Card Info"].AppendLine("Graphics Vendor: " + SystemInfo.graphicsDeviceVendor);
            Form.CurrentReport ["Graphics Card Info"].AppendLine("Graphics Device Version: " + SystemInfo.graphicsDeviceVersion);
        }
    }
}